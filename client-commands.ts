import { CommandType, SendCommand } from './client.js'
import { zCVob, zVEC3 } from './union/classes/index.js'

/**
 * Prepares an HTML component for a player but does not display it immediately.
 *
 * @param playerId - The ID of the player.
 * @param componentName - A unique identifier for the HTML component to set up.
 * @param htmlBody - The URL or HTML content to load into the component.
 * @param x - The x-coordinate for the component's position. Defaults to 0.
 * @param y - The y-coordinate for the component's position. Defaults to 0.
 * @param width - The width of the component. Defaults to -1 (auto-size).
 * @param height - The height of the component. Defaults to -1 (auto-size).
 *
 * @example
 * Prepares the equipment page for the specified player.
 * ```typescript
 * LoadHtmlComponent(player.Id, 'Main', 'http://localhost:3000/#eq');
 * ```
 */
export const LoadHtmlComponent = (
  playerId: number,
  componentName: string,
  htmlBody: string,
  x = 0,
  y = 0,
  width = -1,
  height = -1,
) => {
  SendCommand(
    {
      id: 'LoadHtmlComponent',
      playerId,
      componentName,
      htmlBody,
      x,
      y,
      width,
      height,
    },
    CommandType.CLIENT,
  )
}

/**
 * Creates an HTML component for a player.
 *
 * @param playerId - The ID of the player.
 * @param componentName - The name of the component. If prefixed with "Overlay_", it is always displayed but not clickable. Otherwise, it is displayed only when mouse/keyboard interception is on.
 *
 * @example
 * Creates the overlay page for the specified player. It will be displayed all the time.
 * ```typescript
 * CreateHtmlComponent(player.Id, 'Overlay_Main');
 * ```
 */
export const CreateHtmlComponent = (playerId: number, componentName: string) => {
  SendCommand(
    {
      id: 'CreateHtmlComponent',
      playerId,
      componentName,
    },
    CommandType.CLIENT,
  )
}

/**
 * Removes an HTML component from a player's interface.
 *
 * @param playerId - The ID of the player.
 * @param componentName - The unique identifier of the HTML component to remove.
 *
 * @example
 * Removes the Main page for the specified player.
 * ```typescript
 * RemoveHtmlComponent(player.Id, 'Main');
 * ```
 */
export const RemoveHtmlComponent = (playerId: number, componentName: string) => {
  SendCommand(
    {
      id: 'RemoveHtmlComponent',
      playerId,
      componentName,
    },
    CommandType.CLIENT,
  )
}

/**
 * Shows an HTML component on a player's interface (if previously was hidden)
 *
 * @param playerId - The ID of the player.
 * @param componentName - The unique identifier of the HTML component to show.
 *
 * @example
 * Shows the Main page for the specified player.
 * ```typescript
 * ShowHtmlComponent(player.Id, 'Main');
 * ```
 */
export const ShowHtmlComponent = (playerId: number, componentName: string) => {
  SendCommand(
    {
      id: 'ShowHtmlComponent',
      playerId,
      componentName,
    },
    CommandType.CLIENT,
  )
}

/**
 * Hides an HTML component on a player's interface (but keeps the page alive).
 *
 * @param playerId - The ID of the player.
 * @param componentName - The unique identifier of the HTML component to hide.
 *
 * @example
 * Hides the Main page for the specified player.
 * ```typescript
 * HideHtmlComponent(player.Id, 'Main');
 * ```
 */
export const HideHtmlComponent = (playerId: number, componentName: string) => {
  SendCommand(
    {
      id: 'HideHtmlComponent',
      playerId,
      componentName,
    },
    CommandType.CLIENT,
  )
}

/**
 * Navigates an HTML component to a specified URL.
 *
 * @param playerId - The ID of the player.
 * @param componentName - The unique identifier of the HTML component to navigate.
 * @param url - The URL to navigate the component to.
 *
 * @example
 * Navigates the Main component to the creator page for the specified player.
 * ```typescript
 * NavigateHtmlComponent(player.Id, 'Main', 'http://localhost:3000/#creator');
 * ```
 */
export const NavigateHtmlComponent = (playerId: number, componentName: string, url: string) => {
  SendCommand(
    {
      id: 'NavigateHtmlComponent',
      playerId,
      componentName,
      url,
    },
    CommandType.CLIENT,
  )
}

/**
 * Sends a JSON payload to a specified event for an HTML component.
 *
 * @param playerId - The ID of the player.
 * @param eventName - The name of the event to trigger.
 * @param payload - The JSON data to send with the event.
 *
 * @example
 * Sends a payload to the 'Popup' event for the specified player.
 * ```typescript
 * SendEventToHtmlComponents(player.Id, 'Popup', {
 *   text: "You have gained a level!",
 *   status: "success"
 * });
 * ```
 * React client receives the payload.
 * ```typescript
 * useEffect(() => {
 *   return onGameEvent('Popup', (data: any) => {
 *     const { text, status } = data;
 *     status === 'success' ? popUpSuccess(text) : popUpFailed(text);
 *   });
 * }, []);
 * ```
 */
export const SendEventToHtmlComponents = (playerId: number, eventName: string, payload: any) => {
  SendCommand(
    {
      id: 'SendEventToHtmlComponents',
      playerId,
      eventName,
      payload: JSON.stringify(payload),
    },
    CommandType.CLIENT,
  )
}

/**
 * Activates intercepting mode in the game. The default mouse and keyboard behavior in Gothic will be paused, and all input events will be redirected to the web page.
 *
 * @param playerId - The ID of the player.
 *
 * @example
 * Starts intercepting input for the specified player.
 * ```typescript
 * StartIntercept(player.Id);
 * ```
 */
export const StartIntercept = (playerId: number) => {
  SendCommand(
    {
      id: 'StartIntercept',
      playerId,
    },
    CommandType.CLIENT,
  )
}

/**
 * Deactivates intercepting mode, restoring default mouse and keyboard behavior in Gothic.
 *
 * @param playerId - The ID of the player.
 *
 * @example
 * Stops intercepting input for the specified player.
 * ```typescript
 * StopIntercept(player.Id);
 * ```
 */
export const StopIntercept = (playerId: number) => {
  SendCommand(
    {
      id: 'StopIntercept',
      playerId,
    },
    CommandType.CLIENT,
  )
}

export enum BindCommandType {
  NORMAL,
  OVERLAY_TOGGLE,
}

/**
 * Binds a command to a key press for a player.
 *
 * @param playerId - The ID of the player.
 * @param key - The key code to bind the command to.
 * @param command - The command to execute when the specified key is pressed.
 * @param bindType - The type of binding, either `NORMAL` or `OVERLAY_TOGGLE`. Defaults to `NORMAL`.
 *                  `OVERLAY_TOGGLE` will stop intercept mode when the key is pressed again while intercept is active,
 *                  making it useful for toggling features like the equipment or hero page.
 *
 * @example
 * Binds the F3 key (0x72) to the 'creator' command.
 * ```typescript
 * BindCommandToKeyPress(player.Id, 0x72, 'creator', BindCommandType.OVERLAY_TOGGLE);
 * ```
 */
export const BindCommandToKeyPress = (
  playerId: number,
  key: number,
  command: string,
  bindType: BindCommandType = BindCommandType.NORMAL,
) => {
  SendCommand(
    {
      id: 'BindCommandToKeyPress',
      playerId,
      key,
      command,
      bindType,
    },
    CommandType.CLIENT,
  )
}

/**
 * Removes a key press binding for a player.
 *
 * @param playerId - The ID of the player.
 * @param key - The key code whose binding should be removed.
 *
 * @example
 * Removes the F3 (0x72) key binding for the specified player.
 * ```typescript
 * RemoveBindCommandFromKeyPress(player.Id, 0x72);
 * ```
 */
export const RemoveBindCommandFromKeyPress = (playerId: number, key: number) => {
  SendCommand(
    {
      id: 'RemoveBindCommandFromKeyPress',
      playerId,
      key,
    },
    CommandType.CLIENT,
  )
}

/**
 * Enables a static camera for a player at a specified position and heading.
 *
 * @param playerId - The ID of the player.
 * @param position - The position for the static camera, represented as a `zVEC3`.
 * @param heading - The heading direction for the camera, represented as a `zVEC3`.
 *
 * @example
 * Activates a static camera at position (0, 0, 0) pointing towards the sky.
 * ```typescript
 * const cameraPos = new zVEC3([0, 0, 0]);
 * const heading = new zVEC3([0, 100, 0]);
 * EnableStaticCamera(player.Id, cameraPos, heading);
 * ```
 */
export const EnableStaticCamera = (playerId: number, position: zVEC3, heading: zVEC3) => {
  SendCommand(
    {
      id: 'EnableStaticCamera',
      playerId,
      position: position.toString(),
      heading: heading.toString(),
    },
    CommandType.CLIENT,
  )
}

/**
 * Disables the static camera for a player.
 *
 * @param playerId - The ID of the player.
 *
 * @example
 * Disables the static camera for the specified player.
 * ```typescript
 * DisableStaticCamera(playerId);
 * ```
 */
export const DisableStaticCamera = (playerId: number) => {
  SendCommand(
    {
      id: 'DisableStaticCamera',
      playerId,
    },
    CommandType.CLIENT,
  )
}

/**
 * Enables a closing overlay for a player, allowing the overlay to be closed using the ESC key.
 * This behavior is enabled by default but can be controlled for specific situations.
 *
 * @param playerId - The ID of the player.
 *
 * @example
 * Enables the closing overlay for the specified player.
 * ```typescript
 * EnableClosingOverlay(playerId);
 * ```
 */
export const EnableClosingOverlay = (playerId: number) => {
  SendCommand(
    {
      id: 'EnableClosingOverlay',
      playerId,
    },
    CommandType.CLIENT,
  )
}

/**
 * Disables the closing overlay for a player, preventing it from being closed using the ESC key.
 * This is useful for scenarios where the overlay must remain open regardless of user input.
 *
 * @param playerId - The ID of the player.
 *
 * @example
 * Disables the closing overlay for the specified player.
 * ```typescript
 * DisableClosingOverlay(playerId);
 * ```
 */
export const DisableClosingOverlay = (playerId: number) => {
  SendCommand(
    {
      id: 'DisableClosingOverlay',
      playerId,
    },
    CommandType.CLIENT,
  )
}

/**
 * Plays a sound for a player.
 *
 * @param playerId - The ID of the player.
 * @param sound - The name of the sound file to play.
 * @param slot - The sound slot to use. Defaults to 0.
 * @param freq - The frequency of the sound. Defaults to -1 (uses the default frequency).
 * @param volume - The volume level of the sound. Defaults to -1 (uses the default volume).
 * @param pan - The pan level of the sound. Defaults to -2 (uses the default pan).
 *
 * @example
 * Plays a sound associated with a victory voice line for an NPC after a fight for the specified player.
 * ```typescript
 * PlaySound(player.Id, 'SVM_8_GOODVICTORY.WAV');
 * ```
 */
export const PlaySound = (
  playerId: number,
  sound: string,
  slot: number = 0,
  freq: number = -1,
  volume: number = -1,
  pan: number = -2,
) => {
  SendCommand(
    {
      id: 'PlaySound',
      playerId,
      sound,
      slot,
      freq,
      volume,
      pan,
    },
    CommandType.CLIENT,
  )
}

/**
 * Plays a 3D sound for a player at a specified vob (object) location.
 *
 * @param playerId - The ID of the player.
 * @param sound - The name of the sound file to play.
 * @param vob - The object (vob) location where the sound should be played.
 * @param slot - The sound slot to use. Defaults to 7.
 * @param obstruction - The obstruction level of the sound, determining how much the sound is blocked by obstacles. Defaults to 0.
 * @param volume - The volume level of the sound. Defaults to 1 (full volume).
 * @param radius - The radius within which the sound can be heard. Defaults to -1 (uses the default radius).
 * @param loopType - The loop type of the sound. Defaults to 0 (no loop).
 * @param coneAngleDeg - The cone angle in degrees for directional sound. Defaults to 0.
 * @param reverbLevel - The reverb level of the sound. Defaults to 1 (full reverb).
 * @param isAmbient3D - Whether the sound is ambient (3D environment). Defaults to 0 (non-ambient).
 * @param pitchOffset - The pitch offset for the sound. Defaults to -999999 (uses the default pitch).
 *
 * @example
 * Plays a 3D sound associated with a victory voice line for an NPC after a fight for the specified player.
 * ```typescript
 * PlaySound3D(player.Id, 'SVM_8_GOODVICTORY.WAV', player.Npc);
 * ```
 */
export const PlaySound3D = (
  playerId: number,
  sound: string,
  vob: zCVob,
  slot: number = 7,
  obstruction: number = 0,
  volume: number = 1,
  radius: number = -1,
  loopType: number = 0,
  coneAngleDeg: number = 0,
  reverbLevel: number = 1,
  isAmbient3D: number = 0,
  pitchOffset: number = -999999,
) => {
  SendCommand(
    {
      id: 'PlaySound3D',
      playerId,
      sound,
      vobId: vob.toString(),
      slot,
      obstruction,
      volume,
      radius,
      loopType,
      coneAngleDeg,
      reverbLevel,
      isAmbient3D,
      pitchOffset,
    },
    CommandType.CLIENT,
  )
}

/**
 * Exits the game for a player.
 *
 * @param playerId - The ID of the player.
 *
 * @example
 * The specified player exits the game.
 * ```typescript
 * ExitGame(playerId);
 * ```
 */
export const ExitGame = (playerId: number) => {
  SendCommand(
    {
      id: 'ExitGame',
      playerId,
    },
    CommandType.CLIENT,
  )
}

/**
 * Removes a preexisting object (VOB) from the game world for a specific player.
 *
 * @param playerId - The ID of the player.
 * @param vobName - The name of the VOB (object) to be removed.
 * @param position - The position of the object in the game world, represented as a `zVEC3`.
 * @returns Always returns `null`.
 *
 * @example
 * Removes an object named 'OC_BARRELS_REIHE.3DS' located at position (-945.31, 101.035, -814.381) for the specified player.
 * ```typescript
 * RemovePreexistingObject(playerId, 'OC_BARRELS_REIHE.3DS', new zVEC3([-945.31, 101.035, -814.381]));
 * ```
 */
export const RemovePreexistingObject = (
  playerId: number,
  vobName: string,
  position: zVEC3,
): null => {
  return SendCommand(
    {
      id: 'RemovePreexistingObject',
      playerId,
      vobName,
      position: position.toString(),
    },
    CommandType.CLIENT,
  )
}

/**
 * Toggles a client configuration option for a specific player.
 *
 * @param playerId - The ID of the player.
 * @param option - The name of the configuration option to toggle.
 * @param value - The new value for the configuration option (`true` to enable, `false` to disable).
 * @returns Always returns `null`.
 *
 * @example
 * Adjusts multiple client configuration options for the specified player.
 * ```typescript
 * Client.ToggleClientConfig(player.Id, 'default_bars', false);
 * Client.ToggleClientConfig(player.Id, 'default_inventory', false);
 * Client.ToggleClientConfig(player.Id, 'default_status_screen', true);
 * Client.ToggleClientConfig(player.Id, 'default_game_menu', false);
 * Client.ToggleClientConfig(player.Id, 'default_chat', true);
 * Client.ToggleClientConfig(player.Id, 'friendly_guild', true);
 * ```
 */
export const ToggleClientConfig = (playerId: number, option: string, value: boolean): null => {
  return SendCommand(
    {
      id: 'ToggleClientConfig',
      playerId,
      option,
      value,
    },
    CommandType.CLIENT,
  )
}

export type WorldBuilderPlaceVobCommandArgs = {
  name: string
  type: 'VOB' | 'MOB' | 'NPC' | 'ITEM'
  posX: number
  posY: number
  posZ: number
  rotateX: number
  rotateY: number
  rotateZ: number
}

/**
 * Starts world builder mode for a player.
 *
 * @param playerId - The ID of the player.
 * @param vobName - The visual name for a VOB/MOB or the object name for an ITEM/NPC.
 * @param vobType - The type of the VOB. Valid options are `'VOB'`, `'MOB'`, `'NPC'`, or `'ITEM'`.
 * @param transformMode - Determines the allowed transformations:
 *                        - `'ALL'`: Allows rotating the VOB in all directions (X/Y/Z axes).
 *                        - `'ROTATE'`: Allows rotation only on the Y axis (world rotation). For NPCs, only `'ROTATE'` is valid.
 * @param validationMode - Specifies validation checks:
 *                         - `'ALL'`: Validates both collisions and ground touch points.
 *                         - `'COLLISION'`: Validates only collisions, allowing placement in the air.
 *                         - `'GROUND'`: Validates only ground contact.
 *                         - `'NONE'`: Disables all validation.
 * @returns Always returns `null`.
 *
 * @example
 * Starts world builder mode with specific settings for the specified player.
 * ```typescript
 * Client.StartWorldBuilder(player.Id, 'OC_LOB_GATE_BIG.3DS', 'MOB', 'ALL', 'NONE');
 * ```
 */
export const StartWorldBuilder = (
  playerId: number,
  vobName: string,
  vobType: 'VOB' | 'MOB' | 'NPC' | 'ITEM',
  tranformMode: 'ALL' | 'ROTATE' = 'ROTATE',
  validationMode: 'ALL' | 'COLLISION' | 'GROUND' | 'NONE' = 'ALL',
): null => {
  return SendCommand(
    {
      id: 'StartWorldBuilder',
      playerId,
      vobName,
      vobType,
      tranformMode,
      validationMode,
    },
    CommandType.CLIENT,
  )
}

/**
 * Closes world builder mode for a player, exiting the building interface and returning to normal gameplay.
 *
 * @param playerId - The ID of the player.
 *
 * @example
 * Exits world builder mode for the specified player.
 * ```typescript
 * CloseWorldBuilder(playerId);
 * ```
 */
export const CloseWorldBuilder = (playerId: number): null => {
  return SendCommand(
    {
      id: 'CloseWorldBuilder',
      playerId,
    },
    CommandType.CLIENT,
  )
}

/**
 * Starts mob interaction with focused mob for a player. If the player is not focused on a mob, this command has no effect.
 *
 * @param playerId - The ID of the player.
 *
 * @example
 * Starts mob interaction for the specified player.
 * ```typescript
 * StartMobInteraction(playerId);
 * ```
 */
export const StartMobInteraction = (playerId: number): null => {
  return SendCommand(
    {
      id: 'StartMobInteraction',
      playerId,
    },
    CommandType.CLIENT,
  )
}
