import { GameModeBase } from './index.js'
import { ModuleBase } from './module-base.js'
import { Player } from './player.js'

export class MiddlewareBase<PlayerType extends Player> extends ModuleBase<PlayerType> {
  GameMode: GameModeBase<PlayerType>

  constructor(gamemode: GameModeBase<PlayerType>) {
    super()
    this.GameMode = gamemode
  }
}
