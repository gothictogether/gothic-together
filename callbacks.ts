import { CurrentGameMode } from './client.js'
import { PlayerClass } from './index.js'
import { MiddlewareBase } from './middleware-base.js'
import { ModuleBase } from './module-base.js'
import { Player } from './player.js'
import { GetAllPlayers } from './server-commands.js'
import { oCItem, oCMobInter, oCNpc, zCVob } from './union/classes/index.js'

let Middlewares: MiddlewareBase<Player>[] = []

export const SetMiddlewares = <T extends Player>(middlewares: MiddlewareBase<T>[]) => {
  Middlewares = middlewares
}

const CallCallback = async (
  callbackFunc: (module: ModuleBase<Player>) => Promise<boolean | void>,
) => {
  const modules = [...Middlewares, CurrentGameMode!]

  for (const module of modules) {
    if ((await callbackFunc(module)) == true) {
      return
    }
  }
}

export const FireCallback = async (message: any) => {
  const callback = Callbacks[message['name']]
  if (callback) {
    await BeforeCallback(message)
    await callback(message)
    await AfterCallback(message)
  }
}

const fetchPlayer = (message: any): Player => {
  const existingPlayer = CurrentGameMode!.IdToPlayerMap[message['player']['id']]

  return existingPlayer || new PlayerClass(message['player']['id'], message['player']['name'])
}

const tryParseJson = (body: string): any => {
  try {
    return JSON.parse(body)
  } catch (e) {
    return body
  }
}

const BeforeCallback = async (message: any) => {
  await CallCallback(async (module) => await module.BeforeCallback(message['name'], message))
}

const AfterCallback = async (message: any) => {
  await CallCallback(async (module) => await module.AfterCallback(message['name'], message))
}

const OnGameModeConnected = async (message: any) => {
  const players = GetAllPlayers()

  CurrentGameMode!.Players = []
  CurrentGameMode!.IdToPlayerMap = {}

  players.forEach((p) => {
    const newPlayer = new PlayerClass(p.player.id, p.player.name)
    CurrentGameMode!.Players.push(newPlayer)
    CurrentGameMode!.IdToPlayerMap[newPlayer.Id] = newPlayer
  })

  await CallCallback(async (module) => await module.OnGameModeConnected())
}

const OnInitServer = async (message: any) => {
  CurrentGameMode!.Players = []
  CurrentGameMode!.IdToPlayerMap = {}

  await CallCallback(async (module) => await module.OnInitServer())
}

const OnExitServer = async (message: any) => {
  await CallCallback(async (module) => await module.OnExitServer())
}

const OnTick = async (message: any) => {
  await CallCallback(async (module) => await module.OnTick())
}

const OnTime = async (message: any) => {
  await CallCallback(
    async (module) =>
      await module.OnTime(
        Number(message['day']),
        Number(message['hour']),
        Number(message['minute']),
      ),
  )
}

const OnDamage = async (message: any) => {
  await CallCallback(
    async (module) =>
      await module.OnDamage(
        new oCNpc(message['attackerId']),
        String(message['attacker']),
        new oCNpc(message['targetId']),
        String(message['target']),
        String(message['weapon']),
        Number(message['damageReal']),
      ),
  )
}

const OnPlayerJoinServer = async (message: any) => {
  const player = fetchPlayer(message)

  CurrentGameMode!.Players.push(player)
  CurrentGameMode!.IdToPlayerMap[player.Id] = player

  await CallCallback(async (module) => await module.OnPlayerJoinServer(player))
}

const OnPlayerDisconnectServer = async (message: any) => {
  const player = fetchPlayer(message)

  CurrentGameMode!.Players = CurrentGameMode!.Players.filter((p) => p.Id != player.Id)
  delete CurrentGameMode!.IdToPlayerMap[player.Id]

  await CallCallback(async (module) => await module.OnPlayerDisconnectServer(player))
}

const OnPlayerChatMessage = async (message: any) => {
  const player = fetchPlayer(message)

  await CallCallback(
    async (module) => await module.OnPlayerChatMessage(player, String(message['message'])),
  )
}

const OnPlayerCommand = async (message: any) => {
  const player = fetchPlayer(message)

  await CallCallback(
    async (module) =>
      await module.OnPlayerCommand(
        player,
        String(message['commandName']).toLocaleLowerCase(),
        tryParseJson(message['args']),
      ),
  )
}

const OnPlayerMobInteract = async (message: any) => {
  const player = fetchPlayer(message)

  await CallCallback(
    async (module) =>
      await module.OnPlayerMobInteract(
        player,
        new oCMobInter(message['mobId']),
        String(message['mobName']),
      ),
  )
}

const OnPlayerNpcInteract = async (message: any) => {
  const player = fetchPlayer(message)

  await CallCallback(
    async (module) =>
      await module.OnPlayerNpcInteract(
        player,
        new oCNpc(message['npcId']),
        String(message['npcName']),
      ),
  )
}

const OnPlayerChangeFocus = async (message: any) => {
  const player = fetchPlayer(message)

  await CallCallback(
    async (module) =>
      await module.OnPlayerChangeFocus(
        player,
        new zCVob(message['vobId']),
        String(message['vobName']),
        Number(message['vobType']),
      ),
  )
}

const OnPlayerChangeAttribute = async (message: any) => {
  const player = fetchPlayer(message)

  await CallCallback(
    async (module) =>
      await module.OnPlayerChangeAttribute(
        player,
        Number(message['attribute']),
        Number(message['currentValue']),
        Number(message['change']),
      ),
  )
}

const OnPlayerChangeWeaponMode = async (message: any) => {
  const player = fetchPlayer(message)

  await CallCallback(
    async (module) => await module.OnPlayerChangeWeaponMode(player, Number(message['weaponState'])),
  )
}

const OnPlayerTakeItem = async (message: any) => {
  const player = fetchPlayer(message)

  await CallCallback(
    async (module) =>
      await module.OnPlayerTakeItem(
        player,
        new oCItem(message['itemId']),
        String(message['itemName']),
      ),
  )
}

const OnPlayerDropItem = async (message: any) => {
  const player = fetchPlayer(message)
  await CallCallback(
    async (module) =>
      await module.OnPlayerDropItem(
        player,
        new oCItem(message['itemId']),
        String(message['itemName']),
      ),
  )
}

const OnPlayerEquipItem = async (message: any) => {
  const player = fetchPlayer(message)
  await CallCallback(
    async (module) =>
      await module.OnPlayerEquipItem(
        player,
        new oCItem(message['itemId']),
        String(message['itemName']),
      ),
  )
}

const OnPlayerUnequipItem = async (message: any) => {
  const player = fetchPlayer(message)
  await CallCallback(
    async (module) =>
      await module.OnPlayerUnequipItem(
        player,
        new oCItem(message['itemId']),
        String(message['itemName']),
      ),
  )
}

const OnPlayerUseItem = async (message: any) => {
  const player = fetchPlayer(message)
  await CallCallback(
    async (module) =>
      await module.OnPlayerUseItem(
        player,
        new oCItem(message['itemId']),
        String(message['itemName']),
      ),
  )
}

const Callbacks: { [name: string]: Function } = {
  OnGameModeConnected,
  OnInitServer,
  OnExitServer,
  OnTick,
  OnTime,
  OnDamage,
  OnPlayerJoinServer,
  OnPlayerDisconnectServer,
  OnPlayerChatMessage,
  OnPlayerCommand,
  OnPlayerMobInteract,
  OnPlayerNpcInteract,
  OnPlayerChangeFocus,
  OnPlayerChangeAttribute,
  OnPlayerChangeWeaponMode,
  OnPlayerTakeItem,
  OnPlayerDropItem,
  OnPlayerEquipItem,
  OnPlayerUnequipItem,
  OnPlayerUseItem,
}
