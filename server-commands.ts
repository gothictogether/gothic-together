import { CommandType, SendCommand } from './client.js'
import { BaseUnionObject } from './union/base-union-object.js'
import { oCItem, oCNpc, oCVob, zCVob, zVEC3, zCOLOR, oCMobInter } from './union/classes/index.js'

interface PlayerJSON {
  player: {
    id: number
    name: string
  }
}

export const StorePointer = (uuid: string): null => {
  return SendCommand(
    {
      id: 'StorePointer',
      uuid,
    },
    CommandType.SERVER,
  )
}

export const ReleasePointer = (uuid: string): null => {
  return SendCommand(
    {
      id: 'ReleasePointer',
      uuid,
    },
    CommandType.SERVER,
  )
}

/**
 * Retrieves a list of all players currently connected.
 *
 * @returns An array of `PlayerJSON` objects, each containing a player's `id` and `name`.
 *
 * @example
 * Logs an array of `PlayerJSON` objects.
 * ```typescript
 * const players = GetAllPlayers();
 * console.log(players); // Output: [{ player: { id: 1, name: 'Player1' } }, ...]
 * ```
 */
export const GetAllPlayers = (): PlayerJSON[] => {
  const result = SendCommand(
    {
      id: 'GetAllPlayers',
    },
    CommandType.SERVER,
  )

  return JSON.parse(result) as PlayerJSON[]
}

/**
 * Parses a given string to extract an index, typically used in Gothic scripting.
 *
 * @param search - The string to parse for an index.
 * @returns The extracted index as a number if found, otherwise `null`.
 *
 * @example
 * Parses a routine function to an index number.
 * ```typescript
 * const routineFunc = ParserGetIndex('ZS_STAND_WP') as number;
 * ```
 */
export const ParserGetIndex = (search: string): number | null => {
  const result = SendCommand(
    {
      id: 'ParserGetIndex',
      search,
    },
    CommandType.SERVER,
  )

  return result ? Number(result) : null
}

/**
 * Sets an instance for Gothic Daedalus scripting, typically used in Gothic scripting.
 *
 * @param name - The name of the instance to set.
 * @param instance - The instance of the object.
 * @returns Always returns `null`.
 *
 * @example
 * Sets two variables, `SELF` and `OTHER`, to two NPCs and starts the `ZS_FOLLOW_PLAYER` script.
 * ```typescript
 * ParserSetInstance('SELF', milten);
 * ParserSetInstance('OTHER', GameMode.Players[0]!.Npc);
 * milten.set_startAIState(ParserGetIndex('ZS_FOLLOW_PLAYER')!);
 * ```
 */
export const ParserSetInstance = (name: string, instance: BaseUnionObject): null => {
  return SendCommand(
    {
      id: 'ParserSetInstance',
      name,
      instanceId: instance.toString(),
    },
    CommandType.SERVER,
  )
}

/**
 * Initiates an attack from one NPC to another.
 *
 * @param attacker - The `oCNpc` instance representing the attacking NPC.
 * @param target - The `oCNpc` instance representing the target NPC.
 * @returns Always returns `null`.
 *
 * @example
 * Makes an enemy NPC attack the specified player NPC.
 * ```typescript
 * NpcAttack(enemyNpc, playerNpc);
 * ```
 */
export const NpcAttack = (attacker: oCNpc, target: oCNpc): null => {
  return SendCommand(
    {
      id: 'NpcAttack',
      attackerId: attacker.toString(),
      targetId: target.toString(),
    },
    CommandType.SERVER,
  )
}

/**
 * Creates a new NPC at a specified position.
 *
 * Takes an index and a position to create an NPC. If the creation is successful,
 * it returns an instance of `oCNpc`. Otherwise, it returns `null`.
 *
 * @param index - The unique identifier (string) for the NPC to be created.
 * @param position - The position where the NPC will be created, represented as an instance of `zVEC3`.
 * @returns An instance of `oCNpc` if the NPC is successfully created, otherwise `null`.
 *
 * @example
 * Spawns Captain Garond at the specified position.
 * ```typescript
 * const npcIndex = "Pal_250_Garond";
 * const position = new zVEC3([-2054.679, 177.803, -787.782]);
 * const npc = CreateNpc(npcIndex, position);
 * ```
 */
export const CreateNpc = (index: string, position: zVEC3): oCNpc | null => {
  const result = SendCommand(
    {
      id: 'CreateNpc',
      index,
      position: position.toString(),
    },
    CommandType.SERVER,
  )

  return result ? new oCNpc(result) : null
}

/**
 * Creates a new item at a specified position.
 *
 * @param index - The unique identifier (string) for the item to be created.
 * @param position - The position where the item will be created, represented as an instance of `zVEC3`.
 * @returns An instance of `oCItem` if the item is successfully created, otherwise `null`.
 *
 * @example
 * Spawns a health potion at the specified position.
 * ```typescript
 * const itemIndex = "ITPO_PERM_HEALTH";
 * const position = new zVEC3([-1636.045, 1400.162, -309.314]);
 * Server.CreateItem(itemIndex, position);
 * ```
 */
export const CreateItem = (index: string, position: zVEC3): oCItem | null => {
  const result = SendCommand(
    {
      id: 'CreateItem',
      index,
      position: position.toString(),
    },
    CommandType.SERVER,
  )

  return result ? new oCItem(result) : null
}

/**
 * Creates a new Vob at a specified position.
 *
 * @param visual - The visual identifier (string) for the object to be created.
 * @param position - The position where the object will be created, represented as an instance of `zVEC3`.
 * @returns An instance of `oCVob` if the object is successfully created, otherwise `null`.
 *
 * @example
 * Creates a plank object.
 * ```typescript
 * const visual = "ORC_SQUAREPLANKS_2X3M.3DS";
 * const position = new zVEC3([-2045, 248, -830]);
 * CreateObject(visual, position);
 * ```
 */
export const CreateObject = (visual: string, position: zVEC3): oCVob | null => {
  const result = SendCommand(
    {
      id: 'CreateObject',
      visual,
      position: position.toString(),
    },
    CommandType.SERVER,
  )

  return result ? new oCVob(result) : null
}

/**
 * Creates a new Mob at a specified position.
 *
 * @param visual - The visual identifier (string) for the Mob to be created.
 * @param position - The position where the Mob will be created, represented as an instance of `zVEC3`.
 * @param mobName - Optional. The name of the Mob to be created.
 * @param mobItem - Optional. The item to be assigned to the Mob.
 * @returns An instance of `oCMobInter` if the Mob is successfully created, otherwise `null`.
 *
 * @example
 * Creates a Mob at the specified position.
 * ```typescript
 * const visual = "LAB_PSI.ASC";
 * const position = new zVEC3([500, 100, 200]);
 * const mobName = "MOBNAME_LAB";
 * const mobItem = "ITMI_FLASK";
 * const mob = CreateMob(visual, position, mobName, mobItem);
 * ```
 */
export const CreateMob = (
  visual: string,
  position: zVEC3,
  mobName: string = '',
  mobItem: string = '',
): oCMobInter | null => {
  const result = SendCommand(
    {
      id: 'CreateMob',
      visual,
      mobName,
      mobItem,
      position: position.toString(),
    },
    CommandType.SERVER,
  )

  return result ? new oCMobInter(result) : null
}

/**
 * Adds an item to an NPC's native invetory.
 *
 * @param index - The unique identifier (string) for the item to be added.
 * @param npc - The `oCNpc` instance representing the NPC to receive the item.
 * @param amount - The quantity of the item to be added.
 * @returns An instance of `oCItem` if the item is successfully added, otherwise `null`.
 *
 * @example
 * Adds a 1h sword to Garond's inventory.
 * ```typescript
 * const garondWeapon = PutInInventory("ITMW_1H_MISC_SWORD", garondNpc, 1);
 * ```
 */
export const PutInInventory = (index: string, npc: oCNpc, amount: number): oCItem | null => {
  const result = SendCommand(
    {
      id: 'PutInInventory',
      index,
      npcId: npc.toString(),
      amount: amount,
    },
    CommandType.SERVER,
  )

  return result ? new oCItem(result) : null
}

/**
 * Retrieves the visual name of a given Vob.
 *
 * @param vob - The `zCVob` instance representing the Vob.
 * @returns The visual name as a string if successful, otherwise `null`.
 *
 * @example
 * Retrieves the current Vob's visual name.
 * ```typescript
 * const visual = VobGetVisual(vob);
 * ```
 */
export const VobGetVisual = (vob: zCVob): string | null => {
  const result = SendCommand(
    {
      id: 'VobGetVisual',
      vobId: vob.toString(),
    },
    CommandType.SERVER,
  )

  return result ? String(result) : null
}

/**
 * Retrieves the rotation of a given Vob.
 *
 * @param vob - The `zCVob` instance representing the Vob.
 * @returns The rotation in degrees as a number if successful, otherwise `null`.
 *
 * @example
 * Retrieves the current Vob's rotation.
 * ```typescript
 * const rotation = VobGetRotation(vob);
 * ```
 */
export const VobGetRotation = (vob: zCVob): number | null => {
  const result = SendCommand(
    {
      id: 'VobGetRotation',
      vobId: vob.toString(),
    },
    CommandType.SERVER,
  )

  return result ? Number(result) : null
}

/**
 * Sets the rotation of a given Vob.
 *
 * @param vob - The `zCVob` instance representing the Vob.
 * @param rotation - The rotation value to be set, in degrees.
 * @returns Always returns `null`.
 *
 * @example
 * Rotates the specified Vob to 45 degrees.
 * ```typescript
 * VobSetRotation(vob, 45);
 * ```
 */
export const VobSetRotation = (vob: zCVob, rotation: number): null => {
  return SendCommand(
    {
      id: 'VobSetRotation',
      vobId: vob.toString(),
      rotation,
    },
    CommandType.SERVER,
  )
}

/**
 * Retrieves a Vob by its object name near a specified position.
 *
 * @param vobName - The name of the Vob to find.
 * @param position - The position to search around, represented as `zVEC3`.
 * @returns An instance of `oCVob` if found, otherwise `null`.
 *
 * @example
 * Finds a Planks object near the specified position.
 * ```typescript
 * FindVob('ORC_SQUAREPLANKS_2X3M.3DS', new zVEC3([-2045, 248, -830]));
 * ```
 */
export const FindVob = (vobName: string, position: zVEC3): oCVob | null => {
  const result = SendCommand(
    {
      id: 'FindVob',
      vobName,
      position: position.toString(),
    },
    CommandType.SERVER,
  )

  return result ? new oCVob(result) : null
}

/**
 * Retrieves an NPC by its object name. If multiple NPCs have the same object name, the first one found will be returned.
 *
 * @param npcName - The object name of the NPC to find.
 * @returns An instance of `oCNpc` if found, otherwise `null`.
 *
 * @example
 * Finds the Captain Garond NPC.
 * ```typescript
 * FindNpc('PAL_250_GAROND');
 * ```
 */
export const FindNpc = (npcName: string): oCNpc | null => {
  const result = SendCommand(
    {
      id: 'FindNpc',
      npcName,
    },
    CommandType.SERVER,
  )

  return result ? new oCNpc(result) : null
}

/**
 * Sends a message to all connected players.
 *
 * @param message - The message to broadcast to all players.
 * @param color - Optional. The color of the message, represented as `zCOLOR`. Defaults to white `[255, 255, 255, 255]`.
 * @returns Always returns `null`.
 *
 * @example
 * Sends an announcement to all connected players.
 * ```typescript
 * SendMessageToAll(`${player.Name} found a hidden treasure!`, new zCOLOR(0, 255, 0, 255));
 * ```
 */
export const SendMessageToAll = (
  message: string,
  color: zCOLOR = new zCOLOR([255, 255, 255, 255]),
): null => {
  return SendCommand(
    {
      id: 'SendMessageToAll',
      message,
      color: color.toString(),
    },
    CommandType.SERVER,
  )
}

/**
 * Sends a message to a specified player.
 *
 * @param playerId - The ID of the player to receive the message.
 * @param message - The message to send.
 * @param color - Optional. The color of the message, represented as `zCOLOR`. Defaults to white `[255, 255, 255, 255]`.
 * @returns Always returns `null`.
 *
 * @example
 * Sends a welcome message to a specific player.
 * ```typescript
 * SendMessageToPlayer(playerId, `Welcome back ${player.Name}`, new zCOLOR(0, 255, 0, 255));
 * ```
 */
export const SendMessageToPlayer = (
  playerId: number,
  message: string,
  color: zCOLOR = new zCOLOR([255, 255, 255, 255]),
): null => {
  return SendCommand(
    {
      id: 'SendMessageToPlayer',
      playerId,
      message,
      color: color.toString(),
    },
    CommandType.SERVER,
  )
}

/**
 * Sets the nickname color of a specified player in the chat.
 *
 * @param playerId - The ID of the player whose nickname color will be set.
 * @param color - The `zCOLOR` instance representing the desired color for the player's nickname.
 * @returns Always returns `null`.
 *
 * @example
 * Sets the player's nickname color to red in the chat.
 * ```typescript
 * SetPlayerColor(0, new zCOLOR([255, 0, 0, 255]));
 * ```
 */
export const SetPlayerColor = (playerId: number, color: zCOLOR): null => {
  return SendCommand(
    {
      id: 'SetPlayerColor',
      playerId,
      color: color.toString(),
    },
    CommandType.SERVER,
  )
}

/**
 * Kicks a specified player from the server.
 *
 * @param playerId - The ID of the player to be kicked.
 * @returns Always returns `null`.
 *
 * @example
 * Kicks the specified player from the server.
 * ```typescript
 * KickPlayer(playerId);
 * ```
 */
export const KickPlayer = (playerId: number): null => {
  return SendCommand(
    {
      id: 'KickPlayer',
      playerId,
    },
    CommandType.SERVER,
  )
}

/**
 * Bans a specific player from the server.
 *
 * @param playerId - The ID of the player to be banned.
 * @param comment - Optional. A comment explaining the reason for the ban. Defaults to an empty string.
 * @returns Always returns `null`.
 *
 * @example
 * Bans the specified player from the server with a reason.
 * ```typescript
 * BanPlayer(playerId, "Repeated violations of the rules");
 * ```
 */
export const BanPlayer = (playerId: number, comment: string = ''): null => {
  return SendCommand(
    {
      id: 'BanPlayer',
      playerId,
      comment,
    },
    CommandType.SERVER,
  )
}

/**
 * Retrieves the Discord avatar URL of a specific player.
 *
 * @param playerId - The ID of the player whose avatar URL will be retrieved.
 * @returns The avatar URL as a string if found, otherwise `null`.
 *
 * @example
 * Retrieves the avatar URL for the specified player.
 * ```typescript
 * const avatarUrl = GetPlayerAvatar(playerId);
 * ```
 */
export const GetPlayerAvatar = (playerId: number): string | null => {
  const result = SendCommand(
    {
      id: 'GetPlayerAvatar',
      playerId,
    },
    CommandType.SERVER,
  )

  return result ? String(result) : null
}

/**
 * Retrieves the unique identifier (UUID) of a specific player. This UUID is linked to the Discord account used to connect to the server.
 *
 * @param playerId - The ID of the player whose unique ID will be retrieved.
 * @returns The unique identifier as a string if found, otherwise `null`.
 *
 * @example
 * Retrieves the unique ID (UUID) of a specified player.
 * ```typescript
 * const uniqueId = GetPlayerUniqueId(playerId);
 * ```
 */
export const GetPlayerUniqueId = (playerId: number): string | null => {
  const result = SendCommand(
    {
      id: 'GetPlayerUniqueId',
      playerId,
    },
    CommandType.SERVER,
  )

  return result ? String(result) : null
}

/**
 * Retrieves the ping of a specific player.
 *
 * @param playerId - The ID of the player whose ping will be retrieved.
 * @returns The ping as a number if found, otherwise `null`.
 *
 * @example
 * Retrieves the ping for the specified player.
 * ```typescript
 * const ping = GetPlayerPing(playerId);
 * ```
 */
export const GetPlayerPing = (playerId: number): number | null => {
  const result = SendCommand(
    {
      id: 'GetPlayerPing',
      playerId,
    },
    CommandType.SERVER,
  )

  return result ? Number(result) : null
}

/**
 * Sets an active spell for a specific NPC. If the spell does not exist in the NPC's inventory, it is created and drawn.
 *
 * @param index - The unique identifier of the spell.
 * @param npc - The `oCNpc` instance representing the NPC to set the spell for.
 * @returns An instance of `oCItem` representing the spell if successful, otherwise `null`.
 *
 * @example
 * Sets the active FireBall spell for a specific NPC.
 * ```typescript
 * SetActiveSpell("ItSc_InstantFireBall", garondNpc);
 * ```
 */
export const SetActiveSpell = (index: string, npc: oCNpc): oCItem | null => {
  return SendCommand(
    {
      id: 'SetActiveSpell',
      index,
      npcId: npc.toString(),
    },
    CommandType.SERVER,
  )
}

/**
 * Sets the default spawn position for all players. The player NPC will spawn at this position after connecting.
 *
 * @param position - The position to set as the spawn point, represented as a `zVEC3`.
 * @returns Always returns `null`.
 *
 * @example
 * Sets up a new player spawn position.
 * ```typescript
 * const spawnPos = new zVEC3([0, 0, 0]);
 * SetPlayersSpawnPos(spawnPos);
 * ```
 */
export const SetPlayersSpawnPos = (position: zVEC3): null => {
  return SendCommand(
    {
      id: 'SetPlayersSpawnPos',
      position: position.toString(),
    },
    CommandType.SERVER,
  )
}

/**
 * Sets a spawn position exclusively for the host player. This can be useful when the host is not an active player and should remain hidden.
 *
 * @param position - The position to set as the spawn point, represented as a `zVEC3`.
 * @returns Always returns `null`.
 *
 * @example
 * Sets the spawn position for a player who is considered the host.
 * ```typescript
 * const spawnPos = new zVEC3([10, 20, 30]);
 * SetHostSpawnPos(spawnPos);
 * ```
 */
export const SetHostSpawnPos = (position: zVEC3): null => {
  return SendCommand(
    {
      id: 'SetHostSpawnPos',
      position: position.toString(),
    },
    CommandType.SERVER,
  )
}

/**
 * Retrieves the name of a specific NPC.
 *
 * @param npc - The `oCNpc` instance representing the NPC whose name is to be retrieved.
 * @returns The name of the NPC as a string if successful, otherwise `null`.
 *
 * @example
 * Retrieves the name of the specified NPC.
 * ```typescript
 * const npcName = NpcGetName(garondNpc);
 * console.log(npcName); // Outputs: "Garond"
 * ```
 */
export const NpcGetName = (npc: oCNpc): string | null => {
  const result = SendCommand(
    {
      id: 'NpcGetName',
      npcId: npc.toString(),
    },
    CommandType.SERVER,
  )

  return result ? String(result) : null
}

/**
 * Sets a custom name for a specific NPC, overriding the default NPC name.
 *
 * @param npc - The `oCNpc` instance representing the NPC whose name is to be set.
 * @param name - The new name to assign to the NPC.
 * @returns Always returns `null`.
 *
 * @example
 * Sets a custom name for the specified NPC.
 * ```typescript
 * NpcSetName(garondNpc, "King Garond II");
 * ```
 */
export const NpcSetName = (npc: oCNpc, name: string): null => {
  return SendCommand(
    {
      id: 'NpcSetName',
      npcId: npc.toString(),
      name,
    },
    CommandType.SERVER,
  )
}

/**
 * Sets the maximum distance from the original spawn that an enemy NPC will chase an opponent.
 *
 * @param npc - The `oCNpc` instance representing the NPC.
 * @param distance - The pursuit distance to set.
 * @returns Always returns `null`.
 *
 * @example
 * Sets the pursuit distance for the specified NPC.
 * ```typescript
 * NpcSetPursueDistance(garondNpc, 5000);
 * ```
 */
export const NpcSetPursueDistance = (npc: oCNpc, distance: number): null => {
  return SendCommand(
    {
      id: 'NpcSetPursueDistance',
      npcId: npc.toString(),
      distance,
    },
    CommandType.SERVER,
  )
}

/**
 * Sets the damage for a specific item.
 * 
 * @param item - The `oCItem` instance representing the item.
 * @param index - The index of the damage type (check oEIndexDamage enum).
 * @param damage - The damage value to set.
 * 
 * @example
 * Sets the point damage (used for arrows) for the specified item.
 * ```typescript
 * ItemSetDamageByIndex(item, oEIndexDamage.oEDamageIndex_Point, 100);
 * ```
 */
export const ItemSetDamageByIndex = (item: oCItem, index: number, damage: number): null => {
  return SendCommand(
    {
      id: 'ItemSetDamageByIndex',
      itemId: item.toString(),
      index,
      damage,
    },
    CommandType.SERVER,
  )
}