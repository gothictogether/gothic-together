import { Player } from './player.js'
import { oCItem, oCMobInter, oCNpc, zCVob } from './union/classes/index.js'

export class ModuleBase<PlayerType extends Player> {
  async BeforeCallback(name: string, payload: any): Promise<boolean | void> {}
  async AfterCallback(name: string, payload: any): Promise<boolean | void> {}
  async OnGameModeConnected(): Promise<boolean | void> {}
  async OnInitServer(): Promise<boolean | void> {}
  async OnExitServer(): Promise<boolean | void> {}
  async OnTick(): Promise<boolean | void> {}
  async OnTime(day: number, hour: number, minute: number): Promise<boolean | void> {}
  async OnDamage(
    attacker: oCNpc,
    attrackerName: string,
    target: oCNpc,
    targetName: string,
    weaponName: string,
    damage: number,
  ): Promise<boolean | void> {}
  async OnPlayerJoinServer(player: PlayerType): Promise<boolean | void> {}
  async OnPlayerDisconnectServer(player: PlayerType): Promise<boolean | void> {}
  async OnPlayerChatMessage(player: PlayerType, message: string): Promise<boolean | void> {}
  async OnPlayerCommand(
    player: PlayerType,
    commandName: string,
    args: any,
  ): Promise<boolean | void> {}
  async OnPlayerMobInteract(
    player: PlayerType,
    mob: oCMobInter,
    mobName: string,
  ): Promise<boolean | void> {}
  async OnPlayerNpcInteract(
    player: PlayerType,
    npc: oCNpc,
    npcName: string,
  ): Promise<boolean | void> {}
  async OnPlayerChangeFocus(
    player: PlayerType,
    vob: zCVob,
    vobName: string,
    vobType: number,
  ): Promise<boolean | void> {}
  async OnPlayerChangeAttribute(
    player: PlayerType,
    attribute: number,
    currentValue: number,
    change: number,
  ): Promise<boolean | void> {}
  async OnPlayerChangeWeaponMode(player: PlayerType, weaponMode: number): Promise<boolean | void> {}
  async OnPlayerTakeItem(
    player: PlayerType,
    item: oCItem,
    itemName: string,
  ): Promise<boolean | void> {}
  async OnPlayerDropItem(
    player: PlayerType,
    item: oCItem,
    itemName: string,
  ): Promise<boolean | void> {}
  async OnPlayerEquipItem(
    player: PlayerType,
    item: oCItem,
    itemName: string,
  ): Promise<boolean | void> {}
  async OnPlayerUnequipItem(
    player: PlayerType,
    item: oCItem,
    itemName: string,
  ): Promise<boolean | void> {}
  async OnPlayerUseItem(
    player: PlayerType,
    item: oCItem,
    itemName: string,
  ): Promise<boolean | void> {}
}
