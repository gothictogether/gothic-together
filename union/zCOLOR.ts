export class zCOLOR {
  R: number = 0
  G: number = 0
  B: number = 0
  A: number = 0

  constructor(initialValue: string | [number, number, number, number]) {
    if (typeof initialValue == 'string') {
      const splittedColor = initialValue.split(';')

      this.R = parseFloat(splittedColor[0] as string)
      this.G = parseFloat(splittedColor[1] as string)
      this.B = parseFloat(splittedColor[2] as string)
      this.A = parseFloat(splittedColor[3] as string)
    } else {
      this.R = initialValue[0]
      this.G = initialValue[1]
      this.B = initialValue[2]
      this.A = initialValue[3]
    }
  }

  toString = (): string => {
    return `${this.R};${this.G};${this.B};${this.A}`
  }
}
