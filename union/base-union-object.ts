import { Server } from '../index.js'

export class BaseUnionObject {
  Uuid: string = 'undefined'

  constructor(uuid: string) {
    this.Uuid = uuid
  }

  toString = (): string => {
    return this.Uuid
  }

  store = () => {
    Server.StorePointer(this.Uuid)
  }

  release = () => {
    Server.ReleasePointer(this.Uuid)
  }
}
