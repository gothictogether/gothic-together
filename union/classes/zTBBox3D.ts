import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zVEC3 } from './index.js'
import { zTBSphere3D } from './index.js'

export class zTBBox3D extends BaseUnionObject {
  mins(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zTBBox3D-mins-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_mins(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zTBBox3D-mins-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  maxs(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zTBBox3D-maxs-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_maxs(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zTBBox3D-maxs-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zTBBox3D_OnInit(a1: zTBBox3D): zTBBox3D | null {
    const result: string | null = SendCommand({
      id: 'zTBBox3D-zTBBox3D_OnInit-void-false-zTBBox3D const&',
      parentId: 'NULL',
      a1: a1.toString(),
    })

    return result ? new zTBBox3D(result) : null
  }

  static zTBBox3D_OnInit2(): zTBBox3D | null {
    const result: string | null = SendCommand({
      id: 'zTBBox3D-zTBBox3D_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zTBBox3D(result) : null
  }

  AddPoint(a1: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'zTBBox3D-AddPoint-void-false-zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  Init(): void | null {
    const result: string | null = SendCommand({
      id: 'zTBBox3D-Init-void-false-',
      parentId: this.Uuid,
    })
  }

  SetMaximumBox(): void | null {
    const result: string | null = SendCommand({
      id: 'zTBBox3D-SetMaximumBox-void-false-',
      parentId: this.Uuid,
    })
  }

  GetVolume(): number | null {
    const result: string | null = SendCommand({
      id: 'zTBBox3D-GetVolume-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  CalcGreaterBBox3D(a1: zTBBox3D): void | null {
    const result: string | null = SendCommand({
      id: 'zTBBox3D-CalcGreaterBBox3D-void-false-zTBBox3D const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  ClipToBBox3D(a1: zTBBox3D): void | null {
    const result: string | null = SendCommand({
      id: 'zTBBox3D-ClipToBBox3D-void-false-zTBBox3D const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetDescription(): string | null {
    const result: string | null = SendCommand({
      id: 'zTBBox3D-GetDescription-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  SetByDescription(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'zTBBox3D-SetByDescription-void-false-zSTRING&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  IsIntersecting(a1: zVEC3, a2: zVEC3, a3: number, a4: number): number | null {
    const result: string | null = SendCommand({
      id: 'zTBBox3D-IsIntersecting-int-false-zVEC3 const&,zVEC3 const&,float&,float&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
    })

    return result ? Number(result) : null
  }

  IsIntersectingSweep(a1: zVEC3, a2: zTBBox3D, a3: number): number | null {
    const result: string | null = SendCommand({
      id: 'zTBBox3D-IsIntersectingSweep-int-false-zVEC3 const&,zTBBox3D const&,float&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? Number(result) : null
  }

  IsIntersectingSweep2(a1: zTBBox3D, a2: zTBBox3D, a3: zTBBox3D, a4: number): number | null {
    const result: string | null = SendCommand({
      id: 'zTBBox3D-IsIntersectingSweep-int-false-zTBBox3D const&,zTBBox3D const&,zTBBox3D const&,float&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
    })

    return result ? Number(result) : null
  }

  IsTrivIn(a1: zTBBox3D): number | null {
    const result: string | null = SendCommand({
      id: 'zTBBox3D-IsTrivIn-int-false-zTBBox3D const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  IsTrivInLine(a1: zVEC3, a2: zVEC3): number | null {
    const result: string | null = SendCommand({
      id: 'zTBBox3D-IsTrivInLine-int-false-zVEC3 const&,zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  Classify(a1: zTBBox3D): number | null {
    const result: string | null = SendCommand({
      id: 'zTBBox3D-Classify-int-false-zTBBox3D const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  Scale(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zTBBox3D-Scale-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  Scale2(a1: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'zTBBox3D-Scale-void-false-zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetMinExtent(): number | null {
    const result: string | null = SendCommand({
      id: 'zTBBox3D-GetMinExtent-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetSphere3D(): zTBSphere3D | null {
    const result: string | null = SendCommand({
      id: 'zTBBox3D-GetSphere3D-zTBSphere3D-false-',
      parentId: this.Uuid,
    })

    return result ? new zTBSphere3D(result) : null
  }

  TraceRay(a1: zVEC3, a2: zVEC3, a3: zVEC3): number | null {
    const result: string | null = SendCommand({
      id: 'zTBBox3D-TraceRay-int-false-zVEC3 const&,zVEC3 const&,zVEC3&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? Number(result) : null
  }

  GetScreenSize(): number | null {
    const result: string | null = SendCommand({
      id: 'zTBBox3D-GetScreenSize-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetCenterFloor(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zTBBox3D-GetCenterFloor-zVEC3-false-',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  GetCenter(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zTBBox3D-GetCenter-zVEC3-false-',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  InitZero(): void | null {
    const result: string | null = SendCommand({
      id: 'zTBBox3D-InitZero-void-false-',
      parentId: this.Uuid,
    })
  }

  IsIntersecting2(a1: zTBSphere3D): number | null {
    const result: string | null = SendCommand({
      id: 'zTBBox3D-IsIntersecting-int-false-zTBSphere3D const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  IsIntersecting3(a1: zTBBox3D): number | null {
    const result: string | null = SendCommand({
      id: 'zTBBox3D-IsIntersecting-int-false-zTBBox3D const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }
}
