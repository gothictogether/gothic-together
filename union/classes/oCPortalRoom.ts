import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { oCNpc } from './index.js'

export class oCPortalRoom extends BaseUnionObject {
  portalName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCPortalRoom-portalName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_portalName(a1: string): null {
    SendCommand({
      id: 'SET_oCPortalRoom-portalName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  ownerNpc(): string | null {
    const result: string | null = SendCommand({
      id: 'oCPortalRoom-ownerNpc-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_ownerNpc(a1: string): null {
    SendCommand({
      id: 'SET_oCPortalRoom-ownerNpc-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  ownerGuild(): number | null {
    const result: string | null = SendCommand({
      id: 'oCPortalRoom-ownerGuild-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_ownerGuild(a1: number): null {
    SendCommand({
      id: 'SET_oCPortalRoom-ownerGuild-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCPortalRoom_OnInit(): oCPortalRoom | null {
    const result: string | null = SendCommand({
      id: 'oCPortalRoom-oCPortalRoom_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCPortalRoom(result) : null
  }

  static oCPortalRoom_OnInit2(a1: string): oCPortalRoom | null {
    const result: string | null = SendCommand({
      id: 'oCPortalRoom-oCPortalRoom_OnInit-void-false-zSTRING const&',
      parentId: 'NULL',
      a1: a1.toString(),
    })

    return result ? new oCPortalRoom(result) : null
  }

  GetOwnerNpc(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCPortalRoom-GetOwnerNpc-oCNpc*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  GetOwnerGuild(): number | null {
    const result: string | null = SendCommand({
      id: 'oCPortalRoom-GetOwnerGuild-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetPortalName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCPortalRoom-GetPortalName-zSTRING&-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  IsPortalMyRoom(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCPortalRoom-IsPortalMyRoom-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }
}
