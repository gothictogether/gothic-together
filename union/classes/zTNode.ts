import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'

export class zTNode extends BaseUnionObject {
  isBegin(): number | null {
    const result: string | null = SendCommand({
      id: 'zTNode-isBegin-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_isBegin(a1: number): null {
    SendCommand({
      id: 'SET_zTNode-isBegin-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
