import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'

export class zTNodeMesh extends BaseUnionObject {
  nodeIndex(): number | null {
    const result: string | null = SendCommand({
      id: 'zTNodeMesh-nodeIndex-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_nodeIndex(a1: number): null {
    SendCommand({
      id: 'SET_zTNodeMesh-nodeIndex-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
