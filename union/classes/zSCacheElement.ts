import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zVEC3 } from './index.js'

export class zSCacheElement extends BaseUnionObject {
  m_position(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zSCacheElement-m_position-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_m_position(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zSCacheElement-m_position-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_normal(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zSCacheElement-m_normal-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_m_normal(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zSCacheElement-m_normal-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
