import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'

export class zCObject extends BaseUnionObject {
  refCtr(): number | null {
    const result: string | null = SendCommand({
      id: 'zCObject-refCtr-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_refCtr(a1: number): null {
    SendCommand({
      id: 'SET_zCObject-refCtr-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  hashIndex(): number | null {
    const result: string | null = SendCommand({
      id: 'zCObject-hashIndex-unsigned short-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_hashIndex(a1: number): null {
    SendCommand({
      id: 'SET_zCObject-hashIndex-unsigned short-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  hashNext(): zCObject | null {
    const result: string | null = SendCommand({
      id: 'zCObject-hashNext-zCObject*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCObject(result) : null
  }

  set_hashNext(a1: zCObject): null {
    SendCommand({
      id: 'SET_zCObject-hashNext-zCObject*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  objectName(): string | null {
    const result: string | null = SendCommand({
      id: 'zCObject-objectName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_objectName(a1: string): null {
    SendCommand({
      id: 'SET_zCObject-objectName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCObject_OnInit(): zCObject | null {
    const result: string | null = SendCommand({
      id: 'zCObject-zCObject_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCObject(result) : null
  }

  Release(): number | null {
    const result: string | null = SendCommand({
      id: 'zCObject-Release-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  CreateCopy(): zCObject | null {
    const result: string | null = SendCommand({
      id: 'zCObject-CreateCopy-zCObject*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCObject(result) : null
  }

  GetObjectName(): string | null {
    const result: string | null = SendCommand({
      id: 'zCObject-GetObjectName-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  SetObjectName(a1: string): number | null {
    const result: string | null = SendCommand({
      id: 'zCObject-SetObjectName-int-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCObject-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }
}
