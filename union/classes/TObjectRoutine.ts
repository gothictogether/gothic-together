import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'

export class TObjectRoutine extends BaseUnionObject {
  objName(): string | null {
    const result: string | null = SendCommand({
      id: 'TObjectRoutine-objName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_objName(a1: string): null {
    SendCommand({
      id: 'SET_TObjectRoutine-objName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  stateNum(): number | null {
    const result: string | null = SendCommand({
      id: 'TObjectRoutine-stateNum-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_stateNum(a1: number): null {
    SendCommand({
      id: 'SET_TObjectRoutine-stateNum-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  hour1(): number | null {
    const result: string | null = SendCommand({
      id: 'TObjectRoutine-hour1-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_hour1(a1: number): null {
    SendCommand({
      id: 'SET_TObjectRoutine-hour1-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  min1(): number | null {
    const result: string | null = SendCommand({
      id: 'TObjectRoutine-min1-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_min1(a1: number): null {
    SendCommand({
      id: 'SET_TObjectRoutine-min1-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  type(): number | null {
    const result: string | null = SendCommand({
      id: 'TObjectRoutine-type-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_type(a1: number): null {
    SendCommand({
      id: 'SET_TObjectRoutine-type-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  Release(): void | null {
    const result: string | null = SendCommand({
      id: 'TObjectRoutine-Release-void-false-',
      parentId: this.Uuid,
    })
  }
}
