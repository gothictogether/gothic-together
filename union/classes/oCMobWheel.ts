import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { zCVob } from './index.js'
import { oCNpc } from './index.js'
import { oCMobInter } from './index.js'

export class oCMobWheel extends oCMobInter {
  static oCMobWheel_OnInit(): oCMobWheel | null {
    const result: string | null = SendCommand({
      id: 'oCMobWheel-oCMobWheel_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCMobWheel(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCMobWheel-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override OnTrigger(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobWheel-OnTrigger-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override OnUntrigger(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobWheel-OnUntrigger-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override Interact(
    a1: oCNpc,
    a2: number,
    a3: number,
    a4: number,
    a5: number,
    a6: number,
  ): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobWheel-Interact-void-false-oCNpc*,int,int,int,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
      a5: a5.toString(),
      a6: a6.toString(),
    })
  }
}
