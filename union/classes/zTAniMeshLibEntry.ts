import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCModelAniActive } from './index.js'
import { zCModelMeshLib } from './index.js'

export class zTAniMeshLibEntry extends BaseUnionObject {
  ani(): zCModelAniActive | null {
    const result: string | null = SendCommand({
      id: 'zTAniMeshLibEntry-ani-zCModelAniActive*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCModelAniActive(result) : null
  }

  set_ani(a1: zCModelAniActive): null {
    SendCommand({
      id: 'SET_zTAniMeshLibEntry-ani-zCModelAniActive*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  meshLib(): zCModelMeshLib | null {
    const result: string | null = SendCommand({
      id: 'zTAniMeshLibEntry-meshLib-zCModelMeshLib*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCModelMeshLib(result) : null
  }

  set_meshLib(a1: zCModelMeshLib): null {
    SendCommand({
      id: 'SET_zTAniMeshLibEntry-meshLib-zCModelMeshLib*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
