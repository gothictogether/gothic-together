import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { TFlags } from './index.js'
import { zTBBox2D } from './index.js'
import { zVEC3 } from './index.js'
import { zTBBox3D } from './index.js'
import { zCVertex } from './index.js'
import { zCVertFeature } from './index.js'
import { zTBSphere3D } from './index.js'
import { zCWorld } from './index.js'

export class zCPolygon extends BaseUnionObject {
  lastTimeDrawn(): number | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-lastTimeDrawn-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_lastTimeDrawn(a1: number): null {
    SendCommand({
      id: 'SET_zCPolygon-lastTimeDrawn-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  numClipVert(): number | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-numClipVert-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_numClipVert(a1: number): null {
    SendCommand({
      id: 'SET_zCPolygon-numClipVert-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  polyNumVert(): number | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-polyNumVert-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_polyNumVert(a1: number): null {
    SendCommand({
      id: 'SET_zCPolygon-polyNumVert-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  flags(): TFlags | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-flags-TFlags-false-false',
      parentId: this.Uuid,
    })

    return result ? new TFlags(result) : null
  }

  set_flags(a1: TFlags): null {
    SendCommand({
      id: 'SET_zCPolygon-flags-TFlags-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCPolygon_OnInit(): zCPolygon | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-zCPolygon_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCPolygon(result) : null
  }

  RenderPoly(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-RenderPoly-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  ApplyMorphing(): void | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-ApplyMorphing-void-false-',
      parentId: this.Uuid,
    })
  }

  LightClippedPoly(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-LightClippedPoly-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  TransformProjectLight(): number | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-TransformProjectLight-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  TransformProject(): number | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-TransformProject-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  Unclip(): void | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-Unclip-void-false-',
      parentId: this.Uuid,
    })
  }

  Unclip_Occluder(): void | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-Unclip_Occluder-void-false-',
      parentId: this.Uuid,
    })
  }

  CopyClipFeaturesForTexAniMapping(): void | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-CopyClipFeaturesForTexAniMapping-void-false-',
      parentId: this.Uuid,
    })
  }

  ClipToFrustum(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-ClipToFrustum-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  ClipToFrustum_Occluder(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-ClipToFrustum_Occluder-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  CalcNormal(): number | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-CalcNormal-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  CalcNormalApprox(): number | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-CalcNormalApprox-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetScreenBBox2D(a1: zTBBox2D): void | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-GetScreenBBox2D-void-false-zTBBox2D&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  LightDynCamSpace(a1: zVEC3, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-LightDynCamSpace-void-false-zVEC3 const&,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  GetLightStatAtPos(a1: zVEC3): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-GetLightStatAtPos-zVEC3-false-zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new zVEC3(result) : null
  }

  CheckRayPolyIntersection(a1: zVEC3, a2: zVEC3, a3: zVEC3, a4: number): number | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-CheckRayPolyIntersection-int-false-zVEC3 const&,zVEC3 const&,zVEC3&,float&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
    })

    return result ? Number(result) : null
  }

  CheckRayPolyIntersection2Sided(a1: zVEC3, a2: zVEC3, a3: zVEC3, a4: number): number | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-CheckRayPolyIntersection2Sided-int-false-zVEC3 const&,zVEC3 const&,zVEC3&,float&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
    })

    return result ? Number(result) : null
  }

  GetBBox3D(): zTBBox3D | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-GetBBox3D-zTBBox3D-false-',
      parentId: this.Uuid,
    })

    return result ? new zTBBox3D(result) : null
  }

  GetCenter(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-GetCenter-zVEC3-false-',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  Flip(): void | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-Flip-void-false-',
      parentId: this.Uuid,
    })
  }

  VertPartOfPoly(a1: zCVertex): number | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-VertPartOfPoly-int-false-zCVertex*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EdgePartOfPoly(a1: zCVertex, a2: zCVertex): number | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-EdgePartOfPoly-int-false-zCVertex*,zCVertex*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  EdgePositionsPartOfPoly(a1: zCVertex, a2: zCVertex): number | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-EdgePositionsPartOfPoly-int-false-zCVertex*,zCVertex*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  TexScale(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-TexScale-void-false-float,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  TexMirrorU(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-TexMirrorU-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  TexMirrorV(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-TexMirrorV-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  TexShearU(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-TexShearU-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  TexShearV(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-TexShearV-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  TexApplyPlanarMapping(a1: zVEC3, a2: zVEC3, a3: zVEC3, a4: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-TexApplyPlanarMapping-void-false-zVEC3 const&,zVEC3 const&,zVEC3 const&,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
    })
  }

  TexApplyPlanarMapping2(a1: number, a2: number, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-TexApplyPlanarMapping-void-false-float,float,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  TexCorrectUV(): void | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-TexCorrectUV-void-false-',
      parentId: this.Uuid,
    })
  }

  AllocVerts(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-AllocVerts-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  AddVertex(a1: zCVertex): void | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-AddVertex-void-false-zCVertex*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  AddVertexAndFeature(a1: zCVertex, a2: zCVertFeature): void | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-AddVertexAndFeature-void-false-zCVertex*,zCVertFeature*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  SetVertices(a1: zCVertex, a2: zCVertex, a3: zCVertex): void | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-SetVertices-void-false-zCVertex*,zCVertex*,zCVertex*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  SetVertices2(a1: zCVertex, a2: zCVertex, a3: zCVertex, a4: zCVertex, a5: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-SetVertices-void-false-zCVertex*,zCVertex*,zCVertex*,zCVertex*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
      a5: a5.toString(),
    })
  }

  SetVertex(a1: number, a2: zCVertex): void | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-SetVertex-void-false-int,zCVertex*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  SetFeature(a1: number, a2: zCVertFeature): void | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-SetFeature-void-false-int,zCVertFeature*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  CopyValuesInto(a1: zCPolygon): void | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-CopyValuesInto-void-false-zCPolygon*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CopyPolyInto(a1: zCPolygon): void | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-CopyPolyInto-void-false-zCPolygon*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RemoveVerticesAndFeatures(): void | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-RemoveVerticesAndFeatures-void-false-',
      parentId: this.Uuid,
    })
  }

  GetArea(): number | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-GetArea-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetLongestEdgeLen(): number | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-GetLongestEdgeLen-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  ResetStaticLight(): void | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-ResetStaticLight-void-false-',
      parentId: this.Uuid,
    })
  }

  SetLightStatAlpha(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-SetLightStatAlpha-void-false-unsigned char',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  ResetLightDynToLightStat(): void | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-ResetLightDynToLightStat-void-false-',
      parentId: this.Uuid,
    })
  }

  CheckBBoxPolyIntersection(a1: zTBBox3D): number | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-CheckBBoxPolyIntersection-int-false-zTBBox3D const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  CheckBSpherePolyIntersection(a1: zTBSphere3D): number | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-CheckBSpherePolyIntersection-int-false-zTBSphere3D const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  IsIntersectingProjection(a1: zCPolygon, a2: zVEC3): number | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-IsIntersectingProjection-int-false-zCPolygon*,zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  IsIntersecting(a1: zCPolygon): number | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-IsIntersecting-int-false-zCPolygon*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  IsNeighbourOf(a1: zCPolygon): number | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-IsNeighbourOf-int-false-zCPolygon const*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetStaticLODPossible(): number | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-GetStaticLODPossible-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SmoothSectorBorder(a1: zCWorld): void | null {
    const result: string | null = SendCommand({
      id: 'zCPolygon-SmoothSectorBorder-void-false-zCWorld*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }
}
