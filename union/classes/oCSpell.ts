import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { oCVisualFX } from './index.js'
import { zCVob } from './index.js'
import { oCNpc } from './index.js'
import { zCClassDef } from './index.js'
import { zCObject } from './index.js'

export class oCSpell extends zCObject {
  keyNo(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-keyNo-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_keyNo(a1: number): null {
    SendCommand({
      id: 'SET_oCSpell-keyNo-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  effect(): oCVisualFX | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-effect-oCVisualFX*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCVisualFX(result) : null
  }

  set_effect(a1: oCVisualFX): null {
    SendCommand({
      id: 'SET_oCSpell-effect-oCVisualFX*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  controlWarnFX(): oCVisualFX | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-controlWarnFX-oCVisualFX*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCVisualFX(result) : null
  }

  set_controlWarnFX(a1: oCVisualFX): null {
    SendCommand({
      id: 'SET_oCSpell-controlWarnFX-oCVisualFX*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  spellCaster(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-spellCaster-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_spellCaster(a1: zCVob): null {
    SendCommand({
      id: 'SET_oCSpell-spellCaster-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  spellCasterNpc(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-spellCasterNpc-oCNpc*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  set_spellCasterNpc(a1: oCNpc): null {
    SendCommand({
      id: 'SET_oCSpell-spellCasterNpc-oCNpc*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  spellTarget(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-spellTarget-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_spellTarget(a1: zCVob): null {
    SendCommand({
      id: 'SET_oCSpell-spellTarget-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  spellTargetNpc(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-spellTargetNpc-oCNpc*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  set_spellTargetNpc(a1: oCNpc): null {
    SendCommand({
      id: 'SET_oCSpell-spellTargetNpc-oCNpc*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  saveNpc(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-saveNpc-oCNpc*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  set_saveNpc(a1: oCNpc): null {
    SendCommand({
      id: 'SET_oCSpell-saveNpc-oCNpc*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  manaTimer(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-manaTimer-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_manaTimer(a1: number): null {
    SendCommand({
      id: 'SET_oCSpell-manaTimer-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  manaInvested(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-manaInvested-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_manaInvested(a1: number): null {
    SendCommand({
      id: 'SET_oCSpell-manaInvested-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  spellLevel(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-spellLevel-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_spellLevel(a1: number): null {
    SendCommand({
      id: 'SET_oCSpell-spellLevel-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  spellStatus(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-spellStatus-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_spellStatus(a1: number): null {
    SendCommand({
      id: 'SET_oCSpell-spellStatus-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  spellID(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-spellID-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_spellID(a1: number): null {
    SendCommand({
      id: 'SET_oCSpell-spellID-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  spellInfo(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-spellInfo-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_spellInfo(a1: number): null {
    SendCommand({
      id: 'SET_oCSpell-spellInfo-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  spellEnabled(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-spellEnabled-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_spellEnabled(a1: number): null {
    SendCommand({
      id: 'SET_oCSpell-spellEnabled-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  spellInitDone(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-spellInitDone-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_spellInitDone(a1: number): null {
    SendCommand({
      id: 'SET_oCSpell-spellInitDone-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  timerEffect(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-timerEffect-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_timerEffect(a1: number): null {
    SendCommand({
      id: 'SET_oCSpell-timerEffect-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  canBeDeleted(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-canBeDeleted-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_canBeDeleted(a1: number): null {
    SendCommand({
      id: 'SET_oCSpell-canBeDeleted-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  up(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-up-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_up(a1: number): null {
    SendCommand({
      id: 'SET_oCSpell-up-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  hoverY(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-hoverY-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_hoverY(a1: number): null {
    SendCommand({
      id: 'SET_oCSpell-hoverY-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  hoverOld(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-hoverOld-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_hoverOld(a1: number): null {
    SendCommand({
      id: 'SET_oCSpell-hoverOld-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  hoverDir(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-hoverDir-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_hoverDir(a1: number): null {
    SendCommand({
      id: 'SET_oCSpell-hoverDir-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  spellEnergy(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-spellEnergy-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_spellEnergy(a1: number): null {
    SendCommand({
      id: 'SET_oCSpell-spellEnergy-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  manaInvestTime(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-manaInvestTime-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_manaInvestTime(a1: number): null {
    SendCommand({
      id: 'SET_oCSpell-manaInvestTime-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  damagePerLevel(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-damagePerLevel-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_damagePerLevel(a1: number): null {
    SendCommand({
      id: 'SET_oCSpell-damagePerLevel-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  damageType(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-damageType-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_damageType(a1: number): null {
    SendCommand({
      id: 'SET_oCSpell-damageType-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  spellType(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-spellType-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_spellType(a1: number): null {
    SendCommand({
      id: 'SET_oCSpell-spellType-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  canTurnDuringInvest(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-canTurnDuringInvest-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_canTurnDuringInvest(a1: number): null {
    SendCommand({
      id: 'SET_oCSpell-canTurnDuringInvest-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  canChangeTargetDuringInvest(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-canChangeTargetDuringInvest-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_canChangeTargetDuringInvest(a1: number): null {
    SendCommand({
      id: 'SET_oCSpell-canChangeTargetDuringInvest-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  isMultiEffect(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-isMultiEffect-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_isMultiEffect(a1: number): null {
    SendCommand({
      id: 'SET_oCSpell-isMultiEffect-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  targetCollectAlgo(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-targetCollectAlgo-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_targetCollectAlgo(a1: number): null {
    SendCommand({
      id: 'SET_oCSpell-targetCollectAlgo-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  targetCollectType(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-targetCollectType-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_targetCollectType(a1: number): null {
    SendCommand({
      id: 'SET_oCSpell-targetCollectType-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  targetCollectRange(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-targetCollectRange-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_targetCollectRange(a1: number): null {
    SendCommand({
      id: 'SET_oCSpell-targetCollectRange-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  targetCollectAzi(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-targetCollectAzi-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_targetCollectAzi(a1: number): null {
    SendCommand({
      id: 'SET_oCSpell-targetCollectAzi-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  targetCollectElev(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-targetCollectElev-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_targetCollectElev(a1: number): null {
    SendCommand({
      id: 'SET_oCSpell-targetCollectElev-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCSpell_OnInit(): oCSpell | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-oCSpell_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCSpell(result) : null
  }

  static oCSpell_OnInit2(a1: number): oCSpell | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-oCSpell_OnInit-void-false-int',
      parentId: 'NULL',
      a1: a1.toString(),
    })

    return result ? new oCSpell(result) : null
  }

  InitValues(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-InitValues-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetSpellInstanceName(a1: number): string | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-GetSpellInstanceName-zSTRING-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? String(result) : null
  }

  CreateEffect(): oCVisualFX | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-CreateEffect-oCVisualFX*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCVisualFX(result) : null
  }

  InitByScript(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-InitByScript-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetEnabled(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-SetEnabled-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetEnabled(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-GetEnabled-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  Setup(a1: zCVob, a2: zCVob, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-Setup-void-false-zCVob*,zCVob*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  Invest(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-Invest-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  Cast(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-Cast-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  Stop(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-Stop-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  Kill(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-Kill-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  Open(): void | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-Open-void-false-',
      parentId: this.Uuid,
    })
  }

  Close(): void | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-Close-void-false-',
      parentId: this.Uuid,
    })
  }

  Reset(): void | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-Reset-void-false-',
      parentId: this.Uuid,
    })
  }

  StopTargetEffects(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-StopTargetEffects-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CallScriptInvestedMana(): void | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-CallScriptInvestedMana-void-false-',
      parentId: this.Uuid,
    })
  }

  IsValidTarget(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-IsValidTarget-int-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetVob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-GetVob-zCVob*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  GetSpellID(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-GetSpellID-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetEnergyType(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-GetEnergyType-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetCategory(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-GetCategory-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-GetName-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  GetLevel(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-GetLevel-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsInvestSpell(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-IsInvestSpell-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetInvestedMana(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-SetInvestedMana-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetSpellStatus(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-GetSpellStatus-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetReleaseStatus(): void | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-SetReleaseStatus-void-false-',
      parentId: this.Uuid,
    })
  }

  SetSpellInfo(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-SetSpellInfo-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  DoLogicInvestEffect(): void | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-DoLogicInvestEffect-void-false-',
      parentId: this.Uuid,
    })
  }

  CastSpecificSpell(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-CastSpecificSpell-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  EndTimedEffect(): void | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-EndTimedEffect-void-false-',
      parentId: this.Uuid,
    })
  }

  DoTimedEffect(): void | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-DoTimedEffect-void-false-',
      parentId: this.Uuid,
    })
  }

  CanBeDeleted(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-CanBeDeleted-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  DeleteCaster(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-DeleteCaster-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCSpell-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }
}
