import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'

export class TNpcAIState extends BaseUnionObject {
  index(): number | null {
    const result: string | null = SendCommand({
      id: 'TNpcAIState-index-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_index(a1: number): null {
    SendCommand({
      id: 'SET_TNpcAIState-index-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  loop(): number | null {
    const result: string | null = SendCommand({
      id: 'TNpcAIState-loop-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_loop(a1: number): null {
    SendCommand({
      id: 'SET_TNpcAIState-loop-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  end(): number | null {
    const result: string | null = SendCommand({
      id: 'TNpcAIState-end-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_end(a1: number): null {
    SendCommand({
      id: 'SET_TNpcAIState-end-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  timeBehaviour(): number | null {
    const result: string | null = SendCommand({
      id: 'TNpcAIState-timeBehaviour-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_timeBehaviour(a1: number): null {
    SendCommand({
      id: 'SET_TNpcAIState-timeBehaviour-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  restTime(): number | null {
    const result: string | null = SendCommand({
      id: 'TNpcAIState-restTime-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_restTime(a1: number): null {
    SendCommand({
      id: 'SET_TNpcAIState-restTime-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  phase(): number | null {
    const result: string | null = SendCommand({
      id: 'TNpcAIState-phase-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_phase(a1: number): null {
    SendCommand({
      id: 'SET_TNpcAIState-phase-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  valid(): number | null {
    const result: string | null = SendCommand({
      id: 'TNpcAIState-valid-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_valid(a1: number): null {
    SendCommand({
      id: 'SET_TNpcAIState-valid-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  name(): string | null {
    const result: string | null = SendCommand({
      id: 'TNpcAIState-name-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_name(a1: string): null {
    SendCommand({
      id: 'SET_TNpcAIState-name-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  stateTime(): number | null {
    const result: string | null = SendCommand({
      id: 'TNpcAIState-stateTime-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_stateTime(a1: number): null {
    SendCommand({
      id: 'SET_TNpcAIState-stateTime-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  prgIndex(): number | null {
    const result: string | null = SendCommand({
      id: 'TNpcAIState-prgIndex-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_prgIndex(a1: number): null {
    SendCommand({
      id: 'SET_TNpcAIState-prgIndex-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  isRtnState(): number | null {
    const result: string | null = SendCommand({
      id: 'TNpcAIState-isRtnState-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_isRtnState(a1: number): null {
    SendCommand({
      id: 'SET_TNpcAIState-isRtnState-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static TNpcAIState_OnInit(): TNpcAIState | null {
    const result: string | null = SendCommand({
      id: 'TNpcAIState-TNpcAIState_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new TNpcAIState(result) : null
  }
}
