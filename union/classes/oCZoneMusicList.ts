import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { oCZoneMusic } from './index.js'

export class oCZoneMusicList extends BaseUnionObject {
  entry(): oCZoneMusic | null {
    const result: string | null = SendCommand({
      id: 'oCZoneMusicList-entry-oCZoneMusic*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCZoneMusic(result) : null
  }

  set_entry(a1: oCZoneMusic): null {
    SendCommand({
      id: 'SET_oCZoneMusicList-entry-oCZoneMusic*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  next(): oCZoneMusicList | null {
    const result: string | null = SendCommand({
      id: 'oCZoneMusicList-next-oCZoneMusicList*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCZoneMusicList(result) : null
  }

  set_next(a1: oCZoneMusicList): null {
    SendCommand({
      id: 'SET_oCZoneMusicList-next-oCZoneMusicList*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
