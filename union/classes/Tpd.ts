import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'

export class Tpd extends BaseUnionObject {
  npc(): number | null {
    const result: string | null = SendCommand({
      id: 'Tpd-npc-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_npc(a1: number): null {
    SendCommand({
      id: 'SET_Tpd-npc-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  nr(): number | null {
    const result: string | null = SendCommand({
      id: 'Tpd-nr-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_nr(a1: number): null {
    SendCommand({
      id: 'SET_Tpd-nr-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  important(): number | null {
    const result: string | null = SendCommand({
      id: 'Tpd-important-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_important(a1: number): null {
    SendCommand({
      id: 'SET_Tpd-important-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  conditions(): number | null {
    const result: string | null = SendCommand({
      id: 'Tpd-conditions-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_conditions(a1: number): null {
    SendCommand({
      id: 'SET_Tpd-conditions-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  information(): number | null {
    const result: string | null = SendCommand({
      id: 'Tpd-information-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_information(a1: number): null {
    SendCommand({
      id: 'SET_Tpd-information-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  description(): string | null {
    const result: string | null = SendCommand({
      id: 'Tpd-description-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_description(a1: string): null {
    SendCommand({
      id: 'SET_Tpd-description-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  trade(): number | null {
    const result: string | null = SendCommand({
      id: 'Tpd-trade-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_trade(a1: number): null {
    SendCommand({
      id: 'SET_Tpd-trade-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  permanent(): number | null {
    const result: string | null = SendCommand({
      id: 'Tpd-permanent-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_permanent(a1: number): null {
    SendCommand({
      id: 'SET_Tpd-permanent-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
