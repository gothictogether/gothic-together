import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { zCEffect } from './index.js'

export class zCVobLensFlare extends zCEffect {
  static zCVobLensFlare_OnInit(): zCVobLensFlare | null {
    const result: string | null = SendCommand({
      id: 'zCVobLensFlare-zCVobLensFlare_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCVobLensFlare(result) : null
  }

  SetLensFlareFXByName(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'zCVobLensFlare-SetLensFlareFXByName-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCVobLensFlare-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }
}
