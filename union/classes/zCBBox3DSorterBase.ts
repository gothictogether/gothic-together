import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zTBBox3D } from './index.js'
import { zTBoxSortHandle } from './index.js'

export class zCBBox3DSorterBase extends BaseUnionObject {
  sorted(): number | null {
    const result: string | null = SendCommand({
      id: 'zCBBox3DSorterBase-sorted-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_sorted(a1: number): null {
    SendCommand({
      id: 'SET_zCBBox3DSorterBase-sorted-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  Clear(): void | null {
    const result: string | null = SendCommand({
      id: 'zCBBox3DSorterBase-Clear-void-false-',
      parentId: this.Uuid,
    })
  }

  AllocAbs(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCBBox3DSorterBase-AllocAbs-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetActiveList(a1: zTBBox3D, a2: zTBoxSortHandle): void | null {
    const result: string | null = SendCommand({
      id: 'zCBBox3DSorterBase-GetActiveList-void-false-zTBBox3D const&,zTBoxSortHandle&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  UpdateActiveList(a1: zTBBox3D, a2: zTBoxSortHandle): void | null {
    const result: string | null = SendCommand({
      id: 'zCBBox3DSorterBase-UpdateActiveList-void-false-zTBBox3D const&,zTBoxSortHandle&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  InsertHandle(a1: zTBoxSortHandle): void | null {
    const result: string | null = SendCommand({
      id: 'zCBBox3DSorterBase-InsertHandle-void-false-zTBoxSortHandle&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RemoveHandle(a1: zTBoxSortHandle): void | null {
    const result: string | null = SendCommand({
      id: 'zCBBox3DSorterBase-RemoveHandle-void-false-zTBoxSortHandle&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  Sort(): void | null {
    const result: string | null = SendCommand({
      id: 'zCBBox3DSorterBase-Sort-void-false-',
      parentId: this.Uuid,
    })
  }
}
