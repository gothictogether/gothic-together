import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'

export class zTEngineStats extends BaseUnionObject {
  numVobAI(): number | null {
    const result: string | null = SendCommand({
      id: 'zTEngineStats-numVobAI-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_numVobAI(a1: number): null {
    SendCommand({
      id: 'SET_zTEngineStats-numVobAI-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  numVobAI_only(): number | null {
    const result: string | null = SendCommand({
      id: 'zTEngineStats-numVobAI_only-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_numVobAI_only(a1: number): null {
    SendCommand({
      id: 'SET_zTEngineStats-numVobAI_only-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  numVobAI_full(): number | null {
    const result: string | null = SendCommand({
      id: 'zTEngineStats-numVobAI_full-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_numVobAI_full(a1: number): null {
    SendCommand({
      id: 'SET_zTEngineStats-numVobAI_full-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  numVob_EndMovement(): number | null {
    const result: string | null = SendCommand({
      id: 'zTEngineStats-numVob_EndMovement-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_numVob_EndMovement(a1: number): null {
    SendCommand({
      id: 'SET_zTEngineStats-numVob_EndMovement-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  numVobFirstTestInterpen(): number | null {
    const result: string | null = SendCommand({
      id: 'zTEngineStats-numVobFirstTestInterpen-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_numVobFirstTestInterpen(a1: number): null {
    SendCommand({
      id: 'SET_zTEngineStats-numVobFirstTestInterpen-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  numModelUpdateBBoxTree(): number | null {
    const result: string | null = SendCommand({
      id: 'zTEngineStats-numModelUpdateBBoxTree-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_numModelUpdateBBoxTree(a1: number): null {
    SendCommand({
      id: 'SET_zTEngineStats-numModelUpdateBBoxTree-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  numPolysClipped(): number | null {
    const result: string | null = SendCommand({
      id: 'zTEngineStats-numPolysClipped-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_numPolysClipped(a1: number): null {
    SendCommand({
      id: 'SET_zTEngineStats-numPolysClipped-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  numPlanesClipped(): number | null {
    const result: string | null = SendCommand({
      id: 'zTEngineStats-numPlanesClipped-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_numPlanesClipped(a1: number): null {
    SendCommand({
      id: 'SET_zTEngineStats-numPlanesClipped-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  numTraceRay(): number | null {
    const result: string | null = SendCommand({
      id: 'zTEngineStats-numTraceRay-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_numTraceRay(a1: number): null {
    SendCommand({
      id: 'SET_zTEngineStats-numTraceRay-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  PrintScreen(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'zTEngineStats-PrintScreen-void-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }
}
