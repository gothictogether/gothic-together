import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { zCVob } from './index.js'
import { zCZoneMusic } from './index.js'

export class oCZoneMusic extends zCZoneMusic {
  enabled(): number | null {
    const result: string | null = SendCommand({
      id: 'oCZoneMusic-enabled-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_enabled(a1: number): null {
    SendCommand({
      id: 'SET_oCZoneMusic-enabled-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  local_enabled(): number | null {
    const result: string | null = SendCommand({
      id: 'oCZoneMusic-local_enabled-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_local_enabled(a1: number): null {
    SendCommand({
      id: 'SET_oCZoneMusic-local_enabled-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  priority(): number | null {
    const result: string | null = SendCommand({
      id: 'oCZoneMusic-priority-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_priority(a1: number): null {
    SendCommand({
      id: 'SET_oCZoneMusic-priority-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  ellipsoid(): number | null {
    const result: string | null = SendCommand({
      id: 'oCZoneMusic-ellipsoid-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_ellipsoid(a1: number): null {
    SendCommand({
      id: 'SET_oCZoneMusic-ellipsoid-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  reverbLevel(): number | null {
    const result: string | null = SendCommand({
      id: 'oCZoneMusic-reverbLevel-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_reverbLevel(a1: number): null {
    SendCommand({
      id: 'SET_oCZoneMusic-reverbLevel-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  volumeLevel(): number | null {
    const result: string | null = SendCommand({
      id: 'oCZoneMusic-volumeLevel-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_volumeLevel(a1: number): null {
    SendCommand({
      id: 'SET_oCZoneMusic-volumeLevel-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  loop(): number | null {
    const result: string | null = SendCommand({
      id: 'oCZoneMusic-loop-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_loop(a1: number): null {
    SendCommand({
      id: 'SET_oCZoneMusic-loop-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  dayEntranceDone(): number | null {
    const result: string | null = SendCommand({
      id: 'oCZoneMusic-dayEntranceDone-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_dayEntranceDone(a1: number): null {
    SendCommand({
      id: 'SET_oCZoneMusic-dayEntranceDone-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  nightEntranceDone(): number | null {
    const result: string | null = SendCommand({
      id: 'oCZoneMusic-nightEntranceDone-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_nightEntranceDone(a1: number): null {
    SendCommand({
      id: 'SET_oCZoneMusic-nightEntranceDone-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCZoneMusic_OnInit(): oCZoneMusic | null {
    const result: string | null = SendCommand({
      id: 'oCZoneMusic-oCZoneMusic_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCZoneMusic(result) : null
  }

  SetEnabled(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCZoneMusic-SetEnabled-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  IsEnabled(): number | null {
    const result: string | null = SendCommand({
      id: 'oCZoneMusic-IsEnabled-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetPriority(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCZoneMusic-SetPriority-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetPriority(): number | null {
    const result: string | null = SendCommand({
      id: 'oCZoneMusic-GetPriority-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetEllipsoid(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCZoneMusic-SetEllipsoid-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  IsEllipsoid(): number | null {
    const result: string | null = SendCommand({
      id: 'oCZoneMusic-IsEllipsoid-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetReverb(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCZoneMusic-SetReverb-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetReverb(): number | null {
    const result: string | null = SendCommand({
      id: 'oCZoneMusic-GetReverb-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetVolume(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCZoneMusic-SetVolume-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetVolume(): number | null {
    const result: string | null = SendCommand({
      id: 'oCZoneMusic-GetVolume-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetLoop(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCZoneMusic-SetLoop-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  IsLoop(): number | null {
    const result: string | null = SendCommand({
      id: 'oCZoneMusic-IsLoop-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetDayEntranceDone(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCZoneMusic-SetDayEntranceDone-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  IsDayEntranceDone(): number | null {
    const result: string | null = SendCommand({
      id: 'oCZoneMusic-IsDayEntranceDone-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetNightEntranceDone(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCZoneMusic-SetNightEntranceDone-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  IsNightEntranceDone(): number | null {
    const result: string | null = SendCommand({
      id: 'oCZoneMusic-IsNightEntranceDone-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetCamPosWeightElps(): number | null {
    const result: string | null = SendCommand({
      id: 'oCZoneMusic-GetCamPosWeightElps-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCZoneMusic-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override OnTrigger(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCZoneMusic-OnTrigger-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override OnUntrigger(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCZoneMusic-OnUntrigger-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override GetDefaultZoneClass(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCZoneMusic-GetDefaultZoneClass-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override GetDebugDescString(): string | null {
    const result: string | null = SendCommand({
      id: 'oCZoneMusic-GetDebugDescString-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }
}
