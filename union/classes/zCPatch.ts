import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zVEC3 } from './index.js'

export class zCPatch extends BaseUnionObject {
  center(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCPatch-center-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_center(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCPatch-center-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  centerLight(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCPatch-centerLight-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_centerLight(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCPatch-centerLight-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  normal(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCPatch-normal-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_normal(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCPatch-normal-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  area(): number | null {
    const result: string | null = SendCommand({
      id: 'zCPatch-area-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_area(a1: number): null {
    SendCommand({
      id: 'SET_zCPatch-area-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  radiosity(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCPatch-radiosity-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_radiosity(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCPatch-radiosity-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  radToShoot(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCPatch-radToShoot-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_radToShoot(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCPatch-radToShoot-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  xpos(): number | null {
    const result: string | null = SendCommand({
      id: 'zCPatch-xpos-short-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_xpos(a1: number): null {
    SendCommand({
      id: 'SET_zCPatch-xpos-short-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  ypos(): number | null {
    const result: string | null = SendCommand({
      id: 'zCPatch-ypos-short-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_ypos(a1: number): null {
    SendCommand({
      id: 'SET_zCPatch-ypos-short-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  reflectivity(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCPatch-reflectivity-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_reflectivity(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCPatch-reflectivity-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCPatch_OnInit(): zCPatch | null {
    const result: string | null = SendCommand({
      id: 'zCPatch-zCPatch_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCPatch(result) : null
  }

  DoTransfers(): void | null {
    const result: string | null = SendCommand({
      id: 'zCPatch-DoTransfers-void-false-',
      parentId: this.Uuid,
    })
  }
}
