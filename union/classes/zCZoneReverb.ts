import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { zCZone } from './index.js'

export class zCZoneReverb extends zCZone {
  reverbPresetNr(): number | null {
    const result: string | null = SendCommand({
      id: 'zCZoneReverb-reverbPresetNr-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_reverbPresetNr(a1: number): null {
    SendCommand({
      id: 'SET_zCZoneReverb-reverbPresetNr-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  reverbPresetWeight(): number | null {
    const result: string | null = SendCommand({
      id: 'zCZoneReverb-reverbPresetWeight-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_reverbPresetWeight(a1: number): null {
    SendCommand({
      id: 'SET_zCZoneReverb-reverbPresetWeight-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  innerRangePerc(): number | null {
    const result: string | null = SendCommand({
      id: 'zCZoneReverb-innerRangePerc-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_innerRangePerc(a1: number): null {
    SendCommand({
      id: 'SET_zCZoneReverb-innerRangePerc-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCZoneReverb_OnInit(): zCZoneReverb | null {
    const result: string | null = SendCommand({
      id: 'zCZoneReverb-zCZoneReverb_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCZoneReverb(result) : null
  }

  GetActiveWeight(): number | null {
    const result: string | null = SendCommand({
      id: 'zCZoneReverb-GetActiveWeight-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCZoneReverb-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override GetDefaultZoneClass(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCZoneReverb-GetDefaultZoneClass-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override GetDebugDescString(): string | null {
    const result: string | null = SendCommand({
      id: 'zCZoneReverb-GetDebugDescString-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }
}
