import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zESkyLayerMode } from './index.js'

export class zCSkyLayerData extends BaseUnionObject {
  skyMode(): zESkyLayerMode | null {
    const result: string | null = SendCommand({
      id: 'zCSkyLayerData-skyMode-zESkyLayerMode-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_skyMode(a1: zESkyLayerMode): null {
    SendCommand({
      id: 'SET_zCSkyLayerData-skyMode-zESkyLayerMode-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  texName(): string | null {
    const result: string | null = SendCommand({
      id: 'zCSkyLayerData-texName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_texName(a1: string): null {
    SendCommand({
      id: 'SET_zCSkyLayerData-texName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  texAlpha(): number | null {
    const result: string | null = SendCommand({
      id: 'zCSkyLayerData-texAlpha-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_texAlpha(a1: number): null {
    SendCommand({
      id: 'SET_zCSkyLayerData-texAlpha-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  texScale(): number | null {
    const result: string | null = SendCommand({
      id: 'zCSkyLayerData-texScale-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_texScale(a1: number): null {
    SendCommand({
      id: 'SET_zCSkyLayerData-texScale-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCSkyLayerData_OnInit(): zCSkyLayerData | null {
    const result: string | null = SendCommand({
      id: 'zCSkyLayerData-zCSkyLayerData_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCSkyLayerData(result) : null
  }
}
