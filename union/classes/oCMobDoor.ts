import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { TMobOptPos } from './index.js'
import { oCNpc } from './index.js'
import { oCMobLockable } from './index.js'

export class oCMobDoor extends oCMobLockable {
  addName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCMobDoor-addName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_addName(a1: string): null {
    SendCommand({
      id: 'SET_oCMobDoor-addName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCMobDoor_OnInit(): oCMobDoor | null {
    const result: string | null = SendCommand({
      id: 'oCMobDoor-oCMobDoor_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCMobDoor(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCMobDoor-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override GetScemeName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCMobDoor-GetScemeName-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  SearchFreePositionDoor(a1: oCNpc, a2: number): TMobOptPos | null {
    const result: string | null = SendCommand({
      id: 'oCMobDoor-SearchFreePosition-TMobOptPos*-false-oCNpc*,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new TMobOptPos(result) : null
  }

  override Open(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobDoor-Open-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override Close(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobDoor-Close-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }
}
