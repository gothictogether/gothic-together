import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'

export class oCDoc extends BaseUnionObject {
  levelName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCDoc-levelName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_levelName(a1: string): null {
    SendCommand({
      id: 'SET_oCDoc-levelName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  scalex(): number | null {
    const result: string | null = SendCommand({
      id: 'oCDoc-scalex-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_scalex(a1: number): null {
    SendCommand({
      id: 'SET_oCDoc-scalex-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  scaley(): number | null {
    const result: string | null = SendCommand({
      id: 'oCDoc-scaley-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_scaley(a1: number): null {
    SendCommand({
      id: 'SET_oCDoc-scaley-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  map_gameCoPerPixelx(): number | null {
    const result: string | null = SendCommand({
      id: 'oCDoc-map_gameCoPerPixelx-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_map_gameCoPerPixelx(a1: number): null {
    SendCommand({
      id: 'SET_oCDoc-map_gameCoPerPixelx-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  map_gameStartx(): number | null {
    const result: string | null = SendCommand({
      id: 'oCDoc-map_gameStartx-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_map_gameStartx(a1: number): null {
    SendCommand({
      id: 'SET_oCDoc-map_gameStartx-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  map_gameCoPerPixely(): number | null {
    const result: string | null = SendCommand({
      id: 'oCDoc-map_gameCoPerPixely-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_map_gameCoPerPixely(a1: number): null {
    SendCommand({
      id: 'SET_oCDoc-map_gameCoPerPixely-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  map_gameStarty(): number | null {
    const result: string | null = SendCommand({
      id: 'oCDoc-map_gameStarty-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_map_gameStarty(a1: number): null {
    SendCommand({
      id: 'SET_oCDoc-map_gameStarty-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pixelStartx(): number | null {
    const result: string | null = SendCommand({
      id: 'oCDoc-pixelStartx-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_pixelStartx(a1: number): null {
    SendCommand({
      id: 'SET_oCDoc-pixelStartx-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pixelStarty(): number | null {
    const result: string | null = SendCommand({
      id: 'oCDoc-pixelStarty-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_pixelStarty(a1: number): null {
    SendCommand({
      id: 'SET_oCDoc-pixelStarty-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCDoc_OnInit(a1: string): oCDoc | null {
    const result: string | null = SendCommand({
      id: 'oCDoc-oCDoc_OnInit-void-false-zSTRING const&',
      parentId: 'NULL',
      a1: a1.toString(),
    })

    return result ? new oCDoc(result) : null
  }

  Show(): void | null {
    const result: string | null = SendCommand({
      id: 'oCDoc-Show-void-false-',
      parentId: this.Uuid,
    })
  }

  MapInitCoordinates(
    a1: number,
    a2: number,
    a3: number,
    a4: number,
    a5: number,
    a6: number,
    a7: number,
    a8: number,
  ): void | null {
    const result: string | null = SendCommand({
      id: 'oCDoc-MapInitCoordinates-void-false-float,float,float,float,float,float,float,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
      a5: a5.toString(),
      a6: a6.toString(),
      a7: a7.toString(),
      a8: a8.toString(),
    })
  }

  MapDrawCoordinates(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCDoc-MapDrawCoordinates-void-false-float,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  SetLevelName(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCDoc-SetLevelName-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  HandleEvent(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCDoc-HandleEvent-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }
}
