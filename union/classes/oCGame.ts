import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { oCNpc } from './index.js'
import { zVEC3 } from './index.js'
import { oCGameInfo } from './index.js'
import { zCVobLight } from './index.js'
import { oCWorldTimer } from './index.js'
import { oCGuilds } from './index.js'
import { oCInfoManager } from './index.js'
import { oCNewsManager } from './index.js'
import { oCTradeManager } from './index.js'
import { oCPortalRoomManager } from './index.js'
import { oCSpawnManager } from './index.js'
import { oHEROSTATUS } from './index.js'
import { zCVob } from './index.js'
import { oEGameDialogView } from './index.js'
import { zCCamera } from './index.js'
import { oCWorld } from './index.js'
import { zCSession } from './index.js'

export class oCGame extends zCSession {
  cliprange(): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-cliprange-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_cliprange(a1: number): null {
    SendCommand({
      id: 'SET_oCGame-cliprange-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fogrange(): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-fogrange-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fogrange(a1: number): null {
    SendCommand({
      id: 'SET_oCGame-fogrange-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  inScriptStartup(): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-inScriptStartup-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_inScriptStartup(a1: number): null {
    SendCommand({
      id: 'SET_oCGame-inScriptStartup-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  inLoadSaveGame(): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-inLoadSaveGame-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_inLoadSaveGame(a1: number): null {
    SendCommand({
      id: 'SET_oCGame-inLoadSaveGame-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  inLevelChange(): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-inLevelChange-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_inLevelChange(a1: number): null {
    SendCommand({
      id: 'SET_oCGame-inLevelChange-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  showPlayerStatus(): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-showPlayerStatus-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_showPlayerStatus(a1: number): null {
    SendCommand({
      id: 'SET_oCGame-showPlayerStatus-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  game_drawall(): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-game_drawall-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_game_drawall(a1: number): null {
    SendCommand({
      id: 'SET_oCGame-game_drawall-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  game_frameinfo(): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-game_frameinfo-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_game_frameinfo(a1: number): null {
    SendCommand({
      id: 'SET_oCGame-game_frameinfo-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  game_showaniinfo(): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-game_showaniinfo-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_game_showaniinfo(a1: number): null {
    SendCommand({
      id: 'SET_oCGame-game_showaniinfo-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  game_showwaynet(): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-game_showwaynet-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_game_showwaynet(a1: number): null {
    SendCommand({
      id: 'SET_oCGame-game_showwaynet-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  game_testmode(): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-game_testmode-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_game_testmode(a1: number): null {
    SendCommand({
      id: 'SET_oCGame-game_testmode-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  game_editwaynet(): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-game_editwaynet-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_game_editwaynet(a1: number): null {
    SendCommand({
      id: 'SET_oCGame-game_editwaynet-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  game_showtime(): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-game_showtime-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_game_showtime(a1: number): null {
    SendCommand({
      id: 'SET_oCGame-game_showtime-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  game_showranges(): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-game_showranges-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_game_showranges(a1: number): null {
    SendCommand({
      id: 'SET_oCGame-game_showranges-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  drawWayBoxes(): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-drawWayBoxes-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_drawWayBoxes(a1: number): null {
    SendCommand({
      id: 'SET_oCGame-drawWayBoxes-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  scriptStartup(): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-scriptStartup-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_scriptStartup(a1: number): null {
    SendCommand({
      id: 'SET_oCGame-scriptStartup-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  showFreePoints(): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-showFreePoints-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_showFreePoints(a1: number): null {
    SendCommand({
      id: 'SET_oCGame-showFreePoints-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  showRoutineNpc(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCGame-showRoutineNpc-oCNpc*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  set_showRoutineNpc(a1: oCNpc): null {
    SendCommand({
      id: 'SET_oCGame-showRoutineNpc-oCNpc*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  loadNextLevel(): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-loadNextLevel-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_loadNextLevel(a1: number): null {
    SendCommand({
      id: 'SET_oCGame-loadNextLevel-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  loadNextLevelName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCGame-loadNextLevelName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_loadNextLevelName(a1: string): null {
    SendCommand({
      id: 'SET_oCGame-loadNextLevelName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  loadNextLevelStart(): string | null {
    const result: string | null = SendCommand({
      id: 'oCGame-loadNextLevelStart-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_loadNextLevelStart(a1: string): null {
    SendCommand({
      id: 'SET_oCGame-loadNextLevelStart-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  startpos(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oCGame-startpos-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_startpos(a1: zVEC3): null {
    SendCommand({
      id: 'SET_oCGame-startpos-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  gameInfo(): oCGameInfo | null {
    const result: string | null = SendCommand({
      id: 'oCGame-gameInfo-oCGameInfo*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCGameInfo(result) : null
  }

  set_gameInfo(a1: oCGameInfo): null {
    SendCommand({
      id: 'SET_oCGame-gameInfo-oCGameInfo*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pl_light(): zCVobLight | null {
    const result: string | null = SendCommand({
      id: 'oCGame-pl_light-zCVobLight*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVobLight(result) : null
  }

  set_pl_light(a1: zCVobLight): null {
    SendCommand({
      id: 'SET_oCGame-pl_light-zCVobLight*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pl_lightval(): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-pl_lightval-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_pl_lightval(a1: number): null {
    SendCommand({
      id: 'SET_oCGame-pl_lightval-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  wldTimer(): oCWorldTimer | null {
    const result: string | null = SendCommand({
      id: 'oCGame-wldTimer-oCWorldTimer*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCWorldTimer(result) : null
  }

  set_wldTimer(a1: oCWorldTimer): null {
    SendCommand({
      id: 'SET_oCGame-wldTimer-oCWorldTimer*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  timeStep(): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-timeStep-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_timeStep(a1: number): null {
    SendCommand({
      id: 'SET_oCGame-timeStep-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  singleStep(): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-singleStep-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_singleStep(a1: number): null {
    SendCommand({
      id: 'SET_oCGame-singleStep-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  guilds(): oCGuilds | null {
    const result: string | null = SendCommand({
      id: 'oCGame-guilds-oCGuilds*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCGuilds(result) : null
  }

  set_guilds(a1: oCGuilds): null {
    SendCommand({
      id: 'SET_oCGame-guilds-oCGuilds*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  infoman(): oCInfoManager | null {
    const result: string | null = SendCommand({
      id: 'oCGame-infoman-oCInfoManager*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCInfoManager(result) : null
  }

  set_infoman(a1: oCInfoManager): null {
    SendCommand({
      id: 'SET_oCGame-infoman-oCInfoManager*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  newsman(): oCNewsManager | null {
    const result: string | null = SendCommand({
      id: 'oCGame-newsman-oCNewsManager*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNewsManager(result) : null
  }

  set_newsman(a1: oCNewsManager): null {
    SendCommand({
      id: 'SET_oCGame-newsman-oCNewsManager*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  trademan(): oCTradeManager | null {
    const result: string | null = SendCommand({
      id: 'oCGame-trademan-oCTradeManager*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCTradeManager(result) : null
  }

  set_trademan(a1: oCTradeManager): null {
    SendCommand({
      id: 'SET_oCGame-trademan-oCTradeManager*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  portalman(): oCPortalRoomManager | null {
    const result: string | null = SendCommand({
      id: 'oCGame-portalman-oCPortalRoomManager*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCPortalRoomManager(result) : null
  }

  set_portalman(a1: oCPortalRoomManager): null {
    SendCommand({
      id: 'SET_oCGame-portalman-oCPortalRoomManager*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  spawnman(): oCSpawnManager | null {
    const result: string | null = SendCommand({
      id: 'oCGame-spawnman-oCSpawnManager*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCSpawnManager(result) : null
  }

  set_spawnman(a1: oCSpawnManager): null {
    SendCommand({
      id: 'SET_oCGame-spawnman-oCSpawnManager*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  music_delay(): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-music_delay-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_music_delay(a1: number): null {
    SendCommand({
      id: 'SET_oCGame-music_delay-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  watchnpc(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCGame-watchnpc-oCNpc*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  set_watchnpc(a1: oCNpc): null {
    SendCommand({
      id: 'SET_oCGame-watchnpc-oCNpc*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_bWorldEntered(): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-m_bWorldEntered-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_bWorldEntered(a1: number): null {
    SendCommand({
      id: 'SET_oCGame-m_bWorldEntered-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_fEnterWorldTimer(): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-m_fEnterWorldTimer-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_fEnterWorldTimer(a1: number): null {
    SendCommand({
      id: 'SET_oCGame-m_fEnterWorldTimer-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  initial_hour(): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-initial_hour-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_initial_hour(a1: number): null {
    SendCommand({
      id: 'SET_oCGame-initial_hour-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  initial_minute(): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-initial_minute-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_initial_minute(a1: number): null {
    SendCommand({
      id: 'SET_oCGame-initial_minute-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  debugChannels(): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-debugChannels-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_debugChannels(a1: number): null {
    SendCommand({
      id: 'SET_oCGame-debugChannels-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  debugAllInstances(): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-debugAllInstances-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_debugAllInstances(a1: number): null {
    SendCommand({
      id: 'SET_oCGame-debugAllInstances-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  oldRoutineDay(): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-oldRoutineDay-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_oldRoutineDay(a1: number): null {
    SendCommand({
      id: 'SET_oCGame-oldRoutineDay-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCGame_OnInit(): oCGame | null {
    const result: string | null = SendCommand({
      id: 'oCGame-oCGame_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCGame(result) : null
  }

  SetCameraPosition(): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-SetCameraPosition-void-false-',
      parentId: this.Uuid,
    })
  }

  UpdateViewSettings(): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-UpdateViewSettings-void-false-',
      parentId: this.Uuid,
    })
  }

  CallScriptStartup(): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-CallScriptStartup-void-false-',
      parentId: this.Uuid,
    })
  }

  CallScriptInit(): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-CallScriptInit-void-false-',
      parentId: this.Uuid,
    })
  }

  GetSelfPlayerVob(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCGame-GetSelfPlayerVob-oCNpc*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  GetWorldTimer(): oCWorldTimer | null {
    const result: string | null = SendCommand({
      id: 'oCGame-GetWorldTimer-oCWorldTimer*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCWorldTimer(result) : null
  }

  GetInfoManager(): oCInfoManager | null {
    const result: string | null = SendCommand({
      id: 'oCGame-GetInfoManager-oCInfoManager*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCInfoManager(result) : null
  }

  GetNewsManager(): oCNewsManager | null {
    const result: string | null = SendCommand({
      id: 'oCGame-GetNewsManager-oCNewsManager*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCNewsManager(result) : null
  }

  GetTradeManager(): oCTradeManager | null {
    const result: string | null = SendCommand({
      id: 'oCGame-GetTradeManager-oCTradeManager*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCTradeManager(result) : null
  }

  GetGuilds(): oCGuilds | null {
    const result: string | null = SendCommand({
      id: 'oCGame-GetGuilds-oCGuilds*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCGuilds(result) : null
  }

  GetPortalRoomManager(): oCPortalRoomManager | null {
    const result: string | null = SendCommand({
      id: 'oCGame-GetPortalRoomManager-oCPortalRoomManager*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCPortalRoomManager(result) : null
  }

  GetSpawnManager(): oCSpawnManager | null {
    const result: string | null = SendCommand({
      id: 'oCGame-GetSpawnManager-oCSpawnManager*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCSpawnManager(result) : null
  }

  GetHeroStatus(): oHEROSTATUS | null {
    const result: string | null = SendCommand({
      id: 'oCGame-GetHeroStatus-oHEROSTATUS-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetShowNews(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-SetShowNews-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetShowPlayerStatus(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-SetShowPlayerStatus-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetShowPlayerStatus(): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-GetShowPlayerStatus-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  UpdateScreenResolution(): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-UpdateScreenResolution-void-false-',
      parentId: this.Uuid,
    })
  }

  UpdatePlayerStatus(): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-UpdatePlayerStatus-void-false-',
      parentId: this.Uuid,
    })
  }

  RemovePlayerFromWorld(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCGame-RemovePlayerFromWorld-oCNpc*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  PreSaveGameProcessing(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-PreSaveGameProcessing-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  PostSaveGameProcessing(): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-PostSaveGameProcessing-void-false-',
      parentId: this.Uuid,
    })
  }

  WriteSavegame(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-WriteSavegame-void-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  CheckObjectConsistency(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-CheckObjectConsistency-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  ClearGameState(): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-ClearGameState-void-false-',
      parentId: this.Uuid,
    })
  }

  InitNpcAttitudes(): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-InitNpcAttitudes-void-false-',
      parentId: this.Uuid,
    })
  }

  CacheVisualsIn(): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-CacheVisualsIn-void-false-',
      parentId: this.Uuid,
    })
  }

  CacheVisualsOut(): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-CacheVisualsOut-void-false-',
      parentId: this.Uuid,
    })
  }

  LoadGame(a1: number, a2: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-LoadGame-void-false-int,zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  LoadSavegame(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-LoadSavegame-void-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  ShowDebugInfos(): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-ShowDebugInfos-void-false-',
      parentId: this.Uuid,
    })
  }

  DeleteNpcs(a1: oCNpc, a2: oCNpc, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-DeleteNpcs-void-false-oCNpc*,oCNpc*,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  LoadWorld_novt(a1: number, a2: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-LoadWorld_novt-void-false-int,zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  IsInWorld(a1: string): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-IsInWorld-int-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  InsertObjectRoutine(a1: number, a2: string, a3: number, a4: number, a5: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-InsertObjectRoutine-void-false-int,zSTRING const&,int,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
      a5: a5.toString(),
    })
  }

  ResetObjectRoutineList(): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-ResetObjectRoutineList-void-false-',
      parentId: this.Uuid,
    })
  }

  ClearObjectRoutineList(): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-ClearObjectRoutineList-void-false-',
      parentId: this.Uuid,
    })
  }

  CheckObjectRoutines(): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-CheckObjectRoutines-void-false-',
      parentId: this.Uuid,
    })
  }

  SetObjectRoutineTimeChange(a1: number, a2: number, a3: number, a4: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-SetObjectRoutineTimeChange-void-false-int,int,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
    })
  }

  ToggleShowFreePoints(): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-ToggleShowFreePoints-void-false-',
      parentId: this.Uuid,
    })
  }

  ShowFreePoints(): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-ShowFreePoints-void-false-',
      parentId: this.Uuid,
    })
  }

  RefreshNpcs(): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-RefreshNpcs-void-false-',
      parentId: this.Uuid,
    })
  }

  DeleteTorches(): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-DeleteTorches-void-false-',
      parentId: this.Uuid,
    })
  }

  IsDebuggingChannel(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-IsDebuggingChannel-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  IsDebuggingInstance(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-IsDebuggingInstance-int-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  IsDebuggingAllInstances(): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-IsDebuggingAllInstances-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  OpenView(a1: oEGameDialogView): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-OpenView-int-false-oEGameDialogView',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  CloseView(a1: oEGameDialogView): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-CloseView-int-false-oEGameDialogView',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  SwitchCamToNextNpc(): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-SwitchCamToNextNpc-void-false-',
      parentId: this.Uuid,
    })
  }

  TestKeys(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-TestKeys-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  IA_TestWay(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-IA_TestWay-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  HandleEvent(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-HandleEvent-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  override Init(): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-Init-void-false-',
      parentId: this.Uuid,
    })
  }

  override Done(): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-Done-void-false-',
      parentId: this.Uuid,
    })
  }

  override Render(): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-Render-void-false-',
      parentId: this.Uuid,
    })
  }

  override RenderBlit(): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-RenderBlit-void-false-',
      parentId: this.Uuid,
    })
  }

  override CamInit(): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-CamInit-void-false-',
      parentId: this.Uuid,
    })
  }

  override CamInit2(a1: zCVob, a2: zCCamera): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-CamInit-void-false-zCVob*,zCCamera*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override SetTime(a1: number, a2: number, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-SetTime-void-false-int,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  override GetTime(a1: number, a2: number, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-GetTime-void-false-int&,int&,int&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  override DesktopInit(): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-DesktopInit-void-false-',
      parentId: this.Uuid,
    })
  }

  EnterWorld(a1: oCNpc, a2: number, a3: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-EnterWorld-void-false-oCNpc*,int,zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  Unpause(): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-Unpause-void-false-',
      parentId: this.Uuid,
    })
  }

  SetDrawWaynet(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-SetDrawWaynet-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetDrawWaynet(): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-GetDrawWaynet-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  RenderWaynet(): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-RenderWaynet-void-false-',
      parentId: this.Uuid,
    })
  }

  EnvironmentInit(): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-EnvironmentInit-void-false-',
      parentId: this.Uuid,
    })
  }

  SetRanges(): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-SetRanges-void-false-',
      parentId: this.Uuid,
    })
  }

  SetRangesByCommandLine(): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-SetRangesByCommandLine-void-false-',
      parentId: this.Uuid,
    })
  }

  GetStartPos(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oCGame-GetStartPos-zVEC3-false-',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  SetGameInfo(a1: oCGameInfo): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-SetGameInfo-void-false-oCGameInfo*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  LoadParserFile(a1: string): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-LoadParserFile-int-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  TriggerChangeLevel(a1: string, a2: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-TriggerChangeLevel-void-false-zSTRING const&,zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  GetGameWorld(): oCWorld | null {
    const result: string | null = SendCommand({
      id: 'oCGame-GetGameWorld-oCWorld*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCWorld(result) : null
  }

  GetGameInfo(): oCGameInfo | null {
    const result: string | null = SendCommand({
      id: 'oCGame-GetGameInfo-oCGameInfo*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCGameInfo(result) : null
  }

  OpenLoadscreen(a1: boolean, a2: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-OpenLoadscreen-void-false-bool,zSTRING',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  OpenSavescreen(a1: boolean): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-OpenSavescreen-void-false-bool',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CloseLoadscreen(): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-CloseLoadscreen-void-false-',
      parentId: this.Uuid,
    })
  }

  CloseSavescreen(): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-CloseSavescreen-void-false-',
      parentId: this.Uuid,
    })
  }

  ChangeLevel(a1: string, a2: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-ChangeLevel-void-false-zSTRING const&,zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  LoadWorldStartup(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-LoadWorldStartup-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  LoadWorldStat(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-LoadWorldStat-void-false-zSTRING',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  LoadWorldDyn(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-LoadWorldDyn-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  InitWorldSavegame(a1: number, a2: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-InitWorldSavegame-void-false-int&,zSTRING&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  CheckIfSavegameExists(a1: string): number | null {
    const result: string | null = SendCommand({
      id: 'oCGame-CheckIfSavegameExists-int-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  CompileWorld(): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-CompileWorld-void-false-',
      parentId: this.Uuid,
    })
  }

  WorldInit(): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-WorldInit-void-false-',
      parentId: this.Uuid,
    })
  }

  NpcInit(): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-NpcInit-void-false-',
      parentId: this.Uuid,
    })
  }

  SetAsPlayer(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCGame-SetAsPlayer-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }
}
