import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { zCVob } from './index.js'
import { zVEC3 } from './index.js'
import { zCWorld } from './index.js'
import { zCObject } from './index.js'

export class zCAIBase extends zCObject {
  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCAIBase-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  DoAI(a1: zCVob, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIBase-DoAI-void-false-zCVob*,int&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  CanThisCollideWith(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIBase-CanThisCollideWith-int-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  HasAIDetectedCollision(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIBase-HasAIDetectedCollision-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  AICollisionResponseSelfDetected(a1: zVEC3, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIBase-AICollisionResponseSelfDetected-void-false-zVEC3 const&,int&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  HostVobRemovedFromWorld(a1: zCVob, a2: zCWorld): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIBase-HostVobRemovedFromWorld-void-false-zCVob*,zCWorld*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  HostVobAddedToWorld(a1: zCVob, a2: zCWorld): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIBase-HostVobAddedToWorld-void-false-zCVob*,zCWorld*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  GetIsProjectile(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIBase-GetIsProjectile-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }
}
