import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zVEC3 } from './index.js'
import { zTBBox3D } from './index.js'

export class zCOBBox3D extends BaseUnionObject {
  center(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCOBBox3D-center-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_center(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCOBBox3D-center-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  extent(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCOBBox3D-extent-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_extent(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCOBBox3D-extent-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  TestIntersectionOBB(a1: zCOBBox3D): number | null {
    const result: string | null = SendCommand({
      id: 'zCOBBox3D-TestIntersectionOBB-int-false-zCOBBox3D const*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  TestIntersectionTreeRec(a1: zCOBBox3D): number | null {
    const result: string | null = SendCommand({
      id: 'zCOBBox3D-TestIntersectionTreeRec-int-false-zCOBBox3D const*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  TraceRay(a1: zVEC3, a2: zVEC3, a3: zVEC3): number | null {
    const result: string | null = SendCommand({
      id: 'zCOBBox3D-TraceRay-int-false-zVEC3 const&,zVEC3 const&,zVEC3&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? Number(result) : null
  }

  GetBBox3D(): zTBBox3D | null {
    const result: string | null = SendCommand({
      id: 'zCOBBox3D-GetBBox3D-zTBBox3D-false-',
      parentId: this.Uuid,
    })

    return result ? new zTBBox3D(result) : null
  }

  SetByBBox3D(a1: zTBBox3D): void | null {
    const result: string | null = SendCommand({
      id: 'zCOBBox3D-SetByBBox3D-void-false-zTBBox3D const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }
}
