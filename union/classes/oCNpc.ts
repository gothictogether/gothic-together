import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zVEC3 } from './index.js'
import { zCWaypoint } from './index.js'
import { oTRobustTrace } from './index.js'
import { oCMagFrontier } from './index.js'
import { oCNpc_States } from './index.js'
import { oCNpcInventory } from './index.js'
import { oCItemContainer } from './index.js'
import { zCVob } from './index.js'
import { oCMag_Book } from './index.js'
import { oCInfo } from './index.js'
import { oCNews } from './index.js'
import { oCMission } from './index.js'
import { oCNewsMemory } from './index.js'
import { oCMobInter } from './index.js'
import { oCItem } from './index.js'
import { oCAIHuman } from './index.js'
import { oCAniCtrl_Human } from './index.js'
import { zCRoute } from './index.js'
import { oCNpcMessage } from './index.js'
import { oCMsgDamage } from './index.js'
import { oSDamageDescriptor } from './index.js'
import { oCMsgAttack } from './index.js'
import { oCMsgMagic } from './index.js'
import { oCMsgMovement } from './index.js'
import { oSDirectionInfo } from './index.js'
import { oCMsgConversation } from './index.js'
import { zCEventMessage } from './index.js'
import { TActiveInfo } from './index.js'
import { oEIndexDamage } from './index.js'
import { oETypeDamage } from './index.js'
import { oCNpcTalent } from './index.js'
import { oCVob } from './index.js'
import { zCModel } from './index.js'
import { oCSpell } from './index.js'
import { zCVobSpot } from './index.js'
import { TNpcSlot } from './index.js'
import { oCMsgWeapon } from './index.js'
import { oCMsgUseItem } from './index.js'
import { oCMsgManipulate } from './index.js'
import { zCModelAni } from './index.js'
import { oCMsgState } from './index.js'
import { oCMOB } from './index.js'
import { zCModelNodeInst } from './index.js'
import { zCClassDef } from './index.js'
import { zTVobCharClass } from './index.js'
import { zCWorld } from './index.js'
import { zCCamera } from './index.js'

export class oCNpc extends oCVob {
  idx(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-idx-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_idx(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-idx-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  slot(): string | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-slot-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_slot(a1: string): null {
    SendCommand({
      id: 'SET_oCNpc-slot-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  effect(): string | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-effect-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_effect(a1: string): null {
    SendCommand({
      id: 'SET_oCNpc-effect-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  npcType(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-npcType-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_npcType(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-npcType-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  variousFlags(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-variousFlags-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_variousFlags(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-variousFlags-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  damagetype(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-damagetype-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_damagetype(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-damagetype-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  guild(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-guild-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_guild(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-guild-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  level(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-level-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_level(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-level-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fighttactic(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-fighttactic-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fighttactic(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-fighttactic-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fmode(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-fmode-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fmode(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-fmode-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  voice(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-voice-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_voice(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-voice-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  voicePitch(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-voicePitch-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_voicePitch(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-voicePitch-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  mass(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-mass-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_mass(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-mass-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  daily_routine(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-daily_routine-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_daily_routine(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-daily_routine-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  startAIState(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-startAIState-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_startAIState(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-startAIState-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  spawnPoint(): string | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-spawnPoint-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_spawnPoint(a1: string): null {
    SendCommand({
      id: 'SET_oCNpc-spawnPoint-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  spawnDelay(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-spawnDelay-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_spawnDelay(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-spawnDelay-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  senses(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-senses-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_senses(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-senses-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  senses_range(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-senses_range-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_senses_range(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-senses_range-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  wpname(): string | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-wpname-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_wpname(a1: string): null {
    SendCommand({
      id: 'SET_oCNpc-wpname-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  experience_points(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-experience_points-unsigned long-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_experience_points(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-experience_points-unsigned long-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  experience_points_next_level(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-experience_points_next_level-unsigned long-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_experience_points_next_level(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-experience_points_next_level-unsigned long-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  learn_points(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-learn_points-unsigned long-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_learn_points(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-learn_points-unsigned long-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  bodyStateInterruptableOverride(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-bodyStateInterruptableOverride-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_bodyStateInterruptableOverride(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-bodyStateInterruptableOverride-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  noFocus(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-noFocus-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_noFocus(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-noFocus-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  parserEnd(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-parserEnd-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_parserEnd(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-parserEnd-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  bloodEnabled(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-bloodEnabled-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_bloodEnabled(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-bloodEnabled-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  bloodDistance(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-bloodDistance-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_bloodDistance(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-bloodDistance-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  bloodAmount(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-bloodAmount-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_bloodAmount(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-bloodAmount-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  bloodFlow(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-bloodFlow-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_bloodFlow(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-bloodFlow-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  bloodEmitter(): string | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-bloodEmitter-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_bloodEmitter(a1: string): null {
    SendCommand({
      id: 'SET_oCNpc-bloodEmitter-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  bloodTexture(): string | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-bloodTexture-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_bloodTexture(a1: string): null {
    SendCommand({
      id: 'SET_oCNpc-bloodTexture-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  didHit(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-didHit-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_didHit(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-didHit-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  didParade(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-didParade-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_didParade(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-didParade-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  didShoot(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-didShoot-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_didShoot(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-didShoot-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  hasLockedEnemy(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-hasLockedEnemy-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_hasLockedEnemy(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-hasLockedEnemy-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  isDefending(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-isDefending-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_isDefending(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-isDefending-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  wasAiming(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-wasAiming-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_wasAiming(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-wasAiming-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  enemy(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-enemy-oCNpc*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  set_enemy(a1: oCNpc): null {
    SendCommand({
      id: 'SET_oCNpc-enemy-oCNpc*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  speedTurn(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-speedTurn-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_speedTurn(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-speedTurn-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  foundFleePoint(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-foundFleePoint-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_foundFleePoint(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-foundFleePoint-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  reachedFleePoint(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-reachedFleePoint-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_reachedFleePoint(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-reachedFleePoint-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  vecFlee(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-vecFlee-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_vecFlee(a1: zVEC3): null {
    SendCommand({
      id: 'SET_oCNpc-vecFlee-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  posFlee(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-posFlee-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_posFlee(a1: zVEC3): null {
    SendCommand({
      id: 'SET_oCNpc-posFlee-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  waypointFlee(): zCWaypoint | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-waypointFlee-zCWaypoint*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCWaypoint(result) : null
  }

  set_waypointFlee(a1: zCWaypoint): null {
    SendCommand({
      id: 'SET_oCNpc-waypointFlee-zCWaypoint*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  rbt(): oTRobustTrace | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-rbt-oTRobustTrace-false-false',
      parentId: this.Uuid,
    })

    return result ? new oTRobustTrace(result) : null
  }

  set_rbt(a1: oTRobustTrace): null {
    SendCommand({
      id: 'SET_oCNpc-rbt-oTRobustTrace-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  spellMana(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-spellMana-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_spellMana(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-spellMana-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  magFrontier(): oCMagFrontier | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-magFrontier-oCMagFrontier-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCMagFrontier(result) : null
  }

  set_magFrontier(a1: oCMagFrontier): null {
    SendCommand({
      id: 'SET_oCNpc-magFrontier-oCMagFrontier-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  state(): oCNpc_States | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-state-oCNpc_States-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNpc_States(result) : null
  }

  set_state(a1: oCNpc_States): null {
    SendCommand({
      id: 'SET_oCNpc-state-oCNpc_States-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  inventory2(): oCNpcInventory | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-inventory2-oCNpcInventory-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNpcInventory(result) : null
  }

  set_inventory2(a1: oCNpcInventory): null {
    SendCommand({
      id: 'SET_oCNpc-inventory2-oCNpcInventory-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  trader(): oCItemContainer | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-trader-oCItemContainer*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCItemContainer(result) : null
  }

  set_trader(a1: oCItemContainer): null {
    SendCommand({
      id: 'SET_oCNpc-trader-oCItemContainer*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  tradeNpc(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-tradeNpc-oCNpc*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  set_tradeNpc(a1: oCNpc): null {
    SendCommand({
      id: 'SET_oCNpc-tradeNpc-oCNpc*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  rangeToPlayer(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-rangeToPlayer-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_rangeToPlayer(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-rangeToPlayer-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  voiceIndex(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-voiceIndex-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_voiceIndex(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-voiceIndex-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  showaidebug(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-showaidebug-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_showaidebug(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-showaidebug-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  showNews(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-showNews-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_showNews(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-showNews-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  csAllowedAsRole(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-csAllowedAsRole-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_csAllowedAsRole(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-csAllowedAsRole-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  isSummoned(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-isSummoned-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_isSummoned(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-isSummoned-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  respawnOn(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-respawnOn-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_respawnOn(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-respawnOn-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  movlock(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-movlock-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_movlock(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-movlock-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  drunk(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-drunk-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_drunk(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-drunk-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  mad(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-mad-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_mad(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-mad-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  overlay_wounded(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-overlay_wounded-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_overlay_wounded(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-overlay_wounded-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  inOnDamage(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-inOnDamage-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_inOnDamage(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-inOnDamage-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  autoremoveweapon(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-autoremoveweapon-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_autoremoveweapon(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-autoremoveweapon-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  openinventory(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-openinventory-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_openinventory(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-openinventory-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  askroutine(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-askroutine-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_askroutine(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-askroutine-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  spawnInRange(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-spawnInRange-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_spawnInRange(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-spawnInRange-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  body_TexVarNr(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-body_TexVarNr-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_body_TexVarNr(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-body_TexVarNr-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  body_TexColorNr(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-body_TexColorNr-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_body_TexColorNr(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-body_TexColorNr-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  head_TexVarNr(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-head_TexVarNr-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_head_TexVarNr(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-head_TexVarNr-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  teeth_TexVarNr(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-teeth_TexVarNr-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_teeth_TexVarNr(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-teeth_TexVarNr-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  guildTrue(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-guildTrue-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_guildTrue(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-guildTrue-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  drunk_heal(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-drunk_heal-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_drunk_heal(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-drunk_heal-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  mad_heal(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-mad_heal-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_mad_heal(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-mad_heal-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  spells(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-spells-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_spells(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-spells-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  bodyState(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-bodyState-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_bodyState(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-bodyState-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_bAniMessageRunning(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-m_bAniMessageRunning-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_bAniMessageRunning(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-m_bAniMessageRunning-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  instanz(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-instanz-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_instanz(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-instanz-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  mds_name(): string | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-mds_name-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_mds_name(a1: string): null {
    SendCommand({
      id: 'SET_oCNpc-mds_name-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  body_visualName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-body_visualName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_body_visualName(a1: string): null {
    SendCommand({
      id: 'SET_oCNpc-body_visualName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  head_visualName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-head_visualName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_head_visualName(a1: string): null {
    SendCommand({
      id: 'SET_oCNpc-head_visualName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  model_scale(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-model_scale-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_model_scale(a1: zVEC3): null {
    SendCommand({
      id: 'SET_oCNpc-model_scale-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  model_fatness(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-model_fatness-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_model_fatness(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-model_fatness-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  namenr(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-namenr-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_namenr(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-namenr-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  hpHeal(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-hpHeal-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_hpHeal(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-hpHeal-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  manaHeal(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-manaHeal-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_manaHeal(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-manaHeal-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  swimtime(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-swimtime-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_swimtime(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-swimtime-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  divetime(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-divetime-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_divetime(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-divetime-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  divectr(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-divectr-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_divectr(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-divectr-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fireVob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-fireVob-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_fireVob(a1: zCVob): null {
    SendCommand({
      id: 'SET_oCNpc-fireVob-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fireDamage(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-fireDamage-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fireDamage(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-fireDamage-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fireDamageTimer(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-fireDamageTimer-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fireDamageTimer(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-fireDamageTimer-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  attitude(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-attitude-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_attitude(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-attitude-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  tmp_attitude(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-tmp_attitude-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_tmp_attitude(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-tmp_attitude-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  attTimer(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-attTimer-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_attTimer(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-attTimer-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  knowsPlayer(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-knowsPlayer-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_knowsPlayer(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-knowsPlayer-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  percActive(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-percActive-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_percActive(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-percActive-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  percActiveTime(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-percActiveTime-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_percActiveTime(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-percActiveTime-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  percActiveDelta(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-percActiveDelta-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_percActiveDelta(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-percActiveDelta-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  overrideFallDownHeight(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-overrideFallDownHeight-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_overrideFallDownHeight(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-overrideFallDownHeight-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fallDownHeight(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-fallDownHeight-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fallDownHeight(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-fallDownHeight-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fallDownDamage(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-fallDownDamage-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fallDownDamage(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-fallDownDamage-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  mag_book(): oCMag_Book | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-mag_book-oCMag_Book*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCMag_Book(result) : null
  }

  set_mag_book(a1: oCMag_Book): null {
    SendCommand({
      id: 'SET_oCNpc-mag_book-oCMag_Book*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lastHitSpellID(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-lastHitSpellID-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_lastHitSpellID(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-lastHitSpellID-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lastHitSpellCat(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-lastHitSpellCat-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_lastHitSpellCat(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-lastHitSpellCat-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  askYes(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-askYes-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_askYes(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-askYes-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  askNo(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-askNo-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_askNo(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-askNo-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  canTalk(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-canTalk-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_canTalk(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-canTalk-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  talkOther(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-talkOther-oCNpc*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  set_talkOther(a1: oCNpc): null {
    SendCommand({
      id: 'SET_oCNpc-talkOther-oCNpc*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  info(): oCInfo | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-info-oCInfo*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCInfo(result) : null
  }

  set_info(a1: oCInfo): null {
    SendCommand({
      id: 'SET_oCNpc-info-oCInfo*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  news(): oCNews | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-news-oCNews*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNews(result) : null
  }

  set_news(a1: oCNews): null {
    SendCommand({
      id: 'SET_oCNpc-news-oCNews*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  curMission(): oCMission | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-curMission-oCMission*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCMission(result) : null
  }

  set_curMission(a1: oCMission): null {
    SendCommand({
      id: 'SET_oCNpc-curMission-oCMission*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  knownNews(): oCNewsMemory | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-knownNews-oCNewsMemory-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNewsMemory(result) : null
  }

  set_knownNews(a1: oCNewsMemory): null {
    SendCommand({
      id: 'SET_oCNpc-knownNews-oCNewsMemory-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  carry_vob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-carry_vob-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_carry_vob(a1: zCVob): null {
    SendCommand({
      id: 'SET_oCNpc-carry_vob-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  interactMob(): oCMobInter | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-interactMob-oCMobInter*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCMobInter(result) : null
  }

  set_interactMob(a1: oCMobInter): null {
    SendCommand({
      id: 'SET_oCNpc-interactMob-oCMobInter*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  interactItem(): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-interactItem-oCItem*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCItem(result) : null
  }

  set_interactItem(a1: oCItem): null {
    SendCommand({
      id: 'SET_oCNpc-interactItem-oCItem*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  interactItemCurrentState(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-interactItemCurrentState-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_interactItemCurrentState(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-interactItemCurrentState-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  interactItemTargetState(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-interactItemTargetState-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_interactItemTargetState(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-interactItemTargetState-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  script_aiprio(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-script_aiprio-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_script_aiprio(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-script_aiprio-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  old_script_state(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-old_script_state-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_old_script_state(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-old_script_state-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  human_ai(): oCAIHuman | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-human_ai-oCAIHuman*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCAIHuman(result) : null
  }

  set_human_ai(a1: oCAIHuman): null {
    SendCommand({
      id: 'SET_oCNpc-human_ai-oCAIHuman*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  anictrl(): oCAniCtrl_Human | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-anictrl-oCAniCtrl_Human*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCAniCtrl_Human(result) : null
  }

  set_anictrl(a1: oCAniCtrl_Human): null {
    SendCommand({
      id: 'SET_oCNpc-anictrl-oCAniCtrl_Human*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  route(): zCRoute | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-route-zCRoute*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCRoute(result) : null
  }

  set_route(a1: zCRoute): null {
    SendCommand({
      id: 'SET_oCNpc-route-zCRoute*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  damageMul(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-damageMul-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_damageMul(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-damageMul-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  csg(): oCNpcMessage | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-csg-oCNpcMessage*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNpcMessage(result) : null
  }

  set_csg(a1: oCNpcMessage): null {
    SendCommand({
      id: 'SET_oCNpc-csg-oCNpcMessage*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lastLookMsg(): oCNpcMessage | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-lastLookMsg-oCNpcMessage*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNpcMessage(result) : null
  }

  set_lastLookMsg(a1: oCNpcMessage): null {
    SendCommand({
      id: 'SET_oCNpc-lastLookMsg-oCNpcMessage*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lastPointMsg(): oCNpcMessage | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-lastPointMsg-oCNpcMessage*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNpcMessage(result) : null
  }

  set_lastPointMsg(a1: oCNpcMessage): null {
    SendCommand({
      id: 'SET_oCNpc-lastPointMsg-oCNpcMessage*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  vobcheck_time(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-vobcheck_time-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_vobcheck_time(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-vobcheck_time-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pickvobdelay(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-pickvobdelay-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_pickvobdelay(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-pickvobdelay-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  focus_vob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-focus_vob-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_focus_vob(a1: zCVob): null {
    SendCommand({
      id: 'SET_oCNpc-focus_vob-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fadeAwayTime(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-fadeAwayTime-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fadeAwayTime(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-fadeAwayTime-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  respawnTime(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-respawnTime-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_respawnTime(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-respawnTime-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  selfDist(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-selfDist-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_selfDist(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-selfDist-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fightRangeBase(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-fightRangeBase-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fightRangeBase(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-fightRangeBase-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fightRangeFist(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-fightRangeFist-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fightRangeFist(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-fightRangeFist-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fightRangeG(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-fightRangeG-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fightRangeG(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-fightRangeG-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fight_waitTime(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-fight_waitTime-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fight_waitTime(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-fight_waitTime-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fight_waitForAniEnd(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-fight_waitForAniEnd-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fight_waitForAniEnd(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-fight_waitForAniEnd-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fight_lastStrafeFrame(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-fight_lastStrafeFrame-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fight_lastStrafeFrame(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-fight_lastStrafeFrame-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  soundType(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-soundType-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_soundType(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc-soundType-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  soundVob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-soundVob-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_soundVob(a1: zCVob): null {
    SendCommand({
      id: 'SET_oCNpc-soundVob-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  soundPosition(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-soundPosition-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_soundPosition(a1: zVEC3): null {
    SendCommand({
      id: 'SET_oCNpc-soundPosition-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCNpc_OnInit(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-oCNpc_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCNpc(result) : null
  }

  InitDamage(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-InitDamage-void-false-',
      parentId: this.Uuid,
    })
  }

  EV_DamageOnce(a1: oCMsgDamage): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_DamageOnce-int-false-oCMsgDamage*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_DamagePerFrame(a1: oCMsgDamage): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_DamagePerFrame-int-false-oCMsgDamage*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  OnDamage_Hit(a1: oSDamageDescriptor): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-OnDamage_Hit-void-false-oSDamageDescriptor&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  OnDamage_Condition(a1: oSDamageDescriptor): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-OnDamage_Condition-void-false-oSDamageDescriptor&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  OnDamage_Script(a1: oSDamageDescriptor): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-OnDamage_Script-void-false-oSDamageDescriptor&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  OnDamage_Effects(a1: oSDamageDescriptor): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-OnDamage_Effects-void-false-oSDamageDescriptor&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  OnDamage_Effects_Start(a1: oSDamageDescriptor): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-OnDamage_Effects_Start-void-false-oSDamageDescriptor&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  OnDamage_Effects_End(a1: oSDamageDescriptor): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-OnDamage_Effects_End-void-false-oSDamageDescriptor&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  OnDamage_Anim(a1: oSDamageDescriptor): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-OnDamage_Anim-void-false-oSDamageDescriptor&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  OnDamage_Sound(a1: oSDamageDescriptor): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-OnDamage_Sound-void-false-oSDamageDescriptor&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  OnDamage_Events(a1: oSDamageDescriptor): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-OnDamage_Events-void-false-oSDamageDescriptor&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  OnDamage_State(a1: oSDamageDescriptor): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-OnDamage_State-void-false-oSDamageDescriptor&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  HasFlag(a1: number, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-HasFlag-int-false-unsigned long,unsigned long',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  IsNpcBetweenMeAndTarget(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsNpcBetweenMeAndTarget-int-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  IsInDoubleFightRange(a1: zCVob, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsInDoubleFightRange-int-false-zCVob*,float&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  IsInFightRange(a1: zCVob, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsInFightRange-int-false-zCVob*,float&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  GetFightRange(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetFightRange-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetFightRangeBase(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetFightRangeBase-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetFightRangeBase(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetFightRangeBase-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetFightRangeDynamic(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetFightRangeDynamic-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetFightRangeFist(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetFightRangeFist-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetFightRangeG(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetFightRangeG-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetFightRangeFist(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetFightRangeFist-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetFightRangeG(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetFightRangeG-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetFightActionFromTable(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetFightActionFromTable-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetCurrentFightMove(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetCurrentFightMove-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  FindNextFightAction(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-FindNextFightAction-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  ThinkNextFightAction(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-ThinkNextFightAction-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  FightAttackMelee(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-FightAttackMelee-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  FightAttackBow(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-FightAttackBow-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  EV_AttackBow(a1: oCMsgAttack): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_AttackBow-int-false-oCMsgAttack*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  FightAttackMagic(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-FightAttackMagic-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  EV_AttackMagic(a1: oCMsgAttack): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_AttackMagic-int-false-oCMsgAttack*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_CastSpell(a1: oCMsgMagic): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_CastSpell-int-false-oCMsgMagic*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  CheckRunningFightAnis(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CheckRunningFightAnis-void-false-',
      parentId: this.Uuid,
    })
  }

  GotoFightPosition(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GotoFightPosition-void-false-',
      parentId: this.Uuid,
    })
  }

  Fighting(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-Fighting-void-false-',
      parentId: this.Uuid,
    })
  }

  GetSpellItem(a1: number): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetSpellItem-oCItem*-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new oCItem(result) : null
  }

  ReadySpell(a1: number, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-ReadySpell-int-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  UnreadySpell(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-UnreadySpell-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  HasRangedWeaponAndAmmo(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-HasRangedWeaponAndAmmo-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  CanDrawWeapon(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CanDrawWeapon-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  CanDive(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CanDive-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  CanSwim(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CanSwim-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetClimbRange(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetClimbRange-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetTurnSpeed(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetTurnSpeed-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetJumpRange(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetJumpRange-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  RbtChooseChasmAction(a1: zVEC3, a2: zVEC3): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-RbtChooseChasmAction-int-false-zVEC3 const&,zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  CanGo(a1: number, a2: zVEC3): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CanGo-int-false-float,zVEC3&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  GetAngle(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetAngle-float-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetAngles(a1: zVEC3, a2: number, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetAngles-void-false-zVEC3&,float&,float&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  GetAngles2(a1: zCVob, a2: number, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetAngles-void-false-zCVob*,float&,float&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  GetDistanceToPos2(a1: zVEC3, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetDistanceToPos2-float-false-zVEC3&,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  GetFallDownHeight(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetFallDownHeight-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetRbtObstacleVob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetRbtObstacleVob-zCVob*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  GetVecNormalFromBBox(a1: zCVob, a2: zVEC3): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetVecNormalFromBBox-zVEC3-false-zCVob*,zVEC3&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new zVEC3(result) : null
  }

  SetFallDownDamage(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetFallDownDamage-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetFallDownHeight(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetFallDownHeight-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetRoute(a1: zCRoute): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetRoute-void-false-zCRoute*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CreateFallDamage(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CreateFallDamage-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  Fleeing(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-Fleeing-void-false-',
      parentId: this.Uuid,
    })
  }

  ThinkNextFleeAction(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-ThinkNextFleeAction-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  Follow(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-Follow-void-false-',
      parentId: this.Uuid,
    })
  }

  StandUp(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-StandUp-void-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  Turn(a1: zVEC3): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-Turn-float-false-zVEC3&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  Turning(a1: zVEC3): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-Turning-float-false-zVEC3&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  AI_Flee(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-AI_Flee-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  AI_Follow(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-AI_Follow-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  EV_AlignToFP(a1: oCMsgMovement): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_AlignToFP-int-false-oCMsgMovement*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_CanSeeNpc(a1: oCMsgMovement): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_CanSeeNpc-int-false-oCMsgMovement*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_GoRoute(a1: oCMsgMovement): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_GoRoute-int-false-oCMsgMovement*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_Jump(a1: oCMsgMovement): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_Jump-int-false-oCMsgMovement*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_RobustTrace(a1: oCMsgMovement): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_RobustTrace-int-false-oCMsgMovement*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_StandUp(a1: oCMsgMovement): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_StandUp-int-false-oCMsgMovement*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_Strafe(a1: oCMsgMovement): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_Strafe-int-false-oCMsgMovement*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_WhirlAround(a1: oCMsgMovement): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_WhirlAround-int-false-oCMsgMovement*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_Dodge(a1: oCMsgMovement): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_Dodge-int-false-oCMsgMovement*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_GotoPos(a1: oCMsgMovement): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_GotoPos-int-false-oCMsgMovement*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_GotoVob(a1: oCMsgMovement): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_GotoVob-int-false-oCMsgMovement*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_GotoFP(a1: oCMsgMovement): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_GotoFP-int-false-oCMsgMovement*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_SetWalkMode(a1: oCMsgMovement): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_SetWalkMode-int-false-oCMsgMovement*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_Turn(a1: oCMsgMovement): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_Turn-int-false-oCMsgMovement*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_TurnAway(a1: oCMsgMovement): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_TurnAway-int-false-oCMsgMovement*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_TurnToPos(a1: oCMsgMovement): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_TurnToPos-int-false-oCMsgMovement*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_TurnToVob(a1: oCMsgMovement): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_TurnToVob-int-false-oCMsgMovement*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  ForceRotation(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-ForceRotation-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RbtCheckForSolution(a1: zVEC3, a2: zVEC3): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-RbtCheckForSolution-int-false-zVEC3,zVEC3',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  RbtReset(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-RbtReset-void-false-',
      parentId: this.Uuid,
    })
  }

  RbtInit(a1: zVEC3, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-RbtInit-void-false-zVEC3&,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  RbtUpdate(a1: zVEC3, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-RbtUpdate-void-false-zVEC3&,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  RbtMoveToExactPosition(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-RbtMoveToExactPosition-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  RobustTrace(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-RobustTrace-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  RbtCalcTurnDirection(a1: zVEC3, a2: number, a3: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-RbtCalcTurnDirection-float-false-zVEC3 const&,float,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? Number(result) : null
  }

  RbtIsObjectObstacle(a1: zCVob, a2: zVEC3, a3: zVEC3, a4: zVEC3): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-RbtIsObjectObstacle-int-false-zCVob*,zVEC3 const&,zVEC3 const&,zVEC3&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
    })

    return result ? Number(result) : null
  }

  RbtAvoidObstacles(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-RbtAvoidObstacles-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  RbtIsDirectionValid(a1: zVEC3): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-RbtIsDirectionValid-int-false-zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  RbtInsertDirection(a1: zVEC3): oSDirectionInfo | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-RbtInsertDirection-oSDirectionInfo*-false-zVEC3',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new oSDirectionInfo(result) : null
  }

  RbtCheckLastDirection(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-RbtCheckLastDirection-int-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  RbtCheckIfTargetVisible(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-RbtCheckIfTargetVisible-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  RbtGotoFollowPosition(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-RbtGotoFollowPosition-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  CanStrafe(a1: number, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CanStrafe-int-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  CanJumpBack(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CanJumpBack-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  SetWalkStopChasm(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetWalkStopChasm-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetWalkStopChasm(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetWalkStopChasm-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsWaitingForAnswer(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsWaitingForAnswer-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetCurrentAnswer(a1: number, a2: number, a3: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetCurrentAnswer-int-false-int,int,oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? Number(result) : null
  }

  EV_Ask(a1: oCMsgConversation): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_Ask-int-false-oCMsgConversation*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_WaitForQuestion(a1: oCMsgConversation): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_WaitForQuestion-int-false-oCMsgConversation*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  CanTalk(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CanTalk-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetCanTalk(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetCanTalk-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetTalkingWith(a1: oCNpc, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetTalkingWith-void-false-oCNpc*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  GetTalkingWith(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetTalkingWith-oCNpc*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  StartTalkingWith(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-StartTalkingWith-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  StopTalkingWith(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-StopTalkingWith-void-false-',
      parentId: this.Uuid,
    })
  }

  GetTalkingWithMessage(a1: oCNpc): zCEventMessage | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetTalkingWithMessage-zCEventMessage*-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new zCEventMessage(result) : null
  }

  CanBeTalkedTo(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CanBeTalkedTo-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  StopRunningOU(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-StopRunningOU-void-false-',
      parentId: this.Uuid,
    })
  }

  AssessGivenItem(a1: oCItem, a2: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-AssessGivenItem-int-false-oCItem*,oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  GetTradeNpc(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetTradeNpc-oCNpc*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  SetTradeNpc(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetTradeNpc-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetTradeItem(): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetTradeItem-oCItem*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCItem(result) : null
  }

  OpenTradeContainer(a1: oCItem, a2: oCNpc, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-OpenTradeContainer-void-false-oCItem*,oCNpc*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  CloseTradeContainer(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CloseTradeContainer-void-false-',
      parentId: this.Uuid,
    })
  }

  OpenTradeOffer(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-OpenTradeOffer-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CheckItemReactModule(a1: oCNpc, a2: oCItem): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CheckItemReactModule-int-false-oCNpc*,oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  ExchangeTradeModules(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-ExchangeTradeModules-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetBloodTexture(): string | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetBloodTexture-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  GetActiveInfo(): TActiveInfo | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetActiveInfo-TActiveInfo-false-',
      parentId: this.Uuid,
    })

    return result ? new TActiveInfo(result) : null
  }

  GetActiveInfoWritable(): TActiveInfo | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetActiveInfoWritable-TActiveInfo*-false-',
      parentId: this.Uuid,
    })

    return result ? new TActiveInfo(result) : null
  }

  Shrink(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-Shrink-void-false-',
      parentId: this.Uuid,
    })
  }

  UnShrink(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-UnShrink-void-false-',
      parentId: this.Uuid,
    })
  }

  AvoidShrink(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-AvoidShrink-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  ApplyOverlay(a1: string): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-ApplyOverlay-int-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  RemoveOverlay(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-RemoveOverlay-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetModelScale(a1: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetModelScale-void-false-zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetFatness(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetFatness-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CleanUp(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CleanUp-void-false-',
      parentId: this.Uuid,
    })
  }

  DeleteHumanAI(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-DeleteHumanAI-void-false-',
      parentId: this.Uuid,
    })
  }

  GetGuildName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetGuildName-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  GetName(a1: number): string | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetName-zSTRING-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? String(result) : null
  }

  IsHostile(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsHostile-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  IsFriendly(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsFriendly-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  IsNeutral(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsNeutral-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  IsAngry(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsAngry-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetAttitude(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetAttitude-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetPermAttitude(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetPermAttitude-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  SetAttitude(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetAttitude-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetTmpAttitude(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetTmpAttitude-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetProtectionByIndex(a1: oEIndexDamage): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetProtectionByIndex-int-false-oEIndexDamage',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetProtectionByType(a1: oETypeDamage): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetProtectionByType-int-false-oETypeDamage',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetProtectionByMode(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetProtectionByMode-int-false-unsigned long',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetFullProtection(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetFullProtection-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetProtectionByIndex(a1: oEIndexDamage, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetProtectionByIndex-void-false-oEIndexDamage,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  GetDamageByIndex(a1: oEIndexDamage): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetDamageByIndex-int-false-oEIndexDamage',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetDamageByType(a1: oETypeDamage): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetDamageByType-int-false-oETypeDamage',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetDamageByMode(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetDamageByMode-int-false-unsigned long',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetFullDamage(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetFullDamage-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetAttribute(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetAttribute-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetHitChance(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetHitChance-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  SetHitChance(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetHitChance-void-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  ChangeAttribute(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-ChangeAttribute-void-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  SetAttribute(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetAttribute-void-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  GetOverlay(a1: string): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetOverlay-int-false-zSTRING&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  CheckModelOverlays(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CheckModelOverlays-void-false-',
      parentId: this.Uuid,
    })
  }

  SetToMad(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetToMad-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  HealFromMad(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-HealFromMad-void-false-',
      parentId: this.Uuid,
    })
  }

  SetToDrunk(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetToDrunk-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  HealFromDrunk(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-HealFromDrunk-void-false-',
      parentId: this.Uuid,
    })
  }

  CheckAngryTime(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CheckAngryTime-void-false-',
      parentId: this.Uuid,
    })
  }

  CanRecruitSC(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CanRecruitSC-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsDisguised(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsDisguised-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetGuild(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetGuild-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetGuild(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetGuild-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetTrueGuild(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetTrueGuild-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetTrueGuild(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetTrueGuild-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetCamp(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetCamp-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsGuildFriendly(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsGuildFriendly-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetGuildAttitude(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetGuildAttitude-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  CreateItems(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CreateItems-void-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  SetFlag(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetFlag-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  HasFlag2(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-HasFlag-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetTalent(a1: number, a2: number): oCNpcTalent | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetTalent-oCNpcTalent*-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCNpcTalent(result) : null
  }

  SetTalentValue(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetTalentValue-void-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  GetTalentValue(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetTalentValue-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  SetTalentSkill(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetTalentSkill-void-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  GetTalentSkill(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetTalentSkill-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  HasTalent(a1: number, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-HasTalent-int-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  CanUse(a1: oCItem): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CanUse-int-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  DisplayCannotUse(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-DisplayCannotUse-void-false-',
      parentId: this.Uuid,
    })
  }

  CheckAllCanUse(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CheckAllCanUse-void-false-',
      parentId: this.Uuid,
    })
  }

  GetSlotVob(a1: string): oCVob | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetSlotVob-oCVob*-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new oCVob(result) : null
  }

  GetSlotItem(a1: string): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetSlotItem-oCItem*-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new oCItem(result) : null
  }

  GetItem(a1: number, a2: number): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetItem-oCItem*-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCItem(result) : null
  }

  GetSlotNpc(a1: string): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetSlotNpc-oCNpc*-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new oCNpc(result) : null
  }

  AddItemEffects(a1: oCItem): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-AddItemEffects-void-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RemoveItemEffects(a1: oCItem): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-RemoveItemEffects-void-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  EquipItem(a1: oCItem): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EquipItem-void-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  UnequipItem(a1: oCItem): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-UnequipItem-void-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetCutsceneDistance(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetCutsceneDistance-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetCSEnabled(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetCSEnabled-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetFocusVob(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetFocusVob-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetFocusVob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetFocusVob-zCVob*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  ClearFocusVob(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-ClearFocusVob-void-false-',
      parentId: this.Uuid,
    })
  }

  GetFocusNpc(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetFocusNpc-oCNpc*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  GetNearestFightNpcLeft(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetNearestFightNpcLeft-oCNpc*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  GetNearestFightNpcRight(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetNearestFightNpcRight-oCNpc*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  FocusCheckBBox(a1: zCVob, a2: number, a3: number, a4: number, a5: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-FocusCheckBBox-int-false-zCVob*,int,int,float,float&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
      a5: a5.toString(),
    })

    return result ? Number(result) : null
  }

  FocusCheck(a1: zCVob, a2: number, a3: number, a4: number, a5: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-FocusCheck-int-false-zCVob*,int,int,float,float&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
      a5: a5.toString(),
    })

    return result ? Number(result) : null
  }

  ToggleFocusVob(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-ToggleFocusVob-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CollectFocusVob(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CollectFocusVob-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetNearestValidVob(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetNearestValidVob-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetNearestVob(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetNearestVob-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetAnictrl(): oCAniCtrl_Human | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetAnictrl-oCAniCtrl_Human*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCAniCtrl_Human(result) : null
  }

  ResetToHumanAI(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-ResetToHumanAI-void-false-',
      parentId: this.Uuid,
    })
  }

  SetEnemy(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetEnemy-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetNextEnemy(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetNextEnemy-oCNpc*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  IsConditionValid(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsConditionValid-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsInFightFocus(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsInFightFocus-int-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  HasEquippedStolenItem(a1: oCNpc): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-HasEquippedStolenItem-oCItem*-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new oCItem(result) : null
  }

  Burn(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-Burn-void-false-int,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  StopBurn(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-StopBurn-void-false-',
      parentId: this.Uuid,
    })
  }

  Interrupt(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-Interrupt-void-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  GetWeaponDamage(a1: oCItem): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetWeaponDamage-int-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetBluntDamage(a1: oCItem): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetBluntDamage-int-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  DropUnconscious(a1: number, a2: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-DropUnconscious-void-false-float,oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  CheckUnconscious(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CheckUnconscious-void-false-',
      parentId: this.Uuid,
    })
  }

  GetInterruptPrefix(): string | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetInterruptPrefix-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  CompleteHeal(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CompleteHeal-void-false-',
      parentId: this.Uuid,
    })
  }

  IsDead(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsDead-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsUnconscious(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsUnconscious-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  StartFadeAway(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-StartFadeAway-void-false-',
      parentId: this.Uuid,
    })
  }

  IsFadingAway(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsFadingAway-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  FadeAway(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-FadeAway-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  BeamTo(a1: string): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-BeamTo-int-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  DropInventory(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-DropInventory-void-false-',
      parentId: this.Uuid,
    })
  }

  HasInHand(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-HasInHand-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  HasInHand2(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-HasInHand-int-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  DropAllInHand(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-DropAllInHand-void-false-',
      parentId: this.Uuid,
    })
  }

  GetWeapon(): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetWeapon-oCItem*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCItem(result) : null
  }

  GetEquippedMeleeWeapon(): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetEquippedMeleeWeapon-oCItem*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCItem(result) : null
  }

  GetEquippedRangedWeapon(): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetEquippedRangedWeapon-oCItem*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCItem(result) : null
  }

  GetEquippedArmor(): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetEquippedArmor-oCItem*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCItem(result) : null
  }

  IsSameHeight(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsSameHeight-int-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  TurnToEnemy(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-TurnToEnemy-void-false-',
      parentId: this.Uuid,
    })
  }

  ShieldEquipped(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-ShieldEquipped-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetWeaponDamage2(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetWeaponDamage-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetMovLock(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetMovLock-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  IsMovLock(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsMovLock-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetHead(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetHead-void-false-',
      parentId: this.Uuid,
    })
  }

  SetAdditionalVisuals(
    a1: string,
    a2: number,
    a3: number,
    a4: string,
    a5: number,
    a6: number,
    a7: number,
  ): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetAdditionalVisuals-void-false-zSTRING&,int,int,zSTRING&,int,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
      a5: a5.toString(),
      a6: a6.toString(),
      a7: a7.toString(),
    })
  }

  InitModel(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-InitModel-void-false-',
      parentId: this.Uuid,
    })
  }

  GetModel(): zCModel | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetModel-zCModel*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCModel(result) : null
  }

  GetVisualBody(): string | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetVisualBody-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  GetVisualHead(): string | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetVisualHead-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  StartFaceAni(a1: string, a2: number, a3: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-StartFaceAni-int-false-zSTRING const&,float,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? Number(result) : null
  }

  StartStdFaceAni(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-StartStdFaceAni-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  StopFaceAni(a1: string): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-StopFaceAni-int-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetWeaponMode(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetWeaponMode-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetWeaponMode2_novt(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetWeaponMode2_novt-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetNextWeaponMode(a1: number, a2: number, a3: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetNextWeaponMode-int-false-int,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? Number(result) : null
  }

  Equip(a1: oCItem): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-Equip-void-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  EquipWeapon(a1: oCItem): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EquipWeapon-void-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  EquipFarWeapon(a1: oCItem): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EquipFarWeapon-void-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  EquipArmor(a1: oCItem): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EquipArmor-void-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetToFightMode(a1: oCItem, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetToFightMode-void-false-oCItem*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  SetToFistMode(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetToFistMode-void-false-',
      parentId: this.Uuid,
    })
  }

  Activate(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-Activate-void-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  GetRightHand(): oCVob | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetRightHand-oCVob*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCVob(result) : null
  }

  GetLeftHand(): oCVob | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetLeftHand-oCVob*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCVob(result) : null
  }

  DropVob(a1: oCVob, a2: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-DropVob-void-false-oCVob*,zVEC3&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  SetCarryVob(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetCarryVob-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetCarryVob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetCarryVob-zCVob*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  SetLeftHand(a1: oCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetLeftHand-void-false-oCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetRightHand(a1: oCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetRightHand-void-false-oCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  HasTorch(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-HasTorch-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  ExchangeTorch(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-ExchangeTorch-void-false-',
      parentId: this.Uuid,
    })
  }

  SetTorchAni(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetTorchAni-void-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  CheckSetTorchAni(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CheckSetTorchAni-void-false-',
      parentId: this.Uuid,
    })
  }

  CheckPutbackTorch(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CheckPutbackTorch-void-false-',
      parentId: this.Uuid,
    })
  }

  UseItem(a1: oCItem): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-UseItem-int-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetThrowSpeed(a1: number, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetThrowSpeed-float-false-float,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  HasArrowInHand(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-HasArrowInHand-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  HasArrowInInv(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-HasArrowInInv-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  HasBoltInInv(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-HasBoltInInv-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  HasBoltInHand(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-HasBoltInHand-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsMunitionAvailable(a1: oCItem): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsMunitionAvailable-int-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetTalentInfo(a1: number, a2: string, a3: string, a4: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetTalentInfo-void-false-int,zSTRING&,zSTRING&,zSTRING&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
    })
  }

  GetSpellInfo(a1: number, a2: string, a3: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetSpellInfo-void-false-int,zSTRING&,zSTRING&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  GetAttribInfo(a1: number, a2: string, a3: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetAttribInfo-void-false-int,zSTRING&,zSTRING&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  CreateSpell(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CreateSpell-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  LearnSpell(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-LearnSpell-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  HasSpell(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-HasSpell-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  DestroySpell(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-DestroySpell-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetActiveSpellInfo(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetActiveSpellInfo-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetActiveSpellNr(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetActiveSpellNr-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetActiveSpellCategory(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetActiveSpellCategory-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetActiveSpellLevel(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetActiveSpellLevel-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetActiveSpellIsScroll(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetActiveSpellIsScroll-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  InsertActiveSpell(a1: oCSpell): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-InsertActiveSpell-void-false-oCSpell*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RemoveActiveSpell(a1: oCSpell): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-RemoveActiveSpell-void-false-oCSpell*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RemoveActiveSpell2(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-RemoveActiveSpell-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  DoActiveSpells(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-DoActiveSpells-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  KillActiveSpells(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-KillActiveSpells-void-false-',
      parentId: this.Uuid,
    })
  }

  IsSpellActive(a1: number): oCSpell | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsSpellActive-oCSpell*-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new oCSpell(result) : null
  }

  HasMagic(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-HasMagic-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetNumberOfSpells(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetNumberOfSpells-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  CopyTransformSpellInvariantValuesTo(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CopyTransformSpellInvariantValuesTo-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  OpenScreen_Help(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-OpenScreen_Help-void-false-',
      parentId: this.Uuid,
    })
  }

  OpenScreen_Log(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-OpenScreen_Log-void-false-',
      parentId: this.Uuid,
    })
  }

  OpenScreen_Map(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-OpenScreen_Map-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  OpenScreen_Status(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-OpenScreen_Status-void-false-',
      parentId: this.Uuid,
    })
  }

  IsVoiceActive(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsVoiceActive-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  StopAllVoices(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-StopAllVoices-void-false-',
      parentId: this.Uuid,
    })
  }

  UpdateNextVoice(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-UpdateNextVoice-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  DoSpellBook(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-DoSpellBook-void-false-',
      parentId: this.Uuid,
    })
  }

  OpenSpellBook(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-OpenSpellBook-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CloseSpellBook(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CloseSpellBook-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetSpellBook(): oCMag_Book | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetSpellBook-oCMag_Book*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCMag_Book(result) : null
  }

  HasSenseHear(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-HasSenseHear-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  HasSenseSee(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-HasSenseSee-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  HasSenseSmell(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-HasSenseSmell-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetSenses(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetSenses-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  PrintStateCallDebug(a1: string, a2: number, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-PrintStateCallDebug-void-false-zSTRING const&,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  GetOldScriptState(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetOldScriptState-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetAIState(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetAIState-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetAIStateTime(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetAIStateTime-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsAIState(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsAIState-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  ShowState(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-ShowState-void-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  IsNear(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsNear-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  DetectItem(a1: number, a2: number): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-DetectItem-oCItem*-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCItem(result) : null
  }

  DetectPlayer(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-DetectPlayer-oCNpc*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  FindMobInter(a1: string): oCMobInter | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-FindMobInter-oCMobInter*-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new oCMobInter(result) : null
  }

  FindSpot(a1: string, a2: number, a3: number): zCVobSpot | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-FindSpot-zCVobSpot*-false-zSTRING const&,int,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? new zCVobSpot(result) : null
  }

  ForceVobDetection(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-ForceVobDetection-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  HasVobDetected(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-HasVobDetected-int-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  CheckForOwner(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CheckForOwner-int-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  AI_ForceDetection(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-AI_ForceDetection-void-false-',
      parentId: this.Uuid,
    })
  }

  CanSense(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CanSense-int-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  DetectWitnesses(a1: oCNpc, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-DetectWitnesses-void-false-oCNpc*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  FindNpc(a1: number, a2: number, a3: number, a4: number): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-FindNpc-oCNpc*-false-int,int,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
    })

    return result ? new oCNpc(result) : null
  }

  FindNpcEx(a1: number, a2: number, a3: number, a4: number, a5: number, a6: number): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-FindNpcEx-oCNpc*-false-int,int,int,int,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
      a5: a5.toString(),
      a6: a6.toString(),
    })

    return result ? new oCNpc(result) : null
  }

  FindNpcExAtt(
    a1: number,
    a2: number,
    a3: number,
    a4: number,
    a5: number,
    a6: number,
    a7: number,
  ): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-FindNpcExAtt-oCNpc*-false-int,int,int,int,int,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
      a5: a5.toString(),
      a6: a6.toString(),
      a7: a7.toString(),
    })

    return result ? new oCNpc(result) : null
  }

  GetComrades(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetComrades-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  AreWeStronger(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-AreWeStronger-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  IsEnemyBehindFriend(a1: oCNpc): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsEnemyBehindFriend-oCNpc*-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new oCNpc(result) : null
  }

  InMobInteraction(a1: string, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-InMobInteraction-int-false-zSTRING const&,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  FreeLineOfSight(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-FreeLineOfSight-int-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  FreeLineOfSight2(a1: zVEC3, a2: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-FreeLineOfSight-int-false-zVEC3&,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  FreeLineOfSight_WP(a1: zVEC3, a2: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-FreeLineOfSight_WP-int-false-zVEC3&,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  CanSee(a1: zCVob, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CanSee-int-false-zCVob*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  SetSwimDiveTime(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetSwimDiveTime-void-false-float,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  GetSwimDiveTime(a1: number, a2: number, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetSwimDiveTime-void-false-float&,float&,float&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  Regenerate(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-Regenerate-void-false-',
      parentId: this.Uuid,
    })
  }

  RefreshNpc(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-RefreshNpc-void-false-',
      parentId: this.Uuid,
    })
  }

  SetKnowsPlayer(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetKnowsPlayer-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  KnowsPlayer(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-KnowsPlayer-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  KnowsPlayer2(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-KnowsPlayer-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  HasMunitionInHand(a1: oCItem): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-HasMunitionInHand-int-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  IsInFightMode_S(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsInFightMode_S-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  IsAiming_S(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsAiming_S-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  ClearEM(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-ClearEM-void-false-',
      parentId: this.Uuid,
    })
  }

  PreSaveGameProcessing(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-PreSaveGameProcessing-void-false-',
      parentId: this.Uuid,
    })
  }

  PostSaveGameProcessing(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-PostSaveGameProcessing-void-false-',
      parentId: this.Uuid,
    })
  }

  IdentifyMushroom(a1: oCItem): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IdentifyMushroom-oCItem*-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new oCItem(result) : null
  }

  IdentifyAllMushrooms(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IdentifyAllMushrooms-void-false-',
      parentId: this.Uuid,
    })
  }

  HasMissionItem(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-HasMissionItem-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsInInv(a1: oCItem, a2: number): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsInInv-oCItem*-false-oCItem*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCItem(result) : null
  }

  GetFromInv(a1: number, a2: number): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetFromInv-oCItem*-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCItem(result) : null
  }

  IsInInv2(a1: number, a2: number): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsInInv-oCItem*-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCItem(result) : null
  }

  IsInInv3(a1: string, a2: number): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsInInv-oCItem*-false-zSTRING const&,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCItem(result) : null
  }

  CanCarry(a1: oCItem): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CanCarry-int-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  PutInInv(a1: oCItem): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-PutInInv-oCItem*-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new oCItem(result) : null
  }

  PutInInv2(a1: number, a2: number): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-PutInInv-oCItem*-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCItem(result) : null
  }

  PutInInv3(a1: string, a2: number): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-PutInInv-oCItem*-false-zSTRING const&,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCItem(result) : null
  }

  RemoveFromInv(a1: oCItem, a2: number): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-RemoveFromInv-oCItem*-false-oCItem*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCItem(result) : null
  }

  RemoveFromInv2(a1: number, a2: number): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-RemoveFromInv-oCItem*-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCItem(result) : null
  }

  RemoveFromInv3(a1: string, a2: number): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-RemoveFromInv-oCItem*-false-zSTRING const&,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCItem(result) : null
  }

  IsSlotFree(a1: string): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsSlotFree-int-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  IsSlotFree2(a1: TNpcSlot): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsSlotFree-int-false-TNpcSlot*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  SetToSlotPosition(a1: zCVob, a2: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetToSlotPosition-void-false-zCVob*,zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  CreateInvSlot(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CreateInvSlot-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  DeleteInvSlot(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-DeleteInvSlot-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetInvSlot(a1: string): TNpcSlot | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetInvSlot-TNpcSlot*-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new TNpcSlot(result) : null
  }

  GetInvSlot2(a1: zCVob): TNpcSlot | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetInvSlot-TNpcSlot*-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new TNpcSlot(result) : null
  }

  IsInvSlotAvailable(a1: string): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsInvSlotAvailable-int-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  PutInSlot(a1: string, a2: oCVob, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-PutInSlot-void-false-zSTRING const&,oCVob*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  PutInSlot2(a1: TNpcSlot, a2: oCVob, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-PutInSlot-void-false-TNpcSlot*,oCVob*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  RemoveFromAllSlots(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-RemoveFromAllSlots-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RemoveFromSlot(a1: string, a2: number, a3: number): oCVob | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-RemoveFromSlot-oCVob*-false-zSTRING const&,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? new oCVob(result) : null
  }

  RemoveFromSlot2(a1: TNpcSlot, a2: number, a3: number): oCVob | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-RemoveFromSlot-oCVob*-false-TNpcSlot*,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? new oCVob(result) : null
  }

  DropFromSlot(a1: string): oCVob | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-DropFromSlot-oCVob*-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new oCVob(result) : null
  }

  DropFromSlot2(a1: TNpcSlot): oCVob | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-DropFromSlot-oCVob*-false-TNpcSlot*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new oCVob(result) : null
  }

  UpdateSlots(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-UpdateSlots-void-false-',
      parentId: this.Uuid,
    })
  }

  SetInteractMob(a1: oCMobInter): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetInteractMob-void-false-oCMobInter*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetInteractMob(): oCMobInter | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetInteractMob-oCMobInter*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCMobInter(result) : null
  }

  SetInteractItem(a1: oCItem): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetInteractItem-void-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  EV_DrawWeapon(a1: oCMsgWeapon): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_DrawWeapon-int-false-oCMsgWeapon*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_DrawWeapon1(a1: oCMsgWeapon): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_DrawWeapon1-int-false-oCMsgWeapon*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_DrawWeapon2(a1: oCMsgWeapon): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_DrawWeapon2-int-false-oCMsgWeapon*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_RemoveWeapon(a1: oCMsgWeapon): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_RemoveWeapon-int-false-oCMsgWeapon*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_RemoveWeapon1(a1: oCMsgWeapon): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_RemoveWeapon1-int-false-oCMsgWeapon*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_RemoveWeapon2(a1: oCMsgWeapon): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_RemoveWeapon2-int-false-oCMsgWeapon*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_ChooseWeapon(a1: oCMsgWeapon): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_ChooseWeapon-int-false-oCMsgWeapon*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_ForceRemoveWeapon(a1: oCMsgWeapon): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_ForceRemoveWeapon-int-false-oCMsgWeapon*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_EquipBestWeapon(a1: oCMsgWeapon): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_EquipBestWeapon-int-false-oCMsgWeapon*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EquipBestWeapon(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EquipBestWeapon-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  EV_EquipBestArmor(a1: oCMsgWeapon): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_EquipBestArmor-int-false-oCMsgWeapon*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EquipBestArmor(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EquipBestArmor-void-false-',
      parentId: this.Uuid,
    })
  }

  EV_UnequipWeapons(a1: oCMsgWeapon): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_UnequipWeapons-int-false-oCMsgWeapon*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_UnequipArmor(a1: oCMsgWeapon): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_UnequipArmor-int-false-oCMsgWeapon*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_EquipArmor(a1: oCMsgWeapon): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_EquipArmor-int-false-oCMsgWeapon*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_AttackForward(a1: oCMsgAttack): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_AttackForward-int-false-oCMsgAttack*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_AttackLeft(a1: oCMsgAttack): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_AttackLeft-int-false-oCMsgAttack*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_AttackRight(a1: oCMsgAttack): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_AttackRight-int-false-oCMsgAttack*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_AttackRun(a1: oCMsgAttack): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_AttackRun-int-false-oCMsgAttack*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_AttackFinish(a1: oCMsgAttack): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_AttackFinish-int-false-oCMsgAttack*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_Parade(a1: oCMsgAttack): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_Parade-int-false-oCMsgAttack*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  TransitionAim(a1: number, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-TransitionAim-int-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  InterpolateAim(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-InterpolateAim-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  FinalizeAim(a1: number, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-FinalizeAim-int-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  EV_StopAim(a1: oCMsgAttack): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_StopAim-int-false-oCMsgAttack*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_AimAt(a1: oCMsgAttack): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_AimAt-int-false-oCMsgAttack*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_ShootAt(a1: oCMsgAttack): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_ShootAt-int-false-oCMsgAttack*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_Defend(a1: oCMsgAttack): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_Defend-int-false-oCMsgAttack*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_Drink(a1: oCMsgUseItem): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_Drink-int-false-oCMsgUseItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_TakeVob(a1: oCMsgManipulate): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_TakeVob-int-false-oCMsgManipulate*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_DropVob(a1: oCMsgManipulate): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_DropVob-int-false-oCMsgManipulate*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_ThrowVob(a1: oCMsgManipulate): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_ThrowVob-int-false-oCMsgManipulate*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_Exchange(a1: oCMsgManipulate): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_Exchange-int-false-oCMsgManipulate*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_DropMob(a1: oCMsgManipulate): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_DropMob-int-false-oCMsgManipulate*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_TakeMob(a1: oCMsgManipulate): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_TakeMob-int-false-oCMsgManipulate*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_UseMob(a1: oCMsgManipulate): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_UseMob-int-false-oCMsgManipulate*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_UseMobWithItem(a1: oCMsgManipulate): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_UseMobWithItem-int-false-oCMsgManipulate*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_InsertInteractItem(a1: oCMsgManipulate): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_InsertInteractItem-int-false-oCMsgManipulate*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_ExchangeInteractItem(a1: oCMsgManipulate): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_ExchangeInteractItem-int-false-oCMsgManipulate*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_RemoveInteractItem(a1: oCMsgManipulate): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_RemoveInteractItem-int-false-oCMsgManipulate*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_CreateInteractItem(a1: oCMsgManipulate): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_CreateInteractItem-int-false-oCMsgManipulate*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_DestroyInteractItem(a1: oCMsgManipulate): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_DestroyInteractItem-int-false-oCMsgManipulate*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  InteractItemGetAni(a1: string, a2: number, a3: number): zCModelAni | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-InteractItemGetAni-zCModelAni*-false-zSTRING const&,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? new zCModelAni(result) : null
  }

  EV_PlaceInteractItem(a1: oCMsgManipulate): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_PlaceInteractItem-int-false-oCMsgManipulate*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetInteractItemMaxState(a1: string): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetInteractItemMaxState-int-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  InteractItemIsInState(a1: string, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-InteractItemIsInState-int-false-zSTRING const&,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  EV_EquipItem(a1: oCMsgManipulate): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_EquipItem-int-false-oCMsgManipulate*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_UseItem(a1: oCMsgManipulate): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_UseItem-int-false-oCMsgManipulate*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_UseItemToState(a1: oCMsgManipulate): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_UseItemToState-int-false-oCMsgManipulate*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  CallScript(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CallScript-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CallScript2(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CallScript-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  EV_CallScript(a1: oCMsgManipulate): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_CallScript-int-false-oCMsgManipulate*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_Unconscious(a1: oCMsgState): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_Unconscious-int-false-oCMsgState*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_DoState(a1: oCMsgState): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_DoState-int-false-oCMsgState*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_Wait(a1: oCMsgState): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_Wait-int-false-oCMsgState*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  ApplyTimedOverlayMds(a1: string, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-ApplyTimedOverlayMds-int-false-zSTRING const&,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  EV_OutputSVM_Overlay(a1: oCMsgConversation): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_OutputSVM_Overlay-int-false-oCMsgConversation*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_OutputSVM(a1: oCMsgConversation): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_OutputSVM-int-false-oCMsgConversation*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_Output(a1: oCMsgConversation): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_Output-int-false-oCMsgConversation*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_PlayAni(a1: oCMsgConversation): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_PlayAni-int-false-oCMsgConversation*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_PlayAniFace(a1: oCMsgConversation): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_PlayAniFace-int-false-oCMsgConversation*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  StartDialogAni(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-StartDialogAni-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  EV_PlayAniSound(a1: oCMsgConversation): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_PlayAniSound-int-false-oCMsgConversation*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  ActivateDialogCam(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-ActivateDialogCam-int-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  DeactivateDialogCam(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-DeactivateDialogCam-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  EV_PlaySound(a1: oCMsgConversation): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_PlaySound-int-false-oCMsgConversation*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_SndPlay(a1: oCMsgConversation): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_SndPlay-int-false-oCMsgConversation*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_PrintScreen(a1: oCMsgConversation): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_PrintScreen-int-false-oCMsgConversation*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  AddEffect(a1: string, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-AddEffect-void-false-zSTRING const&,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  RemoveEffect(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-RemoveEffect-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  EV_StartFX(a1: oCMsgConversation): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_StartFX-int-false-oCMsgConversation*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_StopFX(a1: oCMsgConversation): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_StopFX-int-false-oCMsgConversation*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_LookAt(a1: oCMsgConversation): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_LookAt-int-false-oCMsgConversation*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_StopLookAt(a1: oCMsgConversation): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_StopLookAt-int-false-oCMsgConversation*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_PointAt(a1: oCMsgConversation): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_PointAt-int-false-oCMsgConversation*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_StopPointAt(a1: oCMsgConversation): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_StopPointAt-int-false-oCMsgConversation*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_QuickLook(a1: oCMsgConversation): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_QuickLook-int-false-oCMsgConversation*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_Cutscene(a1: oCMsgConversation): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_Cutscene-int-false-oCMsgConversation*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_WaitTillEnd(a1: oCMsgConversation): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_WaitTillEnd-int-false-oCMsgConversation*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_StopProcessInfos(a1: oCMsgConversation): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_StopProcessInfos-int-false-oCMsgConversation*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EV_ProcessInfos(a1: oCMsgConversation): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EV_ProcessInfos-int-false-oCMsgConversation*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  AssessPlayer_S(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-AssessPlayer_S-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  AssessEnemy_S(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-AssessEnemy_S-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  AssessFighter_S(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-AssessFighter_S-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  AssessBody_S(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-AssessBody_S-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  AssessItem_S(a1: oCItem): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-AssessItem_S-int-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  SetNpcsToState(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetNpcsToState-void-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  StopCutscenes(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-StopCutscenes-void-false-',
      parentId: this.Uuid,
    })
  }

  CreatePassivePerception(a1: number, a2: zCVob, a3: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CreatePassivePerception-void-false-int,zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  AssessMurder_S(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-AssessMurder_S-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  AssessThreat_S(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-AssessThreat_S-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  AssessDefeat_S(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-AssessDefeat_S-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  AssessDamage_S(a1: oCNpc, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-AssessDamage_S-int-false-oCNpc*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  AssessOthersDamage_S(a1: oCNpc, a2: oCNpc, a3: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-AssessOthersDamage_S-int-false-oCNpc*,oCNpc*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? Number(result) : null
  }

  AssessRemoveWeapon_S(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-AssessRemoveWeapon_S-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  ObserveIntruder_S(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-ObserveIntruder_S-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  AssessWarn_S(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-AssessWarn_S-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  CatchThief_S(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CatchThief_S-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  AssessTheft_S(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-AssessTheft_S-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  AssessCall_S(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-AssessCall_S-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  AssessTalk_S(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-AssessTalk_S-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  AssessGivenItem_S(a1: oCNpc, a2: oCItem): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-AssessGivenItem_S-int-false-oCNpc*,oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  AssessMagic_S(a1: oCNpc, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-AssessMagic_S-int-false-oCNpc*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  AssessStopMagic_S(a1: oCNpc, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-AssessStopMagic_S-int-false-oCNpc*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  AssessCaster_S(a1: oCSpell): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-AssessCaster_S-int-false-oCSpell*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  AssessFakeGuild_S(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-AssessFakeGuild_S-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  MoveMob_S(a1: oCMOB): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-MoveMob_S-int-false-oCMOB*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  AssessUseMob_S(a1: oCMobInter): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-AssessUseMob_S-int-false-oCMobInter*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  MoveNpc_S(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-MoveNpc_S-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  AssessEnterRoom_S(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-AssessEnterRoom_S-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetDistToSound(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetDistToSound-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetSoundSource(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetSoundSource-zCVob*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  SetSoundSource(a1: number, a2: zCVob, a3: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetSoundSource-void-false-int,zCVob*,zVEC3&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  ClearPerceptionLists(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-ClearPerceptionLists-void-false-',
      parentId: this.Uuid,
    })
  }

  ClearVobList(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-ClearVobList-void-false-',
      parentId: this.Uuid,
    })
  }

  InsertInVobList(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-InsertInVobList-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RemoveFromVobList(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-RemoveFromVobList-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CreateVobList(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CreateVobList-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetPerceptionTime(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetPerceptionTime-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  PerceiveAll(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-PerceiveAll-void-false-',
      parentId: this.Uuid,
    })
  }

  PerceptionCheck(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-PerceptionCheck-void-false-',
      parentId: this.Uuid,
    })
  }

  PercFilterNpc(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-PercFilterNpc-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  PercFilterItem(a1: oCItem): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-PercFilterItem-int-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  ClearPerception(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-ClearPerception-void-false-',
      parentId: this.Uuid,
    })
  }

  EnablePerception(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EnablePerception-void-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  DisablePerception(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-DisablePerception-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  HasPerception(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-HasPerception-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetPerceptionFunc(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetPerceptionFunc-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  SetBodyState(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetBodyState-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetBodyState(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetBodyState-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetFullBodyState(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetFullBodyState-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetBodyStateName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetBodyStateName-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  HasBodyStateModifier(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-HasBodyStateModifier-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  SetBodyStateModifier(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetBodyStateModifier-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  ClrBodyStateModifier(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-ClrBodyStateModifier-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetBodyStateModifierNames(): string | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetBodyStateModifierNames-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  ModifyBodyState(a1: number, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-ModifyBodyState-int-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  IsBodyStateInterruptable(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsBodyStateInterruptable-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  HasBodyStateFreeHands(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-HasBodyStateFreeHands-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsInGlobalCutscene(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsInGlobalCutscene-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  MakeSpellBook(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-MakeSpellBook-void-false-',
      parentId: this.Uuid,
    })
  }

  DestroySpellBook(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-DestroySpellBook-void-false-',
      parentId: this.Uuid,
    })
  }

  GetDamageMultiplier(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetDamageMultiplier-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetDamageMultiplier(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetDamageMultiplier-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetShowNews(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetShowNews-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  UseStandAI(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-UseStandAI-void-false-',
      parentId: this.Uuid,
    })
  }

  SetItemEffectControledByModel(
    a1: oCItem,
    a2: zCModelNodeInst,
    a3: number,
    a4: number,
    a5: number,
  ): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetItemEffectControledByModel-void-false-oCItem*,zCModelNodeInst*,int,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
      a5: a5.toString(),
    })
  }

  IsVictimAwareOfTheft(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsVictimAwareOfTheft-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  CheckSpecialSituations(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CheckSpecialSituations-void-false-',
      parentId: this.Uuid,
    })
  }

  StopTheft(a1: oCNpc, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-StopTheft-void-false-oCNpc*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  OpenInventory(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-OpenInventory-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CloseInventory(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CloseInventory-void-false-',
      parentId: this.Uuid,
    })
  }

  OpenSteal(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-OpenSteal-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  CloseSteal(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CloseSteal-void-false-',
      parentId: this.Uuid,
    })
  }

  OpenDeadNpc(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-OpenDeadNpc-void-false-',
      parentId: this.Uuid,
    })
  }

  CloseDeadNpc(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-CloseDeadNpc-void-false-',
      parentId: this.Uuid,
    })
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override OnTouch(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-OnTouch-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override OnMessage(a1: zCEventMessage, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-OnMessage-void-false-zCEventMessage*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override GetCharacterClass(): zTVobCharClass | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetCharacterClass-zTVobCharClass-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override GetCSStateFlags(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetCSStateFlags-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override ThisVobAddedToWorld(a1: zCWorld): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-ThisVobAddedToWorld-void-false-zCWorld*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override ThisVobRemovedFromWorld(a1: zCWorld): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-ThisVobRemovedFromWorld-void-false-zCWorld*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override ShowDebugInfo(a1: zCCamera): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-ShowDebugInfo-void-false-zCCamera*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override GetInstance(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetInstance-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override GetInstanceName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetInstanceName-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  SetWeaponMode(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetWeaponMode-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetWeaponMode2(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetWeaponMode2-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  DoDie(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-DoDie-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  DoInsertMunition(a1: string): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-DoInsertMunition-oCItem*-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new oCItem(result) : null
  }

  DoRemoveMunition(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-DoRemoveMunition-void-false-',
      parentId: this.Uuid,
    })
  }

  DoSetToFightMode(a1: oCItem): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-DoSetToFightMode-int-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  DoShootArrow(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-DoShootArrow-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  OnDamageNpc(a1: oSDamageDescriptor): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-OnDamage-void-false-oSDamageDescriptor&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  OnDamageNpc2(a1: zCVob, a2: zCVob, a3: number, a4: number, a5: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-OnDamage-void-false-zCVob*,zCVob*,float,int,zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
      a5: a5.toString(),
    })
  }

  ResetPos(a1: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-ResetPos-void-false-zVEC3&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  EmergencyResetPos(a1: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-EmergencyResetPos-void-false-zVEC3&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  InitByScript(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-InitByScript-void-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  Disable(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-Disable-void-false-',
      parentId: this.Uuid,
    })
  }

  Enable(a1: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-Enable-void-false-zVEC3&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  InitHumanAI(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-InitHumanAI-void-false-',
      parentId: this.Uuid,
    })
  }

  IsMoreImportant(a1: zCVob, a2: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsMoreImportant-int-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  DoDoAniEvents(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-DoDoAniEvents-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  DoModelSwapMesh(a1: string, a2: string): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-DoModelSwapMesh-int-false-zSTRING const&,zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  DoTakeVob(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-DoTakeVob-int-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  DoDropVob(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-DoDropVob-int-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  DoRemoveFromInventory(a1: oCItem): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-DoRemoveFromInventory-int-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  DoPutInInventory(a1: oCItem): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-DoPutInInventory-int-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  DoThrowVob(a1: zCVob, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-DoThrowVob-int-false-zCVob*,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  DoExchangeTorch(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-DoExchangeTorch-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsAPlayer(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsAPlayer-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsSelfPlayer(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsSelfPlayer-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetAsPlayer(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-SetAsPlayer-void-false-',
      parentId: this.Uuid,
    })
  }

  IsMonster(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsMonster-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsHuman(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsHuman-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsGoblin(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsGoblin-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsOrc(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsOrc-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsSkeleton(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsSkeleton-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetPlayerNumber(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-GetPlayerNumber-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsAniMessageRunning(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-IsAniMessageRunning-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  ProcessNpc(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-ProcessNpc-void-false-',
      parentId: this.Uuid,
    })
  }

  AllowDiscardingOfSubtree(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc-AllowDiscardingOfSubtree-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }
}
