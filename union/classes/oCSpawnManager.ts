import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zVEC3 } from './index.js'
import { oSSpawnNode } from './index.js'
import { oCNpc } from './index.js'

export class oCSpawnManager extends BaseUnionObject {
  spawningEnabled(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpawnManager-spawningEnabled-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_spawningEnabled(a1: number): null {
    SendCommand({
      id: 'SET_oCSpawnManager-spawningEnabled-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  camPos(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oCSpawnManager-camPos-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_camPos(a1: zVEC3): null {
    SendCommand({
      id: 'SET_oCSpawnManager-camPos-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  insertTime(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpawnManager-insertTime-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_insertTime(a1: number): null {
    SendCommand({
      id: 'SET_oCSpawnManager-insertTime-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  spawnFlags(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpawnManager-spawnFlags-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_spawnFlags(a1: number): null {
    SendCommand({
      id: 'SET_oCSpawnManager-spawnFlags-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCSpawnManager_OnInit(): oCSpawnManager | null {
    const result: string | null = SendCommand({
      id: 'oCSpawnManager-oCSpawnManager_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCSpawnManager(result) : null
  }

  ClearList(): void | null {
    const result: string | null = SendCommand({
      id: 'oCSpawnManager-ClearList-void-false-',
      parentId: this.Uuid,
    })
  }

  InsertInList(a1: oSSpawnNode): void | null {
    const result: string | null = SendCommand({
      id: 'oCSpawnManager-InsertInList-void-false-oSSpawnNode*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RemoveFromList(a1: oSSpawnNode): void | null {
    const result: string | null = SendCommand({
      id: 'oCSpawnManager-RemoveFromList-void-false-oSSpawnNode*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  IsSpawningEnabled(): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpawnManager-IsSpawningEnabled-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetSpawningEnabled(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCSpawnManager-SetSpawningEnabled-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SpawnImmediately(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCSpawnManager-SpawnImmediately-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CheckInsertNpcs(): void | null {
    const result: string | null = SendCommand({
      id: 'oCSpawnManager-CheckInsertNpcs-void-false-',
      parentId: this.Uuid,
    })
  }

  ShowDebugInfo(): void | null {
    const result: string | null = SendCommand({
      id: 'oCSpawnManager-ShowDebugInfo-void-false-',
      parentId: this.Uuid,
    })
  }

  CheckInsertNpc(): void | null {
    const result: string | null = SendCommand({
      id: 'oCSpawnManager-CheckInsertNpc-void-false-',
      parentId: this.Uuid,
    })
  }

  InitCameraPos(): void | null {
    const result: string | null = SendCommand({
      id: 'oCSpawnManager-InitCameraPos-void-false-',
      parentId: this.Uuid,
    })
  }

  InsertNpc(a1: oCNpc, a2: zVEC3): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpawnManager-InsertNpc-int-false-oCNpc*,zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  SummonNpc(a1: number, a2: zVEC3, a3: number): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCSpawnManager-SummonNpc-oCNpc*-false-int,zVEC3 const&,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? new oCNpc(result) : null
  }

  SpawnNpc(a1: number, a2: string, a3: number): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCSpawnManager-SpawnNpc-oCNpc*-false-int,zSTRING const&,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? new oCNpc(result) : null
  }

  SpawnNpc2(a1: oCNpc, a2: string, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCSpawnManager-SpawnNpc-void-false-oCNpc*,zSTRING const&,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  SpawnNpc3(a1: oCNpc, a2: zVEC3, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCSpawnManager-SpawnNpc-void-false-oCNpc*,zVEC3 const&,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  CheckForInvalidDialogCamPos(a1: number, a2: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCSpawnManager-CheckForInvalidDialogCamPos-void-false-float,oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  UseDeadNpcRoutinePos(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpawnManager-UseDeadNpcRoutinePos-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  CanDeadNpcBeRemoved(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpawnManager-CanDeadNpcBeRemoved-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  CheckRemoveNpc(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCSpawnManager-CheckRemoveNpc-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  DeleteNpc(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCSpawnManager-DeleteNpc-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  DeleteAllSummoned(): void | null {
    const result: string | null = SendCommand({
      id: 'oCSpawnManager-DeleteAllSummoned-void-false-',
      parentId: this.Uuid,
    })
  }
}
