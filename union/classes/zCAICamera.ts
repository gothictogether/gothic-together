import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCVob } from './index.js'
import { zVEC3 } from './index.js'
import { zCClassDef } from './index.js'
import { zCAIBase } from './index.js'

export class zCAICamera extends zCAIBase {
  d_showDots(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-d_showDots-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_d_showDots(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-d_showDots-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pathDetectCollision(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-pathDetectCollision-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_pathDetectCollision(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-pathDetectCollision-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  bestRange(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-bestRange-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_bestRange(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-bestRange-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  minRange(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-minRange-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_minRange(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-minRange-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  maxRange(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-maxRange-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_maxRange(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-maxRange-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  bestRotX(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-bestRotX-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_bestRotX(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-bestRotX-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  minRotX(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-minRotX-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_minRotX(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-minRotX-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  maxRotX(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-maxRotX-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_maxRotX(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-maxRotX-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  bestRotY(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-bestRotY-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_bestRotY(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-bestRotY-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  minRotY(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-minRotY-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_minRotY(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-minRotY-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  maxRotY(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-maxRotY-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_maxRotY(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-maxRotY-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  bestRotZ(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-bestRotZ-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_bestRotZ(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-bestRotZ-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  minRotZ(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-minRotZ-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_minRotZ(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-minRotZ-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  maxRotZ(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-maxRotZ-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_maxRotZ(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-maxRotZ-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  rotOffsetX(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-rotOffsetX-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_rotOffsetX(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-rotOffsetX-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  rotOffsetY(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-rotOffsetY-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_rotOffsetY(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-rotOffsetY-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  rotOffsetZ(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-rotOffsetZ-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_rotOffsetZ(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-rotOffsetZ-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  focusOffsetX(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-focusOffsetX-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_focusOffsetX(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-focusOffsetX-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  focusOffsetY(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-focusOffsetY-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_focusOffsetY(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-focusOffsetY-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  focusOffsetZ(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-focusOffsetZ-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_focusOffsetZ(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-focusOffsetZ-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  veloTrans(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-veloTrans-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_veloTrans(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-veloTrans-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  veloRot(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-veloRot-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_veloRot(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-veloRot-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  translate(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-translate-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_translate(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-translate-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  rotate(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-rotate-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_rotate(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-rotate-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  collision(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-collision-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_collision(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-collision-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  endOfDScript(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-endOfDScript-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_endOfDScript(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-endOfDScript-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  camVob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-camVob-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_camVob(a1: zCVob): null {
    SendCommand({
      id: 'SET_zCAICamera-camVob-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  target(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-target-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_target(a1: zCVob): null {
    SendCommand({
      id: 'SET_zCAICamera-target-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  targetAlpha(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-targetAlpha-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_targetAlpha(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-targetAlpha-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  numTargets(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-numTargets-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_numTargets(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-numTargets-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  oldCamSys(): string | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-oldCamSys-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_oldCamSys(a1: string): null {
    SendCommand({
      id: 'SET_zCAICamera-oldCamSys-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  sysChanged(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-sysChanged-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_sysChanged(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-sysChanged-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  playerIsMovable(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-playerIsMovable-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_playerIsMovable(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-playerIsMovable-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  followIdealPos(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-followIdealPos-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_followIdealPos(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-followIdealPos-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  dialogCamDuration(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-dialogCamDuration-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_dialogCamDuration(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-dialogCamDuration-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  numOUsSpoken(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-numOUsSpoken-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_numOUsSpoken(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-numOUsSpoken-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  numDialogCamTakes(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-numDialogCamTakes-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_numDialogCamTakes(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-numDialogCamTakes-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lastNumDialogCamTakes(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-lastNumDialogCamTakes-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_lastNumDialogCamTakes(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-lastNumDialogCamTakes-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lastDialogCamSide(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-lastDialogCamSide-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_lastDialogCamSide(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-lastDialogCamSide-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  firstSpeakerWasPC(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-firstSpeakerWasPC-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_firstSpeakerWasPC(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-firstSpeakerWasPC-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lastPresetName(): string | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-lastPresetName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_lastPresetName(a1: string): null {
    SendCommand({
      id: 'SET_zCAICamera-lastPresetName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  raysCasted(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-raysCasted-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_raysCasted(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-raysCasted-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  underWater(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-underWater-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_underWater(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-underWater-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  inCutsceneMode(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-inCutsceneMode-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_inCutsceneMode(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-inCutsceneMode-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  debugS(): string | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-debugS-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_debugS(a1: string): null {
    SendCommand({
      id: 'SET_zCAICamera-debugS-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  showPath(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-showPath-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_showPath(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-showPath-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  focusVob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-focusVob-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_focusVob(a1: zCVob): null {
    SendCommand({
      id: 'SET_zCAICamera-focusVob-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  curcammode(): string | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-curcammode-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_curcammode(a1: string): null {
    SendCommand({
      id: 'SET_zCAICamera-curcammode-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  npcVolumeRangeOffset(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-npcVolumeRangeOffset-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_npcVolumeRangeOffset(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-npcVolumeRangeOffset-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  camDistOffset(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-camDistOffset-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_camDistOffset(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-camDistOffset-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  camSysFirstPos(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-camSysFirstPos-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_camSysFirstPos(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCAICamera-camSysFirstPos-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  firstPerson(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-firstPerson-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_firstPerson(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-firstPerson-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  targetInPortalRoom(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-targetInPortalRoom-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_targetInPortalRoom(a1: number): null {
    SendCommand({
      id: 'SET_zCAICamera-targetInPortalRoom-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCAICamera_OnInit(): zCAICamera | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-zCAICamera_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCAICamera(result) : null
  }

  StartUp(): void | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-StartUp-void-false-',
      parentId: this.Uuid,
    })
  }

  CleanUp(): void | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-CleanUp-void-false-',
      parentId: this.Uuid,
    })
  }

  GetBestRange(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-GetBestRange-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetMinRange(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-GetMinRange-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetMaxRange(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-GetMaxRange-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetMode(): string | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-GetMode-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  IsModeActive(a1: string): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-IsModeActive-int-false-zSTRING&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  IsModeAvailable(a1: string): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-IsModeAvailable-int-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  SetVob(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-SetVob-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetTarget(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-SetTarget-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  ClearTargetList(): void | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-ClearTargetList-void-false-',
      parentId: this.Uuid,
    })
  }

  AddTarget(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-AddTarget-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SubTarget(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-SubTarget-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  Console_EvalFunc(a1: string, a2: string): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-Console_EvalFunc-int-false-zSTRING const&,zSTRING&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  ParameterChanged(): void | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-ParameterChanged-void-false-',
      parentId: this.Uuid,
    })
  }

  CalcFirstPosition(): void | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-CalcFirstPosition-void-false-',
      parentId: this.Uuid,
    })
  }

  SetByScript(a1: string): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-SetByScript-int-false-zSTRING&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  SetByScriptLine(a1: string): string | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-SetByScriptLine-zSTRING-false-zSTRING&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? String(result) : null
  }

  CreateInstance(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-CreateInstance-void-false-zSTRING&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  AI_LookingCam(): void | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-AI_LookingCam-void-false-',
      parentId: this.Uuid,
    })
  }

  AI_FirstPerson(): void | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-AI_FirstPerson-void-false-',
      parentId: this.Uuid,
    })
  }

  AI_Normal(): void | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-AI_Normal-void-false-',
      parentId: this.Uuid,
    })
  }

  GetCamSysFirstPos(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-GetCamSysFirstPos-zVEC3&-false-',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  SetHintTargetInPortalRoom(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-SetHintTargetInPortalRoom-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CheckKeys(): void | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-CheckKeys-void-false-',
      parentId: this.Uuid,
    })
  }

  SetRotateEnabled(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-SetRotateEnabled-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  StartupDialogCam(): void | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-StartupDialogCam-void-false-',
      parentId: this.Uuid,
    })
  }

  InitDialogCam(): void | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-InitDialogCam-void-false-',
      parentId: this.Uuid,
    })
  }

  SetDialogCamDuration(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-SetDialogCamDuration-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  ReleaseLastDialogCam(): void | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-ReleaseLastDialogCam-void-false-',
      parentId: this.Uuid,
    })
  }

  StartDialogCam(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-StartDialogCam-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  InitHelperVobs(): void | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-InitHelperVobs-void-false-',
      parentId: this.Uuid,
    })
  }

  DeleteHelperVobs(): void | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-DeleteHelperVobs-void-false-',
      parentId: this.Uuid,
    })
  }

  ShowDebug(): void | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-ShowDebug-void-false-',
      parentId: this.Uuid,
    })
  }

  CheckUnderWaterFX(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-CheckUnderWaterFX-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetPreviousMode(): string | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-GetPreviousMode-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override DoAI(a1: zCVob, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCAICamera-DoAI-void-false-zCVob*,int&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }
}
