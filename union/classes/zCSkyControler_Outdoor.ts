import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCSkyState } from './index.js'
import { zVEC3 } from './index.js'
import { zCSkyLayer } from './index.js'
import { zCVob } from './index.js'
import { zTRainFX } from './index.js'
import { zCClassDef } from './index.js'
import { zTAnimationMode } from './index.js'

export class zCSkyControler_Outdoor extends BaseUnionObject {
  initDone(): number | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-initDone-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_initDone(a1: number): null {
    SendCommand({
      id: 'SET_zCSkyControler_Outdoor-initDone-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  masterTime(): number | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-masterTime-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_masterTime(a1: number): null {
    SendCommand({
      id: 'SET_zCSkyControler_Outdoor-masterTime-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  masterTimeLast(): number | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-masterTimeLast-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_masterTimeLast(a1: number): null {
    SendCommand({
      id: 'SET_zCSkyControler_Outdoor-masterTimeLast-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  masterState(): zCSkyState | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-masterState-zCSkyState-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCSkyState(result) : null
  }

  set_masterState(a1: zCSkyState): null {
    SendCommand({
      id: 'SET_zCSkyControler_Outdoor-masterState-zCSkyState-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  state0(): zCSkyState | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-state0-zCSkyState*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCSkyState(result) : null
  }

  set_state0(a1: zCSkyState): null {
    SendCommand({
      id: 'SET_zCSkyControler_Outdoor-state0-zCSkyState*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  state1(): zCSkyState | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-state1-zCSkyState*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCSkyState(result) : null
  }

  set_state1(a1: zCSkyState): null {
    SendCommand({
      id: 'SET_zCSkyControler_Outdoor-state1-zCSkyState*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  dayCounter(): number | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-dayCounter-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_dayCounter(a1: number): null {
    SendCommand({
      id: 'SET_zCSkyControler_Outdoor-dayCounter-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_fSkyScale(): number | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-m_fSkyScale-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_fSkyScale(a1: number): null {
    SendCommand({
      id: 'SET_zCSkyControler_Outdoor-m_fSkyScale-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_bSkyScaleChanged(): number | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-m_bSkyScaleChanged-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_bSkyScaleChanged(a1: number): null {
    SendCommand({
      id: 'SET_zCSkyControler_Outdoor-m_bSkyScaleChanged-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_overrideColor(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-m_overrideColor-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_m_overrideColor(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCSkyControler_Outdoor-m_overrideColor-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_bOverrideColorFlag(): number | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-m_bOverrideColorFlag-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_bOverrideColorFlag(a1: number): null {
    SendCommand({
      id: 'SET_zCSkyControler_Outdoor-m_bOverrideColorFlag-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_bDontRain(): number | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-m_bDontRain-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_bDontRain(a1: number): null {
    SendCommand({
      id: 'SET_zCSkyControler_Outdoor-m_bDontRain-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_bLevelChanged(): number | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-m_bLevelChanged-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_bLevelChanged(a1: number): null {
    SendCommand({
      id: 'SET_zCSkyControler_Outdoor-m_bLevelChanged-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_bDarkSky(): number | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-m_bDarkSky-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_bDarkSky(a1: number): null {
    SendCommand({
      id: 'SET_zCSkyControler_Outdoor-m_bDarkSky-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  resultFogScale(): number | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-resultFogScale-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_resultFogScale(a1: number): null {
    SendCommand({
      id: 'SET_zCSkyControler_Outdoor-resultFogScale-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  heightFogMinY(): number | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-heightFogMinY-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_heightFogMinY(a1: number): null {
    SendCommand({
      id: 'SET_zCSkyControler_Outdoor-heightFogMinY-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  heightFogMaxY(): number | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-heightFogMaxY-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_heightFogMaxY(a1: number): null {
    SendCommand({
      id: 'SET_zCSkyControler_Outdoor-heightFogMaxY-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  userFogFar(): number | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-userFogFar-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_userFogFar(a1: number): null {
    SendCommand({
      id: 'SET_zCSkyControler_Outdoor-userFogFar-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  resultFogNear(): number | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-resultFogNear-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_resultFogNear(a1: number): null {
    SendCommand({
      id: 'SET_zCSkyControler_Outdoor-resultFogNear-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  resultFogFar(): number | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-resultFogFar-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_resultFogFar(a1: number): null {
    SendCommand({
      id: 'SET_zCSkyControler_Outdoor-resultFogFar-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  resultFogSkyNear(): number | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-resultFogSkyNear-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_resultFogSkyNear(a1: number): null {
    SendCommand({
      id: 'SET_zCSkyControler_Outdoor-resultFogSkyNear-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  resultFogSkyFar(): number | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-resultFogSkyFar-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_resultFogSkyFar(a1: number): null {
    SendCommand({
      id: 'SET_zCSkyControler_Outdoor-resultFogSkyFar-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  userFarZScalability(): number | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-userFarZScalability-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_userFarZScalability(a1: number): null {
    SendCommand({
      id: 'SET_zCSkyControler_Outdoor-userFarZScalability-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  skyLayerRainClouds(): zCSkyLayer | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-skyLayerRainClouds-zCSkyLayer-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCSkyLayer(result) : null
  }

  set_skyLayerRainClouds(a1: zCSkyLayer): null {
    SendCommand({
      id: 'SET_zCSkyControler_Outdoor-skyLayerRainClouds-zCSkyLayer-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_bSunVisible(): number | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-m_bSunVisible-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_bSunVisible(a1: number): null {
    SendCommand({
      id: 'SET_zCSkyControler_Outdoor-m_bSunVisible-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_fFadeScale(): number | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-m_fFadeScale-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_fFadeScale(a1: number): null {
    SendCommand({
      id: 'SET_zCSkyControler_Outdoor-m_fFadeScale-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  vobSkyPFX(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-vobSkyPFX-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_vobSkyPFX(a1: zCVob): null {
    SendCommand({
      id: 'SET_zCSkyControler_Outdoor-vobSkyPFX-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  skyPFXTimer(): number | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-skyPFXTimer-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_skyPFXTimer(a1: number): null {
    SendCommand({
      id: 'SET_zCSkyControler_Outdoor-skyPFXTimer-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_bIsMainControler(): number | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-m_bIsMainControler-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_bIsMainControler(a1: number): null {
    SendCommand({
      id: 'SET_zCSkyControler_Outdoor-m_bIsMainControler-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_bWindVec(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-m_bWindVec-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_m_bWindVec(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCSkyControler_Outdoor-m_bWindVec-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  rainFX(): zTRainFX | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-rainFX-zTRainFX-false-false',
      parentId: this.Uuid,
    })

    return result ? new zTRainFX(result) : null
  }

  set_rainFX(a1: zTRainFX): null {
    SendCommand({
      id: 'SET_zCSkyControler_Outdoor-rainFX-zTRainFX-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCSkyControler_Outdoor_OnInit(a1: number): zCSkyControler_Outdoor | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-zCSkyControler_Outdoor_OnInit-void-false-int',
      parentId: 'NULL',
      a1: a1.toString(),
    })

    return result ? new zCSkyControler_Outdoor(result) : null
  }

  Init(): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-Init-void-false-',
      parentId: this.Uuid,
    })
  }

  GetStateTextureSearch(a1: number, a2: number, a3: number): number | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-GetStateTextureSearch-int-false-int,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? Number(result) : null
  }

  ApplyStateTexToLayer(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-ApplyStateTexToLayer-void-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  RenderPlanets(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-RenderPlanets-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  ReadFogColorsFromINI(): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-ReadFogColorsFromINI-void-false-',
      parentId: this.Uuid,
    })
  }

  ApplyFogColorsFromINI(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-ApplyFogColorsFromINI-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CreateDefault(): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-CreateDefault-void-false-',
      parentId: this.Uuid,
    })
  }

  Interpolate(): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-Interpolate-void-false-',
      parentId: this.Uuid,
    })
  }

  CalcPolyLightCLUT(a1: zVEC3, a2: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-CalcPolyLightCLUT-void-false-zVEC3 const&,zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  InitSkyPFX(): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-InitSkyPFX-void-false-',
      parentId: this.Uuid,
    })
  }

  CalcFog(): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-CalcFog-void-false-',
      parentId: this.Uuid,
    })
  }

  RenderSkyPFX(): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-RenderSkyPFX-void-false-',
      parentId: this.Uuid,
    })
  }

  CalcGlobalWind(): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-CalcGlobalWind-void-false-',
      parentId: this.Uuid,
    })
  }

  ProcessRainFX(): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-ProcessRainFX-void-false-',
      parentId: this.Uuid,
    })
  }

  SetRainFXWeight(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-SetRainFXWeight-void-false-float,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  RenderSetup(): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-RenderSetup-void-false-',
      parentId: this.Uuid,
    })
  }

  RenderSky(): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-RenderSky-void-false-',
      parentId: this.Uuid,
    })
  }

  _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  SetTime(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-SetTime-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetTime(): number | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-GetTime-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  ResetTime(): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-ResetTime-void-false-',
      parentId: this.Uuid,
    })
  }

  SetFarZ(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-SetFarZ-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetFarZ(): number | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-GetFarZ-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetFarZScalability(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-SetFarZScalability-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetFarZScalability(): number | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-GetFarZScalability-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetOverrideColor(a1: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-SetOverrideColor-void-false-zVEC3',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetOverrideColorFlag(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-SetOverrideColorFlag-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetCloudShadowScale(): number | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-GetCloudShadowScale-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetCloudShadowScale(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-SetCloudShadowScale-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  UpdateWorldDependencies(): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-UpdateWorldDependencies-void-false-',
      parentId: this.Uuid,
    })
  }

  RenderSkyPre(): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-RenderSkyPre-void-false-',
      parentId: this.Uuid,
    })
  }

  RenderSkyPost(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-RenderSkyPost-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetGlobalWindVec(a1: zVEC3, a2: zTAnimationMode): number | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-GetGlobalWindVec-int-false-zVEC3&,zTAnimationMode',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  SetGlobalSkyScale(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-SetGlobalSkyScale-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetGlobalSkyScale(): number | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-GetGlobalSkyScale-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetGlobalSkyScaleChanged(): number | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-GetGlobalSkyScaleChanged-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetRenderLightning(): number | null {
    const result: string | null = SendCommand({
      id: 'zCSkyControler_Outdoor-GetRenderLightning-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }
}
