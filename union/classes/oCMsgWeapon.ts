import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { TWeaponSubType } from './index.js'
import { zCClassDef } from './index.js'
import { oCNpcMessage } from './index.js'

export class oCMsgWeapon extends oCNpcMessage {
  targetMode(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgWeapon-targetMode-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_targetMode(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgWeapon-targetMode-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  duringRun(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgWeapon-duringRun-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_duringRun(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgWeapon-duringRun-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  initDone(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgWeapon-initDone-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_initDone(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgWeapon-initDone-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  firstTime(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgWeapon-firstTime-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_firstTime(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgWeapon-firstTime-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  useFist(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgWeapon-useFist-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_useFist(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgWeapon-useFist-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  showMagicCircle(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgWeapon-showMagicCircle-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_showMagicCircle(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgWeapon-showMagicCircle-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  ani(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgWeapon-ani-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_ani(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgWeapon-ani-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCMsgWeapon_OnInit(): oCMsgWeapon | null {
    const result: string | null = SendCommand({
      id: 'oCMsgWeapon-oCMsgWeapon_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCMsgWeapon(result) : null
  }

  static oCMsgWeapon_OnInit2(a1: TWeaponSubType, a2: number, a3: number): oCMsgWeapon | null {
    const result: string | null = SendCommand({
      id: 'oCMsgWeapon-oCMsgWeapon_OnInit-void-false-TWeaponSubType,int,int',
      parentId: 'NULL',
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? new oCMsgWeapon(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCMsgWeapon-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override MD_GetNumOfSubTypes(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgWeapon-MD_GetNumOfSubTypes-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override MD_GetSubTypeString(a1: number): string | null {
    const result: string | null = SendCommand({
      id: 'oCMsgWeapon-MD_GetSubTypeString-zSTRING-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? String(result) : null
  }

  override MD_GetMinTime(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgWeapon-MD_GetMinTime-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }
}
