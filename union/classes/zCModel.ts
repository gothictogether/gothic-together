import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCModelAniActive } from './index.js'
import { zCVob } from './index.js'
import { zTBBox3D } from './index.js'
import { zVEC3 } from './index.js'
import { zCModelPrototype } from './index.js'
import { zCModelAni } from './index.js'
import { zCModelNodeInst } from './index.js'
import { zTAniAttachment } from './index.js'
import { zCClassDef } from './index.js'
import { zTTraceRayReport } from './index.js'
import { zCWorld } from './index.js'

export class zCModel extends BaseUnionObject {
  numActiveAnis(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-numActiveAnis-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_numActiveAnis(a1: number): null {
    SendCommand({
      id: 'SET_zCModel-numActiveAnis-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  activeAniList(): zCModelAniActive | null {
    const result: string | null = SendCommand({
      id: 'zCModel-activeAniList-zCModelAniActive*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCModelAniActive(result) : null
  }

  set_activeAniList(a1: zCModelAniActive): null {
    SendCommand({
      id: 'SET_zCModel-activeAniList-zCModelAniActive*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  homeVob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'zCModel-homeVob-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_homeVob(a1: zCVob): null {
    SendCommand({
      id: 'SET_zCModel-homeVob-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lastTimeBBox3DTreeUpdate(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-lastTimeBBox3DTreeUpdate-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_lastTimeBBox3DTreeUpdate(a1: number): null {
    SendCommand({
      id: 'SET_zCModel-lastTimeBBox3DTreeUpdate-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  bbox3D(): zTBBox3D | null {
    const result: string | null = SendCommand({
      id: 'zCModel-bbox3D-zTBBox3D-false-false',
      parentId: this.Uuid,
    })

    return result ? new zTBBox3D(result) : null
  }

  set_bbox3D(a1: zTBBox3D): null {
    SendCommand({
      id: 'SET_zCModel-bbox3D-zTBBox3D-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  bbox3DLocalFixed(): zTBBox3D | null {
    const result: string | null = SendCommand({
      id: 'zCModel-bbox3DLocalFixed-zTBBox3D-false-false',
      parentId: this.Uuid,
    })

    return result ? new zTBBox3D(result) : null
  }

  set_bbox3DLocalFixed(a1: zTBBox3D): null {
    SendCommand({
      id: 'SET_zCModel-bbox3DLocalFixed-zTBBox3D-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  bbox3DCollDet(): zTBBox3D | null {
    const result: string | null = SendCommand({
      id: 'zCModel-bbox3DCollDet-zTBBox3D-false-false',
      parentId: this.Uuid,
    })

    return result ? new zTBBox3D(result) : null
  }

  set_bbox3DCollDet(a1: zTBBox3D): null {
    SendCommand({
      id: 'SET_zCModel-bbox3DCollDet-zTBBox3D-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  modelDistanceToCam(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-modelDistanceToCam-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_modelDistanceToCam(a1: number): null {
    SendCommand({
      id: 'SET_zCModel-modelDistanceToCam-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  n_bIsInMobInteraction(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-n_bIsInMobInteraction-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_n_bIsInMobInteraction(a1: number): null {
    SendCommand({
      id: 'SET_zCModel-n_bIsInMobInteraction-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fatness(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-fatness-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fatness(a1: number): null {
    SendCommand({
      id: 'SET_zCModel-fatness-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  modelScale(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCModel-modelScale-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_modelScale(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCModel-modelScale-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  aniTransScale(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCModel-aniTransScale-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_aniTransScale(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCModel-aniTransScale-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  rootPosLocal(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCModel-rootPosLocal-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_rootPosLocal(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCModel-rootPosLocal-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  vobTrans(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCModel-vobTrans-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_vobTrans(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCModel-vobTrans-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  vobTransRing(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCModel-vobTransRing-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_vobTransRing(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCModel-vobTransRing-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  newAniStarted(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-newAniStarted-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_newAniStarted(a1: number): null {
    SendCommand({
      id: 'SET_zCModel-newAniStarted-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_bSmoothRootNode(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-m_bSmoothRootNode-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_bSmoothRootNode(a1: number): null {
    SendCommand({
      id: 'SET_zCModel-m_bSmoothRootNode-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  relaxWeight(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-relaxWeight-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_relaxWeight(a1: number): null {
    SendCommand({
      id: 'SET_zCModel-relaxWeight-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_bDrawHandVisualsOnly(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-m_bDrawHandVisualsOnly-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_bDrawHandVisualsOnly(a1: number): null {
    SendCommand({
      id: 'SET_zCModel-m_bDrawHandVisualsOnly-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  modelVelocity(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCModel-modelVelocity-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_modelVelocity(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCModel-modelVelocity-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  actVelRingPos(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-actVelRingPos-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_actVelRingPos(a1: number): null {
    SendCommand({
      id: 'SET_zCModel-actVelRingPos-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  isVisible(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-isVisible-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_isVisible(a1: number): null {
    SendCommand({
      id: 'SET_zCModel-isVisible-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  isFlying(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-isFlying-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_isFlying(a1: number): null {
    SendCommand({
      id: 'SET_zCModel-isFlying-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  randAnisEnabled(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-randAnisEnabled-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_randAnisEnabled(a1: number): null {
    SendCommand({
      id: 'SET_zCModel-randAnisEnabled-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lerpSamples(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-lerpSamples-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_lerpSamples(a1: number): null {
    SendCommand({
      id: 'SET_zCModel-lerpSamples-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  modelScaleOn(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-modelScaleOn-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_modelScaleOn(a1: number): null {
    SendCommand({
      id: 'SET_zCModel-modelScaleOn-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  doVobRot(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-doVobRot-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_doVobRot(a1: number): null {
    SendCommand({
      id: 'SET_zCModel-doVobRot-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  nodeShadowEnabled(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-nodeShadowEnabled-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_nodeShadowEnabled(a1: number): null {
    SendCommand({
      id: 'SET_zCModel-nodeShadowEnabled-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  dynLightMode(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-dynLightMode-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_dynLightMode(a1: number): null {
    SendCommand({
      id: 'SET_zCModel-dynLightMode-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  timeScale(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-timeScale-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_timeScale(a1: number): null {
    SendCommand({
      id: 'SET_zCModel-timeScale-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCModel_OnInit(a1: zCModelPrototype): zCModel | null {
    const result: string | null = SendCommand({
      id: 'zCModel-zCModel_OnInit-void-false-zCModelPrototype*',
      parentId: 'NULL',
      a1: a1.toString(),
    })

    return result ? new zCModel(result) : null
  }

  GetAniFromAniID(a1: number): zCModelAni | null {
    const result: string | null = SendCommand({
      id: 'zCModel-GetAniFromAniID-zCModelAni*-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new zCModelAni(result) : null
  }

  IsAniActive(a1: zCModelAni): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-IsAniActive-int-false-zCModelAni*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  Init(): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-Init-void-false-',
      parentId: this.Uuid,
    })
  }

  CopyProtoNodeList(): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-CopyProtoNodeList-void-false-',
      parentId: this.Uuid,
    })
  }

  ApplyModelProtoOverlay(a1: string): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-ApplyModelProtoOverlay-int-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  ApplyModelProtoOverlay2(a1: zCModelPrototype): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-ApplyModelProtoOverlay-int-false-zCModelPrototype*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  HasAppliedModelProtoOverlay(a1: zCModelPrototype): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-HasAppliedModelProtoOverlay-int-false-zCModelPrototype*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  HasAppliedModelProtoOverlay2(a1: string): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-HasAppliedModelProtoOverlay-int-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  RemoveModelProtoOverlay(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-RemoveModelProtoOverlay-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RemoveModelProtoOverlay2(a1: zCModelPrototype): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-RemoveModelProtoOverlay-void-false-zCModelPrototype*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CalcNodeListBBoxWorld(): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-CalcNodeListBBoxWorld-void-false-',
      parentId: this.Uuid,
    })
  }

  GetBBox3DNodeWorld(a1: zCModelNodeInst): zTBBox3D | null {
    const result: string | null = SendCommand({
      id: 'zCModel-GetBBox3DNodeWorld-zTBBox3D-false-zCModelNodeInst*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new zTBBox3D(result) : null
  }

  GetNodePositionWorld(a1: zCModelNodeInst): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCModel-GetNodePositionWorld-zVEC3-false-zCModelNodeInst*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new zVEC3(result) : null
  }

  CalcModelBBox3DWorld(): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-CalcModelBBox3DWorld-void-false-',
      parentId: this.Uuid,
    })
  }

  SetDynLightMode(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-SetDynLightMode-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetLowestLODNumPolys(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-GetLowestLODNumPolys-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetNumMaterials(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-GetNumMaterials-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetRandAnisEnabled(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-SetRandAnisEnabled-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetActiveAni(a1: number): zCModelAniActive | null {
    const result: string | null = SendCommand({
      id: 'zCModel-GetActiveAni-zCModelAniActive*-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new zCModelAniActive(result) : null
  }

  GetActiveAni2(a1: zCModelAni): zCModelAniActive | null {
    const result: string | null = SendCommand({
      id: 'zCModel-GetActiveAni-zCModelAniActive*-false-zCModelAni*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new zCModelAniActive(result) : null
  }

  StopAni(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-StopAni-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  StopAni2(a1: zCModelAni): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-StopAni-void-false-zCModelAni*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  StopAni3(a1: zCModelAniActive): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-StopAni-void-false-zCModelAniActive*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetAniTransLerp(a1: zCModelAni, a2: number, a3: number): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCModel-GetAniTransLerp-zVEC3-false-zCModelAni*,float,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? new zVEC3(result) : null
  }

  StartAni(a1: string, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-StartAni-void-false-zSTRING const&,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  StartAni2(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-StartAni-void-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  StartAni3(a1: zCModelAni, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-StartAni-void-false-zCModelAni*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  AssertActiveAniListAlloced(): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-AssertActiveAniListAlloced-void-false-',
      parentId: this.Uuid,
    })
  }

  DoAniEvents(a1: zCModelAniActive): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-DoAniEvents-void-false-zCModelAniActive*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  AdvanceAni(a1: zCModelAniActive, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-AdvanceAni-void-false-zCModelAniActive*,int&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  AdvanceAnis(): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-AdvanceAnis-void-false-',
      parentId: this.Uuid,
    })
  }

  SetModelScale(a1: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-SetModelScale-void-false-zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  IsStateActive(a1: zCModelAni): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-IsStateActive-int-false-zCModelAni const*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  SearchNode(a1: string): zCModelNodeInst | null {
    const result: string | null = SendCommand({
      id: 'zCModel-SearchNode-zCModelNodeInst*-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new zCModelNodeInst(result) : null
  }

  RemoveMeshLibAll(): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-RemoveMeshLibAll-void-false-',
      parentId: this.Uuid,
    })
  }

  RemoveMeshLib(a1: string): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-RemoveMeshLib-int-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  ApplyMeshLib(a1: string): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-ApplyMeshLib-int-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  UpdateMeshLibTexAniState(): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-UpdateMeshLibTexAniState-void-false-',
      parentId: this.Uuid,
    })
  }

  FadeOutAni(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-FadeOutAni-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  FadeOutAni2(a1: zCModelAni): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-FadeOutAni-void-false-zCModelAni*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  FadeOutAni3(a1: zCModelAniActive): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-FadeOutAni-void-false-zCModelAniActive*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  FadeOutAnisLayerRange(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-FadeOutAnisLayerRange-void-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  StopAnisLayerRange(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-StopAnisLayerRange-void-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  GetProgressPercent(a1: string): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-GetProgressPercent-float-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetProgressPercent2(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-GetProgressPercent-float-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  SetCombineAniXY(a1: number, a2: number, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-SetCombineAniXY-void-false-int,float,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  GetCombineAniXY(a1: number, a2: number, a3: number): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-GetCombineAniXY-int-false-int,float&,float&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? Number(result) : null
  }

  CalcNodeListAniBlending(): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-CalcNodeListAniBlending-void-false-',
      parentId: this.Uuid,
    })
  }

  CalcTransBlending(): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-CalcTransBlending-void-false-',
      parentId: this.Uuid,
    })
  }

  AttachChildVobToNode(a1: zCVob, a2: zCModelNodeInst): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-AttachChildVobToNode-void-false-zCVob*,zCModelNodeInst*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  RemoveChildVobFromNode(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-RemoveChildVobFromNode-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetAttachedNodeVob(a1: zCModelNodeInst): zCVob | null {
    const result: string | null = SendCommand({
      id: 'zCModel-GetAttachedNodeVob-zCVob*-false-zCModelNodeInst*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new zCVob(result) : null
  }

  RemoveAllChildVobsFromNode(): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-RemoveAllChildVobsFromNode-void-false-',
      parentId: this.Uuid,
    })
  }

  UpdateAttachedVobs(): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-UpdateAttachedVobs-void-false-',
      parentId: this.Uuid,
    })
  }

  RemoveStartedVobFX(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-RemoveStartedVobFX-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetVelocityRing(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCModel-GetVelocityRing-zVEC3-false-',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  ResetVelocity(): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-ResetVelocity-void-false-',
      parentId: this.Uuid,
    })
  }

  GetAniMinMaxWeight(a1: zCModelAniActive, a2: number, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-GetAniMinMaxWeight-void-false-zCModelAniActive*,float&,float&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  PrintStatus(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-PrintStatus-void-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  CorrectAniFreezer(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-CorrectAniFreezer-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  ShowAniListAdd(a1: zCModelAni): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-ShowAniListAdd-void-false-zCModelAni*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  ShowAniList(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-ShowAniList-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SearchAniAttachList(a1: number): zTAniAttachment | null {
    const result: string | null = SendCommand({
      id: 'zCModel-SearchAniAttachList-zTAniAttachment*-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new zTAniAttachment(result) : null
  }

  RemoveAniAttachment(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-RemoveAniAttachment-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RemoveAllAniAttachments(): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-RemoveAllAniAttachments-void-false-',
      parentId: this.Uuid,
    })
  }

  RemoveAllVobFX(): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-RemoveAllVobFX-void-false-',
      parentId: this.Uuid,
    })
  }

  GetCreateAniAttachment(a1: number): zTAniAttachment | null {
    const result: string | null = SendCommand({
      id: 'zCModel-GetCreateAniAttachment-zTAniAttachment*-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new zTAniAttachment(result) : null
  }

  DeleteRandAniList(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-DeleteRandAniList-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  InsertRandAni(a1: number, a2: number, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-InsertRandAni-void-false-int,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  GetRandAniFreq(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-GetRandAniFreq-float-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  SetRandAniFreq(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-SetRandAniFreq-void-false-int,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  RecalcRootPosLocal(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-RecalcRootPosLocal-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetAniIDFromAniName(a1: string): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-GetAniIDFromAniName-int-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCModel-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  IsBBox3DLocal(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-IsBBox3DLocal-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetBBox3D(): zTBBox3D | null {
    const result: string | null = SendCommand({
      id: 'zCModel-GetBBox3D-zTBBox3D-false-',
      parentId: this.Uuid,
    })

    return result ? new zTBBox3D(result) : null
  }

  GetVisualName(): string | null {
    const result: string | null = SendCommand({
      id: 'zCModel-GetVisualName-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  SetVisualUsedBy(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-SetVisualUsedBy-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetRenderSortKey(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-GetRenderSortKey-unsigned long-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  CanTraceRay(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-CanTraceRay-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  TraceRay(a1: zVEC3, a2: zVEC3, a3: number, a4: zTTraceRayReport): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-TraceRay-int-false-zVEC3 const&,zVEC3 const&,int,zTTraceRayReport&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
    })

    return result ? Number(result) : null
  }

  HostVobRemovedFromWorld(a1: zCVob, a2: zCWorld): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-HostVobRemovedFromWorld-void-false-zCVob*,zCWorld*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  GetFileExtension(a1: number): string | null {
    const result: string | null = SendCommand({
      id: 'zCModel-GetFileExtension-zSTRING-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? String(result) : null
  }

  StartAnimation(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-StartAnimation-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  StopAnimation(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'zCModel-StopAnimation-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  IsAnimationActive(a1: string): number | null {
    const result: string | null = SendCommand({
      id: 'zCModel-IsAnimationActive-int-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetAnyAnimation(): string | null {
    const result: string | null = SendCommand({
      id: 'zCModel-GetAnyAnimation-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }
}
