import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCPolygon } from './index.js'

export class zSPolySavedValues extends BaseUnionObject {
  poly(): zCPolygon | null {
    const result: string | null = SendCommand({
      id: 'zSPolySavedValues-poly-zCPolygon*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCPolygon(result) : null
  }

  set_poly(a1: zCPolygon): null {
    SendCommand({
      id: 'SET_zSPolySavedValues-poly-zCPolygon*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
