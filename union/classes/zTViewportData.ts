import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'

export class zTViewportData extends BaseUnionObject {
  xmin(): number | null {
    const result: string | null = SendCommand({
      id: 'zTViewportData-xmin-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_xmin(a1: number): null {
    SendCommand({
      id: 'SET_zTViewportData-xmin-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  ymin(): number | null {
    const result: string | null = SendCommand({
      id: 'zTViewportData-ymin-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_ymin(a1: number): null {
    SendCommand({
      id: 'SET_zTViewportData-ymin-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  xdim(): number | null {
    const result: string | null = SendCommand({
      id: 'zTViewportData-xdim-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_xdim(a1: number): null {
    SendCommand({
      id: 'SET_zTViewportData-xdim-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  ydim(): number | null {
    const result: string | null = SendCommand({
      id: 'zTViewportData-ydim-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_ydim(a1: number): null {
    SendCommand({
      id: 'SET_zTViewportData-ydim-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  xminFloat(): number | null {
    const result: string | null = SendCommand({
      id: 'zTViewportData-xminFloat-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_xminFloat(a1: number): null {
    SendCommand({
      id: 'SET_zTViewportData-xminFloat-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  yminFloat(): number | null {
    const result: string | null = SendCommand({
      id: 'zTViewportData-yminFloat-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_yminFloat(a1: number): null {
    SendCommand({
      id: 'SET_zTViewportData-yminFloat-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  xmaxFloat(): number | null {
    const result: string | null = SendCommand({
      id: 'zTViewportData-xmaxFloat-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_xmaxFloat(a1: number): null {
    SendCommand({
      id: 'SET_zTViewportData-xmaxFloat-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  ymaxFloat(): number | null {
    const result: string | null = SendCommand({
      id: 'zTViewportData-ymaxFloat-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_ymaxFloat(a1: number): null {
    SendCommand({
      id: 'SET_zTViewportData-ymaxFloat-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  xdimFloat(): number | null {
    const result: string | null = SendCommand({
      id: 'zTViewportData-xdimFloat-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_xdimFloat(a1: number): null {
    SendCommand({
      id: 'SET_zTViewportData-xdimFloat-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  ydimFloat(): number | null {
    const result: string | null = SendCommand({
      id: 'zTViewportData-ydimFloat-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_ydimFloat(a1: number): null {
    SendCommand({
      id: 'SET_zTViewportData-ydimFloat-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  xdimFloatMinus1(): number | null {
    const result: string | null = SendCommand({
      id: 'zTViewportData-xdimFloatMinus1-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_xdimFloatMinus1(a1: number): null {
    SendCommand({
      id: 'SET_zTViewportData-xdimFloatMinus1-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  ydimFloatMinus1(): number | null {
    const result: string | null = SendCommand({
      id: 'zTViewportData-ydimFloatMinus1-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_ydimFloatMinus1(a1: number): null {
    SendCommand({
      id: 'SET_zTViewportData-ydimFloatMinus1-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  xcenter(): number | null {
    const result: string | null = SendCommand({
      id: 'zTViewportData-xcenter-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_xcenter(a1: number): null {
    SendCommand({
      id: 'SET_zTViewportData-xcenter-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  ycenter(): number | null {
    const result: string | null = SendCommand({
      id: 'zTViewportData-ycenter-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_ycenter(a1: number): null {
    SendCommand({
      id: 'SET_zTViewportData-ycenter-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
