import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCVob } from './index.js'
import { zCClassDef } from './index.js'
import { zCEffect } from './index.js'

export class zCTouchAnimate extends zCEffect {
  touchSoundName(): string | null {
    const result: string | null = SendCommand({
      id: 'zCTouchAnimate-touchSoundName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_touchSoundName(a1: string): null {
    SendCommand({
      id: 'SET_zCTouchAnimate-touchSoundName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCTouchAnimate_OnInit(): zCTouchAnimate | null {
    const result: string | null = SendCommand({
      id: 'zCTouchAnimate-zCTouchAnimate_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCTouchAnimate(result) : null
  }

  IsTouching(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'zCTouchAnimate-IsTouching-int-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  StartAni(): void | null {
    const result: string | null = SendCommand({
      id: 'zCTouchAnimate-StartAni-void-false-',
      parentId: this.Uuid,
    })
  }

  StopAni(): void | null {
    const result: string | null = SendCommand({
      id: 'zCTouchAnimate-StopAni-void-false-',
      parentId: this.Uuid,
    })
  }

  IsAniActive(): number | null {
    const result: string | null = SendCommand({
      id: 'zCTouchAnimate-IsAniActive-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetVobProperties(): void | null {
    const result: string | null = SendCommand({
      id: 'zCTouchAnimate-SetVobProperties-void-false-',
      parentId: this.Uuid,
    })
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCTouchAnimate-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override OnTouch(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCTouchAnimate-OnTouch-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override OnUntouch(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCTouchAnimate-OnUntouch-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override OnTick(): void | null {
    const result: string | null = SendCommand({
      id: 'zCTouchAnimate-OnTick-void-false-',
      parentId: this.Uuid,
    })
  }

  GetSoundName(): string | null {
    const result: string | null = SendCommand({
      id: 'zCTouchAnimate-GetSoundName-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }
}
