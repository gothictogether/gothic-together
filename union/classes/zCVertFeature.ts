import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zVEC3 } from './index.js'

export class zCVertFeature extends BaseUnionObject {
  vertNormal(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCVertFeature-vertNormal-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_vertNormal(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCVertFeature-vertNormal-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  texu(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVertFeature-texu-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_texu(a1: number): null {
    SendCommand({
      id: 'SET_zCVertFeature-texu-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  texv(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVertFeature-texv-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_texv(a1: number): null {
    SendCommand({
      id: 'SET_zCVertFeature-texv-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCVertFeature_OnInit(): zCVertFeature | null {
    const result: string | null = SendCommand({
      id: 'zCVertFeature-zCVertFeature_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCVertFeature(result) : null
  }
}
