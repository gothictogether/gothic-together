import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { oCPortalRoom } from './index.js'
import { oCNpc } from './index.js'

export class oCPortalRoomManager extends BaseUnionObject {
  oldPlayerRoom(): oCPortalRoom | null {
    const result: string | null = SendCommand({
      id: 'oCPortalRoomManager-oldPlayerRoom-oCPortalRoom*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCPortalRoom(result) : null
  }

  set_oldPlayerRoom(a1: oCPortalRoom): null {
    SendCommand({
      id: 'SET_oCPortalRoomManager-oldPlayerRoom-oCPortalRoom*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  curPlayerRoom(): oCPortalRoom | null {
    const result: string | null = SendCommand({
      id: 'oCPortalRoomManager-curPlayerRoom-oCPortalRoom*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCPortalRoom(result) : null
  }

  set_curPlayerRoom(a1: oCPortalRoom): null {
    SendCommand({
      id: 'SET_oCPortalRoomManager-curPlayerRoom-oCPortalRoom*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCPortalRoomManager_OnInit(): oCPortalRoomManager | null {
    const result: string | null = SendCommand({
      id: 'oCPortalRoomManager-oCPortalRoomManager_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCPortalRoomManager(result) : null
  }

  CleanUp(): void | null {
    const result: string | null = SendCommand({
      id: 'oCPortalRoomManager-CleanUp-void-false-',
      parentId: this.Uuid,
    })
  }

  AssignPortalToNpc(a1: string, a2: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCPortalRoomManager-AssignPortalToNpc-void-false-zSTRING const&,zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  AssignPortalToGuild(a1: string, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCPortalRoomManager-AssignPortalToGuild-void-false-zSTRING const&,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  GetPortalRoomIndex(a1: string): number | null {
    const result: string | null = SendCommand({
      id: 'oCPortalRoomManager-GetPortalRoomIndex-int-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  IsPlayerInMyRoom(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCPortalRoomManager-IsPlayerInMyRoom-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  IsNSCInPlayerRoom(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCPortalRoomManager-IsNSCInPlayerRoom-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  WasPlayerInMyRoom(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCPortalRoomManager-WasPlayerInMyRoom-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  IsPortalMyRoom(a1: string, a2: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCPortalRoomManager-IsPortalMyRoom-int-false-zSTRING const&,oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  GetCurPlayerPortalRoomOwner(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCPortalRoomManager-GetCurPlayerPortalRoomOwner-oCNpc*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  GetCurPlayerPortalRoomGuild(): number | null {
    const result: string | null = SendCommand({
      id: 'oCPortalRoomManager-GetCurPlayerPortalRoomGuild-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetFormerPlayerPortalRoomOwner(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCPortalRoomManager-GetFormerPlayerPortalRoomOwner-oCNpc*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  GetFormerPlayerPortalRoomGuild(): number | null {
    const result: string | null = SendCommand({
      id: 'oCPortalRoomManager-GetFormerPlayerPortalRoomGuild-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  HasPlayerChangedPortalRoom(): number | null {
    const result: string | null = SendCommand({
      id: 'oCPortalRoomManager-HasPlayerChangedPortalRoom-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetCurNpcPortalRoomOwner(a1: oCNpc): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCPortalRoomManager-GetCurNpcPortalRoomOwner-oCNpc*-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new oCNpc(result) : null
  }

  GetCurNpcPortalRoomGuild(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCPortalRoomManager-GetCurNpcPortalRoomGuild-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  ShowDebugInfo(): void | null {
    const result: string | null = SendCommand({
      id: 'oCPortalRoomManager-ShowDebugInfo-void-false-',
      parentId: this.Uuid,
    })
  }

  ShowPortalInfo(): void | null {
    const result: string | null = SendCommand({
      id: 'oCPortalRoomManager-ShowPortalInfo-void-false-',
      parentId: this.Uuid,
    })
  }
}
