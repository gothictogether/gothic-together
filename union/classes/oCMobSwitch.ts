import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { oCMobInter } from './index.js'

export class oCMobSwitch extends oCMobInter {
  static oCMobSwitch_OnInit(): oCMobSwitch | null {
    const result: string | null = SendCommand({
      id: 'oCMobSwitch-oCMobSwitch_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCMobSwitch(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCMobSwitch-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }
}
