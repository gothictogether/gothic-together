import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCModelAniActive } from './index.js'

export class zTNodeAni extends BaseUnionObject {
  modelAni(): zCModelAniActive | null {
    const result: string | null = SendCommand({
      id: 'zTNodeAni-modelAni-zCModelAniActive*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCModelAniActive(result) : null
  }

  set_modelAni(a1: zCModelAniActive): null {
    SendCommand({
      id: 'SET_zTNodeAni-modelAni-zCModelAniActive*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  weight(): number | null {
    const result: string | null = SendCommand({
      id: 'zTNodeAni-weight-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_weight(a1: number): null {
    SendCommand({
      id: 'SET_zTNodeAni-weight-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  weightSpeed(): number | null {
    const result: string | null = SendCommand({
      id: 'zTNodeAni-weightSpeed-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_weightSpeed(a1: number): null {
    SendCommand({
      id: 'SET_zTNodeAni-weightSpeed-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  blendState(): number | null {
    const result: string | null = SendCommand({
      id: 'zTNodeAni-blendState-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_blendState(a1: number): null {
    SendCommand({
      id: 'SET_zTNodeAni-blendState-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
