import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCPatch } from './index.js'

export class zCTransfer extends BaseUnionObject {
  targetPatch(): zCPatch | null {
    const result: string | null = SendCommand({
      id: 'zCTransfer-targetPatch-zCPatch*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCPatch(result) : null
  }

  set_targetPatch(a1: zCPatch): null {
    SendCommand({
      id: 'SET_zCTransfer-targetPatch-zCPatch*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  formFactor(): number | null {
    const result: string | null = SendCommand({
      id: 'zCTransfer-formFactor-unsigned short-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_formFactor(a1: number): null {
    SendCommand({
      id: 'SET_zCTransfer-formFactor-unsigned short-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
