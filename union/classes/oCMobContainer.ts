import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { oCItemContainer } from './index.js'
import { zCClassDef } from './index.js'
import { zCEventMessage } from './index.js'
import { zCVob } from './index.js'
import { oCNpc } from './index.js'
import { oCItem } from './index.js'
import { oCMobLockable } from './index.js'

export class oCMobContainer extends oCMobLockable {
  contains(): string | null {
    const result: string | null = SendCommand({
      id: 'oCMobContainer-contains-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_contains(a1: string): null {
    SendCommand({
      id: 'SET_oCMobContainer-contains-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  items(): oCItemContainer | null {
    const result: string | null = SendCommand({
      id: 'oCMobContainer-items-oCItemContainer*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCItemContainer(result) : null
  }

  set_items(a1: oCItemContainer): null {
    SendCommand({
      id: 'SET_oCMobContainer-items-oCItemContainer*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCMobContainer_OnInit(): oCMobContainer | null {
    const result: string | null = SendCommand({
      id: 'oCMobContainer-oCMobContainer_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCMobContainer(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCMobContainer-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override OnMessage(a1: zCEventMessage, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobContainer-OnMessage-void-false-zCEventMessage*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override Destroy(): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobContainer-Destroy-void-false-',
      parentId: this.Uuid,
    })
  }

  override Reset(): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobContainer-Reset-void-false-',
      parentId: this.Uuid,
    })
  }

  override IsIn(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobContainer-IsIn-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  override Open(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobContainer-Open-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override Close(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobContainer-Close-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  Insert(a1: oCItem): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobContainer-Insert-void-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  Remove(a1: oCItem): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobContainer-Remove-void-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  Remove2(a1: oCItem, a2: number): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCMobContainer-Remove-oCItem*-false-oCItem*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCItem(result) : null
  }

  CreateContents(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobContainer-CreateContents-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }
}
