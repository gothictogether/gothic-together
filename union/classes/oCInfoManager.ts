import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { oCInfo } from './index.js'
import { oCNpc } from './index.js'

export class oCInfoManager extends BaseUnionObject {
  GetInformation(a1: oCNpc, a2: oCNpc, a3: number): oCInfo | null {
    const result: string | null = SendCommand({
      id: 'oCInfoManager-GetInformation-oCInfo*-false-oCNpc*,oCNpc*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? new oCInfo(result) : null
  }

  GetInformation2(a1: number): oCInfo | null {
    const result: string | null = SendCommand({
      id: 'oCInfoManager-GetInformation-oCInfo*-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new oCInfo(result) : null
  }

  GetInfoCount(a1: oCNpc, a2: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCInfoManager-GetInfoCount-int-false-oCNpc*,oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  GetInfoCountImportant(a1: oCNpc, a2: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCInfoManager-GetInfoCountImportant-int-false-oCNpc*,oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  GetInfoCountUnimportant(a1: oCNpc, a2: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCInfoManager-GetInfoCountUnimportant-int-false-oCNpc*,oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  GetInfo(a1: oCNpc, a2: oCNpc, a3: number): oCInfo | null {
    const result: string | null = SendCommand({
      id: 'oCInfoManager-GetInfo-oCInfo*-false-oCNpc*,oCNpc*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? new oCInfo(result) : null
  }

  GetInfoImportant(a1: oCNpc, a2: oCNpc, a3: number): oCInfo | null {
    const result: string | null = SendCommand({
      id: 'oCInfoManager-GetInfoImportant-oCInfo*-false-oCNpc*,oCNpc*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? new oCInfo(result) : null
  }

  GetInfoUnimportant(a1: oCNpc, a2: oCNpc, a3: number): oCInfo | null {
    const result: string | null = SendCommand({
      id: 'oCInfoManager-GetInfoUnimportant-oCInfo*-false-oCNpc*,oCNpc*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? new oCInfo(result) : null
  }

  InformationTold(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCInfoManager-InformationTold-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  RestoreParserInstances(): void | null {
    const result: string | null = SendCommand({
      id: 'oCInfoManager-RestoreParserInstances-void-false-',
      parentId: this.Uuid,
    })
  }
}
