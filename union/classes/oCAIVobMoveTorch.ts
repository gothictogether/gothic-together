import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { zCVob } from './index.js'
import { oCAIVobMove } from './index.js'

export class oCAIVobMoveTorch extends oCAIVobMove {
  timer(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIVobMoveTorch-timer-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_timer(a1: number): null {
    SendCommand({
      id: 'SET_oCAIVobMoveTorch-timer-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCAIVobMoveTorch_OnInit(): oCAIVobMoveTorch | null {
    const result: string | null = SendCommand({
      id: 'oCAIVobMoveTorch-oCAIVobMoveTorch_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCAIVobMoveTorch(result) : null
  }

  CheckWater(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIVobMoveTorch-CheckWater-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  BurnedOut(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIVobMoveTorch-BurnedOut-void-false-',
      parentId: this.Uuid,
    })
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCAIVobMoveTorch-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override DoAI(a1: zCVob, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIVobMoveTorch-DoAI-void-false-zCVob*,int&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }
}
