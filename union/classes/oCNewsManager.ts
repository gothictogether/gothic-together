import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { oCNpc } from './index.js'
import { oCNews } from './index.js'

export class oCNewsManager extends BaseUnionObject {
  called_BAssessAndMem(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNewsManager-called_BAssessAndMem-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_called_BAssessAndMem(a1: number): null {
    SendCommand({
      id: 'SET_oCNewsManager-called_BAssessAndMem-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  sentry(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNewsManager-sentry-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_sentry(a1: number): null {
    SendCommand({
      id: 'SET_oCNewsManager-sentry-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCNewsManager_OnInit(): oCNewsManager | null {
    const result: string | null = SendCommand({
      id: 'oCNewsManager-oCNewsManager_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCNewsManager(result) : null
  }

  CreateNews(a1: number, a2: number, a3: oCNpc, a4: oCNpc, a5: oCNpc, a6: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNewsManager-CreateNews-void-false-int,int,oCNpc*,oCNpc*,oCNpc*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
      a5: a5.toString(),
      a6: a6.toString(),
    })
  }

  SpreadToGuild(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNewsManager-SpreadToGuild-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  IsInSpreadList(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNewsManager-IsInSpreadList-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  SpreadNews(a1: oCNews): void | null {
    const result: string | null = SendCommand({
      id: 'oCNewsManager-SpreadNews-void-false-oCNews*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }
}
