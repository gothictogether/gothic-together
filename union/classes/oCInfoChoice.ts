import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'

export class oCInfoChoice extends BaseUnionObject {
  Text(): string | null {
    const result: string | null = SendCommand({
      id: 'oCInfoChoice-Text-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_Text(a1: string): null {
    SendCommand({
      id: 'SET_oCInfoChoice-Text-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  Function(): number | null {
    const result: string | null = SendCommand({
      id: 'oCInfoChoice-Function-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_Function(a1: number): null {
    SendCommand({
      id: 'SET_oCInfoChoice-Function-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
