import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCPolygon } from './index.js'

export class zCPortal extends zCPolygon {
  lastTimeCompletely(): number | null {
    const result: string | null = SendCommand({
      id: 'zCPortal-lastTimeCompletely-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_lastTimeCompletely(a1: number): null {
    SendCommand({
      id: 'SET_zCPortal-lastTimeCompletely-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  RemoveFromBsp(): void | null {
    const result: string | null = SendCommand({
      id: 'zCPortal-RemoveFromBsp-void-false-',
      parentId: this.Uuid,
    })
  }

  Init(): void | null {
    const result: string | null = SendCommand({
      id: 'zCPortal-Init-void-false-',
      parentId: this.Uuid,
    })
  }
}
