import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zTMdl_AniEventType } from './index.js'

export class zCModelAniEvent extends BaseUnionObject {
  aniEventType(): zTMdl_AniEventType | null {
    const result: string | null = SendCommand({
      id: 'zCModelAniEvent-aniEventType-zTMdl_AniEventType-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_aniEventType(a1: zTMdl_AniEventType): null {
    SendCommand({
      id: 'SET_zCModelAniEvent-aniEventType-zTMdl_AniEventType-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  frameNr(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelAniEvent-frameNr-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_frameNr(a1: number): null {
    SendCommand({
      id: 'SET_zCModelAniEvent-frameNr-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  tagString(): string | null {
    const result: string | null = SendCommand({
      id: 'zCModelAniEvent-tagString-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_tagString(a1: string): null {
    SendCommand({
      id: 'SET_zCModelAniEvent-tagString-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  value1(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelAniEvent-value1-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_value1(a1: number): null {
    SendCommand({
      id: 'SET_zCModelAniEvent-value1-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  value2(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelAniEvent-value2-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_value2(a1: number): null {
    SendCommand({
      id: 'SET_zCModelAniEvent-value2-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  value3(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelAniEvent-value3-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_value3(a1: number): null {
    SendCommand({
      id: 'SET_zCModelAniEvent-value3-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  value4(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelAniEvent-value4-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_value4(a1: number): null {
    SendCommand({
      id: 'SET_zCModelAniEvent-value4-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCModelAniEvent_OnInit(): zCModelAniEvent | null {
    const result: string | null = SendCommand({
      id: 'zCModelAniEvent-zCModelAniEvent_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCModelAniEvent(result) : null
  }
}
