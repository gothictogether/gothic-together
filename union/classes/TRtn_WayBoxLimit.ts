import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zTBBox3D } from './index.js'
import { oCNpc } from './index.js'

export class TRtn_WayBoxLimit extends BaseUnionObject {
  begin(): number | null {
    const result: string | null = SendCommand({
      id: 'TRtn_WayBoxLimit-begin-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_begin(a1: number): null {
    SendCommand({
      id: 'SET_TRtn_WayBoxLimit-begin-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  bbox(): zTBBox3D | null {
    const result: string | null = SendCommand({
      id: 'TRtn_WayBoxLimit-bbox-zTBBox3D-false-false',
      parentId: this.Uuid,
    })

    return result ? new zTBBox3D(result) : null
  }

  set_bbox(a1: zTBBox3D): null {
    SendCommand({
      id: 'SET_TRtn_WayBoxLimit-bbox-zTBBox3D-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  value(): number | null {
    const result: string | null = SendCommand({
      id: 'TRtn_WayBoxLimit-value-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_value(a1: number): null {
    SendCommand({
      id: 'SET_TRtn_WayBoxLimit-value-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  npc(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'TRtn_WayBoxLimit-npc-oCNpc*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  set_npc(a1: oCNpc): null {
    SendCommand({
      id: 'SET_TRtn_WayBoxLimit-npc-oCNpc*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
