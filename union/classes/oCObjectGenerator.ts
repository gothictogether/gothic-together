import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCVob } from './index.js'

export class oCObjectGenerator extends zCVob {
  speed(): number | null {
    const result: string | null = SendCommand({
      id: 'oCObjectGenerator-speed-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_speed(a1: number): null {
    SendCommand({
      id: 'SET_oCObjectGenerator-speed-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  override objectName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCObjectGenerator-objectName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  override set_objectName(a1: string): null {
    SendCommand({
      id: 'SET_oCObjectGenerator-objectName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCObjectGenerator_OnInit(): oCObjectGenerator | null {
    const result: string | null = SendCommand({
      id: 'oCObjectGenerator-oCObjectGenerator_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCObjectGenerator(result) : null
  }

  SetObjectNameGenerator(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCObjectGenerator-SetObjectName-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetObjectSpeed(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCObjectGenerator-SetObjectSpeed-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override OnTrigger(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCObjectGenerator-OnTrigger-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }
}
