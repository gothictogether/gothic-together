import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { TMobInterDirection } from './index.js'
import { zVEC3 } from './index.js'
import { zCVob } from './index.js'
import { oCNpc } from './index.js'
import { zCClassDef } from './index.js'
import { zCEventMessage } from './index.js'
import { TMobOptPos } from './index.js'
import { oCMOB } from './index.js'

export class oCMobInter extends oCMOB {
  triggerTarget(): string | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-triggerTarget-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_triggerTarget(a1: string): null {
    SendCommand({
      id: 'SET_oCMobInter-triggerTarget-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  useWithItem(): string | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-useWithItem-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_useWithItem(a1: string): null {
    SendCommand({
      id: 'SET_oCMobInter-useWithItem-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  sceme(): string | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-sceme-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_sceme(a1: string): null {
    SendCommand({
      id: 'SET_oCMobInter-sceme-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  conditionFunc(): string | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-conditionFunc-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_conditionFunc(a1: string): null {
    SendCommand({
      id: 'SET_oCMobInter-conditionFunc-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  onStateFuncName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-onStateFuncName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_onStateFuncName(a1: string): null {
    SendCommand({
      id: 'SET_oCMobInter-onStateFuncName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  state(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-state-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_state(a1: number): null {
    SendCommand({
      id: 'SET_oCMobInter-state-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  state_num(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-state_num-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_state_num(a1: number): null {
    SendCommand({
      id: 'SET_oCMobInter-state_num-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  state_target(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-state_target-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_state_target(a1: number): null {
    SendCommand({
      id: 'SET_oCMobInter-state_target-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  rewind(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-rewind-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_rewind(a1: number): null {
    SendCommand({
      id: 'SET_oCMobInter-rewind-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  mobStateAni(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-mobStateAni-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_mobStateAni(a1: number): null {
    SendCommand({
      id: 'SET_oCMobInter-mobStateAni-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  npcStateAni(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-npcStateAni-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_npcStateAni(a1: number): null {
    SendCommand({
      id: 'SET_oCMobInter-npcStateAni-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  npcsMax(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-npcsMax-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_npcsMax(a1: number): null {
    SendCommand({
      id: 'SET_oCMobInter-npcsMax-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  npcsNeeded(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-npcsNeeded-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_npcsNeeded(a1: number): null {
    SendCommand({
      id: 'SET_oCMobInter-npcsNeeded-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  npcsCurrent(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-npcsCurrent-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_npcsCurrent(a1: number): null {
    SendCommand({
      id: 'SET_oCMobInter-npcsCurrent-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  tmpState(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-tmpState-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_tmpState(a1: number): null {
    SendCommand({
      id: 'SET_oCMobInter-tmpState-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  tmpStateChanged(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-tmpStateChanged-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_tmpStateChanged(a1: number): null {
    SendCommand({
      id: 'SET_oCMobInter-tmpStateChanged-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  Direction(): TMobInterDirection | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-Direction-TMobInterDirection-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_Direction(a1: TMobInterDirection): null {
    SendCommand({
      id: 'SET_oCMobInter-Direction-TMobInterDirection-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  onInterruptReturnToSlotPos(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-onInterruptReturnToSlotPos-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_onInterruptReturnToSlotPos(a1: number): null {
    SendCommand({
      id: 'SET_oCMobInter-onInterruptReturnToSlotPos-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  startPos(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-startPos-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_startPos(a1: zVEC3): null {
    SendCommand({
      id: 'SET_oCMobInter-startPos-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  aniCombHeight(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-aniCombHeight-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_aniCombHeight(a1: number): null {
    SendCommand({
      id: 'SET_oCMobInter-aniCombHeight-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  inUseVob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-inUseVob-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_inUseVob(a1: zCVob): null {
    SendCommand({
      id: 'SET_oCMobInter-inUseVob-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  timerEnd(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-timerEnd-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_timerEnd(a1: number): null {
    SendCommand({
      id: 'SET_oCMobInter-timerEnd-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCMobInter_OnInit(): oCMobInter | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-oCMobInter_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCMobInter(result) : null
  }

  SetTempState(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-SetTempState-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  IsTempStateChanged(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-IsTempStateChanged-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetStateToTempState(): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-SetStateToTempState-void-false-',
      parentId: this.Uuid,
    })
  }

  SetMobBodyState(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-SetMobBodyState-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  HasUseWithItem(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-HasUseWithItem-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  ScanIdealPositions(): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-ScanIdealPositions-void-false-',
      parentId: this.Uuid,
    })
  }

  GetFreePosition(a1: oCNpc, a2: zVEC3): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-GetFreePosition-int-false-oCNpc*,zVEC3&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  SetHeading(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-SetHeading-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetIdealPosition(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-SetIdealPosition-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SendStateChange(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-SendStateChange-void-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  SendEndInteraction(a1: oCNpc, a2: number, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-SendEndInteraction-void-false-oCNpc*,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  StartTransitionAniNpc(a1: oCNpc, a2: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-StartTransitionAniNpc-void-false-oCNpc*,zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  IsMultiMob(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-IsMultiMob-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsAvailable(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-IsAvailable-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  MarkAsUsed(a1: number, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-MarkAsUsed-void-false-float,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override OnTrigger(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-OnTrigger-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override OnUntrigger(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-OnUntrigger-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override OnDamage(a1: zCVob, a2: zCVob, a3: number, a4: number, a5: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-OnDamage-void-false-zCVob*,zCVob*,float,int,zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
      a5: a5.toString(),
    })
  }

  override OnMessage(a1: zCEventMessage, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-OnMessage-void-false-zCEventMessage*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override OnTick(): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-OnTick-void-false-',
      parentId: this.Uuid,
    })
  }

  override GetTriggerTarget(a1: number): string | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-GetTriggerTarget-zSTRING-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? String(result) : null
  }

  override GetScemeName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-GetScemeName-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  GetState(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-GetState-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetStateNum(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-GetStateNum-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetDirection(): TMobInterDirection | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-GetDirection-TMobInterDirection-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetDirection(a1: TMobInterDirection): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-SetDirection-void-false-TMobInterDirection',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetUseWithItem(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-SetUseWithItem-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetUseWithItem(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-GetUseWithItem-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  Reset(): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-Reset-void-false-',
      parentId: this.Uuid,
    })
  }

  Interact(a1: oCNpc, a2: number, a3: number, a4: number, a5: number, a6: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-Interact-void-false-oCNpc*,int,int,int,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
      a5: a5.toString(),
      a6: a6.toString(),
    })
  }

  EndInteraction(a1: oCNpc, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-EndInteraction-void-false-oCNpc*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  InterruptInteraction(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-InterruptInteraction-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  StopInteraction(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-StopInteraction-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CanInteractWith(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-CanInteractWith-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  IsInteractingWith(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-IsInteractingWith-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  IsOccupied(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-IsOccupied-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  AI_UseMobToState(a1: oCNpc, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-AI_UseMobToState-int-false-oCNpc*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  IsIn(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-IsIn-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  IsInState(a1: oCNpc, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-IsInState-int-false-oCNpc*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  StartInteraction(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-StartInteraction-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  StartStateChange(a1: oCNpc, a2: number, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-StartStateChange-void-false-oCNpc*,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  CheckStateChange(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-CheckStateChange-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CanChangeState(a1: oCNpc, a2: number, a3: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-CanChangeState-int-false-oCNpc*,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? Number(result) : null
  }

  GetTransitionNames(a1: number, a2: number, a3: string, a4: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-GetTransitionNames-void-false-int,int,zSTRING&,zSTRING&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
    })
  }

  OnBeginStateChange(a1: oCNpc, a2: number, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-OnBeginStateChange-void-false-oCNpc*,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  OnEndStateChange(a1: oCNpc, a2: number, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-OnEndStateChange-void-false-oCNpc*,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  CallOnStateFunc(a1: oCNpc, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-CallOnStateFunc-void-false-oCNpc*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  SendCallOnStateFunc(a1: oCNpc, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-SendCallOnStateFunc-void-false-oCNpc*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  SearchFreePositionMob(a1: oCNpc, a2: number): TMobOptPos | null {
    const result: string | null = SendCommand({
      id: 'oCMobInter-SearchFreePosition-TMobOptPos*-false-oCNpc*,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new TMobOptPos(result) : null
  }
}
