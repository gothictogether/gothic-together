import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCVob } from './index.js'
import { zCAIBase } from './index.js'

export class zCAIBaseSound extends zCAIBase {
  slideSoundHandle(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIBaseSound-slideSoundHandle-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_slideSoundHandle(a1: number): null {
    SendCommand({
      id: 'SET_zCAIBaseSound-slideSoundHandle-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  slideSoundOn(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIBaseSound-slideSoundOn-char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_slideSoundOn(a1: number): null {
    SendCommand({
      id: 'SET_zCAIBaseSound-slideSoundOn-char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  RemoveSlideSound(): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIBaseSound-RemoveSlideSound-void-false-',
      parentId: this.Uuid,
    })
  }

  CheckSlideSound(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIBaseSound-CheckSlideSound-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }
}
