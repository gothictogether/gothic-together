import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zVEC3 } from './index.js'
import { zCModelNodeInst } from './index.js'

export class zCModelNode extends BaseUnionObject {
  parentNode(): zCModelNode | null {
    const result: string | null = SendCommand({
      id: 'zCModelNode-parentNode-zCModelNode*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCModelNode(result) : null
  }

  set_parentNode(a1: zCModelNode): null {
    SendCommand({
      id: 'SET_zCModelNode-parentNode-zCModelNode*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  nodeName(): string | null {
    const result: string | null = SendCommand({
      id: 'zCModelNode-nodeName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_nodeName(a1: string): null {
    SendCommand({
      id: 'SET_zCModelNode-nodeName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  nodeRotAxis(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCModelNode-nodeRotAxis-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_nodeRotAxis(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCModelNode-nodeRotAxis-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  nodeRotAngle(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelNode-nodeRotAngle-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_nodeRotAngle(a1: number): null {
    SendCommand({
      id: 'SET_zCModelNode-nodeRotAngle-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  translation(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCModelNode-translation-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_translation(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCModelNode-translation-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lastInstNode(): zCModelNodeInst | null {
    const result: string | null = SendCommand({
      id: 'zCModelNode-lastInstNode-zCModelNodeInst*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCModelNodeInst(result) : null
  }

  set_lastInstNode(a1: zCModelNodeInst): null {
    SendCommand({
      id: 'SET_zCModelNode-lastInstNode-zCModelNodeInst*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCModelNode_OnInit(): zCModelNode | null {
    const result: string | null = SendCommand({
      id: 'zCModelNode-zCModelNode_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCModelNode(result) : null
  }

  static zCModelNode_OnInit2(a1: zCModelNode): zCModelNode | null {
    const result: string | null = SendCommand({
      id: 'zCModelNode-zCModelNode_OnInit-void-false-zCModelNode const&',
      parentId: 'NULL',
      a1: a1.toString(),
    })

    return result ? new zCModelNode(result) : null
  }
}
