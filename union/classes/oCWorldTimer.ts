import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'

export class oCWorldTimer extends BaseUnionObject {
  worldTime(): number | null {
    const result: string | null = SendCommand({
      id: 'oCWorldTimer-worldTime-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_worldTime(a1: number): null {
    SendCommand({
      id: 'SET_oCWorldTimer-worldTime-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  day(): number | null {
    const result: string | null = SendCommand({
      id: 'oCWorldTimer-day-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_day(a1: number): null {
    SendCommand({
      id: 'SET_oCWorldTimer-day-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCWorldTimer_OnInit(): oCWorldTimer | null {
    const result: string | null = SendCommand({
      id: 'oCWorldTimer-oCWorldTimer_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCWorldTimer(result) : null
  }

  Timer(): void | null {
    const result: string | null = SendCommand({
      id: 'oCWorldTimer-Timer-void-false-',
      parentId: this.Uuid,
    })
  }

  GetDay(): number | null {
    const result: string | null = SendCommand({
      id: 'oCWorldTimer-GetDay-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetDay(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCWorldTimer-SetDay-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetTime(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCWorldTimer-GetTime-void-false-int&,int&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  SetTime(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCWorldTimer-SetTime-void-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  GetFullTime(): number | null {
    const result: string | null = SendCommand({
      id: 'oCWorldTimer-GetFullTime-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetFullTime(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCWorldTimer-SetFullTime-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetTimeString(): string | null {
    const result: string | null = SendCommand({
      id: 'oCWorldTimer-GetTimeString-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  IsLater(a1: number, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCWorldTimer-IsLater-int-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  IsLaterEqual(a1: number, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCWorldTimer-IsLaterEqual-int-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  IsTimeBetween(a1: number, a2: number, a3: number, a4: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCWorldTimer-IsTimeBetween-int-false-int,int,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
    })

    return result ? Number(result) : null
  }

  GetPassedTime(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCWorldTimer-GetPassedTime-int-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetSkyTime(): number | null {
    const result: string | null = SendCommand({
      id: 'oCWorldTimer-GetSkyTime-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsDay(): number | null {
    const result: string | null = SendCommand({
      id: 'oCWorldTimer-IsDay-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsNight(): number | null {
    const result: string | null = SendCommand({
      id: 'oCWorldTimer-IsNight-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }
}
