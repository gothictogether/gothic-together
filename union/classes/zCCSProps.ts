import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zTCSRunBehaviour } from './index.js'
import { zCClassDef } from './index.js'
import { zCObject } from './index.js'

export class zCCSProps extends zCObject {
  name(): string | null {
    const result: string | null = SendCommand({
      id: 'zCCSProps-name-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_name(a1: string): null {
    SendCommand({
      id: 'SET_zCCSProps-name-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  csLoop(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCSProps-csLoop-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_csLoop(a1: number): null {
    SendCommand({
      id: 'SET_zCCSProps-csLoop-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  globalCutscene(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCSProps-globalCutscene-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_globalCutscene(a1: number): null {
    SendCommand({
      id: 'SET_zCCSProps-globalCutscene-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  distance(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCSProps-distance-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_distance(a1: number): null {
    SendCommand({
      id: 'SET_zCCSProps-distance-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  hasToBeTriggerd(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCSProps-hasToBeTriggerd-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_hasToBeTriggerd(a1: number): null {
    SendCommand({
      id: 'SET_zCCSProps-hasToBeTriggerd-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  range(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCSProps-range-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_range(a1: number): null {
    SendCommand({
      id: 'SET_zCCSProps-range-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  ignore(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCSProps-ignore-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_ignore(a1: number): null {
    SendCommand({
      id: 'SET_zCCSProps-ignore-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  numLockedBlocks(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCSProps-numLockedBlocks-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_numLockedBlocks(a1: number): null {
    SendCommand({
      id: 'SET_zCCSProps-numLockedBlocks-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  stageName(): string | null {
    const result: string | null = SendCommand({
      id: 'zCCSProps-stageName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_stageName(a1: string): null {
    SendCommand({
      id: 'SET_zCCSProps-stageName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  scriptFuncOnStop(): string | null {
    const result: string | null = SendCommand({
      id: 'zCCSProps-scriptFuncOnStop-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_scriptFuncOnStop(a1: string): null {
    SendCommand({
      id: 'SET_zCCSProps-scriptFuncOnStop-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  runBehaviour(): zTCSRunBehaviour | null {
    const result: string | null = SendCommand({
      id: 'zCCSProps-runBehaviour-zTCSRunBehaviour-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_runBehaviour(a1: zTCSRunBehaviour): null {
    SendCommand({
      id: 'SET_zCCSProps-runBehaviour-zTCSRunBehaviour-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  runBehaviourValue(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCSProps-runBehaviourValue-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_runBehaviourValue(a1: number): null {
    SendCommand({
      id: 'SET_zCCSProps-runBehaviourValue-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCCSProps_OnInit(): zCCSProps | null {
    const result: string | null = SendCommand({
      id: 'zCCSProps-zCCSProps_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCCSProps(result) : null
  }

  GetName(): string | null {
    const result: string | null = SendCommand({
      id: 'zCCSProps-GetName-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  GetScriptFuncOnStop(): string | null {
    const result: string | null = SendCommand({
      id: 'zCCSProps-GetScriptFuncOnStop-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCCSProps-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  GetCheckRoleResult(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCSProps-GetCheckRoleResult-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  CheckDistance(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'zCCSProps-CheckDistance-int-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  CheckRange(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'zCCSProps-CheckRange-int-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  PrintDebugInfo(): void | null {
    const result: string | null = SendCommand({
      id: 'zCCSProps-PrintDebugInfo-void-false-',
      parentId: this.Uuid,
    })
  }
}
