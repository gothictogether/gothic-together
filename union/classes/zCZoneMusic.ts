import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { zCZone } from './index.js'

export class zCZoneMusic extends zCZone {
  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCZoneMusic-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }
}
