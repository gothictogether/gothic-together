import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCModelAni } from './index.js'
import { zVEC3 } from './index.js'
import { zTAniAttachment } from './index.js'
import { zTMdl_AniDir } from './index.js'
import { zCModel } from './index.js'

export class zCModelAniActive extends BaseUnionObject {
  protoAni(): zCModelAni | null {
    const result: string | null = SendCommand({
      id: 'zCModelAniActive-protoAni-zCModelAni*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCModelAni(result) : null
  }

  set_protoAni(a1: zCModelAni): null {
    SendCommand({
      id: 'SET_zCModelAniActive-protoAni-zCModelAni*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  nextAni(): zCModelAni | null {
    const result: string | null = SendCommand({
      id: 'zCModelAniActive-nextAni-zCModelAni*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCModelAni(result) : null
  }

  set_nextAni(a1: zCModelAni): null {
    SendCommand({
      id: 'SET_zCModelAniActive-nextAni-zCModelAni*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  advanceDir(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelAniActive-advanceDir-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_advanceDir(a1: number): null {
    SendCommand({
      id: 'SET_zCModelAniActive-advanceDir-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  actFrame(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelAniActive-actFrame-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_actFrame(a1: number): null {
    SendCommand({
      id: 'SET_zCModelAniActive-actFrame-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  actAniEvent(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelAniActive-actAniEvent-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_actAniEvent(a1: number): null {
    SendCommand({
      id: 'SET_zCModelAniActive-actAniEvent-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  combAniX(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelAniActive-combAniX-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_combAniX(a1: number): null {
    SendCommand({
      id: 'SET_zCModelAniActive-combAniX-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  combAniY(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelAniActive-combAniY-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_combAniY(a1: number): null {
    SendCommand({
      id: 'SET_zCModelAniActive-combAniY-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  isFadingOut(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelAniActive-isFadingOut-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_isFadingOut(a1: number): null {
    SendCommand({
      id: 'SET_zCModelAniActive-isFadingOut-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  isFirstTime(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelAniActive-isFirstTime-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_isFirstTime(a1: number): null {
    SendCommand({
      id: 'SET_zCModelAniActive-isFirstTime-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  nextAniOverride(): zCModelAni | null {
    const result: string | null = SendCommand({
      id: 'zCModelAniActive-nextAniOverride-zCModelAni*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCModelAni(result) : null
  }

  set_nextAniOverride(a1: zCModelAni): null {
    SendCommand({
      id: 'SET_zCModelAniActive-nextAniOverride-zCModelAni*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  blendInOverride(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelAniActive-blendInOverride-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_blendInOverride(a1: number): null {
    SendCommand({
      id: 'SET_zCModelAniActive-blendInOverride-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  blendOutOverride(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelAniActive-blendOutOverride-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_blendOutOverride(a1: number): null {
    SendCommand({
      id: 'SET_zCModelAniActive-blendOutOverride-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lastPos(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCModelAniActive-lastPos-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_lastPos(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCModelAniActive-lastPos-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  thisPos(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCModelAniActive-thisPos-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_thisPos(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCModelAniActive-thisPos-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  rotFirstTime(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelAniActive-rotFirstTime-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_rotFirstTime(a1: number): null {
    SendCommand({
      id: 'SET_zCModelAniActive-rotFirstTime-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  transWeight(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelAniActive-transWeight-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_transWeight(a1: number): null {
    SendCommand({
      id: 'SET_zCModelAniActive-transWeight-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  aniAttachment(): zTAniAttachment | null {
    const result: string | null = SendCommand({
      id: 'zCModelAniActive-aniAttachment-zTAniAttachment*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zTAniAttachment(result) : null
  }

  set_aniAttachment(a1: zTAniAttachment): null {
    SendCommand({
      id: 'SET_zCModelAniActive-aniAttachment-zTAniAttachment*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  randAniTimer(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelAniActive-randAniTimer-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_randAniTimer(a1: number): null {
    SendCommand({
      id: 'SET_zCModelAniActive-randAniTimer-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCModelAniActive_OnInit(): zCModelAniActive | null {
    const result: string | null = SendCommand({
      id: 'zCModelAniActive-zCModelAniActive_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCModelAniActive(result) : null
  }

  SetDirection(a1: zTMdl_AniDir): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelAniActive-SetDirection-void-false-zTMdl_AniDir',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetProgressPercent(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelAniActive-GetProgressPercent-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetProgressPercent(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelAniActive-SetProgressPercent-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetActFrame(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelAniActive-SetActFrame-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  DoCombineAni(a1: zCModel, a2: number, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelAniActive-DoCombineAni-void-false-zCModel*,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }
}
