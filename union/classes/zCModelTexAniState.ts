import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCModelMeshLib } from './index.js'

export class zCModelTexAniState extends BaseUnionObject {
  numNodeTex(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelTexAniState-numNodeTex-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_numNodeTex(a1: number): null {
    SendCommand({
      id: 'SET_zCModelTexAniState-numNodeTex-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCModelTexAniState_OnInit(): zCModelTexAniState | null {
    const result: string | null = SendCommand({
      id: 'zCModelTexAniState-zCModelTexAniState_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCModelTexAniState(result) : null
  }

  DeleteTexList(): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelTexAniState-DeleteTexList-void-false-',
      parentId: this.Uuid,
    })
  }

  UpdateTexList(): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelTexAniState-UpdateTexList-void-false-',
      parentId: this.Uuid,
    })
  }

  BuildTexListFromMeshLib(a1: zCModelMeshLib): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelTexAniState-BuildTexListFromMeshLib-void-false-zCModelMeshLib*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }
}
