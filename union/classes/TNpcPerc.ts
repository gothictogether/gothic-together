import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'

export class TNpcPerc extends BaseUnionObject {
  percID(): number | null {
    const result: string | null = SendCommand({
      id: 'TNpcPerc-percID-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_percID(a1: number): null {
    SendCommand({
      id: 'SET_TNpcPerc-percID-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  percFunc(): number | null {
    const result: string | null = SendCommand({
      id: 'TNpcPerc-percFunc-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_percFunc(a1: number): null {
    SendCommand({
      id: 'SET_TNpcPerc-percFunc-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
