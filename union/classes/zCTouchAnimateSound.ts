import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { zCTouchAnimate } from './index.js'

export class zCTouchAnimateSound extends zCTouchAnimate {
  override touchSoundName(): string | null {
    const result: string | null = SendCommand({
      id: 'zCTouchAnimateSound-touchSoundName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  override set_touchSoundName(a1: string): null {
    SendCommand({
      id: 'SET_zCTouchAnimateSound-touchSoundName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCTouchAnimateSound_OnInit(): zCTouchAnimateSound | null {
    const result: string | null = SendCommand({
      id: 'zCTouchAnimateSound-zCTouchAnimateSound_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCTouchAnimateSound(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCTouchAnimateSound-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override GetSoundName(): string | null {
    const result: string | null = SendCommand({
      id: 'zCTouchAnimateSound-GetSoundName-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }
}
