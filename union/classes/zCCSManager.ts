import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { zCEventMessage } from './index.js'
import { zCCSPlayer } from './index.js'
import { zCWorld } from './index.js'
import { zCCSProps } from './index.js'
import { zCObject } from './index.js'

export class zCCSManager extends zCObject {
  iterA(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCSManager-iterA-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_iterA(a1: number): null {
    SendCommand({
      id: 'SET_zCCSManager-iterA-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  iterB(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCSManager-iterB-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_iterB(a1: number): null {
    SendCommand({
      id: 'SET_zCCSManager-iterB-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  iterator(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCSManager-iterator-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_iterator(a1: number): null {
    SendCommand({
      id: 'SET_zCCSManager-iterator-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCCSManager_OnInit(): zCCSManager | null {
    const result: string | null = SendCommand({
      id: 'zCCSManager-zCCSManager_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCCSManager(result) : null
  }

  IsDeactivated(a1: string): number | null {
    const result: string | null = SendCommand({
      id: 'zCCSManager-IsDeactivated-int-false-zSTRING&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  PoolCountStart(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'zCCSManager-PoolCountStart-void-false-zSTRING&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  PoolCountStop(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'zCCSManager-PoolCountStop-void-false-zSTRING&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  PoolHasFlags(a1: string, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'zCCSManager-PoolHasFlags-int-false-zSTRING&,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  PoolClrFlags(a1: string, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCCSManager-PoolClrFlags-void-false-zSTRING&,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  PoolSetFlags(a1: string, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCCSManager-PoolSetFlags-void-false-zSTRING&,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  PoolNumPlayed(a1: string): number | null {
    const result: string | null = SendCommand({
      id: 'zCCSManager-PoolNumPlayed-int-false-zSTRING&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  PoolAllowedToStart(a1: string): number | null {
    const result: string | null = SendCommand({
      id: 'zCCSManager-PoolAllowedToStart-int-false-zSTRING&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  PoolResetAll(): void | null {
    const result: string | null = SendCommand({
      id: 'zCCSManager-PoolResetAll-void-false-',
      parentId: this.Uuid,
    })
  }

  PoolResetByHour(): void | null {
    const result: string | null = SendCommand({
      id: 'zCCSManager-PoolResetByHour-void-false-',
      parentId: this.Uuid,
    })
  }

  PoolResetByDay(): void | null {
    const result: string | null = SendCommand({
      id: 'zCCSManager-PoolResetByDay-void-false-',
      parentId: this.Uuid,
    })
  }

  AddCommand(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'zCCSManager-AddCommand-void-false-zSTRING&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetNumOfShortCom(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCSManager-GetNumOfShortCom-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetShortCom(a1: number): string | null {
    const result: string | null = SendCommand({
      id: 'zCCSManager-GetShortCom-zSTRING-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? String(result) : null
  }

  LibForceToLoad(): void | null {
    const result: string | null = SendCommand({
      id: 'zCCSManager-LibForceToLoad-void-false-',
      parentId: this.Uuid,
    })
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCCSManager-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  CreateMessage(a1: number): zCEventMessage | null {
    const result: string | null = SendCommand({
      id: 'zCCSManager-CreateMessage-zCEventMessage*-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new zCEventMessage(result) : null
  }

  CreateOuMessage(): zCEventMessage | null {
    const result: string | null = SendCommand({
      id: 'zCCSManager-CreateOuMessage-zCEventMessage*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCEventMessage(result) : null
  }

  CreateCutscenePlayer(a1: zCWorld): zCCSPlayer | null {
    const result: string | null = SendCommand({
      id: 'zCCSManager-CreateCutscenePlayer-zCCSPlayer*-false-zCWorld*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new zCCSPlayer(result) : null
  }

  CreateProperties(): zCCSProps | null {
    const result: string | null = SendCommand({
      id: 'zCCSManager-CreateProperties-zCCSProps*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCCSProps(result) : null
  }

  RemoveCutscenePlayer(a1: zCCSPlayer): void | null {
    const result: string | null = SendCommand({
      id: 'zCCSManager-RemoveCutscenePlayer-void-false-zCCSPlayer*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  LibIsLoaded(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCSManager-LibIsLoaded-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  LibDelOU(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCCSManager-LibDelOU-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  LibNullOU(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCCSManager-LibNullOU-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  LibLoad(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCCSManager-LibLoad-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  LibStore(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCCSManager-LibStore-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  LibValidateOU(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'zCCSManager-LibValidateOU-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  LibValidateOU2(a1: string): number | null {
    const result: string | null = SendCommand({
      id: 'zCCSManager-LibValidateOU-int-false-zSTRING&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  LibGetSvmModuleName(a1: number): string | null {
    const result: string | null = SendCommand({
      id: 'zCCSManager-LibGetSvmModuleName-zSTRING-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? String(result) : null
  }

  LibIsSvmModuleRunning(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'zCCSManager-LibIsSvmModuleRunning-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  LibSvmModuleStart(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'zCCSManager-LibSvmModuleStart-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  LibSvmModuleStop(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'zCCSManager-LibSvmModuleStop-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  InsertPlayerInList(a1: zCCSPlayer): void | null {
    const result: string | null = SendCommand({
      id: 'zCCSManager-InsertPlayerInList-void-false-zCCSPlayer*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RemovePlayerFromList(a1: zCCSPlayer): void | null {
    const result: string | null = SendCommand({
      id: 'zCCSManager-RemovePlayerFromList-void-false-zCCSPlayer*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  LibCheckLoaded(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCCSManager-LibCheckLoaded-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }
}
