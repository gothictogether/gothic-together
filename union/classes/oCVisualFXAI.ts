import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCVob } from './index.js'
import { zCWorld } from './index.js'
import { zCAIBase } from './index.js'

export class oCVisualFXAI extends zCAIBase {
  vob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFXAI-vob-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_vob(a1: zCVob): null {
    SendCommand({
      id: 'SET_oCVisualFXAI-vob-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  delete_it(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFXAI-delete_it-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_delete_it(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFXAI-delete_it-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  override DoAI(a1: zCVob, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFXAI-DoAI-void-false-zCVob*,int&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override HostVobAddedToWorld(a1: zCVob, a2: zCWorld): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFXAI-HostVobAddedToWorld-void-false-zCVob*,zCWorld*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override GetIsProjectile(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFXAI-GetIsProjectile-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }
}
