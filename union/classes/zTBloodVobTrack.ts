import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCVob } from './index.js'

export class zTBloodVobTrack extends BaseUnionObject {
  bloodVob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'zTBloodVobTrack-bloodVob-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_bloodVob(a1: zCVob): null {
    SendCommand({
      id: 'SET_zTBloodVobTrack-bloodVob-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  alpha(): number | null {
    const result: string | null = SendCommand({
      id: 'zTBloodVobTrack-alpha-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_alpha(a1: number): null {
    SendCommand({
      id: 'SET_zTBloodVobTrack-alpha-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
