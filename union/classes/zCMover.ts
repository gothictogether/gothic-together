import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zVEC3 } from './index.js'
import { zTMoverState } from './index.js'
import { zCModel } from './index.js'
import { zTMoverAniType } from './index.js'
import { zTMoverBehavior } from './index.js'
import { zTTouchBehavior } from './index.js'
import { zTPosLerpType } from './index.js'
import { zTSpeedType } from './index.js'
import { zCVob } from './index.js'
import { zCClassDef } from './index.js'
import { zCEventMessage } from './index.js'
import { zCTrigger } from './index.js'

export class zCMover extends zCTrigger {
  actKeyPosDelta(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCMover-actKeyPosDelta-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_actKeyPosDelta(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCMover-actKeyPosDelta-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  actKeyframeF(): number | null {
    const result: string | null = SendCommand({
      id: 'zCMover-actKeyframeF-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_actKeyframeF(a1: number): null {
    SendCommand({
      id: 'SET_zCMover-actKeyframeF-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  actKeyframe(): number | null {
    const result: string | null = SendCommand({
      id: 'zCMover-actKeyframe-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_actKeyframe(a1: number): null {
    SendCommand({
      id: 'SET_zCMover-actKeyframe-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  nextKeyframe(): number | null {
    const result: string | null = SendCommand({
      id: 'zCMover-nextKeyframe-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_nextKeyframe(a1: number): null {
    SendCommand({
      id: 'SET_zCMover-nextKeyframe-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  moveSpeedUnit(): number | null {
    const result: string | null = SendCommand({
      id: 'zCMover-moveSpeedUnit-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_moveSpeedUnit(a1: number): null {
    SendCommand({
      id: 'SET_zCMover-moveSpeedUnit-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  advanceDir(): number | null {
    const result: string | null = SendCommand({
      id: 'zCMover-advanceDir-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_advanceDir(a1: number): null {
    SendCommand({
      id: 'SET_zCMover-advanceDir-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  moverState(): zTMoverState | null {
    const result: string | null = SendCommand({
      id: 'zCMover-moverState-zTMoverState-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_moverState(a1: zTMoverState): null {
    SendCommand({
      id: 'SET_zCMover-moverState-zTMoverState-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  numTriggerEvents(): number | null {
    const result: string | null = SendCommand({
      id: 'zCMover-numTriggerEvents-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_numTriggerEvents(a1: number): null {
    SendCommand({
      id: 'SET_zCMover-numTriggerEvents-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  stayOpenTimeDest(): number | null {
    const result: string | null = SendCommand({
      id: 'zCMover-stayOpenTimeDest-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_stayOpenTimeDest(a1: number): null {
    SendCommand({
      id: 'SET_zCMover-stayOpenTimeDest-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  model(): zCModel | null {
    const result: string | null = SendCommand({
      id: 'zCMover-model-zCModel*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCModel(result) : null
  }

  set_model(a1: zCModel): null {
    SendCommand({
      id: 'SET_zCMover-model-zCModel*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  soundMovingHandle(): number | null {
    const result: string | null = SendCommand({
      id: 'zCMover-soundMovingHandle-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_soundMovingHandle(a1: number): null {
    SendCommand({
      id: 'SET_zCMover-soundMovingHandle-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  moveSpeed(): number | null {
    const result: string | null = SendCommand({
      id: 'zCMover-moveSpeed-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_moveSpeed(a1: number): null {
    SendCommand({
      id: 'SET_zCMover-moveSpeed-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  stayOpenTimeSec(): number | null {
    const result: string | null = SendCommand({
      id: 'zCMover-stayOpenTimeSec-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_stayOpenTimeSec(a1: number): null {
    SendCommand({
      id: 'SET_zCMover-stayOpenTimeSec-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  touchBlockerDamage(): number | null {
    const result: string | null = SendCommand({
      id: 'zCMover-touchBlockerDamage-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_touchBlockerDamage(a1: number): null {
    SendCommand({
      id: 'SET_zCMover-touchBlockerDamage-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  moverLocked(): number | null {
    const result: string | null = SendCommand({
      id: 'zCMover-moverLocked-char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_moverLocked(a1: number): null {
    SendCommand({
      id: 'SET_zCMover-moverLocked-char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  autoLinkEnabled(): number | null {
    const result: string | null = SendCommand({
      id: 'zCMover-autoLinkEnabled-char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_autoLinkEnabled(a1: number): null {
    SendCommand({
      id: 'SET_zCMover-autoLinkEnabled-char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  autoRotate(): number | null {
    const result: string | null = SendCommand({
      id: 'zCMover-autoRotate-char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_autoRotate(a1: number): null {
    SendCommand({
      id: 'SET_zCMover-autoRotate-char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  moverAniType(): zTMoverAniType | null {
    const result: string | null = SendCommand({
      id: 'zCMover-moverAniType-zTMoverAniType-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_moverAniType(a1: zTMoverAniType): null {
    SendCommand({
      id: 'SET_zCMover-moverAniType-zTMoverAniType-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  moverBehavior(): zTMoverBehavior | null {
    const result: string | null = SendCommand({
      id: 'zCMover-moverBehavior-zTMoverBehavior-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_moverBehavior(a1: zTMoverBehavior): null {
    SendCommand({
      id: 'SET_zCMover-moverBehavior-zTMoverBehavior-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  touchBehavior(): zTTouchBehavior | null {
    const result: string | null = SendCommand({
      id: 'zCMover-touchBehavior-zTTouchBehavior-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_touchBehavior(a1: zTTouchBehavior): null {
    SendCommand({
      id: 'SET_zCMover-touchBehavior-zTTouchBehavior-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  posLerpType(): zTPosLerpType | null {
    const result: string | null = SendCommand({
      id: 'zCMover-posLerpType-zTPosLerpType-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_posLerpType(a1: zTPosLerpType): null {
    SendCommand({
      id: 'SET_zCMover-posLerpType-zTPosLerpType-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  speedType(): zTSpeedType | null {
    const result: string | null = SendCommand({
      id: 'zCMover-speedType-zTSpeedType-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_speedType(a1: zTSpeedType): null {
    SendCommand({
      id: 'SET_zCMover-speedType-zTSpeedType-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  soundOpenStart(): string | null {
    const result: string | null = SendCommand({
      id: 'zCMover-soundOpenStart-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_soundOpenStart(a1: string): null {
    SendCommand({
      id: 'SET_zCMover-soundOpenStart-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  soundOpenEnd(): string | null {
    const result: string | null = SendCommand({
      id: 'zCMover-soundOpenEnd-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_soundOpenEnd(a1: string): null {
    SendCommand({
      id: 'SET_zCMover-soundOpenEnd-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  soundMoving(): string | null {
    const result: string | null = SendCommand({
      id: 'zCMover-soundMoving-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_soundMoving(a1: string): null {
    SendCommand({
      id: 'SET_zCMover-soundMoving-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  soundCloseStart(): string | null {
    const result: string | null = SendCommand({
      id: 'zCMover-soundCloseStart-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_soundCloseStart(a1: string): null {
    SendCommand({
      id: 'SET_zCMover-soundCloseStart-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  soundCloseEnd(): string | null {
    const result: string | null = SendCommand({
      id: 'zCMover-soundCloseEnd-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_soundCloseEnd(a1: string): null {
    SendCommand({
      id: 'SET_zCMover-soundCloseEnd-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  soundLock(): string | null {
    const result: string | null = SendCommand({
      id: 'zCMover-soundLock-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_soundLock(a1: string): null {
    SendCommand({
      id: 'SET_zCMover-soundLock-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  soundUnlock(): string | null {
    const result: string | null = SendCommand({
      id: 'zCMover-soundUnlock-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_soundUnlock(a1: string): null {
    SendCommand({
      id: 'SET_zCMover-soundUnlock-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  soundUseLocked(): string | null {
    const result: string | null = SendCommand({
      id: 'zCMover-soundUseLocked-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_soundUseLocked(a1: string): null {
    SendCommand({
      id: 'SET_zCMover-soundUseLocked-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCMover_OnInit(): zCMover | null {
    const result: string | null = SendCommand({
      id: 'zCMover-zCMover_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCMover(result) : null
  }

  override ClearStateInternals(): void | null {
    const result: string | null = SendCommand({
      id: 'zCMover-ClearStateInternals-void-false-',
      parentId: this.Uuid,
    })
  }

  UpdateInternals(): void | null {
    const result: string | null = SendCommand({
      id: 'zCMover-UpdateInternals-void-false-',
      parentId: this.Uuid,
    })
  }

  AdvanceKeyframe_KF(): void | null {
    const result: string | null = SendCommand({
      id: 'zCMover-AdvanceKeyframe_KF-void-false-',
      parentId: this.Uuid,
    })
  }

  SetToKeyframe_KF(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCMover-SetToKeyframe_KF-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  InterpolateKeyframes_KF(): void | null {
    const result: string | null = SendCommand({
      id: 'zCMover-InterpolateKeyframes_KF-void-false-',
      parentId: this.Uuid,
    })
  }

  AdvanceMover(): void | null {
    const result: string | null = SendCommand({
      id: 'zCMover-AdvanceMover-void-false-',
      parentId: this.Uuid,
    })
  }

  SetToKeyframe(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCMover-SetToKeyframe-void-false-float,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  InvertMovement(): void | null {
    const result: string | null = SendCommand({
      id: 'zCMover-InvertMovement-void-false-',
      parentId: this.Uuid,
    })
  }

  StartMovingSound(): void | null {
    const result: string | null = SendCommand({
      id: 'zCMover-StartMovingSound-void-false-',
      parentId: this.Uuid,
    })
  }

  MoveToKeyframe(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCMover-MoveToKeyframe-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  DoOpen(): void | null {
    const result: string | null = SendCommand({
      id: 'zCMover-DoOpen-void-false-',
      parentId: this.Uuid,
    })
  }

  FinishedOpening(): void | null {
    const result: string | null = SendCommand({
      id: 'zCMover-FinishedOpening-void-false-',
      parentId: this.Uuid,
    })
  }

  DoClose(): void | null {
    const result: string | null = SendCommand({
      id: 'zCMover-DoClose-void-false-',
      parentId: this.Uuid,
    })
  }

  FinishedClosing(): void | null {
    const result: string | null = SendCommand({
      id: 'zCMover-FinishedClosing-void-false-',
      parentId: this.Uuid,
    })
  }

  Unlock(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCMover-Unlock-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  Lock(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCMover-Lock-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  IsKeyToThisMover(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'zCMover-IsKeyToThisMover-int-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  TriggerMover(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCMover-TriggerMover-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCMover-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override OnTrigger(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCMover-OnTrigger-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override OnUntrigger(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCMover-OnUntrigger-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override OnTouch(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCMover-OnTouch-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override OnDamage(a1: zCVob, a2: zCVob, a3: number, a4: number, a5: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'zCMover-OnDamage-void-false-zCVob*,zCVob*,float,int,zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
      a5: a5.toString(),
    })
  }

  override OnMessage(a1: zCEventMessage, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCMover-OnMessage-void-false-zCEventMessage*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override OnTick(): void | null {
    const result: string | null = SendCommand({
      id: 'zCMover-OnTick-void-false-',
      parentId: this.Uuid,
    })
  }

  override PostLoad(): void | null {
    const result: string | null = SendCommand({
      id: 'zCMover-PostLoad-void-false-',
      parentId: this.Uuid,
    })
  }

  override CanThisCollideWith(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'zCMover-CanThisCollideWith-int-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }
}
