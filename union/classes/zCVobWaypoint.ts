import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { zCVob } from './index.js'

export class zCVobWaypoint extends zCVob {
  static zCVobWaypoint_OnInit(): zCVobWaypoint | null {
    const result: string | null = SendCommand({
      id: 'zCVobWaypoint-zCVobWaypoint_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCVobWaypoint(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCVobWaypoint-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }
}
