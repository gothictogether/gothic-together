import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zVEC3 } from './index.js'
import { zCClassDef } from './index.js'
import { zCVob } from './index.js'
import { zCEffect } from './index.js'

export class zCEarthquake extends zCEffect {
  radiusSquare(): number | null {
    const result: string | null = SendCommand({
      id: 'zCEarthquake-radiusSquare-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_radiusSquare(a1: number): null {
    SendCommand({
      id: 'SET_zCEarthquake-radiusSquare-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  timeMSec(): number | null {
    const result: string | null = SendCommand({
      id: 'zCEarthquake-timeMSec-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_timeMSec(a1: number): null {
    SendCommand({
      id: 'SET_zCEarthquake-timeMSec-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  amplitude(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCEarthquake-amplitude-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_amplitude(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCEarthquake-amplitude-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCEarthquake_OnInit(): zCEarthquake | null {
    const result: string | null = SendCommand({
      id: 'zCEarthquake-zCEarthquake_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCEarthquake(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCEarthquake-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override OnTrigger(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCEarthquake-OnTrigger-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override OnUntrigger(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCEarthquake-OnUntrigger-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }
}
