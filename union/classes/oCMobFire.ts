import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCVob } from './index.js'
import { zCClassDef } from './index.js'
import { zVEC3 } from './index.js'
import { oCNpc } from './index.js'
import { oCMobInter } from './index.js'

export class oCMobFire extends oCMobInter {
  fireSlot(): string | null {
    const result: string | null = SendCommand({
      id: 'oCMobFire-fireSlot-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_fireSlot(a1: string): null {
    SendCommand({
      id: 'SET_oCMobFire-fireSlot-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fireVobtreeName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCMobFire-fireVobtreeName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_fireVobtreeName(a1: string): null {
    SendCommand({
      id: 'SET_oCMobFire-fireVobtreeName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fireVobtree(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCMobFire-fireVobtree-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_fireVobtree(a1: zCVob): null {
    SendCommand({
      id: 'SET_oCMobFire-fireVobtree-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCMobFire_OnInit(): oCMobFire | null {
    const result: string | null = SendCommand({
      id: 'oCMobFire-oCMobFire_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCMobFire(result) : null
  }

  DeleteEffects(): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobFire-DeleteEffects-void-false-',
      parentId: this.Uuid,
    })
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCMobFire-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override OnTrigger(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobFire-OnTrigger-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override OnUntrigger(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobFire-OnUntrigger-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override OnDamage(a1: zCVob, a2: zCVob, a3: number, a4: number, a5: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobFire-OnDamage-void-false-zCVob*,zCVob*,float,int,zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
      a5: a5.toString(),
    })
  }

  override OnEndStateChange(a1: oCNpc, a2: number, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobFire-OnEndStateChange-void-false-oCNpc*,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  PreSave(): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobFire-PreSave-void-false-',
      parentId: this.Uuid,
    })
  }

  PostSave(): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobFire-PostSave-void-false-',
      parentId: this.Uuid,
    })
  }
}
