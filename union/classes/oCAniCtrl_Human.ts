import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { oCNpc } from './index.js'
import { zCVob } from './index.js'
import { zVEC3 } from './index.js'
import { zCModelAni } from './index.js'
import { zCModelAniActive } from './index.js'
import { oCItem } from './index.js'
import { zCClassDef } from './index.js'
import { zCAIPlayer } from './index.js'

export class oCAniCtrl_Human extends zCAIPlayer {
  angle_slide1(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-angle_slide1-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_angle_slide1(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-angle_slide1-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  angle_slide2(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-angle_slide2-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_angle_slide2(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-angle_slide2-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  angle_heading(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-angle_heading-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_angle_heading(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-angle_heading-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  angle_horiz(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-angle_horiz-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_angle_horiz(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-angle_horiz-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  angle_ground(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-angle_ground-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_angle_ground(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-angle_ground-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  npc(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-npc-oCNpc*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  set_npc(a1: oCNpc): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-npc-oCNpc*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  targetVob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-targetVob-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_targetVob(a1: zCVob): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-targetVob-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  stopTurnVob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-stopTurnVob-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_stopTurnVob(a1: zCVob): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-stopTurnVob-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  stopTurnVobSign(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-stopTurnVobSign-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_stopTurnVobSign(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-stopTurnVobSign-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  actionMode(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-actionMode-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_actionMode(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-actionMode-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  wmode(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-wmode-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_wmode(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-wmode-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  wmode_last(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-wmode_last-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_wmode_last(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-wmode_last-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  wmode_selected(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-wmode_selected-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_wmode_selected(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-wmode_selected-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  changeweapon(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-changeweapon-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_changeweapon(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-changeweapon-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  walkmode(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-walkmode-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_walkmode(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-walkmode-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  nextwalkmode(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-nextwalkmode-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_nextwalkmode(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-nextwalkmode-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lastRunWalkSneak(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-lastRunWalkSneak-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_lastRunWalkSneak(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-lastRunWalkSneak-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  always_walk(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-always_walk-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_always_walk(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-always_walk-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  do_jump(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-do_jump-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_do_jump(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-do_jump-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  defaultSurfaceAlign(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-defaultSurfaceAlign-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_defaultSurfaceAlign(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-defaultSurfaceAlign-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  autoRollDirection(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-autoRollDirection-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_autoRollDirection(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-autoRollDirection-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lookTargetx(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-lookTargetx-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_lookTargetx(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-lookTargetx-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lookTargety(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-lookTargety-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_lookTargety(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-lookTargety-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  distance(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-distance-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_distance(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-distance-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  hitpos(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-hitpos-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_hitpos(a1: zVEC3): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-hitpos-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  limbname(): string | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-limbname-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_limbname(a1: string): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-limbname-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  paradeBeginFrame(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-paradeBeginFrame-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_paradeBeginFrame(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-paradeBeginFrame-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  paradeEndFrame(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-paradeEndFrame-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_paradeEndFrame(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-paradeEndFrame-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  canEnableNextCombo(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-canEnableNextCombo-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_canEnableNextCombo(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-canEnableNextCombo-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  endCombo(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-endCombo-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_endCombo(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-endCombo-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  comboCanHit(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-comboCanHit-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_comboCanHit(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-comboCanHit-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  hitPosUsed(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-hitPosUsed-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_hitPosUsed(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-hitPosUsed-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  hitGraphical(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-hitGraphical-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_hitGraphical(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-hitGraphical-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  canDoCollisionFX(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-canDoCollisionFX-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_canDoCollisionFX(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-canDoCollisionFX-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  comboNr(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-comboNr-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_comboNr(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-comboNr-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  comboMax(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-comboMax-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_comboMax(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-comboMax-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lastHitAniFrame(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-lastHitAniFrame-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_lastHitAniFrame(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-lastHitAniFrame-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  hitAniID(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-hitAniID-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_hitAniID(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-hitAniID-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  hitTarget(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-hitTarget-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_hitTarget(a1: zCVob): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-hitTarget-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  anioffset_lastper(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-anioffset_lastper-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_anioffset_lastper(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-anioffset_lastper-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  anioffset_thisper(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-anioffset_thisper-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_anioffset_thisper(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-anioffset_thisper-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  anioffset(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-anioffset-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_anioffset(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-anioffset-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  anioffset_ani(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-anioffset_ani-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_anioffset_ani(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-anioffset_ani-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  s_dead1(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-s_dead1-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_s_dead1(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-s_dead1-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  s_dead2(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-s_dead2-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_s_dead2(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-s_dead2-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  s_hang(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-s_hang-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_s_hang(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-s_hang-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_hang_2_stand(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_hang_2_stand-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_hang_2_stand(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_hang_2_stand-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_stand_2_jumpuplow(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_stand_2_jumpuplow-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_stand_2_jumpuplow(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_stand_2_jumpuplow-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  s_jumpuplow(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-s_jumpuplow-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_s_jumpuplow(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-s_jumpuplow-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_jumpuplow_2_stand(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_jumpuplow_2_stand-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_jumpuplow_2_stand(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_jumpuplow_2_stand-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_stand_2_jumpupmid(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_stand_2_jumpupmid-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_stand_2_jumpupmid(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_stand_2_jumpupmid-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  s_jumpupmid(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-s_jumpupmid-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_s_jumpupmid(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-s_jumpupmid-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_jumpupmid_2_stand(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_jumpupmid_2_stand-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_jumpupmid_2_stand(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_jumpupmid_2_stand-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_stumble(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_stumble-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_stumble(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_stumble-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_stumbleb(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_stumbleb-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_stumbleb(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_stumbleb-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_fallen_2_stand(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_fallen_2_stand-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_fallen_2_stand(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_fallen_2_stand-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_fallenb_2_stand(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_fallenb_2_stand-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_fallenb_2_stand(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_fallenb_2_stand-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_walk_2_walkwl(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_walk_2_walkwl-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_walk_2_walkwl(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_walk_2_walkwl-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_walkwl_2_walk(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_walkwl_2_walk-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_walkwl_2_walk(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_walkwl_2_walk-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  s_walkwl(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-s_walkwl-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_s_walkwl(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-s_walkwl-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_walkwl_2_walkwr(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_walkwl_2_walkwr-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_walkwl_2_walkwr(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_walkwl_2_walkwr-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_walkwr_2_walkwl(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_walkwr_2_walkwl-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_walkwr_2_walkwl(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_walkwr_2_walkwl-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  s_walkwr(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-s_walkwr-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_s_walkwr(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-s_walkwr-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_walkwr_2_walk(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_walkwr_2_walk-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_walkwr_2_walk(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_walkwr_2_walk-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_walk_2_walkwbl(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_walk_2_walkwbl-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_walk_2_walkwbl(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_walk_2_walkwbl-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_walkwbl_2_walk(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_walkwbl_2_walk-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_walkwbl_2_walk(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_walkwbl_2_walk-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  s_walkwbl(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-s_walkwbl-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_s_walkwbl(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-s_walkwbl-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_walkwbl_2_walkwbr(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_walkwbl_2_walkwbr-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_walkwbl_2_walkwbr(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_walkwbl_2_walkwbr-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_walkwbr_2_walkwbl(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_walkwbr_2_walkwbl-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_walkwbr_2_walkwbl(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_walkwbr_2_walkwbl-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  s_walkwbr(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-s_walkwbr-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_s_walkwbr(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-s_walkwbr-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_walkwbr_2_walk(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_walkwbr_2_walk-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_walkwbr_2_walk(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_walkwbr_2_walk-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _s_walk(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_s_walk-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__s_walk(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_s_walk-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _t_walk_2_walkl(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_t_walk_2_walkl-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__t_walk_2_walkl(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_t_walk_2_walkl-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _t_walkl_2_walk(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_t_walkl_2_walk-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__t_walkl_2_walk(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_t_walkl_2_walk-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _s_walkl(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_s_walkl-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__s_walkl(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_s_walkl-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _t_walkl_2_walkr(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_t_walkl_2_walkr-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__t_walkl_2_walkr(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_t_walkl_2_walkr-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _t_walkr_2_walkl(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_t_walkr_2_walkl-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__t_walkr_2_walkl(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_t_walkr_2_walkl-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _s_walkr(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_s_walkr-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__s_walkr(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_s_walkr-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _t_walkr_2_walk(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_t_walkr_2_walk-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__t_walkr_2_walk(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_t_walkr_2_walk-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _t_turnl(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_t_turnl-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__t_turnl(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_t_turnl-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _t_turnr(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_t_turnr-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__t_turnr(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_t_turnr-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _t_strafel(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_t_strafel-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__t_strafel(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_t_strafel-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _t_strafer(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_t_strafer-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__t_strafer(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_t_strafer-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _t_walk_2_walkbl(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_t_walk_2_walkbl-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__t_walk_2_walkbl(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_t_walk_2_walkbl-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _t_walkbl_2_walk(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_t_walkbl_2_walk-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__t_walkbl_2_walk(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_t_walkbl_2_walk-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _s_walkbl(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_s_walkbl-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__s_walkbl(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_s_walkbl-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _t_walkbl_2_walkbr(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_t_walkbl_2_walkbr-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__t_walkbl_2_walkbr(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_t_walkbl_2_walkbr-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _t_walkbr_2_walkbl(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_t_walkbr_2_walkbl-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__t_walkbr_2_walkbl(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_t_walkbr_2_walkbl-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _s_walkbr(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_s_walkbr-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__s_walkbr(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_s_walkbr-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _t_walkbr_2_walk(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_t_walkbr_2_walk-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__t_walkbr_2_walk(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_t_walkbr_2_walk-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  s_jumpstand(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-s_jumpstand-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_s_jumpstand(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-s_jumpstand-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_stand_2_jumpstand(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_stand_2_jumpstand-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_stand_2_jumpstand(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_stand_2_jumpstand-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_jumpstand_2_stand(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_jumpstand_2_stand-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_jumpstand_2_stand(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_jumpstand_2_stand-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _t_jumpb(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_t_jumpb-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__t_jumpb(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_t_jumpb-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _t_stand_2_jump(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_t_stand_2_jump-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__t_stand_2_jump(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_t_stand_2_jump-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _s_jump(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_s_jump-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__s_jump(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_s_jump-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_jump_2_stand(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_jump_2_stand-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_jump_2_stand(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_jump_2_stand-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _t_stand_2_jumpup(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_t_stand_2_jumpup-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__t_stand_2_jumpup(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_t_stand_2_jumpup-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _s_jumpup(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_s_jumpup-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__s_jumpup(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_s_jumpup-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _t_jumpup_2_falldn(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_t_jumpup_2_falldn-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__t_jumpup_2_falldn(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_t_jumpup_2_falldn-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _t_jump_2_falldn(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_t_jump_2_falldn-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__t_jump_2_falldn(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_t_jump_2_falldn-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_walkwl_2_swimf(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_walkwl_2_swimf-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_walkwl_2_swimf(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_walkwl_2_swimf-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  s_swimf(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-s_swimf-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_s_swimf(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-s_swimf-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_swimf_2_walkwl(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_swimf_2_walkwl-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_swimf_2_walkwl(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_swimf_2_walkwl-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_walkwbl_2_swimb(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_walkwbl_2_swimb-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_walkwbl_2_swimb(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_walkwbl_2_swimb-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  s_swimb(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-s_swimb-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_s_swimb(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-s_swimb-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_swimb_2_walkwbl(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_swimb_2_walkwbl-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_swimb_2_walkwbl(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_swimb_2_walkwbl-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_swimf_2_swim(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_swimf_2_swim-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_swimf_2_swim(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_swimf_2_swim-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  s_swim(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-s_swim-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_s_swim(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-s_swim-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_swim_2_swimf(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_swim_2_swimf-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_swim_2_swimf(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_swim_2_swimf-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_swim_2_swimb(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_swim_2_swimb-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_swim_2_swimb(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_swim_2_swimb-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_swimb_2_swim(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_swimb_2_swim-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_swimb_2_swim(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_swimb_2_swim-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_warn(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_warn-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_warn(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_warn-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_swim_2_dive(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_swim_2_dive-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_swim_2_dive(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_swim_2_dive-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  s_dive(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-s_dive-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_s_dive(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-s_dive-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_divef_2_swim(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_divef_2_swim-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_divef_2_swim(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_divef_2_swim-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_dive_2_divef(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_dive_2_divef-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_dive_2_divef(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_dive_2_divef-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  s_divef(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-s_divef-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_s_divef(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-s_divef-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_divef_2_dive(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_divef_2_dive-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_divef_2_dive(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_divef_2_dive-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_dive_2_drowned(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_dive_2_drowned-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_dive_2_drowned(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_dive_2_drowned-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  s_drowned(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-s_drowned-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_s_drowned(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-s_drowned-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_swimturnl(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_swimturnl-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_swimturnl(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_swimturnl-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_swimturnr(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_swimturnr-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_swimturnr(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_swimturnr-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_diveturnl(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_diveturnl-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_diveturnl(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_diveturnl-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_diveturnr(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_diveturnr-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_diveturnr(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_diveturnr-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _t_walkl_2_aim(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_t_walkl_2_aim-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__t_walkl_2_aim(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_t_walkl_2_aim-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _t_walkr_2_aim(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_t_walkr_2_aim-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__t_walkr_2_aim(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_t_walkr_2_aim-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _t_walk_2_aim(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_t_walk_2_aim-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__t_walk_2_aim(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_t_walk_2_aim-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _s_aim(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_s_aim-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__s_aim(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_s_aim-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _t_aim_2_walk(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_t_aim_2_walk-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__t_aim_2_walk(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_t_aim_2_walk-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _t_hitl(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_t_hitl-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__t_hitl(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_t_hitl-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _t_hitr(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_t_hitr-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__t_hitr(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_t_hitr-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _t_hitback(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_t_hitback-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__t_hitback(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_t_hitback-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _t_hitf(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_t_hitf-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__t_hitf(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_t_hitf-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _t_hitfstep(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_t_hitfstep-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__t_hitfstep(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_t_hitfstep-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _s_hitf(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_s_hitf-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__s_hitf(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_s_hitf-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _t_aim_2_defend(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_t_aim_2_defend-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__t_aim_2_defend(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_t_aim_2_defend-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _s_defend(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_s_defend-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__s_defend(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_s_defend-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _t_defend_2_aim(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_t_defend_2_aim-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__t_defend_2_aim(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_t_defend_2_aim-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _t_paradeL(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_t_paradeL-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__t_paradeL(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_t_paradeL-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _t_paradeM(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_t_paradeM-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__t_paradeM(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_t_paradeM-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _t_paradeS(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_t_paradeS-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__t_paradeS(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_t_paradeS-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _t_hitfrun(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_t_hitfrun-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__t_hitfrun(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_t_hitfrun-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_stand_2_iaim(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_stand_2_iaim-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_stand_2_iaim(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_stand_2_iaim-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  s_iaim(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-s_iaim-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_s_iaim(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-s_iaim-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_iaim_2_stand(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_iaim_2_stand-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_iaim_2_stand(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_iaim_2_stand-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_iaim_2_idrop(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_iaim_2_idrop-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_iaim_2_idrop(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_iaim_2_idrop-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  s_idrop(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-s_idrop-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_s_idrop(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-s_idrop-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_idrop_2_stand(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_idrop_2_stand-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_idrop_2_stand(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_idrop_2_stand-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_iaim_2_ithrow(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_iaim_2_ithrow-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_iaim_2_ithrow(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_iaim_2_ithrow-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  s_ithrow(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-s_ithrow-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_s_ithrow(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-s_ithrow-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_ithrow_2_stand(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_ithrow_2_stand-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_ithrow_2_stand(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_ithrow_2_stand-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_stand_2_iget(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_stand_2_iget-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_stand_2_iget(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_stand_2_iget-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  s_iget(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-s_iget-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_s_iget(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-s_iget-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_iget_2_stand(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_iget_2_stand-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_iget_2_stand(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_iget_2_stand-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  s_oget(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-s_oget-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_s_oget(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-s_oget-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _t_stand_2_torch(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_t_stand_2_torch-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__t_stand_2_torch(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_t_stand_2_torch-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _s_torch(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_s_torch-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__s_torch(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_s_torch-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _t_torch_2_stand(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_t_torch_2_stand-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__t_torch_2_stand(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_t_torch_2_stand-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  hitani(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-hitani-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_hitani(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-hitani-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  help(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-help-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_help(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-help-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  help1(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-help1-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_help1(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-help1-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  help2(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-help2-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_help2(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-help2-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  s_fall(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-s_fall-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_s_fall(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-s_fall-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  s_fallb(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-s_fallb-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_s_fallb(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-s_fallb-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  s_fallen(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-s_fallen-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_s_fallen(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-s_fallen-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  s_fallenb(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-s_fallenb-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_s_fallenb(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-s_fallenb-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  s_falldn(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-s_falldn-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_s_falldn(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-s_falldn-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _t_runl_2_jump(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_t_runl_2_jump-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__t_runl_2_jump(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_t_runl_2_jump-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _t_runr_2_jump(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_t_runr_2_jump-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__t_runr_2_jump(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_t_runr_2_jump-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  _t_jump_2_runl(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_t_jump_2_runl-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set__t_jump_2_runl(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-_t_jump_2_runl-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  s_look(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-s_look-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_s_look(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-s_look-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  s_point(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-s_point-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_s_point(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-s_point-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  dummy1(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-dummy1-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_dummy1(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-dummy1-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  dummy2(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-dummy2-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_dummy2(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-dummy2-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  dummy3(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-dummy3-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_dummy3(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-dummy3-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  dummy4(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-dummy4-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_dummy4(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-dummy4-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  togglewalk(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-togglewalk-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_togglewalk(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-togglewalk-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_stand_2_cast(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_stand_2_cast-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_stand_2_cast(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_stand_2_cast-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  s_cast(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-s_cast-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_s_cast(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-s_cast-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_cast_2_shoot(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_cast_2_shoot-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_cast_2_shoot(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_cast_2_shoot-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_cast_2_stand(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_cast_2_stand-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_cast_2_stand(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_cast_2_stand-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  s_shoot(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-s_shoot-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_s_shoot(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-s_shoot-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t_shoot_2_stand(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-t_shoot_2_stand-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t_shoot_2_stand(a1: number): null {
    SendCommand({
      id: 'SET_oCAniCtrl_Human-t_shoot_2_stand-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCAniCtrl_Human_OnInit(): oCAniCtrl_Human | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-oCAniCtrl_Human_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCAniCtrl_Human(result) : null
  }

  GetActionMode(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-GetActionMode-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  StartAni(a1: number, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-StartAni-int-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  SetNextAni(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-SetNextAni-void-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  InitAnimations(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-InitAnimations-void-false-',
      parentId: this.Uuid,
    })
  }

  SearchStandAni(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-SearchStandAni-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  Reset(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-Reset-void-false-',
      parentId: this.Uuid,
    })
  }

  InitAngleValuesForConsole(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-InitAngleValuesForConsole-void-false-',
      parentId: this.Uuid,
    })
  }

  SetScriptValues(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-SetScriptValues-void-false-',
      parentId: this.Uuid,
    })
  }

  SetInterruptFollowAni(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-SetInterruptFollowAni-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  InitAllAnis(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-InitAllAnis-void-false-',
      parentId: this.Uuid,
    })
  }

  SetWalkMode(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-SetWalkMode-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  InitFightAnis(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-InitFightAnis-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetFightAnis(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-SetFightAnis-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  StartAniWithOffset(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-StartAniWithOffset-void-false-int,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  DoAniWithOffset(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-DoAniWithOffset-void-false-',
      parentId: this.Uuid,
    })
  }

  CorrectFightPosition(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-CorrectFightPosition-void-false-',
      parentId: this.Uuid,
    })
  }

  GetWalkModeString(): string | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-GetWalkModeString-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  CorrectAniStates(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-CorrectAniStates-void-false-',
      parentId: this.Uuid,
    })
  }

  CheckWaterLevel(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-CheckWaterLevel-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetAlwaysWalk(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-SetAlwaysWalk-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  IsAlwaysWalk(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-IsAlwaysWalk-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  CanToggleWalkModeTo(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-CanToggleWalkModeTo-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  CanToggleWalkMode(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-CanToggleWalkMode-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  ToggleWalkMode(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-ToggleWalkMode-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  IsDead(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-IsDead-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsFallen(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-IsFallen-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsAiming(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-IsAiming-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsStateAniActive(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-IsStateAniActive-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  IsStateAniActive2(a1: zCModelAni): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-IsStateAniActive-int-false-zCModelAni*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  IsStanding(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-IsStanding-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsJumpStanding(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-IsJumpStanding-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsWalking(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-IsWalking-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsRunning(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-IsRunning-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsWalkingBackward(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-IsWalkingBackward-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  StopTurnAnis(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-StopTurnAnis-void-false-',
      parentId: this.Uuid,
    })
  }

  Turn(a1: number, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-Turn-float-false-float,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  override DoAutoRoll(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-DoAutoRoll-void-false-',
      parentId: this.Uuid,
    })
  }

  TurnDegrees(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-TurnDegrees-void-false-float,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  WallInFront(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-WallInFront-int-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  IsDefending(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-IsDefending-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetFightLimbs(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-GetFightLimbs-void-false-',
      parentId: this.Uuid,
    })
  }

  DoSparks(a1: zVEC3, a2: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-DoSparks-void-false-zVEC3&,zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  GetLayerAni(a1: number): zCModelAniActive | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-GetLayerAni-zCModelAniActive*-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new zCModelAniActive(result) : null
  }

  CheckLayerAni(a1: number, a2: string): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-CheckLayerAni-int-false-int,zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  ShowWeaponTrail(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-ShowWeaponTrail-void-false-',
      parentId: this.Uuid,
    })
  }

  StartHitCombo(a1: number, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-StartHitCombo-void-false-int,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  SetComboHitTarget(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-SetComboHitTarget-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  StartHitGraphical(a1: number, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-StartHitGraphical-void-false-int,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  GetStopTurnVobSign(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-GetStopTurnVobSign-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  HitCombo(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-HitCombo-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  CheckHitTarget(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-CheckHitTarget-zCVob*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  CreateHit(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-CreateHit-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CheckMeleeWeaponHitsLevel(a1: oCItem): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-CheckMeleeWeaponHitsLevel-int-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  HitInterrupt(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-HitInterrupt-void-false-',
      parentId: this.Uuid,
    })
  }

  HitGraphical(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-HitGraphical-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsInPreHit(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-IsInPreHit-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsInPostHit(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-IsInPostHit-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsInCombo(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-IsInCombo-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  FirstPersonDrawWeapon(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-FirstPersonDrawWeapon-void-false-',
      parentId: this.Uuid,
    })
  }

  GetHitDirection(): string | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-GetHitDirection-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  IsParadeRunning(): zCModelAniActive | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-IsParadeRunning-zCModelAniActive*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCModelAniActive(result) : null
  }

  CanParade(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-CanParade-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  StartParadeEffects(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-StartParadeEffects-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  StrafeLeft(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-StrafeLeft-void-false-',
      parentId: this.Uuid,
    })
  }

  StrafeRight(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-StrafeRight-void-false-',
      parentId: this.Uuid,
    })
  }

  GetFootPos(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-GetFootPos-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  PC_GoForward(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-PC_GoForward-void-false-',
      parentId: this.Uuid,
    })
  }

  PC_GoBackward(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-PC_GoBackward-void-false-',
      parentId: this.Uuid,
    })
  }

  PC_JumpForward(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-PC_JumpForward-void-false-',
      parentId: this.Uuid,
    })
  }

  CanJump(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-CanJump-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  CanJumpLedge(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-CanJumpLedge-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  JumpForward(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-JumpForward-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  Swim_CanClimbLedge(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-Swim_CanClimbLedge-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  Swim_ClimbLedge(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-Swim_ClimbLedge-void-false-',
      parentId: this.Uuid,
    })
  }

  CanWalkOnPoly(a1: zVEC3): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-CanWalkOnPoly-int-false-zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  DrawWeapon1(a1: number, a2: number, a3: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-DrawWeapon1-int-false-int,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? Number(result) : null
  }

  ChooseNextWeapon2(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-ChooseNextWeapon2-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  DrawWeapon2(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-DrawWeapon2-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  RemoveWeapon1(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-RemoveWeapon1-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  RemoveWeapon2(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-RemoveWeapon2-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  StartUp(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-StartUp-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  LerpFeetToTarget(a1: number, a2: number, a3: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-LerpFeetToTarget-void-false-int,int,zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  RelaxHandToTarget(a1: zVEC3, a2: zVEC3, a3: number, a4: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-RelaxHandToTarget-void-false-zVEC3 const&,zVEC3 const&,int,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
    })
  }

  CheckSpecialStates(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-CheckSpecialStates-void-false-',
      parentId: this.Uuid,
    })
  }

  CheckJump(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-CheckJump-void-false-',
      parentId: this.Uuid,
    })
  }

  HackNPCToSwim(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-HackNPCToSwim-void-false-',
      parentId: this.Uuid,
    })
  }

  CheckFallStates(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-CheckFallStates-void-false-',
      parentId: this.Uuid,
    })
  }

  CombineAniLerp(a1: number, a2: number, a3: number, a4: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-CombineAniLerp-float-false-float,float,float,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
    })

    return result ? Number(result) : null
  }

  InterpolateCombineAni(a1: number, a2: number, a3: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-InterpolateCombineAni-int-false-float,float,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? Number(result) : null
  }

  LookAtTarget(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-LookAtTarget-void-false-',
      parentId: this.Uuid,
    })
  }

  SetLookAtTarget(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-SetLookAtTarget-void-false-float,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  SetLookAtTarget2(a1: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-SetLookAtTarget-void-false-zVEC3&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetLookAtTarget3(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-SetLookAtTarget-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  StopLookAtTarget(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-StopLookAtTarget-void-false-',
      parentId: this.Uuid,
    })
  }

  StartCombineAni(a1: zVEC3, a2: number, a3: number, a4: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-StartCombineAni-void-false-zVEC3&,int,float,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
    })
  }

  StartCombineAni2(a1: zCVob, a2: number, a3: number, a4: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-StartCombineAni-void-false-zCVob*,int,float,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
    })
  }

  StopCombineAni(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-StopCombineAni-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  DetectChasm(a1: zVEC3, a2: zVEC3, a3: number, a4: zVEC3): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-DetectChasm-int-false-zVEC3 const&,zVEC3 const&,float&,zVEC3&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
    })

    return result ? Number(result) : null
  }

  IsInCriticalAni(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-IsInCriticalAni-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetActionMode(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-SetActionMode-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  _Stand(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_Stand-void-false-',
      parentId: this.Uuid,
    })
  }

  _Forward(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_Forward-void-false-',
      parentId: this.Uuid,
    })
  }

  _Backward(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_Backward-void-false-',
      parentId: this.Uuid,
    })
  }

  SetAnimations(a1: number, a2: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-SetAnimations-void-false-int,zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  TransitionToInvest(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-TransitionToInvest-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  TransitionToCast(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-TransitionToCast-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  TransitionToStand(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-TransitionToStand-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsInCastAni(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-IsInCastAni-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsInWeaponChoose(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-IsInWeaponChoose-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetWaterLevel(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-GetWaterLevel-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsInWater(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-IsInWater-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override StartStandAni(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-StartStandAni-void-false-',
      parentId: this.Uuid,
    })
  }

  override StartFallDownAni(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-StartFallDownAni-void-false-',
      parentId: this.Uuid,
    })
  }

  Init(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCAniCtrl_Human-Init-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }
}
