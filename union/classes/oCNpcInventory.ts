import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { oCNpc } from './index.js'
import { oCItem } from './index.js'
import { oCItemContainer } from './index.js'

export class oCNpcInventory extends oCItemContainer {
  owner(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-owner-oCNpc*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  set_owner(a1: oCNpc): null {
    SendCommand({
      id: 'SET_oCNpcInventory-owner-oCNpc*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  packAbility(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-packAbility-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_packAbility(a1: number): null {
    SendCommand({
      id: 'SET_oCNpcInventory-packAbility-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  packString(): string | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-packString-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_packString(a1: string): null {
    SendCommand({
      id: 'SET_oCNpcInventory-packString-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  override maxSlots(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-maxSlots-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override set_maxSlots(a1: number): null {
    SendCommand({
      id: 'SET_oCNpcInventory-maxSlots-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCNpcInventory_OnInit(): oCNpcInventory | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-oCNpcInventory_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCNpcInventory(result) : null
  }

  ClearInventory(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-ClearInventory-void-false-',
      parentId: this.Uuid,
    })
  }

  SetOwner(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-SetOwner-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetOwner(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-GetOwner-oCNpc*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  GetNumItemsInCategory(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-GetNumItemsInCategory-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetItem(a1: number): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-GetItem-oCItem*-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new oCItem(result) : null
  }

  GetCategory(a1: oCItem): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-GetCategory-int-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetAmount(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-GetAmount-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  HandleTrade(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-HandleTrade-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  CheckForEquippedItems(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-CheckForEquippedItems-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CanCarry(a1: oCItem): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-CanCarry-int-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  SetPackAbility(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-SetPackAbility-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  UnpackCategory(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-UnpackCategory-void-false-',
      parentId: this.Uuid,
    })
  }

  GetNumItemsInPackString(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-GetNumItemsInPackString-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetPackedItemBySlot(a1: number, a2: string): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-GetPackedItemBySlot-int-false-int,zSTRING&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  CreateFromPackString(a1: string): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-CreateFromPackString-oCItem*-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new oCItem(result) : null
  }

  GetPackedItemInfo(a1: string, a2: number, a3: number, a4: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-GetPackedItemInfo-int-false-zSTRING const&,int,int&,int&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
    })

    return result ? Number(result) : null
  }

  PackSingleItem(a1: oCItem): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-PackSingleItem-int-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  PackAllItems(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-PackAllItems-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  UnpackAllItems(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-UnpackAllItems-void-false-',
      parentId: this.Uuid,
    })
  }

  PackItemsInCategory(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-PackItemsInCategory-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  UnpackItemsInCategory(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-UnpackItemsInCategory-void-false-',
      parentId: this.Uuid,
    })
  }

  override HandleEvent(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-HandleEvent-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  override Open(a1: number, a2: number, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-Open-void-false-int,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  override Close(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-Close-void-false-',
      parentId: this.Uuid,
    })
  }

  override Insert(a1: oCItem): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-Insert-oCItem*-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new oCItem(result) : null
  }

  RemoveInv(a1: number, a2: number): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-Remove-oCItem*-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCItem(result) : null
  }

  RemoveInv2(a1: string, a2: number): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-Remove-oCItem*-false-zSTRING const&,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCItem(result) : null
  }

  override RemoveByPtr(a1: oCItem, a2: number): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-RemoveByPtr-oCItem*-false-oCItem*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCItem(result) : null
  }

  override Draw(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-Draw-void-false-',
      parentId: this.Uuid,
    })
  }

  override DrawCategory(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-DrawCategory-void-false-',
      parentId: this.Uuid,
    })
  }

  RemoveInv3(a1: oCItem): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-Remove-void-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RemoveInv4(a1: oCItem, a2: number): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-Remove-oCItem*-false-oCItem*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCItem(result) : null
  }

  IsIn(a1: oCItem, a2: number): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-IsIn-oCItem*-false-oCItem*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCItem(result) : null
  }

  IsIn2(a1: number, a2: number): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-IsIn-oCItem*-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCItem(result) : null
  }

  IsIn3(a1: string, a2: number): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-IsIn-oCItem*-false-zSTRING const&,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCItem(result) : null
  }

  override IsEmpty(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-IsEmpty-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsEmptyInv2(a1: number, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcInventory-IsEmpty-int-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }
}
