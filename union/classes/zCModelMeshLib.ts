import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCModel } from './index.js'
import { zCModelPrototype } from './index.js'
import { zCClassDef } from './index.js'
import { zCObject } from './index.js'

export class zCModelMeshLib extends zCObject {
  static zCModelMeshLib_OnInit(): zCModelMeshLib | null {
    const result: string | null = SendCommand({
      id: 'zCModelMeshLib-zCModelMeshLib_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCModelMeshLib(result) : null
  }

  ReleaseData(): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelMeshLib-ReleaseData-void-false-',
      parentId: this.Uuid,
    })
  }

  AllocNumNodeVisuals(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelMeshLib-AllocNumNodeVisuals-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  ApplyToModel(a1: zCModel): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelMeshLib-ApplyToModel-void-false-zCModel*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  ApplyToModel2(a1: zCModelPrototype): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelMeshLib-ApplyToModel-void-false-zCModelPrototype*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RemoveFromModel(a1: zCModel): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelMeshLib-RemoveFromModel-void-false-zCModel*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  BuildFromModel(a1: zCModelPrototype): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelMeshLib-BuildFromModel-void-false-zCModelPrototype*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  BuildFromModel2(a1: zCModel): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelMeshLib-BuildFromModel-void-false-zCModel*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SaveMDM(a1: zCModelPrototype): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelMeshLib-SaveMDM-void-false-zCModelPrototype*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCModelMeshLib-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }
}
