import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { oCNpc } from './index.js'
import { zCWorld } from './index.js'
import { oCItem } from './index.js'

export class oCItemContainer extends BaseUnionObject {
  npc(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-npc-oCNpc*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  set_npc(a1: oCNpc): null {
    SendCommand({
      id: 'SET_oCItemContainer-npc-oCNpc*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  titleText(): string | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-titleText-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_titleText(a1: string): null {
    SendCommand({
      id: 'SET_oCItemContainer-titleText-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  invMode(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-invMode-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_invMode(a1: number): null {
    SendCommand({
      id: 'SET_oCItemContainer-invMode-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  selectedItem(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-selectedItem-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_selectedItem(a1: number): null {
    SendCommand({
      id: 'SET_oCItemContainer-selectedItem-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  offset(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-offset-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_offset(a1: number): null {
    SendCommand({
      id: 'SET_oCItemContainer-offset-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  maxSlotsCol(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-maxSlotsCol-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_maxSlotsCol(a1: number): null {
    SendCommand({
      id: 'SET_oCItemContainer-maxSlotsCol-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  maxSlotsColScr(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-maxSlotsColScr-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_maxSlotsColScr(a1: number): null {
    SendCommand({
      id: 'SET_oCItemContainer-maxSlotsColScr-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  maxSlotsRow(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-maxSlotsRow-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_maxSlotsRow(a1: number): null {
    SendCommand({
      id: 'SET_oCItemContainer-maxSlotsRow-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  maxSlotsRowScr(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-maxSlotsRowScr-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_maxSlotsRowScr(a1: number): null {
    SendCommand({
      id: 'SET_oCItemContainer-maxSlotsRowScr-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  maxSlots(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-maxSlots-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_maxSlots(a1: number): null {
    SendCommand({
      id: 'SET_oCItemContainer-maxSlots-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  marginTop(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-marginTop-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_marginTop(a1: number): null {
    SendCommand({
      id: 'SET_oCItemContainer-marginTop-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  marginLeft(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-marginLeft-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_marginLeft(a1: number): null {
    SendCommand({
      id: 'SET_oCItemContainer-marginLeft-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  frame(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-frame-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_frame(a1: number): null {
    SendCommand({
      id: 'SET_oCItemContainer-frame-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  right(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-right-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_right(a1: number): null {
    SendCommand({
      id: 'SET_oCItemContainer-right-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  ownList(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-ownList-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_ownList(a1: number): null {
    SendCommand({
      id: 'SET_oCItemContainer-ownList-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  prepared(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-prepared-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_prepared(a1: number): null {
    SendCommand({
      id: 'SET_oCItemContainer-prepared-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  passive(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-passive-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_passive(a1: number): null {
    SendCommand({
      id: 'SET_oCItemContainer-passive-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  TransferCount(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-TransferCount-short-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_TransferCount(a1: number): null {
    SendCommand({
      id: 'SET_oCItemContainer-TransferCount-short-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  rndWorld(): zCWorld | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-rndWorld-zCWorld*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCWorld(result) : null
  }

  set_rndWorld(a1: zCWorld): null {
    SendCommand({
      id: 'SET_oCItemContainer-rndWorld-zCWorld*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  posx(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-posx-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_posx(a1: number): null {
    SendCommand({
      id: 'SET_oCItemContainer-posx-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  posy(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-posy-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_posy(a1: number): null {
    SendCommand({
      id: 'SET_oCItemContainer-posy-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_bManipulateItemsDisabled(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-m_bManipulateItemsDisabled-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_bManipulateItemsDisabled(a1: number): null {
    SendCommand({
      id: 'SET_oCItemContainer-m_bManipulateItemsDisabled-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_bCanTransferMoreThanOneItem(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-m_bCanTransferMoreThanOneItem-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_bCanTransferMoreThanOneItem(a1: number): null {
    SendCommand({
      id: 'SET_oCItemContainer-m_bCanTransferMoreThanOneItem-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCItemContainer_OnInit(): oCItemContainer | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-oCItemContainer_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCItemContainer(result) : null
  }

  GetNextContainerLeft(a1: oCItemContainer): oCItemContainer | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-GetNextContainerLeft-oCItemContainer*-false-oCItemContainer*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new oCItemContainer(result) : null
  }

  GetNextContainerRight(a1: oCItemContainer): oCItemContainer | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-GetNextContainerRight-oCItemContainer*-false-oCItemContainer*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new oCItemContainer(result) : null
  }

  ActivateNextContainer(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-ActivateNextContainer-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  HandleEvent(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-HandleEvent-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  Open(a1: number, a2: number, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-Open-void-false-int,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  OpenPassive(a1: number, a2: number, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-OpenPassive-void-false-int,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  GetName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-GetName-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  SetName(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-SetName-void-false-zSTRING&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetMode(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-GetMode-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetMode(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-SetMode-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  Close(): void | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-Close-void-false-',
      parentId: this.Uuid,
    })
  }

  Activate(): void | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-Activate-void-false-',
      parentId: this.Uuid,
    })
  }

  Deactivate(): void | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-Deactivate-void-false-',
      parentId: this.Uuid,
    })
  }

  IsOpen(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-IsOpen-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsActive(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-IsActive-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsEmpty(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-IsEmpty-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsSplitScreen(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-IsSplitScreen-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  Insert(a1: oCItem): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-Insert-oCItem*-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new oCItem(result) : null
  }

  Remove(a1: oCItem): void | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-Remove-void-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  Remove2(a1: oCItem, a2: number): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-Remove-oCItem*-false-oCItem*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCItem(result) : null
  }

  RemoveByPtr(a1: oCItem, a2: number): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-RemoveByPtr-oCItem*-false-oCItem*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCItem(result) : null
  }

  GetSelectedItem(): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-GetSelectedItem-oCItem*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCItem(result) : null
  }

  GetSelectedItemCount(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-GetSelectedItemCount-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetSize(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-GetSize-void-false-int&,int&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  DisableManipulateItems(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-DisableManipulateItems-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CanManipulateItems(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-CanManipulateItems-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  DisableTransferMoreThanOneItem(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-DisableTransferMoreThanOneItem-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CanTransferMoreThanOneItem(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-CanTransferMoreThanOneItem-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsPassive(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-IsPassive-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetTransferCount(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-GetTransferCount-short-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetTransferCount(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-SetTransferCount-void-false-short',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  IncTransferCount(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-IncTransferCount-void-false-short',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  Init(a1: number, a2: number, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-Init-void-false-int,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  GetPosition(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-GetPosition-void-false-int&,int&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  LoadGrafix(): void | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-LoadGrafix-void-false-',
      parentId: this.Uuid,
    })
  }

  DeleteContents(): void | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-DeleteContents-void-false-',
      parentId: this.Uuid,
    })
  }

  NextItem(): void | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-NextItem-void-false-',
      parentId: this.Uuid,
    })
  }

  NextItemLine(): void | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-NextItemLine-void-false-',
      parentId: this.Uuid,
    })
  }

  PrevItem(): void | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-PrevItem-void-false-',
      parentId: this.Uuid,
    })
  }

  PrevItemLine(): void | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-PrevItemLine-void-false-',
      parentId: this.Uuid,
    })
  }

  CheckSelectedItem(): void | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-CheckSelectedItem-void-false-',
      parentId: this.Uuid,
    })
  }

  TransferItem(a1: number, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-TransferItem-int-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  Draw(): void | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-Draw-void-false-',
      parentId: this.Uuid,
    })
  }

  DrawCategory(): void | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-DrawCategory-void-false-',
      parentId: this.Uuid,
    })
  }

  DrawItemInfo(a1: oCItem, a2: zCWorld): void | null {
    const result: string | null = SendCommand({
      id: 'oCItemContainer-DrawItemInfo-void-false-oCItem*,zCWorld*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }
}
