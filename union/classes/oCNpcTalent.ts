import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { oTEnumNpcTalent } from './index.js'
import { zCClassDef } from './index.js'
import { zCObject } from './index.js'

export class oCNpcTalent extends zCObject {
  m_talent(): oTEnumNpcTalent | null {
    const result: string | null = SendCommand({
      id: 'oCNpcTalent-m_talent-oTEnumNpcTalent-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_talent(a1: oTEnumNpcTalent): null {
    SendCommand({
      id: 'SET_oCNpcTalent-m_talent-oTEnumNpcTalent-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_skill(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcTalent-m_skill-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_skill(a1: number): null {
    SendCommand({
      id: 'SET_oCNpcTalent-m_skill-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_value(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcTalent-m_value-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_value(a1: number): null {
    SendCommand({
      id: 'SET_oCNpcTalent-m_value-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCNpcTalent_OnInit(): oCNpcTalent | null {
    const result: string | null = SendCommand({
      id: 'oCNpcTalent-oCNpcTalent_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCNpcTalent(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCNpcTalent-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }
}
