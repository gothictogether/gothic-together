import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { oCNpc } from './index.js'
import { TNpcAIState } from './index.js'
import { oCRtnEntry } from './index.js'
import { zCRoute } from './index.js'
import { zVEC3 } from './index.js'
import { oCItem } from './index.js'

export class oCNpc_States extends BaseUnionObject {
  name(): string | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-name-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_name(a1: string): null {
    SendCommand({
      id: 'SET_oCNpc_States-name-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  npc(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-npc-oCNpc*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  set_npc(a1: oCNpc): null {
    SendCommand({
      id: 'SET_oCNpc_States-npc-oCNpc*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  curState(): TNpcAIState | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-curState-TNpcAIState-false-false',
      parentId: this.Uuid,
    })

    return result ? new TNpcAIState(result) : null
  }

  set_curState(a1: TNpcAIState): null {
    SendCommand({
      id: 'SET_oCNpc_States-curState-TNpcAIState-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  nextState(): TNpcAIState | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-nextState-TNpcAIState-false-false',
      parentId: this.Uuid,
    })

    return result ? new TNpcAIState(result) : null
  }

  set_nextState(a1: TNpcAIState): null {
    SendCommand({
      id: 'SET_oCNpc_States-nextState-TNpcAIState-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lastAIState(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-lastAIState-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_lastAIState(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc_States-lastAIState-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  hasRoutine(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-hasRoutine-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_hasRoutine(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc_States-hasRoutine-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  rtnChanged(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-rtnChanged-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_rtnChanged(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc_States-rtnChanged-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  rtnBefore(): oCRtnEntry | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-rtnBefore-oCRtnEntry*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCRtnEntry(result) : null
  }

  set_rtnBefore(a1: oCRtnEntry): null {
    SendCommand({
      id: 'SET_oCNpc_States-rtnBefore-oCRtnEntry*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  rtnNow(): oCRtnEntry | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-rtnNow-oCRtnEntry*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCRtnEntry(result) : null
  }

  set_rtnNow(a1: oCRtnEntry): null {
    SendCommand({
      id: 'SET_oCNpc_States-rtnNow-oCRtnEntry*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  rtnRoute(): zCRoute | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-rtnRoute-zCRoute*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCRoute(result) : null
  }

  set_rtnRoute(a1: zCRoute): null {
    SendCommand({
      id: 'SET_oCNpc_States-rtnRoute-zCRoute*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  rtnOverlay(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-rtnOverlay-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_rtnOverlay(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc_States-rtnOverlay-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  rtnOverlayCount(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-rtnOverlayCount-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_rtnOverlayCount(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc_States-rtnOverlayCount-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  walkmode_routine(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-walkmode_routine-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_walkmode_routine(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc_States-walkmode_routine-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  weaponmode_routine(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-weaponmode_routine-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_weaponmode_routine(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc_States-weaponmode_routine-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  startNewRoutine(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-startNewRoutine-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_startNewRoutine(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc_States-startNewRoutine-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  aiStateDriven(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-aiStateDriven-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_aiStateDriven(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc_States-aiStateDriven-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  aiStatePosition(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-aiStatePosition-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_aiStatePosition(a1: zVEC3): null {
    SendCommand({
      id: 'SET_oCNpc_States-aiStatePosition-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  parOther(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-parOther-oCNpc*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  set_parOther(a1: oCNpc): null {
    SendCommand({
      id: 'SET_oCNpc_States-parOther-oCNpc*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  parVictim(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-parVictim-oCNpc*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  set_parVictim(a1: oCNpc): null {
    SendCommand({
      id: 'SET_oCNpc_States-parVictim-oCNpc*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  parItem(): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-parItem-oCItem*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCItem(result) : null
  }

  set_parItem(a1: oCItem): null {
    SendCommand({
      id: 'SET_oCNpc_States-parItem-oCItem*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  rntChangeCount(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-rntChangeCount-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_rntChangeCount(a1: number): null {
    SendCommand({
      id: 'SET_oCNpc_States-rntChangeCount-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCNpc_States_OnInit(): oCNpc_States | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-oCNpc_States_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCNpc_States(result) : null
  }

  ClearParserReferences(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-ClearParserReferences-void-false-',
      parentId: this.Uuid,
    })
  }

  SetOwner(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-SetOwner-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetState(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-GetState-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsInState(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-IsInState-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  IsScriptStateActive(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-IsScriptStateActive-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetStateTime(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-GetStateTime-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetStateTime(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-SetStateTime-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetRoutine(a1: oCRtnEntry, a2: oCRtnEntry): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-SetRoutine-void-false-oCRtnEntry*,oCRtnEntry*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  StartRtnState(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-StartRtnState-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  ActivateRtnState(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-ActivateRtnState-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  StartAIState(a1: string, a2: number, a3: number, a4: number, a5: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-StartAIState-int-false-zSTRING const&,int,int,float,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
      a5: a5.toString(),
    })

    return result ? Number(result) : null
  }

  StartAIState2(a1: number, a2: number, a3: number, a4: number, a5: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-StartAIState-int-false-int,int,int,float,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
      a5: a5.toString(),
    })

    return result ? Number(result) : null
  }

  CanPlayerUseAIState(a1: TNpcAIState): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-CanPlayerUseAIState-int-false-TNpcAIState const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  CanPlayerUseAIState2(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-CanPlayerUseAIState-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  DoAIState(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-DoAIState-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  ClearAIState(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-ClearAIState-void-false-',
      parentId: this.Uuid,
    })
  }

  EndCurrentState(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-EndCurrentState-void-false-',
      parentId: this.Uuid,
    })
  }

  IsInRoutine(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-IsInRoutine-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsInCutscene(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-IsInCutscene-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  StartOutputUnit(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-StartOutputUnit-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  StartCutscene(a1: string, a2: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-StartCutscene-void-false-zSTRING const&,zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  CloseCutscenes(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-CloseCutscenes-void-false-',
      parentId: this.Uuid,
    })
  }

  SetParserInstanceNpc(a1: number, a2: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-SetParserInstanceNpc-void-false-int,oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  SetParserInstanceItem(a1: oCItem): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-SetParserInstanceItem-void-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  InitRoutine(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-InitRoutine-void-false-',
      parentId: this.Uuid,
    })
  }

  ChangeRoutine(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-ChangeRoutine-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  ChangeRoutine2(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-ChangeRoutine-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetRoutineName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-GetRoutineName-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  BeginInsertOverlayRoutine(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-BeginInsertOverlayRoutine-void-false-',
      parentId: this.Uuid,
    })
  }

  StopInsertOverlayRoutine(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-StopInsertOverlayRoutine-void-false-',
      parentId: this.Uuid,
    })
  }

  RemoveOverlayEntry(a1: oCRtnEntry): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-RemoveOverlayEntry-void-false-oCRtnEntry*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RemoveOverlay(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-RemoveOverlay-void-false-',
      parentId: this.Uuid,
    })
  }

  InsertRoutine(
    a1: number,
    a2: number,
    a3: number,
    a4: number,
    a5: number,
    a6: string,
    a7: number,
  ): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-InsertRoutine-void-false-int,int,int,int,int,zSTRING const&,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
      a5: a5.toString(),
      a6: a6.toString(),
      a7: a7.toString(),
    })
  }

  InsertRoutineCS(a1: string, a2: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-InsertRoutineCS-void-false-zSTRING const&,zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  GetLastRoutineState(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-GetLastRoutineState-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetLastState(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-GetLastState-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsAIStateDriven(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-IsAIStateDriven-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  InitAIStateDriven(a1: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-InitAIStateDriven-void-false-zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetAIStatePosition(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-GetAIStatePosition-zVEC3-false-',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  GetStateInfo(): string | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-GetStateInfo-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  GetLastError(): string | null {
    const result: string | null = SendCommand({
      id: 'oCNpc_States-GetLastError-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }
}
