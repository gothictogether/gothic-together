import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCAICamera } from './index.js'
import { oCMobInter } from './index.js'
import { oHEROSTATUS } from './index.js'
import { zVEC3 } from './index.js'
import { zCClassDef } from './index.js'
import { zCVob } from './index.js'
import { oCNpc } from './index.js'
import { oCAniCtrl_Human } from './index.js'

export class oCAIHuman extends oCAniCtrl_Human {
  aiCam(): zCAICamera | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-aiCam-zCAICamera*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCAICamera(result) : null
  }

  set_aiCam(a1: zCAICamera): null {
    SendCommand({
      id: 'SET_oCAIHuman-aiCam-zCAICamera*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  forcejump(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-forcejump-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_forcejump(a1: number): null {
    SendCommand({
      id: 'SET_oCAIHuman-forcejump-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lookedAround(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-lookedAround-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_lookedAround(a1: number): null {
    SendCommand({
      id: 'SET_oCAIHuman-lookedAround-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  sprintActive(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-sprintActive-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_sprintActive(a1: number): null {
    SendCommand({
      id: 'SET_oCAIHuman-sprintActive-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  crawlActive(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-crawlActive-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_crawlActive(a1: number): null {
    SendCommand({
      id: 'SET_oCAIHuman-crawlActive-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  showai(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-showai-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_showai(a1: number): null {
    SendCommand({
      id: 'SET_oCAIHuman-showai-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  startObserveIntruder(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-startObserveIntruder-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_startObserveIntruder(a1: number): null {
    SendCommand({
      id: 'SET_oCAIHuman-startObserveIntruder-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  dontKnowAniPlayed(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-dontKnowAniPlayed-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_dontKnowAniPlayed(a1: number): null {
    SendCommand({
      id: 'SET_oCAIHuman-dontKnowAniPlayed-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  spellReleased(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-spellReleased-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_spellReleased(a1: number): null {
    SendCommand({
      id: 'SET_oCAIHuman-spellReleased-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  spellCastedLastFrame(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-spellCastedLastFrame-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_spellCastedLastFrame(a1: number): null {
    SendCommand({
      id: 'SET_oCAIHuman-spellCastedLastFrame-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  eyeBlinkActivated(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-eyeBlinkActivated-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_eyeBlinkActivated(a1: number): null {
    SendCommand({
      id: 'SET_oCAIHuman-eyeBlinkActivated-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  thirdPersonFallback(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-thirdPersonFallback-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_thirdPersonFallback(a1: number): null {
    SendCommand({
      id: 'SET_oCAIHuman-thirdPersonFallback-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  createFlyDamage(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-createFlyDamage-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_createFlyDamage(a1: number): null {
    SendCommand({
      id: 'SET_oCAIHuman-createFlyDamage-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCAIHuman_OnInit(): oCAIHuman | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-oCAIHuman_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCAIHuman(result) : null
  }

  CheckActiveSpells(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-CheckActiveSpells-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  MagicInvestSpell(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-MagicInvestSpell-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  MagicCheckSpellStates(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-MagicCheckSpellStates-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  MagicMode(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-MagicMode-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  DoSimpleAI(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-DoSimpleAI-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  Pressed(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-Pressed-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  Toggled(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-Toggled-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  SetCamModeMob(a1: oCMobInter): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-SetCamModeMob-void-false-oCMobInter*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  ChangeCamMode(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-ChangeCamMode-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  InitCamModes(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-InitCamModes-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetCamMode(a1: string, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-SetCamMode-void-false-zSTRING const&,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  WeaponChoose(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-WeaponChoose-void-false-',
      parentId: this.Uuid,
    })
  }

  BowMode(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-BowMode-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetEnemyThreat(): oHEROSTATUS | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-GetEnemyThreat-oHEROSTATUS-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  CheckFightCamera(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-CheckFightCamera-void-false-',
      parentId: this.Uuid,
    })
  }

  FightMelee(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-FightMelee-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  CheckMobInteraction(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-CheckMobInteraction-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  CheckItemInteraction(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-CheckItemInteraction-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  StandActions(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-StandActions-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  PC_CheckSpecialStates(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-PC_CheckSpecialStates-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  PC_Diving(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-PC_Diving-void-false-',
      parentId: this.Uuid,
    })
  }

  PC_Turn(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-PC_Turn-void-false-float,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  PC_SpecialMove(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-PC_SpecialMove-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  PC_ActionMove(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-PC_ActionMove-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  PC_WeaponMove(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-PC_WeaponMove-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  PC_SlowMove(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-PC_SlowMove-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  PC_Sneak(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-PC_Sneak-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  PC_Turnings(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-PC_Turnings-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  PC_Strafe(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-PC_Strafe-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  _WalkCycle(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-_WalkCycle-void-false-',
      parentId: this.Uuid,
    })
  }

  ResetObserveSuspectCounter(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-ResetObserveSuspectCounter-void-false-',
      parentId: this.Uuid,
    })
  }

  CreateObserveSuspect(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-CreateObserveSuspect-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CreateFootStepSound(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-CreateFootStepSound-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  ResetAssessUseMobCounter(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-ResetAssessUseMobCounter-void-false-',
      parentId: this.Uuid,
    })
  }

  CreateAssessUseMob(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-CreateAssessUseMob-void-false-',
      parentId: this.Uuid,
    })
  }

  SetCrawlMode(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-SetCrawlMode-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetSprintMode(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-SetSprintMode-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  MoveCamera(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-MoveCamera-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  CamMessages(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-CamMessages-void-false-',
      parentId: this.Uuid,
    })
  }

  CheckFocusVob(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-CheckFocusVob-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  Moving(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-Moving-void-false-',
      parentId: this.Uuid,
    })
  }

  ChangeCamModeBySituation(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-ChangeCamModeBySituation-void-false-',
      parentId: this.Uuid,
    })
  }

  DoMonsterSpecial(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-DoMonsterSpecial-void-false-',
      parentId: this.Uuid,
    })
  }

  SetShowAI(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-SetShowAI-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetShowAI(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-GetShowAI-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  StartFlyDamage(a1: number, a2: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-StartFlyDamage-void-false-float,zVEC3&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override DoAI(a1: zCVob, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-DoAI-void-false-zCVob*,int&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override CanThisCollideWith(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-CanThisCollideWith-int-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  override Init(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-Init-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  AddIgnoreCD(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-AddIgnoreCD-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SubIgnoreCD(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-SubIgnoreCD-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  DoHackSpecials(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman-DoHackSpecials-void-false-',
      parentId: this.Uuid,
    })
  }
}
