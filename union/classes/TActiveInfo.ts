import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { oCNpc } from './index.js'

export class TActiveInfo extends BaseUnionObject {
  modified(): number | null {
    const result: string | null = SendCommand({
      id: 'TActiveInfo-modified-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_modified(a1: number): null {
    SendCommand({
      id: 'SET_TActiveInfo-modified-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  putTorchBackInHand(): number | null {
    const result: string | null = SendCommand({
      id: 'TActiveInfo-putTorchBackInHand-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_putTorchBackInHand(a1: number): null {
    SendCommand({
      id: 'SET_TActiveInfo-putTorchBackInHand-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  changeTorchAni(): number | null {
    const result: string | null = SendCommand({
      id: 'TActiveInfo-changeTorchAni-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_changeTorchAni(a1: number): null {
    SendCommand({
      id: 'SET_TActiveInfo-changeTorchAni-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  changeTorchAniTo(): number | null {
    const result: string | null = SendCommand({
      id: 'TActiveInfo-changeTorchAniTo-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_changeTorchAniTo(a1: number): null {
    SendCommand({
      id: 'SET_TActiveInfo-changeTorchAniTo-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static TActiveInfo_OnInit(a1: oCNpc): TActiveInfo | null {
    const result: string | null = SendCommand({
      id: 'TActiveInfo-TActiveInfo_OnInit-void-false-oCNpc const*',
      parentId: 'NULL',
      a1: a1.toString(),
    })

    return result ? new TActiveInfo(result) : null
  }
}
