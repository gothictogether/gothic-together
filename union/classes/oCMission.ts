import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { oCNpc } from './index.js'
import { TMisStatus } from './index.js'

export class oCMission extends BaseUnionObject {
  name(): string | null {
    const result: string | null = SendCommand({
      id: 'oCMission-name-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_name(a1: string): null {
    SendCommand({
      id: 'SET_oCMission-name-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  description(): string | null {
    const result: string | null = SendCommand({
      id: 'oCMission-description-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_description(a1: string): null {
    SendCommand({
      id: 'SET_oCMission-description-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  duration(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMission-duration-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_duration(a1: number): null {
    SendCommand({
      id: 'SET_oCMission-duration-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  important(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMission-important-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_important(a1: number): null {
    SendCommand({
      id: 'SET_oCMission-important-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  offerConditions(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMission-offerConditions-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_offerConditions(a1: number): null {
    SendCommand({
      id: 'SET_oCMission-offerConditions-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  offerFunc(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMission-offerFunc-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_offerFunc(a1: number): null {
    SendCommand({
      id: 'SET_oCMission-offerFunc-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  successConditions(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMission-successConditions-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_successConditions(a1: number): null {
    SendCommand({
      id: 'SET_oCMission-successConditions-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  successFunc(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMission-successFunc-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_successFunc(a1: number): null {
    SendCommand({
      id: 'SET_oCMission-successFunc-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  failureConditions(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMission-failureConditions-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_failureConditions(a1: number): null {
    SendCommand({
      id: 'SET_oCMission-failureConditions-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  failureFunc(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMission-failureFunc-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_failureFunc(a1: number): null {
    SendCommand({
      id: 'SET_oCMission-failureFunc-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  obsoleteConditions(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMission-obsoleteConditions-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_obsoleteConditions(a1: number): null {
    SendCommand({
      id: 'SET_oCMission-obsoleteConditions-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  obsoleteFunc(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMission-obsoleteFunc-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_obsoleteFunc(a1: number): null {
    SendCommand({
      id: 'SET_oCMission-obsoleteFunc-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  runningFunc(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMission-runningFunc-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_runningFunc(a1: number): null {
    SendCommand({
      id: 'SET_oCMission-runningFunc-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  instance(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMission-instance-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_instance(a1: number): null {
    SendCommand({
      id: 'SET_oCMission-instance-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  npc(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCMission-npc-oCNpc*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  set_npc(a1: oCNpc): null {
    SendCommand({
      id: 'SET_oCMission-npc-oCNpc*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  player(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCMission-player-oCNpc*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  set_player(a1: oCNpc): null {
    SendCommand({
      id: 'SET_oCMission-player-oCNpc*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  curID(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMission-curID-unsigned long-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_curID(a1: number): null {
    SendCommand({
      id: 'SET_oCMission-curID-unsigned long-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  available(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMission-available-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_available(a1: number): null {
    SendCommand({
      id: 'SET_oCMission-available-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  status(): TMisStatus | null {
    const result: string | null = SendCommand({
      id: 'oCMission-status-TMisStatus*-false-false',
      parentId: this.Uuid,
    })

    return result ? new TMisStatus(result) : null
  }

  set_status(a1: TMisStatus): null {
    SendCommand({
      id: 'SET_oCMission-status-TMisStatus*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  deleted(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMission-deleted-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_deleted(a1: number): null {
    SendCommand({
      id: 'SET_oCMission-deleted-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCMission_OnInit(a1: number): oCMission | null {
    const result: string | null = SendCommand({
      id: 'oCMission-oCMission_OnInit-void-false-int',
      parentId: 'NULL',
      a1: a1.toString(),
    })

    return result ? new oCMission(result) : null
  }

  AddEntry(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCMission-AddEntry-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetCurrentUser(a1: oCNpc, a2: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCMission-SetCurrentUser-void-false-oCNpc*,oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  GetStatusInfo(): TMisStatus | null {
    const result: string | null = SendCommand({
      id: 'oCMission-GetStatusInfo-TMisStatus*-false-',
      parentId: this.Uuid,
    })

    return result ? new TMisStatus(result) : null
  }

  GetStartTime(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMission-GetStartTime-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetStartTime(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCMission-SetStartTime-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CheckMission(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMission-CheckMission-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsAvailable(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMission-IsAvailable-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  OfferConditions(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMission-OfferConditions-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  Offer(): void | null {
    const result: string | null = SendCommand({
      id: 'oCMission-Offer-void-false-',
      parentId: this.Uuid,
    })
  }

  SuccessConditions(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMission-SuccessConditions-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  Success(): void | null {
    const result: string | null = SendCommand({
      id: 'oCMission-Success-void-false-',
      parentId: this.Uuid,
    })
  }

  FailureConditions(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMission-FailureConditions-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  Failure(): void | null {
    const result: string | null = SendCommand({
      id: 'oCMission-Failure-void-false-',
      parentId: this.Uuid,
    })
  }

  ObsoleteConditions(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMission-ObsoleteConditions-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  Obsolete(): void | null {
    const result: string | null = SendCommand({
      id: 'oCMission-Obsolete-void-false-',
      parentId: this.Uuid,
    })
  }

  Running(): void | null {
    const result: string | null = SendCommand({
      id: 'oCMission-Running-void-false-',
      parentId: this.Uuid,
    })
  }

  GetStatus(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMission-GetStatus-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetStatus(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCMission-SetStatus-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  OnTime(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMission-OnTime-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }
}
