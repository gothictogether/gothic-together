import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zVEC3 } from './index.js'

export class zTBSphere3D extends BaseUnionObject {
  center(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zTBSphere3D-center-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_center(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zTBSphere3D-center-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  radius(): number | null {
    const result: string | null = SendCommand({
      id: 'zTBSphere3D-radius-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_radius(a1: number): null {
    SendCommand({
      id: 'SET_zTBSphere3D-radius-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  IsIntersecting(a1: zVEC3, a2: zVEC3): number | null {
    const result: string | null = SendCommand({
      id: 'zTBSphere3D-IsIntersecting-int-false-zVEC3 const&,zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  IsIntersecting2(a1: zTBSphere3D): number | null {
    const result: string | null = SendCommand({
      id: 'zTBSphere3D-IsIntersecting-int-false-zTBSphere3D const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }
}
