import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zTListProcess } from './index.js'
import { zCVob } from './index.js'
import { zCClassDef } from './index.js'
import { zCTrigger } from './index.js'

export class zCTriggerList extends zCTrigger {
  listProcess(): zTListProcess | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerList-listProcess-zTListProcess-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_listProcess(a1: zTListProcess): null {
    SendCommand({
      id: 'SET_zCTriggerList-listProcess-zTListProcess-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  actTarget(): number | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerList-actTarget-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_actTarget(a1: number): null {
    SendCommand({
      id: 'SET_zCTriggerList-actTarget-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  sendOnTrigger(): number | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerList-sendOnTrigger-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_sendOnTrigger(a1: number): null {
    SendCommand({
      id: 'SET_zCTriggerList-sendOnTrigger-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCTriggerList_OnInit(): zCTriggerList | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerList-zCTriggerList_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCTriggerList(result) : null
  }

  Init(): void | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerList-Init-void-false-',
      parentId: this.Uuid,
    })
  }

  TriggerActTarget(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerList-TriggerActTarget-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  ProcessActTarget(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerList-ProcessActTarget-int-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  FinishActivation(): void | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerList-FinishActivation-void-false-',
      parentId: this.Uuid,
    })
  }

  DoTriggering(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerList-DoTriggering-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerList-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override OnTimer(): void | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerList-OnTimer-void-false-',
      parentId: this.Uuid,
    })
  }

  GetTriggerTarget2(a1: Number): string | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerList-GetTriggerTarget-zSTRING-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? String(result) : null
  }

  override TriggerTarget(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerList-TriggerTarget-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override UntriggerTarget(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerList-UntriggerTarget-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }
}
