import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zTConfig } from './index.js'
import { zCVob } from './index.js'
import { zCModel } from './index.js'
import { zCWorld } from './index.js'
import { zVEC3 } from './index.js'
import { zCModelNodeInst } from './index.js'
import { zTMovementState } from './index.js'
import { zTLedgeInfo } from './index.js'
import { zCModelAni } from './index.js'
import { zCClassDef } from './index.js'
import { zCAIBase } from './index.js'

export class zCAIPlayer extends zCAIBase {
  config(): zTConfig | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-config-zTConfig-false-false',
      parentId: this.Uuid,
    })

    return result ? new zTConfig(result) : null
  }

  set_config(a1: zTConfig): null {
    SendCommand({
      id: 'SET_zCAIPlayer-config-zTConfig-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  vob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-vob-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_vob(a1: zCVob): null {
    SendCommand({
      id: 'SET_zCAIPlayer-vob-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  model(): zCModel | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-model-zCModel*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCModel(result) : null
  }

  set_model(a1: zCModel): null {
    SendCommand({
      id: 'SET_zCAIPlayer-model-zCModel*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  world(): zCWorld | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-world-zCWorld*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCWorld(result) : null
  }

  set_world(a1: zCWorld): null {
    SendCommand({
      id: 'SET_zCAIPlayer-world-zCWorld*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  centerPos(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-centerPos-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_centerPos(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCAIPlayer-centerPos-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  feetY(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-feetY-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_feetY(a1: number): null {
    SendCommand({
      id: 'SET_zCAIPlayer-feetY-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  headY(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-headY-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_headY(a1: number): null {
    SendCommand({
      id: 'SET_zCAIPlayer-headY-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  aboveFloor(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-aboveFloor-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_aboveFloor(a1: number): null {
    SendCommand({
      id: 'SET_zCAIPlayer-aboveFloor-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  waterLevel(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-waterLevel-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_waterLevel(a1: number): null {
    SendCommand({
      id: 'SET_zCAIPlayer-waterLevel-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  velocityLen2(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-velocityLen2-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_velocityLen2(a1: number): null {
    SendCommand({
      id: 'SET_zCAIPlayer-velocityLen2-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  velocity(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-velocity-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_velocity(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCAIPlayer-velocity-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fallDownDistanceY(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-fallDownDistanceY-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fallDownDistanceY(a1: number): null {
    SendCommand({
      id: 'SET_zCAIPlayer-fallDownDistanceY-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fallDownStartY(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-fallDownStartY-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fallDownStartY(a1: number): null {
    SendCommand({
      id: 'SET_zCAIPlayer-fallDownStartY-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  slidePolyNormal(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-slidePolyNormal-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_slidePolyNormal(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCAIPlayer-slidePolyNormal-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  checkWaterCollBodyLen(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-checkWaterCollBodyLen-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_checkWaterCollBodyLen(a1: number): null {
    SendCommand({
      id: 'SET_zCAIPlayer-checkWaterCollBodyLen-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  modelHeadNode(): zCModelNodeInst | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-modelHeadNode-zCModelNodeInst*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCModelNodeInst(result) : null
  }

  set_modelHeadNode(a1: zCModelNodeInst): null {
    SendCommand({
      id: 'SET_zCAIPlayer-modelHeadNode-zCModelNodeInst*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  zMV_DO_SURFACE_ALIGN(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-zMV_DO_SURFACE_ALIGN-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_zMV_DO_SURFACE_ALIGN(a1: number): null {
    SendCommand({
      id: 'SET_zCAIPlayer-zMV_DO_SURFACE_ALIGN-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  zMV_DO_DETECT_WALK_STOP_CHASM(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-zMV_DO_DETECT_WALK_STOP_CHASM-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_zMV_DO_DETECT_WALK_STOP_CHASM(a1: number): null {
    SendCommand({
      id: 'SET_zCAIPlayer-zMV_DO_DETECT_WALK_STOP_CHASM-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  zMV_DO_WALL_SLIDING(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-zMV_DO_WALL_SLIDING-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_zMV_DO_WALL_SLIDING(a1: number): null {
    SendCommand({
      id: 'SET_zCAIPlayer-zMV_DO_WALL_SLIDING-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  zMV_DO_HEIGHT_CORRECTION(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-zMV_DO_HEIGHT_CORRECTION-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_zMV_DO_HEIGHT_CORRECTION(a1: number): null {
    SendCommand({
      id: 'SET_zCAIPlayer-zMV_DO_HEIGHT_CORRECTION-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  cdStatOriginal(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-cdStatOriginal-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_cdStatOriginal(a1: number): null {
    SendCommand({
      id: 'SET_zCAIPlayer-cdStatOriginal-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  cdStat(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-cdStat-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_cdStat(a1: number): null {
    SendCommand({
      id: 'SET_zCAIPlayer-cdStat-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  startFXModelLandDust(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-startFXModelLandDust-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_startFXModelLandDust(a1: number): null {
    SendCommand({
      id: 'SET_zCAIPlayer-startFXModelLandDust-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  forceModelHalt(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-forceModelHalt-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_forceModelHalt(a1: number): null {
    SendCommand({
      id: 'SET_zCAIPlayer-forceModelHalt-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  autoRollEnabled(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-autoRollEnabled-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_autoRollEnabled(a1: number): null {
    SendCommand({
      id: 'SET_zCAIPlayer-autoRollEnabled-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  modelAnisInPlace(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-modelAnisInPlace-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_modelAnisInPlace(a1: number): null {
    SendCommand({
      id: 'SET_zCAIPlayer-modelAnisInPlace-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  oldState(): zTMovementState | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-oldState-zTMovementState-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_oldState(a1: zTMovementState): null {
    SendCommand({
      id: 'SET_zCAIPlayer-oldState-zTMovementState-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  state(): zTMovementState | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-state-zTMovementState-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_state(a1: zTMovementState): null {
    SendCommand({
      id: 'SET_zCAIPlayer-state-zTMovementState-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  oldWaterLevel(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-oldWaterLevel-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_oldWaterLevel(a1: number): null {
    SendCommand({
      id: 'SET_zCAIPlayer-oldWaterLevel-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_eCollObjectState(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-m_eCollObjectState-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_eCollObjectState(a1: number): null {
    SendCommand({
      id: 'SET_zCAIPlayer-m_eCollObjectState-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_eCollObjectStateOld(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-m_eCollObjectStateOld-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_eCollObjectStateOld(a1: number): null {
    SendCommand({
      id: 'SET_zCAIPlayer-m_eCollObjectStateOld-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  bleedingPerc(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-bleedingPerc-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_bleedingPerc(a1: number): null {
    SendCommand({
      id: 'SET_zCAIPlayer-bleedingPerc-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  bleedingLastPos(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-bleedingLastPos-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_bleedingLastPos(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCAIPlayer-bleedingLastPos-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  bleedingNextDist(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-bleedingNextDist-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_bleedingNextDist(a1: number): null {
    SendCommand({
      id: 'SET_zCAIPlayer-bleedingNextDist-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  weaponTrailVob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-weaponTrailVob-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_weaponTrailVob(a1: zCVob): null {
    SendCommand({
      id: 'SET_zCAIPlayer-weaponTrailVob-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  waterRingVob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-waterRingVob-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_waterRingVob(a1: zCVob): null {
    SendCommand({
      id: 'SET_zCAIPlayer-waterRingVob-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  waterRingTimer(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-waterRingTimer-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_waterRingTimer(a1: number): null {
    SendCommand({
      id: 'SET_zCAIPlayer-waterRingTimer-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  autoRollPos(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-autoRollPos-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_autoRollPos(a1: number): null {
    SendCommand({
      id: 'SET_zCAIPlayer-autoRollPos-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  autoRollPosDest(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-autoRollPosDest-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_autoRollPosDest(a1: number): null {
    SendCommand({
      id: 'SET_zCAIPlayer-autoRollPosDest-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  autoRollSpeed(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-autoRollSpeed-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_autoRollSpeed(a1: number): null {
    SendCommand({
      id: 'SET_zCAIPlayer-autoRollSpeed-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  autoRollMaxAngle(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-autoRollMaxAngle-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_autoRollMaxAngle(a1: number): null {
    SendCommand({
      id: 'SET_zCAIPlayer-autoRollMaxAngle-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  bloodDefaultTexName(): string | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-bloodDefaultTexName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_bloodDefaultTexName(a1: string): null {
    SendCommand({
      id: 'SET_zCAIPlayer-bloodDefaultTexName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCAIPlayer_OnInit(): zCAIPlayer | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-zCAIPlayer_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCAIPlayer(result) : null
  }

  RemoveEffects(): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-RemoveEffects-void-false-',
      parentId: this.Uuid,
    })
  }

  GetJumpUpHeight(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-GetJumpUpHeight-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetJumpUpForceByHeight(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-SetJumpUpForceByHeight-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetVob(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-SetVob-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  DoAutoRoll(): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-DoAutoRoll-void-false-',
      parentId: this.Uuid,
    })
  }

  ResetAutoRoll(): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-ResetAutoRoll-void-false-',
      parentId: this.Uuid,
    })
  }

  CalcModelAnisInPlace(): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-CalcModelAnisInPlace-void-false-',
      parentId: this.Uuid,
    })
  }

  DoSurfaceAlignment(): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-DoSurfaceAlignment-void-false-',
      parentId: this.Uuid,
    })
  }

  IsSliding(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-IsSliding-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  CheckFloorSliding(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-CheckFloorSliding-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  CheckPhysics(): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-CheckPhysics-void-false-',
      parentId: this.Uuid,
    })
  }

  PropagateCollObjectStates(a1: zTMovementState): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-PropagateCollObjectStates-void-false-zTMovementState',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CalcForceModelHalt(): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-CalcForceModelHalt-void-false-',
      parentId: this.Uuid,
    })
  }

  CalcStateVars(): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-CalcStateVars-void-false-',
      parentId: this.Uuid,
    })
  }

  DoProceduralMovement(): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-DoProceduralMovement-void-false-',
      parentId: this.Uuid,
    })
  }

  Begin(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-Begin-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  End(): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-End-void-false-',
      parentId: this.Uuid,
    })
  }

  UpdateEffects(): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-UpdateEffects-void-false-',
      parentId: this.Uuid,
    })
  }

  SetBloodDefaultTexture(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-SetBloodDefaultTexture-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  AddWeaponTrailSegment(a1: zVEC3, a2: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-AddWeaponTrailSegment-void-false-zVEC3 const&,zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  CreateLedgeInfo(): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-CreateLedgeInfo-void-false-',
      parentId: this.Uuid,
    })
  }

  GetLedgeInfo(): zTLedgeInfo | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-GetLedgeInfo-zTLedgeInfo*-false-',
      parentId: this.Uuid,
    })

    return result ? new zTLedgeInfo(result) : null
  }

  GetFoundLedge(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-GetFoundLedge-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  ClearFoundLedge(): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-ClearFoundLedge-void-false-',
      parentId: this.Uuid,
    })
  }

  DetectClimbUpLedge(a1: zVEC3, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-DetectClimbUpLedge-int-false-zVEC3&,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  AlignModelToLedge(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-AlignModelToLedge-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetHandPositionWorld(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-GetHandPositionWorld-zVEC3-false-',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  AlignToFloor(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-AlignToFloor-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetModelFloorWorld(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-GetModelFloorWorld-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  CheckEnoughSpaceMoveDir(a1: zVEC3, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-CheckEnoughSpaceMoveDir-int-false-zVEC3 const&,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  CheckEnoughSpaceMoveForward(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-CheckEnoughSpaceMoveForward-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  CheckEnoughSpaceMoveBackward(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-CheckEnoughSpaceMoveBackward-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  CheckEnoughSpaceMoveRight(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-CheckEnoughSpaceMoveRight-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  CheckEnoughSpaceMoveLeft(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-CheckEnoughSpaceMoveLeft-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetProtoYHeight(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-GetProtoYHeight-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetSurfaceAlignScanOrigin(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-GetSurfaceAlignScanOrigin-zVEC3-false-',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  DiveRotateX(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-DiveRotateX-void-false-float const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetCDStat(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-SetCDStat-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  LandAndStartAni(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-LandAndStartAni-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  LandAndStartAni2(a1: zCModelAni): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-LandAndStartAni-void-false-zCModelAni*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CheckForceModelHalt(a1: string): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-CheckForceModelHalt-int-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  CheckForceModelHalt2(a1: zCModelAni): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-CheckForceModelHalt-int-false-zCModelAni*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  ShouldCorrectFloorHeight(): number | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-ShouldCorrectFloorHeight-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  BeginStateSwitch(): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-BeginStateSwitch-void-false-',
      parentId: this.Uuid,
    })
  }

  SetPhysicsEnabled(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-SetPhysicsEnabled-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  StartPhysicsWithVel(): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-StartPhysicsWithVel-void-false-',
      parentId: this.Uuid,
    })
  }

  Print(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-Print-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  Line3D(a1: zVEC3, a2: zVEC3, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-Line3D-void-false-zVEC3 const&,zVEC3 const&,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  PrintScreen(a1: number, a2: number, a3: string): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-PrintScreen-void-false-int,int,zSTRING',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  GetModel(): zCModel | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-GetModel-zCModel*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCModel(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override DoAI(a1: zCVob, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-DoAI-void-false-zCVob*,int&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override HostVobRemovedFromWorld(a1: zCVob, a2: zCWorld): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-HostVobRemovedFromWorld-void-false-zCVob*,zCWorld*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  StartStandAni(): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-StartStandAni-void-false-',
      parentId: this.Uuid,
    })
  }

  StartFallDownAni(): void | null {
    const result: string | null = SendCommand({
      id: 'zCAIPlayer-StartFallDownAni-void-false-',
      parentId: this.Uuid,
    })
  }
}
