import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'

export class oSActionBlock extends BaseUnionObject {
  max(): number | null {
    const result: string | null = SendCommand({
      id: 'oSActionBlock-max-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_max(a1: number): null {
    SendCommand({
      id: 'SET_oSActionBlock-max-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  GetOwnAction(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oSActionBlock-GetOwnAction-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }
}
