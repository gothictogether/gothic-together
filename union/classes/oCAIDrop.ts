import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCVob } from './index.js'
import { zVEC3 } from './index.js'
import { zCClassDef } from './index.js'
import { oCAISound } from './index.js'

export class oCAIDrop extends oCAISound {
  vob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCAIDrop-vob-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_vob(a1: zCVob): null {
    SendCommand({
      id: 'SET_oCAIDrop-vob-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  owner(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCAIDrop-owner-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_owner(a1: zCVob): null {
    SendCommand({
      id: 'SET_oCAIDrop-owner-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  collisionOccured(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIDrop-collisionOccured-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_collisionOccured(a1: number): null {
    SendCommand({
      id: 'SET_oCAIDrop-collisionOccured-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  timer(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIDrop-timer-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_timer(a1: number): null {
    SendCommand({
      id: 'SET_oCAIDrop-timer-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  count(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIDrop-count-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_count(a1: number): null {
    SendCommand({
      id: 'SET_oCAIDrop-count-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCAIDrop_OnInit(): oCAIDrop | null {
    const result: string | null = SendCommand({
      id: 'oCAIDrop-oCAIDrop_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCAIDrop(result) : null
  }

  AddIgnoreCDVob(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIDrop-AddIgnoreCDVob-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  ClearIgnoreCDVob(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIDrop-ClearIgnoreCDVob-void-false-',
      parentId: this.Uuid,
    })
  }

  SetupAIVob(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIDrop-SetupAIVob-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  SetVelocity(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIDrop-SetVelocity-void-false-float,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  SetStartPosition(a1: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIDrop-SetStartPosition-void-false-zVEC3&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCAIDrop-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override DoAI(a1: zCVob, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIDrop-DoAI-void-false-zCVob*,int&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override CanThisCollideWith(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIDrop-CanThisCollideWith-int-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  override HasAIDetectedCollision(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIDrop-HasAIDetectedCollision-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }
}
