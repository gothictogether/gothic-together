import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { zCZone } from './index.js'

export class zCZoneZFog extends zCZone {
  fogRangeCenter(): number | null {
    const result: string | null = SendCommand({
      id: 'zCZoneZFog-fogRangeCenter-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fogRangeCenter(a1: number): null {
    SendCommand({
      id: 'SET_zCZoneZFog-fogRangeCenter-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  innerRangePerc(): number | null {
    const result: string | null = SendCommand({
      id: 'zCZoneZFog-innerRangePerc-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_innerRangePerc(a1: number): null {
    SendCommand({
      id: 'SET_zCZoneZFog-innerRangePerc-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  bFadeOutSky(): number | null {
    const result: string | null = SendCommand({
      id: 'zCZoneZFog-bFadeOutSky-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_bFadeOutSky(a1: number): null {
    SendCommand({
      id: 'SET_zCZoneZFog-bFadeOutSky-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  bOverrideColor(): number | null {
    const result: string | null = SendCommand({
      id: 'zCZoneZFog-bOverrideColor-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_bOverrideColor(a1: number): null {
    SendCommand({
      id: 'SET_zCZoneZFog-bOverrideColor-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCZoneZFog_OnInit(): zCZoneZFog | null {
    const result: string | null = SendCommand({
      id: 'zCZoneZFog-zCZoneZFog_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCZoneZFog(result) : null
  }

  GetActiveRange(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'zCZoneZFog-GetActiveRange-float-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetSkyFadeWeight(): number | null {
    const result: string | null = SendCommand({
      id: 'zCZoneZFog-GetSkyFadeWeight-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCZoneZFog-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override GetDefaultZoneClass(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCZoneZFog-GetDefaultZoneClass-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }
}
