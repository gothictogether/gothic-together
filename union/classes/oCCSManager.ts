import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { zCEventMessage } from './index.js'
import { zCCSPlayer } from './index.js'
import { zCWorld } from './index.js'
import { zCCSProps } from './index.js'
import { zCCSManager } from './index.js'

export class oCCSManager extends zCCSManager {
  static oCCSManager_OnInit(): oCCSManager | null {
    const result: string | null = SendCommand({
      id: 'oCCSManager-oCCSManager_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCCSManager(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCCSManager-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override CreateMessage(a1: number): zCEventMessage | null {
    const result: string | null = SendCommand({
      id: 'oCCSManager-CreateMessage-zCEventMessage*-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new zCEventMessage(result) : null
  }

  override CreateOuMessage(): zCEventMessage | null {
    const result: string | null = SendCommand({
      id: 'oCCSManager-CreateOuMessage-zCEventMessage*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCEventMessage(result) : null
  }

  override CreateCutscenePlayer(a1: zCWorld): zCCSPlayer | null {
    const result: string | null = SendCommand({
      id: 'oCCSManager-CreateCutscenePlayer-zCCSPlayer*-false-zCWorld*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new zCCSPlayer(result) : null
  }

  override CreateProperties(): zCCSProps | null {
    const result: string | null = SendCommand({
      id: 'oCCSManager-CreateProperties-zCCSProps*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCCSProps(result) : null
  }
}
