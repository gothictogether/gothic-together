import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCVob } from './index.js'
import { zVEC3 } from './index.js'

export class zTModelLimbColl extends BaseUnionObject {
  hitVob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'zTModelLimbColl-hitVob-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_hitVob(a1: zCVob): null {
    SendCommand({
      id: 'SET_zTModelLimbColl-hitVob-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  approxCollisionPos(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zTModelLimbColl-approxCollisionPos-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_approxCollisionPos(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zTModelLimbColl-approxCollisionPos-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zTModelLimbColl_OnInit(): zTModelLimbColl | null {
    const result: string | null = SendCommand({
      id: 'zTModelLimbColl-zTModelLimbColl_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zTModelLimbColl(result) : null
  }
}
