import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zTCacheEntry } from './index.js'

export class zCLineCache extends BaseUnionObject {
  numEntries(): number | null {
    const result: string | null = SendCommand({
      id: 'zCLineCache-numEntries-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_numEntries(a1: number): null {
    SendCommand({
      id: 'SET_zCLineCache-numEntries-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  nextFree(): number | null {
    const result: string | null = SendCommand({
      id: 'zCLineCache-nextFree-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_nextFree(a1: number): null {
    SendCommand({
      id: 'SET_zCLineCache-nextFree-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  cache(): zTCacheEntry | null {
    const result: string | null = SendCommand({
      id: 'zCLineCache-cache-zTCacheEntry*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zTCacheEntry(result) : null
  }

  set_cache(a1: zTCacheEntry): null {
    SendCommand({
      id: 'SET_zCLineCache-cache-zTCacheEntry*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
