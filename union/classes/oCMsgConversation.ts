import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCVob } from './index.js'
import { zVEC3 } from './index.js'
import { zCModelAni } from './index.js'
import { zCEventMessage } from './index.js'
import { TConversationSubType } from './index.js'
import { zCClassDef } from './index.js'
import { oCNpcMessage } from './index.js'

export class oCMsgConversation extends oCNpcMessage {
  text(): string | null {
    const result: string | null = SendCommand({
      id: 'oCMsgConversation-text-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_text(a1: string): null {
    SendCommand({
      id: 'SET_oCMsgConversation-text-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  name(): string | null {
    const result: string | null = SendCommand({
      id: 'oCMsgConversation-name-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_name(a1: string): null {
    SendCommand({
      id: 'SET_oCMsgConversation-name-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  target(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCMsgConversation-target-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_target(a1: zCVob): null {
    SendCommand({
      id: 'SET_oCMsgConversation-target-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  targetPos(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oCMsgConversation-targetPos-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_targetPos(a1: zVEC3): null {
    SendCommand({
      id: 'SET_oCMsgConversation-targetPos-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  ani(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgConversation-ani-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_ani(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgConversation-ani-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  cAni(): zCModelAni | null {
    const result: string | null = SendCommand({
      id: 'oCMsgConversation-cAni-zCModelAni*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCModelAni(result) : null
  }

  set_cAni(a1: zCModelAni): null {
    SendCommand({
      id: 'SET_oCMsgConversation-cAni-zCModelAni*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  watchMsg(): zCEventMessage | null {
    const result: string | null = SendCommand({
      id: 'oCMsgConversation-watchMsg-zCEventMessage*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCEventMessage(result) : null
  }

  set_watchMsg(a1: zCEventMessage): null {
    SendCommand({
      id: 'SET_oCMsgConversation-watchMsg-zCEventMessage*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  handle(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgConversation-handle-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_handle(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgConversation-handle-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  timer(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgConversation-timer-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_timer(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgConversation-timer-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  number(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgConversation-number-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_number(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgConversation-number-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  f_no(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgConversation-f_no-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_f_no(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgConversation-f_no-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  f_yes(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgConversation-f_yes-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_f_yes(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgConversation-f_yes-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCMsgConversation_OnInit(): oCMsgConversation | null {
    const result: string | null = SendCommand({
      id: 'oCMsgConversation-oCMsgConversation_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCMsgConversation(result) : null
  }

  static oCMsgConversation_OnInit2(a1: TConversationSubType): oCMsgConversation | null {
    const result: string | null = SendCommand({
      id: 'oCMsgConversation-oCMsgConversation_OnInit-void-false-TConversationSubType',
      parentId: 'NULL',
      a1: a1.toString(),
    })

    return result ? new oCMsgConversation(result) : null
  }

  static oCMsgConversation_OnInit3(a1: TConversationSubType, a2: string): oCMsgConversation | null {
    const result: string | null = SendCommand({
      id: 'oCMsgConversation-oCMsgConversation_OnInit-void-false-TConversationSubType,zSTRING const&',
      parentId: 'NULL',
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCMsgConversation(result) : null
  }

  static oCMsgConversation_OnInit4(
    a1: TConversationSubType,
    a2: string,
    a3: string,
  ): oCMsgConversation | null {
    const result: string | null = SendCommand({
      id: 'oCMsgConversation-oCMsgConversation_OnInit-void-false-TConversationSubType,zSTRING const&,zSTRING const&',
      parentId: 'NULL',
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? new oCMsgConversation(result) : null
  }

  static oCMsgConversation_OnInit5(a1: TConversationSubType, a2: zCVob): oCMsgConversation | null {
    const result: string | null = SendCommand({
      id: 'oCMsgConversation-oCMsgConversation_OnInit-void-false-TConversationSubType,zCVob*',
      parentId: 'NULL',
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCMsgConversation(result) : null
  }

  static oCMsgConversation_OnInit6(a1: TConversationSubType, a2: number): oCMsgConversation | null {
    const result: string | null = SendCommand({
      id: 'oCMsgConversation-oCMsgConversation_OnInit-void-false-TConversationSubType,int',
      parentId: 'NULL',
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCMsgConversation(result) : null
  }

  static oCMsgConversation_OnInit7(
    a1: TConversationSubType,
    a2: number,
    a3: number,
    a4: number,
  ): oCMsgConversation | null {
    const result: string | null = SendCommand({
      id: 'oCMsgConversation-oCMsgConversation_OnInit-void-false-TConversationSubType,int,int,float',
      parentId: 'NULL',
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
    })

    return result ? new oCMsgConversation(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCMsgConversation-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override IsOverlay(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgConversation-IsOverlay-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override Delete(): void | null {
    const result: string | null = SendCommand({
      id: 'oCMsgConversation-Delete-void-false-',
      parentId: this.Uuid,
    })
  }

  override MD_GetNumOfSubTypes(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgConversation-MD_GetNumOfSubTypes-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override MD_GetSubTypeString(a1: number): string | null {
    const result: string | null = SendCommand({
      id: 'oCMsgConversation-MD_GetSubTypeString-zSTRING-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? String(result) : null
  }

  override MD_GetVobRefName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCMsgConversation-MD_GetVobRefName-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  override MD_SetVobRefName(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCMsgConversation-MD_SetVobRefName-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override MD_SetVobParam(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCMsgConversation-MD_SetVobParam-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override MD_GetMinTime(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgConversation-MD_GetMinTime-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }
}
