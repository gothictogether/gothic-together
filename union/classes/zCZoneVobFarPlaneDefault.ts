import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { zCWorld } from './index.js'
import { zCZoneVobFarPlane } from './index.js'

export class zCZoneVobFarPlaneDefault extends zCZoneVobFarPlane {
  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCZoneVobFarPlaneDefault-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override ThisVobAddedToWorld(a1: zCWorld): void | null {
    const result: string | null = SendCommand({
      id: 'zCZoneVobFarPlaneDefault-ThisVobAddedToWorld-void-false-zCWorld*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }
}
