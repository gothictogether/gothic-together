import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCWaypoint } from './index.js'
import { zCWay } from './index.js'
import { zVEC3 } from './index.js'

export class zCRoute extends BaseUnionObject {
  startwp(): zCWaypoint | null {
    const result: string | null = SendCommand({
      id: 'zCRoute-startwp-zCWaypoint*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCWaypoint(result) : null
  }

  set_startwp(a1: zCWaypoint): null {
    SendCommand({
      id: 'SET_zCRoute-startwp-zCWaypoint*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  target(): zCWaypoint | null {
    const result: string | null = SendCommand({
      id: 'zCRoute-target-zCWaypoint*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCWaypoint(result) : null
  }

  set_target(a1: zCWaypoint): null {
    SendCommand({
      id: 'SET_zCRoute-target-zCWaypoint*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  way(): zCWay | null {
    const result: string | null = SendCommand({
      id: 'zCRoute-way-zCWay*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCWay(result) : null
  }

  set_way(a1: zCWay): null {
    SendCommand({
      id: 'SET_zCRoute-way-zCWay*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCRoute_OnInit(): zCRoute | null {
    const result: string | null = SendCommand({
      id: 'zCRoute-zCRoute_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCRoute(result) : null
  }

  SetStart(a1: zCWaypoint): void | null {
    const result: string | null = SendCommand({
      id: 'zCRoute-SetStart-void-false-zCWaypoint*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetNextWP(): zCWaypoint | null {
    const result: string | null = SendCommand({
      id: 'zCRoute-GetNextWP-zCWaypoint*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCWaypoint(result) : null
  }

  GetTargetWP(): zCWaypoint | null {
    const result: string | null = SendCommand({
      id: 'zCRoute-GetTargetWP-zCWaypoint*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCWaypoint(result) : null
  }

  GetCurrentWay(): zCWay | null {
    const result: string | null = SendCommand({
      id: 'zCRoute-GetCurrentWay-zCWay*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCWay(result) : null
  }

  GetDesc(): string | null {
    const result: string | null = SendCommand({
      id: 'zCRoute-GetDesc-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  GetRemainingDesc(): string | null {
    const result: string | null = SendCommand({
      id: 'zCRoute-GetRemainingDesc-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  GetNumberOfWaypoints(): number | null {
    const result: string | null = SendCommand({
      id: 'zCRoute-GetNumberOfWaypoints-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsEmpty(): number | null {
    const result: string | null = SendCommand({
      id: 'zCRoute-IsEmpty-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsLastTarget(): number | null {
    const result: string | null = SendCommand({
      id: 'zCRoute-IsLastTarget-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetInterpolatedPosition(a1: number, a2: number, a3: zVEC3): number | null {
    const result: string | null = SendCommand({
      id: 'zCRoute-GetInterpolatedPosition-int-false-float,int,zVEC3&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? Number(result) : null
  }
}
