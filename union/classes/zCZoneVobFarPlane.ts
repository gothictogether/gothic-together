import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { zCZone } from './index.js'

export class zCZoneVobFarPlane extends zCZone {
  vobFarZ(): number | null {
    const result: string | null = SendCommand({
      id: 'zCZoneVobFarPlane-vobFarZ-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_vobFarZ(a1: number): null {
    SendCommand({
      id: 'SET_zCZoneVobFarPlane-vobFarZ-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  innerRangePerc(): number | null {
    const result: string | null = SendCommand({
      id: 'zCZoneVobFarPlane-innerRangePerc-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_innerRangePerc(a1: number): null {
    SendCommand({
      id: 'SET_zCZoneVobFarPlane-innerRangePerc-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCZoneVobFarPlane_OnInit(): zCZoneVobFarPlane | null {
    const result: string | null = SendCommand({
      id: 'zCZoneVobFarPlane-zCZoneVobFarPlane_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCZoneVobFarPlane(result) : null
  }

  GetActiveFarZ(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'zCZoneVobFarPlane-GetActiveFarZ-float-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCZoneVobFarPlane-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override GetDefaultZoneClass(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCZoneVobFarPlane-GetDefaultZoneClass-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }
}
