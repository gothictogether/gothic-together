import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zVEC3 } from './index.js'
import { zCVob } from './index.js'
import { zCClassDef } from './index.js'
import { zCCamera } from './index.js'
import { oCAIVobMove } from './index.js'
import { oTSndMaterial } from './index.js'

export class oCVob extends zCVob {
  static oCVob_OnInit(): oCVob | null {
    const result: string | null = SendCommand({
      id: 'oCVob-oCVob_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCVob(result) : null
  }

  GetShowDebug(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVob-GetShowDebug-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  ToggleShowDebug(): void | null {
    const result: string | null = SendCommand({
      id: 'oCVob-ToggleShowDebug-void-false-',
      parentId: this.Uuid,
    })
  }

  SetShowDebug(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCVob-SetShowDebug-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  HasNpcEnoughSpace(a1: zVEC3): number | null {
    const result: string | null = SendCommand({
      id: 'oCVob-HasNpcEnoughSpace-int-false-zVEC3&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  HasEnoughSpace(a1: zVEC3): number | null {
    const result: string | null = SendCommand({
      id: 'oCVob-HasEnoughSpace-int-false-zVEC3&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  SearchNpcPosition(a1: zVEC3): number | null {
    const result: string | null = SendCommand({
      id: 'oCVob-SearchNpcPosition-int-false-zVEC3&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  SearchFreePosition(a1: zVEC3, a2: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'oCVob-SearchFreePosition-int-false-zVEC3&,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  SetOnFloor(a1: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'oCVob-SetOnFloor-void-false-zVEC3&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetFloorPosition(a1: zVEC3): number | null {
    const result: string | null = SendCommand({
      id: 'oCVob-GetFloorPosition-int-false-zVEC3&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCVob-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override GetCSStateFlags(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVob-GetCSStateFlags-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  Init(): void | null {
    const result: string | null = SendCommand({
      id: 'oCVob-Init-void-false-',
      parentId: this.Uuid,
    })
  }

  ShowDebugInfo(a1: zCCamera): void | null {
    const result: string | null = SendCommand({
      id: 'oCVob-ShowDebugInfo-void-false-zCCamera*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetInstance(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVob-GetInstance-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetInstanceName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCVob-GetInstanceName-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  IsOwnedByGuild(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCVob-IsOwnedByGuild-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  IsOwnedByNpc(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCVob-IsOwnedByNpc-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  DoFocusCheckBBox(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVob-DoFocusCheckBBox-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetAIVobMove(): oCAIVobMove | null {
    const result: string | null = SendCommand({
      id: 'oCVob-GetAIVobMove-oCAIVobMove*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCAIVobMove(result) : null
  }

  SetSoundMaterial(a1: oTSndMaterial): void | null {
    const result: string | null = SendCommand({
      id: 'oCVob-SetSoundMaterial-void-false-oTSndMaterial',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetSoundMaterial(): oTSndMaterial | null {
    const result: string | null = SendCommand({
      id: 'oCVob-GetSoundMaterial-oTSndMaterial-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }
}
