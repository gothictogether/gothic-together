import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCVob } from './index.js'
import { zVEC3 } from './index.js'
import { zCWorld } from './index.js'

export class oCParticleControl extends BaseUnionObject {
  pfxVob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCParticleControl-pfxVob-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_pfxVob(a1: zCVob): null {
    SendCommand({
      id: 'SET_oCParticleControl-pfxVob-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pfxVobID(): number | null {
    const result: string | null = SendCommand({
      id: 'oCParticleControl-pfxVobID-unsigned long-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_pfxVobID(a1: number): null {
    SendCommand({
      id: 'SET_oCParticleControl-pfxVobID-unsigned long-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pfxName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCParticleControl-pfxName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_pfxName(a1: string): null {
    SendCommand({
      id: 'SET_oCParticleControl-pfxName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  startpos(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oCParticleControl-startpos-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_startpos(a1: zVEC3): null {
    SendCommand({
      id: 'SET_oCParticleControl-startpos-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  IsInEditMode(): number | null {
    const result: string | null = SendCommand({
      id: 'oCParticleControl-IsInEditMode-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_IsInEditMode(a1: number): null {
    SendCommand({
      id: 'SET_oCParticleControl-IsInEditMode-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  rnd_world(): zCWorld | null {
    const result: string | null = SendCommand({
      id: 'oCParticleControl-rnd_world-zCWorld*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCWorld(result) : null
  }

  set_rnd_world(a1: zCWorld): null {
    SendCommand({
      id: 'SET_oCParticleControl-rnd_world-zCWorld*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCParticleControl_OnInit(): oCParticleControl | null {
    const result: string | null = SendCommand({
      id: 'oCParticleControl-oCParticleControl_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCParticleControl(result) : null
  }

  static oCParticleControl_OnInit2(a1: zCWorld): oCParticleControl | null {
    const result: string | null = SendCommand({
      id: 'oCParticleControl-oCParticleControl_OnInit-void-false-zCWorld*',
      parentId: 'NULL',
      a1: a1.toString(),
    })

    return result ? new oCParticleControl(result) : null
  }

  GetPFXVob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCParticleControl-GetPFXVob-zCVob*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  SelectActivePFX(): void | null {
    const result: string | null = SendCommand({
      id: 'oCParticleControl-SelectActivePFX-void-false-',
      parentId: this.Uuid,
    })
  }

  EditActivePFX(): void | null {
    const result: string | null = SendCommand({
      id: 'oCParticleControl-EditActivePFX-void-false-',
      parentId: this.Uuid,
    })
  }

  SetStartPos(a1: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'oCParticleControl-SetStartPos-void-false-zVEC3&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  StartActivePFX(a1: zCVob, a2: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCParticleControl-StartActivePFX-void-false-zCVob*,zSTRING&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  StartActivePFX2(): void | null {
    const result: string | null = SendCommand({
      id: 'oCParticleControl-StartActivePFX-void-false-',
      parentId: this.Uuid,
    })
  }

  CreateNewPFX(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCParticleControl-CreateNewPFX-void-false-zSTRING&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetPFXName(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCParticleControl-SetPFXName-void-false-zSTRING&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetWorld(a1: zCWorld): void | null {
    const result: string | null = SendCommand({
      id: 'oCParticleControl-SetWorld-void-false-zCWorld*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  DeleteLastPFX(): void | null {
    const result: string | null = SendCommand({
      id: 'oCParticleControl-DeleteLastPFX-void-false-',
      parentId: this.Uuid,
    })
  }

  UpdateInternals(): void | null {
    const result: string | null = SendCommand({
      id: 'oCParticleControl-UpdateInternals-void-false-',
      parentId: this.Uuid,
    })
  }

  EndEditActive(): void | null {
    const result: string | null = SendCommand({
      id: 'oCParticleControl-EndEditActive-void-false-',
      parentId: this.Uuid,
    })
  }
}
