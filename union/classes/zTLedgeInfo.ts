import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zVEC3 } from './index.js'

export class zTLedgeInfo extends BaseUnionObject {
  point(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zTLedgeInfo-point-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_point(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zTLedgeInfo-point-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  normal(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zTLedgeInfo-normal-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_normal(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zTLedgeInfo-normal-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  cont(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zTLedgeInfo-cont-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_cont(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zTLedgeInfo-cont-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  maxMoveForward(): number | null {
    const result: string | null = SendCommand({
      id: 'zTLedgeInfo-maxMoveForward-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_maxMoveForward(a1: number): null {
    SendCommand({
      id: 'SET_zTLedgeInfo-maxMoveForward-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
