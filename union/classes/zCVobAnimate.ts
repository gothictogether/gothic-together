import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { zCVob } from './index.js'
import { zCEffect } from './index.js'

export class zCVobAnimate extends zCEffect {
  startOn(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobAnimate-startOn-char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_startOn(a1: number): null {
    SendCommand({
      id: 'SET_zCVobAnimate-startOn-char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  isRunning(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobAnimate-isRunning-char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_isRunning(a1: number): null {
    SendCommand({
      id: 'SET_zCVobAnimate-isRunning-char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCVobAnimate_OnInit(): zCVobAnimate | null {
    const result: string | null = SendCommand({
      id: 'zCVobAnimate-zCVobAnimate_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCVobAnimate(result) : null
  }

  StartAni(): void | null {
    const result: string | null = SendCommand({
      id: 'zCVobAnimate-StartAni-void-false-',
      parentId: this.Uuid,
    })
  }

  StopAni(): void | null {
    const result: string | null = SendCommand({
      id: 'zCVobAnimate-StopAni-void-false-',
      parentId: this.Uuid,
    })
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCVobAnimate-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override OnTrigger(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCVobAnimate-OnTrigger-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override OnUntrigger(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCVobAnimate-OnUntrigger-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }
}
