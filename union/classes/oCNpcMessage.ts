import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { zCEventMessage } from './index.js'

export class oCNpcMessage extends zCEventMessage {
  targetVobName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCNpcMessage-targetVobName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_targetVobName(a1: string): null {
    SendCommand({
      id: 'SET_oCNpcMessage-targetVobName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  highPriority(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcMessage-highPriority-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_highPriority(a1: number): null {
    SendCommand({
      id: 'SET_oCNpcMessage-highPriority-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  deleted(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcMessage-deleted-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_deleted(a1: number): null {
    SendCommand({
      id: 'SET_oCNpcMessage-deleted-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  inUse(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcMessage-inUse-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_inUse(a1: number): null {
    SendCommand({
      id: 'SET_oCNpcMessage-inUse-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCNpcMessage_OnInit(): oCNpcMessage | null {
    const result: string | null = SendCommand({
      id: 'oCNpcMessage-oCNpcMessage_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCNpcMessage(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCNpcMessage-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override IsOverlay(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcMessage-IsOverlay-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override IsHighPriority(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcMessage-IsHighPriority-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override IsJob(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcMessage-IsJob-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override GetIgnoreCutsceneMode(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcMessage-GetIgnoreCutsceneMode-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override Delete(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpcMessage-Delete-void-false-',
      parentId: this.Uuid,
    })
  }

  override IsDeleteable(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcMessage-IsDeleteable-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override IsDeleted(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcMessage-IsDeleted-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetInUse(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpcMessage-SetInUse-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  IsInUse(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcMessage-IsInUse-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetHighPriority(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpcMessage-SetHighPriority-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }
}
