import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCVob } from './index.js'
import { TUseItemSubType } from './index.js'
import { zCClassDef } from './index.js'
import { oCNpcMessage } from './index.js'

export class oCMsgUseItem extends oCNpcMessage {
  vob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCMsgUseItem-vob-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_vob(a1: zCVob): null {
    SendCommand({
      id: 'SET_oCMsgUseItem-vob-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  ani(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgUseItem-ani-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_ani(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgUseItem-ani-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  state(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgUseItem-state-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_state(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgUseItem-state-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCMsgUseItem_OnInit(): oCMsgUseItem | null {
    const result: string | null = SendCommand({
      id: 'oCMsgUseItem-oCMsgUseItem_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCMsgUseItem(result) : null
  }

  static oCMsgUseItem_OnInit2(a1: TUseItemSubType, a2: zCVob): oCMsgUseItem | null {
    const result: string | null = SendCommand({
      id: 'oCMsgUseItem-oCMsgUseItem_OnInit-void-false-TUseItemSubType,zCVob*',
      parentId: 'NULL',
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCMsgUseItem(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCMsgUseItem-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override IsNetRelevant(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgUseItem-IsNetRelevant-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }
}
