import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCTriggerDummy0 } from './index.js'
import { zCVob } from './index.js'
import { zCClassDef } from './index.js'
import { zVEC3 } from './index.js'
import { zCEventMessage } from './index.js'
import { zCTriggerBase } from './index.js'

export class zCTrigger extends zCTriggerBase {
  filterFlags(): zCTriggerDummy0 | null {
    const result: string | null = SendCommand({
      id: 'zCTrigger-filterFlags-zCTriggerDummy0-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCTriggerDummy0(result) : null
  }

  set_filterFlags(a1: zCTriggerDummy0): null {
    SendCommand({
      id: 'SET_zCTrigger-filterFlags-zCTriggerDummy0-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  respondToVobName(): string | null {
    const result: string | null = SendCommand({
      id: 'zCTrigger-respondToVobName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_respondToVobName(a1: string): null {
    SendCommand({
      id: 'SET_zCTrigger-respondToVobName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  numCanBeActivated(): number | null {
    const result: string | null = SendCommand({
      id: 'zCTrigger-numCanBeActivated-short-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_numCanBeActivated(a1: number): null {
    SendCommand({
      id: 'SET_zCTrigger-numCanBeActivated-short-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  retriggerWaitSec(): number | null {
    const result: string | null = SendCommand({
      id: 'zCTrigger-retriggerWaitSec-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_retriggerWaitSec(a1: number): null {
    SendCommand({
      id: 'SET_zCTrigger-retriggerWaitSec-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  damageThreshold(): number | null {
    const result: string | null = SendCommand({
      id: 'zCTrigger-damageThreshold-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_damageThreshold(a1: number): null {
    SendCommand({
      id: 'SET_zCTrigger-damageThreshold-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fireDelaySec(): number | null {
    const result: string | null = SendCommand({
      id: 'zCTrigger-fireDelaySec-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fireDelaySec(a1: number): null {
    SendCommand({
      id: 'SET_zCTrigger-fireDelaySec-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  nextTimeTriggerable(): number | null {
    const result: string | null = SendCommand({
      id: 'zCTrigger-nextTimeTriggerable-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_nextTimeTriggerable(a1: number): null {
    SendCommand({
      id: 'SET_zCTrigger-nextTimeTriggerable-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  savedOtherVob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'zCTrigger-savedOtherVob-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_savedOtherVob(a1: zCVob): null {
    SendCommand({
      id: 'SET_zCTrigger-savedOtherVob-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  countCanBeActivated(): number | null {
    const result: string | null = SendCommand({
      id: 'zCTrigger-countCanBeActivated-short-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_countCanBeActivated(a1: number): null {
    SendCommand({
      id: 'SET_zCTrigger-countCanBeActivated-short-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCTrigger_OnInit(): zCTrigger | null {
    const result: string | null = SendCommand({
      id: 'zCTrigger-zCTrigger_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCTrigger(result) : null
  }

  override GetTriggerTarget(): string | null {
    const result: string | null = SendCommand({
      id: 'zCTrigger-GetTriggerTarget-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  ClearStateInternals(): void | null {
    const result: string | null = SendCommand({
      id: 'zCTrigger-ClearStateInternals-void-false-',
      parentId: this.Uuid,
    })
  }

  TriggerTargetPost(): void | null {
    const result: string | null = SendCommand({
      id: 'zCTrigger-TriggerTargetPost-void-false-',
      parentId: this.Uuid,
    })
  }

  ActivateTrigger(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCTrigger-ActivateTrigger-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  DeactivateTrigger(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCTrigger-DeactivateTrigger-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCTrigger-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override OnTrigger(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCTrigger-OnTrigger-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override OnUntrigger(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCTrigger-OnUntrigger-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override OnTouch(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCTrigger-OnTouch-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override OnUntouch(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCTrigger-OnUntouch-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override OnDamage(a1: zCVob, a2: zCVob, a3: number, a4: number, a5: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'zCTrigger-OnDamage-void-false-zCVob*,zCVob*,float,int,zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
      a5: a5.toString(),
    })
  }

  override OnMessage(a1: zCEventMessage, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCTrigger-OnMessage-void-false-zCEventMessage*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override OnTimer(): void | null {
    const result: string | null = SendCommand({
      id: 'zCTrigger-OnTimer-void-false-',
      parentId: this.Uuid,
    })
  }

  TriggerTarget(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCTrigger-TriggerTarget-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  UntriggerTarget(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCTrigger-UntriggerTarget-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CanBeActivatedNow(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'zCTrigger-CanBeActivatedNow-int-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }
}
