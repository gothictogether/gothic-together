import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'

export class zTAniAttachment extends BaseUnionObject {
  aniID(): number | null {
    const result: string | null = SendCommand({
      id: 'zTAniAttachment-aniID-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_aniID(a1: number): null {
    SendCommand({
      id: 'SET_zTAniAttachment-aniID-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  randAniFreq(): number | null {
    const result: string | null = SendCommand({
      id: 'zTAniAttachment-randAniFreq-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_randAniFreq(a1: number): null {
    SendCommand({
      id: 'SET_zTAniAttachment-randAniFreq-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  randAniProbSum(): number | null {
    const result: string | null = SendCommand({
      id: 'zTAniAttachment-randAniProbSum-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_randAniProbSum(a1: number): null {
    SendCommand({
      id: 'SET_zTAniAttachment-randAniProbSum-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zTAniAttachment_OnInit(): zTAniAttachment | null {
    const result: string | null = SendCommand({
      id: 'zTAniAttachment-zTAniAttachment_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zTAniAttachment(result) : null
  }
}
