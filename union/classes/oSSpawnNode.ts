import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { oCNpc } from './index.js'
import { zVEC3 } from './index.js'

export class oSSpawnNode extends BaseUnionObject {
  npc(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oSSpawnNode-npc-oCNpc*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  set_npc(a1: oCNpc): null {
    SendCommand({
      id: 'SET_oSSpawnNode-npc-oCNpc*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  spawnPos(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oSSpawnNode-spawnPos-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_spawnPos(a1: zVEC3): null {
    SendCommand({
      id: 'SET_oSSpawnNode-spawnPos-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  timer(): number | null {
    const result: string | null = SendCommand({
      id: 'oSSpawnNode-timer-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_timer(a1: number): null {
    SendCommand({
      id: 'SET_oSSpawnNode-timer-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
