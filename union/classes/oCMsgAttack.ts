import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCVob } from './index.js'
import { TAttackSubType } from './index.js'
import { zCClassDef } from './index.js'
import { oCNpcMessage } from './index.js'

export class oCMsgAttack extends oCNpcMessage {
  combo(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgAttack-combo-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_combo(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgAttack-combo-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  target(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCMsgAttack-target-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_target(a1: zCVob): null {
    SendCommand({
      id: 'SET_oCMsgAttack-target-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  hitAni(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgAttack-hitAni-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_hitAni(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgAttack-hitAni-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  startFrame(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgAttack-startFrame-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_startFrame(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgAttack-startFrame-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  enableNextHit(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgAttack-enableNextHit-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_enableNextHit(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgAttack-enableNextHit-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  reachedTarget(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgAttack-reachedTarget-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_reachedTarget(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgAttack-reachedTarget-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCMsgAttack_OnInit(): oCMsgAttack | null {
    const result: string | null = SendCommand({
      id: 'oCMsgAttack-oCMsgAttack_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCMsgAttack(result) : null
  }

  static oCMsgAttack_OnInit2(a1: TAttackSubType, a2: number, a3: number): oCMsgAttack | null {
    const result: string | null = SendCommand({
      id: 'oCMsgAttack-oCMsgAttack_OnInit-void-false-TAttackSubType,int,int',
      parentId: 'NULL',
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? new oCMsgAttack(result) : null
  }

  static oCMsgAttack_OnInit3(a1: TAttackSubType, a2: zCVob, a3: number): oCMsgAttack | null {
    const result: string | null = SendCommand({
      id: 'oCMsgAttack-oCMsgAttack_OnInit-void-false-TAttackSubType,zCVob*,float',
      parentId: 'NULL',
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? new oCMsgAttack(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCMsgAttack-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override IsOverlay(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgAttack-IsOverlay-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override IsNetRelevant(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgAttack-IsNetRelevant-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override MD_GetNumOfSubTypes(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgAttack-MD_GetNumOfSubTypes-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override MD_GetSubTypeString(a1: number): string | null {
    const result: string | null = SendCommand({
      id: 'oCMsgAttack-MD_GetSubTypeString-zSTRING-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? String(result) : null
  }
}
