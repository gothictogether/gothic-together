import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { zCVob } from './index.js'
import { zCTrigger } from './index.js'

export class oCTriggerScript extends zCTrigger {
  scriptFunc(): string | null {
    const result: string | null = SendCommand({
      id: 'oCTriggerScript-scriptFunc-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_scriptFunc(a1: string): null {
    SendCommand({
      id: 'SET_oCTriggerScript-scriptFunc-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCTriggerScript_OnInit(): oCTriggerScript | null {
    const result: string | null = SendCommand({
      id: 'oCTriggerScript-oCTriggerScript_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCTriggerScript(result) : null
  }

  SetScriptFunc(a1: string, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCTriggerScript-SetScriptFunc-void-false-zSTRING const&,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCTriggerScript-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override TriggerTarget(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCTriggerScript-TriggerTarget-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override UntriggerTarget(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCTriggerScript-UntriggerTarget-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }
}
