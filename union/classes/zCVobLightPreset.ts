import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCVobLightData } from './index.js'
import { zCClassDef } from './index.js'
import { zCObject } from './index.js'

export class zCVobLightPreset extends zCObject {
  lightData(): zCVobLightData | null {
    const result: string | null = SendCommand({
      id: 'zCVobLightPreset-lightData-zCVobLightData-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVobLightData(result) : null
  }

  set_lightData(a1: zCVobLightData): null {
    SendCommand({
      id: 'SET_zCVobLightPreset-lightData-zCVobLightData-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  presetName(): string | null {
    const result: string | null = SendCommand({
      id: 'zCVobLightPreset-presetName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_presetName(a1: string): null {
    SendCommand({
      id: 'SET_zCVobLightPreset-presetName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCVobLightPreset-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }
}
