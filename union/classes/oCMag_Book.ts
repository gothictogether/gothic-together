import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCWorld } from './index.js'
import { zCVob } from './index.js'
import { zCModel } from './index.js'
import { zVEC3 } from './index.js'
import { oCSpell } from './index.js'
import { oCItem } from './index.js'
import { oCNpc } from './index.js'

export class oCMag_Book extends BaseUnionObject {
  wld(): zCWorld | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-wld-zCWorld*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCWorld(result) : null
  }

  set_wld(a1: zCWorld): null {
    SendCommand({
      id: 'SET_oCMag_Book-wld-zCWorld*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  owner(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-owner-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_owner(a1: zCVob): null {
    SendCommand({
      id: 'SET_oCMag_Book-owner-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  model(): zCModel | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-model-zCModel*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCModel(result) : null
  }

  set_model(a1: zCModel): null {
    SendCommand({
      id: 'SET_oCMag_Book-model-zCModel*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  spellnr(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-spellnr-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_spellnr(a1: number): null {
    SendCommand({
      id: 'SET_oCMag_Book-spellnr-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  MAG_HEIGHT(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-MAG_HEIGHT-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_MAG_HEIGHT(a1: number): null {
    SendCommand({
      id: 'SET_oCMag_Book-MAG_HEIGHT-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  active(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-active-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_active(a1: number): null {
    SendCommand({
      id: 'SET_oCMag_Book-active-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  remove_all(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-remove_all-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_remove_all(a1: number): null {
    SendCommand({
      id: 'SET_oCMag_Book-remove_all-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  in_movement(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-in_movement-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_in_movement(a1: number): null {
    SendCommand({
      id: 'SET_oCMag_Book-in_movement-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  show_handsymbol(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-show_handsymbol-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_show_handsymbol(a1: number): null {
    SendCommand({
      id: 'SET_oCMag_Book-show_handsymbol-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  step(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-step-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_step(a1: number): null {
    SendCommand({
      id: 'SET_oCMag_Book-step-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  action(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-action-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_action(a1: number): null {
    SendCommand({
      id: 'SET_oCMag_Book-action-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fullangle(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-fullangle-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fullangle(a1: number): null {
    SendCommand({
      id: 'SET_oCMag_Book-fullangle-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  open(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-open-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_open(a1: number): null {
    SendCommand({
      id: 'SET_oCMag_Book-open-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  open_delay_timer(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-open_delay_timer-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_open_delay_timer(a1: number): null {
    SendCommand({
      id: 'SET_oCMag_Book-open_delay_timer-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  show_particles(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-show_particles-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_show_particles(a1: number): null {
    SendCommand({
      id: 'SET_oCMag_Book-show_particles-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  targetdir(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-targetdir-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_targetdir(a1: number): null {
    SendCommand({
      id: 'SET_oCMag_Book-targetdir-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  t1(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-t1-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_t1(a1: number): null {
    SendCommand({
      id: 'SET_oCMag_Book-t1-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  targetpos(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-targetpos-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_targetpos(a1: zVEC3): null {
    SendCommand({
      id: 'SET_oCMag_Book-targetpos-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  startpos(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-startpos-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_startpos(a1: zVEC3): null {
    SendCommand({
      id: 'SET_oCMag_Book-startpos-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  nextRegister(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-nextRegister-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_nextRegister(a1: number): null {
    SendCommand({
      id: 'SET_oCMag_Book-nextRegister-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCMag_Book_OnInit(): oCMag_Book | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-oCMag_Book_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCMag_Book(result) : null
  }

  CheckConsistency(): void | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-CheckConsistency-void-false-',
      parentId: this.Uuid,
    })
  }

  SetOwner(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-SetOwner-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetWorld(a1: zCWorld): void | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-SetWorld-void-false-zCWorld*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  Register(a1: oCSpell, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-Register-int-false-oCSpell*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  Register2(a1: oCItem, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-Register-int-false-oCItem*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  DeRegister(a1: oCItem): number | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-DeRegister-int-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  DeRegister2(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-DeRegister-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  CreateNewSpell(a1: number): oCSpell | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-CreateNewSpell-oCSpell*-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new oCSpell(result) : null
  }

  Register3(a1: number, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-Register-int-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  IsIn(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-IsIn-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  Spell_Setup(a1: number, a2: oCNpc, a3: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-Spell_Setup-void-false-int,oCNpc*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  Spell_Setup2(a1: oCNpc, a2: zCVob, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-Spell_Setup-void-false-oCNpc*,zCVob*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  Spell_Invest(): void | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-Spell_Invest-void-false-',
      parentId: this.Uuid,
    })
  }

  StartInvestEffect(a1: zCVob, a2: number, a3: number, a4: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-StartInvestEffect-void-false-zCVob*,int,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
    })
  }

  Spell_Cast(): void | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-Spell_Cast-void-false-',
      parentId: this.Uuid,
    })
  }

  StartCastEffect(a1: zCVob, a2: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-StartCastEffect-void-false-zCVob*,zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  Spell_InCast(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-Spell_InCast-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  Open(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-Open-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  Close(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-Close-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  Ease(a1: number, a2: number, a3: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-Ease-float-false-float,float,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? Number(result) : null
  }

  EasePos(a1: number, a2: zVEC3, a3: zVEC3): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-EasePos-zVEC3-false-float,zVEC3&,zVEC3&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? new zVEC3(result) : null
  }

  GetAngle(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-GetAngle-float-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  IsInMovement(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-IsInMovement-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsActive(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-IsActive-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetSelectedSpell(): oCSpell | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-GetSelectedSpell-oCSpell*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCSpell(result) : null
  }

  GetSelectedSpellNr(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-GetSelectedSpellNr-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  Spell_Open(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-Spell_Open-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  StopSelectedSpell(): void | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-StopSelectedSpell-void-false-',
      parentId: this.Uuid,
    })
  }

  KillSelectedSpell(): void | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-KillSelectedSpell-void-false-',
      parentId: this.Uuid,
    })
  }

  Spell_Stop(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-Spell_Stop-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  Left(): void | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-Left-void-false-',
      parentId: this.Uuid,
    })
  }

  Right(): void | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-Right-void-false-',
      parentId: this.Uuid,
    })
  }

  CalcPos(a1: zCVob, a2: number): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-CalcPos-zVEC3-false-zCVob*,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new zVEC3(result) : null
  }

  DoOpen(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-DoOpen-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  DoClose(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-DoClose-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetFrontSpell(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-SetFrontSpell-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  DoTurn(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-DoTurn-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  ShowHandSymbol(): void | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-ShowHandSymbol-void-false-',
      parentId: this.Uuid,
    })
  }

  SetShowHandSymbol(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-SetShowHandSymbol-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  DoPerFrame(): void | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-DoPerFrame-void-false-',
      parentId: this.Uuid,
    })
  }

  GetNoOfSpells(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-GetNoOfSpells-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetSpell(a1: oCItem, a2: number): oCSpell | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-GetSpell-oCSpell*-false-oCItem*,int&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCSpell(result) : null
  }

  GetSpell2(a1: number): oCSpell | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-GetSpell-oCSpell*-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new oCSpell(result) : null
  }

  GetSpellItem(a1: number): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-GetSpellItem-oCItem*-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new oCItem(result) : null
  }

  GetSpellItem2(a1: oCSpell): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-GetSpellItem-oCItem*-false-oCSpell*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new oCItem(result) : null
  }

  NextRegisterAt(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-NextRegisterAt-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetSpellByKey(a1: number): oCSpell | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-GetSpellByKey-oCSpell*-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new oCSpell(result) : null
  }

  GetSpellItemByKey(a1: number): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-GetSpellItemByKey-oCItem*-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new oCItem(result) : null
  }

  GetNoOfSpellByKey(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-GetNoOfSpellByKey-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetKeyByItem(a1: oCItem): number | null {
    const result: string | null = SendCommand({
      id: 'oCMag_Book-GetKeyByItem-int-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }
}
