import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCPolygon } from './index.js'
import { zESkyLayerMode } from './index.js'
import { zCSkyState } from './index.js'

export class zCSkyLayer extends BaseUnionObject {
  skyPoly(): zCPolygon | null {
    const result: string | null = SendCommand({
      id: 'zCSkyLayer-skyPoly-zCPolygon*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCPolygon(result) : null
  }

  set_skyPoly(a1: zCPolygon): null {
    SendCommand({
      id: 'SET_zCSkyLayer-skyPoly-zCPolygon*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  skyMode(): zESkyLayerMode | null {
    const result: string | null = SendCommand({
      id: 'zCSkyLayer-skyMode-zESkyLayerMode-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_skyMode(a1: zESkyLayerMode): null {
    SendCommand({
      id: 'SET_zCSkyLayer-skyMode-zESkyLayerMode-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCSkyLayer_OnInit(): zCSkyLayer | null {
    const result: string | null = SendCommand({
      id: 'zCSkyLayer-zCSkyLayer_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCSkyLayer(result) : null
  }

  SetDomeMeshEnabled(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyLayer-SetDomeMeshEnabled-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RenderSkyLayer(a1: zCSkyState): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyLayer-RenderSkyLayer-void-false-zCSkyState*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RenderSkyPoly(a1: zCSkyState): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyLayer-RenderSkyPoly-void-false-zCSkyState*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RenderSkyDome(a1: zCSkyState): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyLayer-RenderSkyDome-void-false-zCSkyState*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  ColorizeSkyDome(): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyLayer-ColorizeSkyDome-void-false-',
      parentId: this.Uuid,
    })
  }
}
