import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zVEC3 } from './index.js'

export class zCOutdoorRainFX extends BaseUnionObject {
  m_numFlyParticle(): number | null {
    const result: string | null = SendCommand({
      id: 'zCOutdoorRainFX-m_numFlyParticle-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_numFlyParticle(a1: number): null {
    SendCommand({
      id: 'SET_zCOutdoorRainFX-m_numFlyParticle-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_numImpactParticle(): number | null {
    const result: string | null = SendCommand({
      id: 'zCOutdoorRainFX-m_numImpactParticle-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_numImpactParticle(a1: number): null {
    SendCommand({
      id: 'SET_zCOutdoorRainFX-m_numImpactParticle-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_effectWeight(): number | null {
    const result: string | null = SendCommand({
      id: 'zCOutdoorRainFX-m_effectWeight-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_effectWeight(a1: number): null {
    SendCommand({
      id: 'SET_zCOutdoorRainFX-m_effectWeight-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_numDestFlyParticle(): number | null {
    const result: string | null = SendCommand({
      id: 'zCOutdoorRainFX-m_numDestFlyParticle-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_numDestFlyParticle(a1: number): null {
    SendCommand({
      id: 'SET_zCOutdoorRainFX-m_numDestFlyParticle-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_camPosLastFrame(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCOutdoorRainFX-m_camPosLastFrame-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_m_camPosLastFrame(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCOutdoorRainFX-m_camPosLastFrame-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_currentCacheIndex(): number | null {
    const result: string | null = SendCommand({
      id: 'zCOutdoorRainFX-m_currentCacheIndex-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_currentCacheIndex(a1: number): null {
    SendCommand({
      id: 'SET_zCOutdoorRainFX-m_currentCacheIndex-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_soundHandle(): number | null {
    const result: string | null = SendCommand({
      id: 'zCOutdoorRainFX-m_soundHandle-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_soundHandle(a1: number): null {
    SendCommand({
      id: 'SET_zCOutdoorRainFX-m_soundHandle-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_soundActive(): number | null {
    const result: string | null = SendCommand({
      id: 'zCOutdoorRainFX-m_soundActive-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_soundActive(a1: number): null {
    SendCommand({
      id: 'SET_zCOutdoorRainFX-m_soundActive-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_positionUpdateVector(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCOutdoorRainFX-m_positionUpdateVector-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_m_positionUpdateVector(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCOutdoorRainFX-m_positionUpdateVector-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_velocityLen(): number | null {
    const result: string | null = SendCommand({
      id: 'zCOutdoorRainFX-m_velocityLen-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_velocityLen(a1: number): null {
    SendCommand({
      id: 'SET_zCOutdoorRainFX-m_velocityLen-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_timeLen(): number | null {
    const result: string | null = SendCommand({
      id: 'zCOutdoorRainFX-m_timeLen-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_timeLen(a1: number): null {
    SendCommand({
      id: 'SET_zCOutdoorRainFX-m_timeLen-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCOutdoorRainFX_OnInit(): zCOutdoorRainFX | null {
    const result: string | null = SendCommand({
      id: 'zCOutdoorRainFX-zCOutdoorRainFX_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCOutdoorRainFX(result) : null
  }

  UpdateSound(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCOutdoorRainFX-UpdateSound-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetEffectWeight(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCOutdoorRainFX-SetEffectWeight-void-false-float,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  CheckCameraBeam(a1: zVEC3): number | null {
    const result: string | null = SendCommand({
      id: 'zCOutdoorRainFX-CheckCameraBeam-int-false-zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  UpdateParticles(): void | null {
    const result: string | null = SendCommand({
      id: 'zCOutdoorRainFX-UpdateParticles-void-false-',
      parentId: this.Uuid,
    })
  }
}
