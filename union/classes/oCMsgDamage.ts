import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { oSDamageDescriptor } from './index.js'
import { TDamageSubType } from './index.js'
import { zCClassDef } from './index.js'
import { oCNpcMessage } from './index.js'

export class oCMsgDamage extends oCNpcMessage {
  descDamage(): oSDamageDescriptor | null {
    const result: string | null = SendCommand({
      id: 'oCMsgDamage-descDamage-oSDamageDescriptor-false-false',
      parentId: this.Uuid,
    })

    return result ? new oSDamageDescriptor(result) : null
  }

  set_descDamage(a1: oSDamageDescriptor): null {
    SendCommand({
      id: 'SET_oCMsgDamage-descDamage-oSDamageDescriptor-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCMsgDamage_OnInit(): oCMsgDamage | null {
    const result: string | null = SendCommand({
      id: 'oCMsgDamage-oCMsgDamage_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCMsgDamage(result) : null
  }

  static oCMsgDamage_OnInit2(a1: TDamageSubType): oCMsgDamage | null {
    const result: string | null = SendCommand({
      id: 'oCMsgDamage-oCMsgDamage_OnInit-void-false-TDamageSubType',
      parentId: 'NULL',
      a1: a1.toString(),
    })

    return result ? new oCMsgDamage(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCMsgDamage-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override IsOverlay(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgDamage-IsOverlay-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override IsNetRelevant(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgDamage-IsNetRelevant-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override IsHighPriority(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgDamage-IsHighPriority-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override IsDeleteable(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgDamage-IsDeleteable-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override MD_GetNumOfSubTypes(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgDamage-MD_GetNumOfSubTypes-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override MD_GetSubTypeString(a1: number): string | null {
    const result: string | null = SendCommand({
      id: 'oCMsgDamage-MD_GetSubTypeString-zSTRING-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? String(result) : null
  }
}
