import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCVobLightData } from './index.js'
import { zCClassDef } from './index.js'
import { zCVob } from './index.js'
import { zCWorld } from './index.js'

export class zCVobLight extends zCVob {
  lightData(): zCVobLightData | null {
    const result: string | null = SendCommand({
      id: 'zCVobLight-lightData-zCVobLightData-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVobLightData(result) : null
  }

  set_lightData(a1: zCVobLightData): null {
    SendCommand({
      id: 'SET_zCVobLight-lightData-zCVobLightData-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lightPresetInUse(): string | null {
    const result: string | null = SendCommand({
      id: 'zCVobLight-lightPresetInUse-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_lightPresetInUse(a1: string): null {
    SendCommand({
      id: 'SET_zCVobLight-lightPresetInUse-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCVobLight_OnInit(): zCVobLight | null {
    const result: string | null = SendCommand({
      id: 'zCVobLight-zCVobLight_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCVobLight(result) : null
  }

  DoAnimation(): void | null {
    const result: string | null = SendCommand({
      id: 'zCVobLight-DoAnimation-void-false-',
      parentId: this.Uuid,
    })
  }

  SetRange(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCVobLight-SetRange-void-false-float,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  AddThisToPresetList(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'zCVobLight-AddThisToPresetList-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetByPreset(a1: string): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobLight-SetByPreset-int-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  SetByPresetInUse(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobLight-SetByPresetInUse-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCVobLight-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override OnTrigger(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCVobLight-OnTrigger-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override OnUntrigger(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCVobLight-OnUntrigger-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override EndMovement(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCVobLight-EndMovement-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override ThisVobAddedToWorld(a1: zCWorld): void | null {
    const result: string | null = SendCommand({
      id: 'zCVobLight-ThisVobAddedToWorld-void-false-zCWorld*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }
}
