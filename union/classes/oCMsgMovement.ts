import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCRoute } from './index.js'
import { zCVob } from './index.js'
import { zVEC3 } from './index.js'
import { TMovementSubType } from './index.js'
import { zCClassDef } from './index.js'
import { oCNpcMessage } from './index.js'

export class oCMsgMovement extends oCNpcMessage {
  targetName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMovement-targetName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_targetName(a1: string): null {
    SendCommand({
      id: 'SET_oCMsgMovement-targetName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  route(): zCRoute | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMovement-route-zCRoute*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCRoute(result) : null
  }

  set_route(a1: zCRoute): null {
    SendCommand({
      id: 'SET_oCMsgMovement-route-zCRoute*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  targetVob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMovement-targetVob-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_targetVob(a1: zCVob): null {
    SendCommand({
      id: 'SET_oCMsgMovement-targetVob-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  targetPos(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMovement-targetPos-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_targetPos(a1: zVEC3): null {
    SendCommand({
      id: 'SET_oCMsgMovement-targetPos-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  angle(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMovement-angle-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_angle(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgMovement-angle-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  timer(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMovement-timer-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_timer(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgMovement-timer-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  targetMode(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMovement-targetMode-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_targetMode(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgMovement-targetMode-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  ani(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMovement-ani-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_ani(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgMovement-ani-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCMsgMovement_OnInit(): oCMsgMovement | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMovement-oCMsgMovement_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCMsgMovement(result) : null
  }

  static oCMsgMovement_OnInit2(a1: TMovementSubType, a2: string): oCMsgMovement | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMovement-oCMsgMovement_OnInit-void-false-TMovementSubType,zSTRING const&',
      parentId: 'NULL',
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCMsgMovement(result) : null
  }

  static oCMsgMovement_OnInit3(a1: TMovementSubType, a2: zCVob): oCMsgMovement | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMovement-oCMsgMovement_OnInit-void-false-TMovementSubType,zCVob*',
      parentId: 'NULL',
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCMsgMovement(result) : null
  }

  static oCMsgMovement_OnInit4(a1: TMovementSubType, a2: zVEC3): oCMsgMovement | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMovement-oCMsgMovement_OnInit-void-false-TMovementSubType,zVEC3 const&',
      parentId: 'NULL',
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCMsgMovement(result) : null
  }

  static oCMsgMovement_OnInit5(a1: TMovementSubType, a2: number): oCMsgMovement | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMovement-oCMsgMovement_OnInit-void-false-TMovementSubType,float',
      parentId: 'NULL',
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCMsgMovement(result) : null
  }

  static oCMsgMovement_OnInit6(a1: TMovementSubType, a2: number): oCMsgMovement | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMovement-oCMsgMovement_OnInit-void-false-TMovementSubType,int',
      parentId: 'NULL',
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCMsgMovement(result) : null
  }

  Init(): void | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMovement-Init-void-false-',
      parentId: this.Uuid,
    })
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMovement-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override Delete(): void | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMovement-Delete-void-false-',
      parentId: this.Uuid,
    })
  }

  override MD_GetNumOfSubTypes(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMovement-MD_GetNumOfSubTypes-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override MD_GetSubTypeString(a1: number): string | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMovement-MD_GetSubTypeString-zSTRING-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? String(result) : null
  }

  override MD_GetVobRefName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMovement-MD_GetVobRefName-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  override MD_SetVobRefName(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMovement-MD_SetVobRefName-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override MD_SetVobParam(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMovement-MD_SetVobParam-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override MD_GetMinTime(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMovement-MD_GetMinTime-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }
}
