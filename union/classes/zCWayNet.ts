import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCWorld } from './index.js'
import { zCWaypoint } from './index.js'
import { zVEC3 } from './index.js'
import { zCCamera } from './index.js'
import { zCVobWaypoint } from './index.js'
import { zCRoute } from './index.js'
import { zCVob } from './index.js'
import { zCClassDef } from './index.js'
import { zCObject } from './index.js'

export class zCWayNet extends zCObject {
  world(): zCWorld | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-world-zCWorld*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCWorld(result) : null
  }

  set_world(a1: zCWorld): null {
    SendCommand({
      id: 'SET_zCWayNet-world-zCWorld*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  routeCtr(): number | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-routeCtr-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_routeCtr(a1: number): null {
    SendCommand({
      id: 'SET_zCWayNet-routeCtr-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCWayNet_OnInit(): zCWayNet | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-zCWayNet_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCWayNet(result) : null
  }

  static zCWayNet_OnInit2(a1: zCWorld): zCWayNet | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-zCWayNet_OnInit-void-false-zCWorld*',
      parentId: 'NULL',
      a1: a1.toString(),
    })

    return result ? new zCWayNet(result) : null
  }

  HasWaypoint(a1: number, a2: number, a3: number): zCWaypoint | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-HasWaypoint-zCWaypoint*-false-float,float,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? new zCWaypoint(result) : null
  }

  HasWaypoint2(a1: zCWaypoint): number | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-HasWaypoint-int-false-zCWaypoint*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  HasWaypoint3(a1: zVEC3): zCWaypoint | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-HasWaypoint-zCWaypoint*-false-zVEC3&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new zCWaypoint(result) : null
  }

  InsertWaypoint(a1: number, a2: number, a3: number): zCWaypoint | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-InsertWaypoint-zCWaypoint*-false-float,float,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? new zCWaypoint(result) : null
  }

  InsertWaypoint2(a1: zCWaypoint): void | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-InsertWaypoint-void-false-zCWaypoint*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  InsertWaypoint3(a1: zCWaypoint, a2: zCWaypoint, a3: zCWaypoint): void | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-InsertWaypoint-void-false-zCWaypoint*,zCWaypoint*,zCWaypoint*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  DeleteWaypoint(a1: zCWaypoint): void | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-DeleteWaypoint-void-false-zCWaypoint*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CreateWay(a1: zCWaypoint, a2: zCWaypoint): void | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-CreateWay-void-false-zCWaypoint*,zCWaypoint*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  DeleteWay(a1: zCWaypoint, a2: zCWaypoint): void | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-DeleteWay-void-false-zCWaypoint*,zCWaypoint*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  GetNearestWaypoint(a1: zVEC3): zCWaypoint | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-GetNearestWaypoint-zCWaypoint*-false-zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new zCWaypoint(result) : null
  }

  GetSecNearestWaypoint(a1: zVEC3): zCWaypoint | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-GetSecNearestWaypoint-zCWaypoint*-false-zVEC3&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new zCWaypoint(result) : null
  }

  CorrectHeight(): void | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-CorrectHeight-void-false-',
      parentId: this.Uuid,
    })
  }

  Draw(a1: zCCamera): void | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-Draw-void-false-zCCamera*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  UpdateVobDependencies(): void | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-UpdateVobDependencies-void-false-',
      parentId: this.Uuid,
    })
  }

  ClearVobDependencies(): void | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-ClearVobDependencies-void-false-',
      parentId: this.Uuid,
    })
  }

  CreateVobDependencies(a1: zCWorld): void | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-CreateVobDependencies-void-false-zCWorld*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  MergeWaypoints(): string | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-MergeWaypoints-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  CheckConsistency(a1: number): string | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-CheckConsistency-zSTRING-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? String(result) : null
  }

  AddWays(a1: zCWaypoint, a2: zCWaypoint): void | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-AddWays-void-false-zCWaypoint*,zCWaypoint*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  GetWaypoint(a1: string): zCWaypoint | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-GetWaypoint-zCWaypoint*-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new zCWaypoint(result) : null
  }

  SearchWaypoint(a1: zCVobWaypoint): zCWaypoint | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-SearchWaypoint-zCWaypoint*-false-zCVobWaypoint*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new zCWaypoint(result) : null
  }

  FindRoute(a1: zVEC3, a2: zVEC3, a3: zCVob): zCRoute | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-FindRoute-zCRoute*-false-zVEC3 const&,zVEC3 const&,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? new zCRoute(result) : null
  }

  FindRoute2(a1: zVEC3, a2: zCWaypoint, a3: zCVob): zCRoute | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-FindRoute-zCRoute*-false-zVEC3 const&,zCWaypoint*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? new zCRoute(result) : null
  }

  FindRoute3(a1: zVEC3, a2: string, a3: zCVob): zCRoute | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-FindRoute-zCRoute*-false-zVEC3 const&,zSTRING const&,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? new zCRoute(result) : null
  }

  FindRoute4(a1: zCWaypoint, a2: zCWaypoint, a3: zCVob): zCRoute | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-FindRoute-zCRoute*-false-zCWaypoint*,zCWaypoint*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? new zCRoute(result) : null
  }

  FindRoute5(a1: string, a2: string, a3: zCVob): zCRoute | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-FindRoute-zCRoute*-false-zSTRING const&,zSTRING const&,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? new zCRoute(result) : null
  }

  InsertInOpen(a1: zCWaypoint): void | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-InsertInOpen-void-false-zCWaypoint*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  IsInOpen(a1: zCWaypoint): number | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-IsInOpen-int-false-zCWaypoint*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  InsertInClosed(a1: zCWaypoint): void | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-InsertInClosed-void-false-zCWaypoint*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RemoveFromClosed(a1: zCWaypoint): void | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-RemoveFromClosed-void-false-zCWaypoint*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  IsInClosed(a1: zCWaypoint): number | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-IsInClosed-int-false-zCWaypoint*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  IsInAnyList(a1: zCWaypoint): number | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-IsInAnyList-int-false-zCWaypoint*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  EstimateCost(a1: zCWaypoint, a2: zCWaypoint): number | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-EstimateCost-int-false-zCWaypoint*,zCWaypoint*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  CreateRoute(a1: zCWaypoint): zCRoute | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-CreateRoute-zCRoute*-false-zCWaypoint*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new zCRoute(result) : null
  }

  AStar(a1: zCWaypoint, a2: zCWaypoint, a3: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-AStar-int-false-zCWaypoint*,zCWaypoint*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? Number(result) : null
  }

  RemoveUnusedWPVobs(): void | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-RemoveUnusedWPVobs-void-false-',
      parentId: this.Uuid,
    })
  }

  CalcProperties(a1: zCWorld): void | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-CalcProperties-void-false-zCWorld*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCWayNet-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }
}
