import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { oCNpc } from './index.js'

export class TMobOptPos extends BaseUnionObject {
  distance(): number | null {
    const result: string | null = SendCommand({
      id: 'TMobOptPos-distance-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_distance(a1: number): null {
    SendCommand({
      id: 'SET_TMobOptPos-distance-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  npc(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'TMobOptPos-npc-oCNpc*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  set_npc(a1: oCNpc): null {
    SendCommand({
      id: 'SET_TMobOptPos-npc-oCNpc*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  nodeName(): string | null {
    const result: string | null = SendCommand({
      id: 'TMobOptPos-nodeName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_nodeName(a1: string): null {
    SendCommand({
      id: 'SET_TMobOptPos-nodeName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
