import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { oCItemReactModuleDummy0 } from './index.js'

export class oCItemReactModule extends BaseUnionObject {
  next(): oCItemReactModule | null {
    const result: string | null = SendCommand({
      id: 'oCItemReactModule-next-oCItemReactModule*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCItemReactModule(result) : null
  }

  set_next(a1: oCItemReactModule): null {
    SendCommand({
      id: 'SET_oCItemReactModule-next-oCItemReactModule*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pd(): oCItemReactModuleDummy0 | null {
    const result: string | null = SendCommand({
      id: 'oCItemReactModule-pd-oCItemReactModuleDummy0-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCItemReactModuleDummy0(result) : null
  }

  set_pd(a1: oCItemReactModuleDummy0): null {
    SendCommand({
      id: 'SET_oCItemReactModule-pd-oCItemReactModuleDummy0-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCItemReactModule_OnInit(a1: number): oCItemReactModule | null {
    const result: string | null = SendCommand({
      id: 'oCItemReactModule-oCItemReactModule_OnInit-void-false-int',
      parentId: 'NULL',
      a1: a1.toString(),
    })

    return result ? new oCItemReactModule(result) : null
  }

  Validate(): void | null {
    const result: string | null = SendCommand({
      id: 'oCItemReactModule-Validate-void-false-',
      parentId: this.Uuid,
    })
  }

  Conditions(a1: number, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemReactModule-Conditions-int-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  GetOfferedInstance(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCItemReactModule-GetOfferedInstance-void-false-int&,int&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  StartReaction(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemReactModule-StartReaction-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetDataSize(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemReactModule-GetDataSize-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetNpcInstance(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemReactModule-GetNpcInstance-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }
}
