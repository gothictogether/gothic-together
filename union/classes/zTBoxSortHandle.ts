import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCBBox3DSorterBase } from './index.js'
import { zTBBox3D } from './index.js'

export class zTBoxSortHandle extends BaseUnionObject {
  mySorter(): zCBBox3DSorterBase | null {
    const result: string | null = SendCommand({
      id: 'zTBoxSortHandle-mySorter-zCBBox3DSorterBase*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCBBox3DSorterBase(result) : null
  }

  set_mySorter(a1: zCBBox3DSorterBase): null {
    SendCommand({
      id: 'SET_zTBoxSortHandle-mySorter-zCBBox3DSorterBase*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  bbox3D(): zTBBox3D | null {
    const result: string | null = SendCommand({
      id: 'zTBoxSortHandle-bbox3D-zTBBox3D-false-false',
      parentId: this.Uuid,
    })

    return result ? new zTBBox3D(result) : null
  }

  set_bbox3D(a1: zTBBox3D): null {
    SendCommand({
      id: 'SET_zTBoxSortHandle-bbox3D-zTBBox3D-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  ClearActive(): void | null {
    const result: string | null = SendCommand({
      id: 'zTBoxSortHandle-ClearActive-void-false-',
      parentId: this.Uuid,
    })
  }
}
