import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zVEC3 } from './index.js'
import { zCVob } from './index.js'

export class oCTrajectory extends BaseUnionObject {
  mode(): number | null {
    const result: string | null = SendCommand({
      id: 'oCTrajectory-mode-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_mode(a1: number): null {
    SendCommand({
      id: 'SET_oCTrajectory-mode-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  length(): number | null {
    const result: string | null = SendCommand({
      id: 'oCTrajectory-length-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_length(a1: number): null {
    SendCommand({
      id: 'SET_oCTrajectory-length-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lastKey(): number | null {
    const result: string | null = SendCommand({
      id: 'oCTrajectory-lastKey-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_lastKey(a1: number): null {
    SendCommand({
      id: 'SET_oCTrajectory-lastKey-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  Calc(): void | null {
    const result: string | null = SendCommand({
      id: 'oCTrajectory-Calc-void-false-',
      parentId: this.Uuid,
    })
  }

  Changed(): void | null {
    const result: string | null = SendCommand({
      id: 'oCTrajectory-Changed-void-false-',
      parentId: this.Uuid,
    })
  }

  Draw(): void | null {
    const result: string | null = SendCommand({
      id: 'oCTrajectory-Draw-void-false-',
      parentId: this.Uuid,
    })
  }

  Clear(): void | null {
    const result: string | null = SendCommand({
      id: 'oCTrajectory-Clear-void-false-',
      parentId: this.Uuid,
    })
  }

  RecalcByColl(a1: zVEC3, a2: zVEC3, a3: zVEC3, a4: number): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oCTrajectory-RecalcByColl-zVEC3-false-zVEC3 const&,zVEC3 const&,zVEC3 const&,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
    })

    return result ? new zVEC3(result) : null
  }

  ApplyGrav(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCTrajectory-ApplyGrav-void-false-float const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  VobCross(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'oCTrajectory-VobCross-int-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetNumKeys(): number | null {
    const result: string | null = SendCommand({
      id: 'oCTrajectory-GetNumKeys-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }
}
