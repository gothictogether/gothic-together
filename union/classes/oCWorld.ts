import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { oCVob } from './index.js'
import { zTVobType } from './index.js'
import { zCVob } from './index.js'
import { zCClassDef } from './index.js'
import { zTWorldLoadMode } from './index.js'
import { zTWorldSaveMode } from './index.js'
import { zCWorld } from './index.js'

export class oCWorld extends zCWorld {
  worldFilename(): string | null {
    const result: string | null = SendCommand({
      id: 'oCWorld-worldFilename-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_worldFilename(a1: string): null {
    SendCommand({
      id: 'SET_oCWorld-worldFilename-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  worldName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCWorld-worldName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_worldName(a1: string): null {
    SendCommand({
      id: 'SET_oCWorld-worldName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCWorld_OnInit(): oCWorld | null {
    const result: string | null = SendCommand({
      id: 'oCWorld-oCWorld_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCWorld(result) : null
  }

  GetWorldName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCWorld-GetWorldName-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  GetWorldFilename(): string | null {
    const result: string | null = SendCommand({
      id: 'oCWorld-GetWorldFilename-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  CreateVob_novt(a1: zTVobType, a2: string): oCVob | null {
    const result: string | null = SendCommand({
      id: 'oCWorld-CreateVob_novt-oCVob*-false-zTVobType,zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCVob(result) : null
  }

  ClearNpcPerceptionVobLists(): void | null {
    const result: string | null = SendCommand({
      id: 'oCWorld-ClearNpcPerceptionVobLists-void-false-',
      parentId: this.Uuid,
    })
  }

  InsertInLists(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCWorld-InsertInLists-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RemoveFromLists(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCWorld-RemoveFromLists-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCWorld-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override LoadWorld(a1: string, a2: zTWorldLoadMode): number | null {
    const result: string | null = SendCommand({
      id: 'oCWorld-LoadWorld-int-false-zSTRING const&,zTWorldLoadMode',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  override SaveWorld(a1: string, a2: zTWorldSaveMode, a3: number, a4: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCWorld-SaveWorld-int-false-zSTRING const&,zTWorldSaveMode,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
    })

    return result ? Number(result) : null
  }

  override DisposeWorld(): void | null {
    const result: string | null = SendCommand({
      id: 'oCWorld-DisposeWorld-void-false-',
      parentId: this.Uuid,
    })
  }

  override RemoveVob(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCWorld-RemoveVob-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override SearchVobByName(a1: string): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCWorld-SearchVobByName-zCVob*-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new zCVob(result) : null
  }

  CreateVob(a1: zTVobType, a2: number): oCVob | null {
    const result: string | null = SendCommand({
      id: 'oCWorld-CreateVob-oCVob*-false-zTVobType,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCVob(result) : null
  }

  InsertVobInWorld(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCWorld-InsertVobInWorld-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  EnableVob(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCWorld-EnableVob-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  DisableVob(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCWorld-DisableVob-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  DisposeVobs(): void | null {
    const result: string | null = SendCommand({
      id: 'oCWorld-DisposeVobs-void-false-',
      parentId: this.Uuid,
    })
  }
}
