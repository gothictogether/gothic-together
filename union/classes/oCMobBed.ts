import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { oCNpc } from './index.js'
import { TMobOptPos } from './index.js'
import { oCMobInter } from './index.js'

export class oCMobBed extends oCMobInter {
  addName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCMobBed-addName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_addName(a1: string): null {
    SendCommand({
      id: 'SET_oCMobBed-addName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCMobBed_OnInit(): oCMobBed | null {
    const result: string | null = SendCommand({
      id: 'oCMobBed-oCMobBed_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCMobBed(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCMobBed-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override GetScemeName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCMobBed-GetScemeName-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  override StartInteraction(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobBed-StartInteraction-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override OnBeginStateChange(a1: oCNpc, a2: number, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobBed-OnBeginStateChange-void-false-oCNpc*,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  override OnEndStateChange(a1: oCNpc, a2: number, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobBed-OnEndStateChange-void-false-oCNpc*,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  SearchFreePositionBed(a1: oCNpc, a2: number): TMobOptPos | null {
    const result: string | null = SendCommand({
      id: 'oCMobBed-SearchFreePosition-TMobOptPos*-false-oCNpc*,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new TMobOptPos(result) : null
  }
}
