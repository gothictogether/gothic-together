import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'

export class zTCacheEntry extends BaseUnionObject {
  x1(): number | null {
    const result: string | null = SendCommand({
      id: 'zTCacheEntry-x1-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_x1(a1: number): null {
    SendCommand({
      id: 'SET_zTCacheEntry-x1-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  y1(): number | null {
    const result: string | null = SendCommand({
      id: 'zTCacheEntry-y1-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_y1(a1: number): null {
    SendCommand({
      id: 'SET_zTCacheEntry-y1-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  x2(): number | null {
    const result: string | null = SendCommand({
      id: 'zTCacheEntry-x2-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_x2(a1: number): null {
    SendCommand({
      id: 'SET_zTCacheEntry-x2-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  y2(): number | null {
    const result: string | null = SendCommand({
      id: 'zTCacheEntry-y2-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_y2(a1: number): null {
    SendCommand({
      id: 'SET_zTCacheEntry-y2-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  color(): number | null {
    const result: string | null = SendCommand({
      id: 'zTCacheEntry-color-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_color(a1: number): null {
    SendCommand({
      id: 'SET_zTCacheEntry-color-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
