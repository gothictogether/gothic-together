import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'

export class zCTriggerDummy0 extends BaseUnionObject {
  reactToOnTrigger(): number | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerDummy0-reactToOnTrigger-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_reactToOnTrigger(a1: number): null {
    SendCommand({
      id: 'SET_zCTriggerDummy0-reactToOnTrigger-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  reactToOnTouch(): number | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerDummy0-reactToOnTouch-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_reactToOnTouch(a1: number): null {
    SendCommand({
      id: 'SET_zCTriggerDummy0-reactToOnTouch-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  reactToOnDamage(): number | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerDummy0-reactToOnDamage-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_reactToOnDamage(a1: number): null {
    SendCommand({
      id: 'SET_zCTriggerDummy0-reactToOnDamage-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  respondToObject(): number | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerDummy0-respondToObject-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_respondToObject(a1: number): null {
    SendCommand({
      id: 'SET_zCTriggerDummy0-respondToObject-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  respondToPC(): number | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerDummy0-respondToPC-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_respondToPC(a1: number): null {
    SendCommand({
      id: 'SET_zCTriggerDummy0-respondToPC-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  respondToNPC(): number | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerDummy0-respondToNPC-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_respondToNPC(a1: number): null {
    SendCommand({
      id: 'SET_zCTriggerDummy0-respondToNPC-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
