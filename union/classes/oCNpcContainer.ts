import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { oCItem } from './index.js'
import { oCStealContainer } from './index.js'

export class oCNpcContainer extends oCStealContainer {
  static oCNpcContainer_OnInit(): oCNpcContainer | null {
    const result: string | null = SendCommand({
      id: 'oCNpcContainer-oCNpcContainer_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCNpcContainer(result) : null
  }

  override HandleEvent(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcContainer-HandleEvent-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  override Insert(a1: oCItem): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCNpcContainer-Insert-oCItem*-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new oCItem(result) : null
  }

  override Remove(a1: oCItem): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpcContainer-Remove-void-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override CreateList(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpcContainer-CreateList-void-false-',
      parentId: this.Uuid,
    })
  }
}
