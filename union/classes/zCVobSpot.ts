import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCVob } from './index.js'
import { zCClassDef } from './index.js'

export class zCVobSpot extends zCVob {
  timerEnd(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobSpot-timerEnd-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_timerEnd(a1: number): null {
    SendCommand({
      id: 'SET_zCVobSpot-timerEnd-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  inUseVob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'zCVobSpot-inUseVob-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_inUseVob(a1: zCVob): null {
    SendCommand({
      id: 'SET_zCVobSpot-inUseVob-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCVobSpot_OnInit(): zCVobSpot | null {
    const result: string | null = SendCommand({
      id: 'zCVobSpot-zCVobSpot_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCVobSpot(result) : null
  }

  IsAvailable(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobSpot-IsAvailable-int-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  MarkAsUsed(a1: number, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCVobSpot-MarkAsUsed-void-false-float,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCVobSpot-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }
}
