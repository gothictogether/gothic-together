import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCVob } from './index.js'
import { zVEC3 } from './index.js'
import { TConversationSubTypeMagic } from './index.js'
import { zCClassDef } from './index.js'
import { oCNpcMessage } from './index.js'

export class oCMsgMagic extends oCNpcMessage {
  what(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMagic-what-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_what(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgMagic-what-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  level(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMagic-level-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_level(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgMagic-level-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  removeSymbol(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMagic-removeSymbol-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_removeSymbol(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgMagic-removeSymbol-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  manaInvested(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMagic-manaInvested-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_manaInvested(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgMagic-manaInvested-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  energyLeft(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMagic-energyLeft-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_energyLeft(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgMagic-energyLeft-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  target(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMagic-target-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_target(a1: zCVob): null {
    SendCommand({
      id: 'SET_oCMsgMagic-target-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  targetPos(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMagic-targetPos-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_targetPos(a1: zVEC3): null {
    SendCommand({
      id: 'SET_oCMsgMagic-targetPos-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  activeSpell(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMagic-activeSpell-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_activeSpell(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgMagic-activeSpell-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCMsgMagic_OnInit(): oCMsgMagic | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMagic-oCMsgMagic_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCMsgMagic(result) : null
  }

  static oCMsgMagic_OnInit2(a1: TConversationSubTypeMagic): oCMsgMagic | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMagic-oCMsgMagic_OnInit-void-false-TConversationSubTypeMagic',
      parentId: 'NULL',
      a1: a1.toString(),
    })

    return result ? new oCMsgMagic(result) : null
  }

  static oCMsgMagic_OnInit3(
    a1: TConversationSubTypeMagic,
    a2: number,
    a3: number,
  ): oCMsgMagic | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMagic-oCMsgMagic_OnInit-void-false-TConversationSubTypeMagic,int,int',
      parentId: 'NULL',
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? new oCMsgMagic(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMagic-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override IsNetRelevant(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMagic-IsNetRelevant-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override IsHighPriority(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMagic-IsHighPriority-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override IsJob(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMagic-IsJob-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override MD_GetNumOfSubTypes(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMagic-MD_GetNumOfSubTypes-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override MD_GetSubTypeString(a1: number): string | null {
    const result: string | null = SendCommand({
      id: 'oCMsgMagic-MD_GetSubTypeString-zSTRING-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? String(result) : null
  }
}
