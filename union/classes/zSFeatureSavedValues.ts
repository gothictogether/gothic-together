import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCVertFeature } from './index.js'
import { zVEC3 } from './index.js'

export class zSFeatureSavedValues extends BaseUnionObject {
  feat(): zCVertFeature | null {
    const result: string | null = SendCommand({
      id: 'zSFeatureSavedValues-feat-zCVertFeature*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVertFeature(result) : null
  }

  set_feat(a1: zCVertFeature): null {
    SendCommand({
      id: 'SET_zSFeatureSavedValues-feat-zCVertFeature*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  mapping(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zSFeatureSavedValues-mapping-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_mapping(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zSFeatureSavedValues-mapping-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  vertNormal(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zSFeatureSavedValues-vertNormal-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_vertNormal(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zSFeatureSavedValues-vertNormal-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
