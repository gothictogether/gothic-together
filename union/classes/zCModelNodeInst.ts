import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCModelNode } from './index.js'
import { zTBBox3D } from './index.js'
import { zCModelTexAniState } from './index.js'
import { zCModelAniActive } from './index.js'
import { zCModel } from './index.js'

export class zCModelNodeInst extends BaseUnionObject {
  parentNode(): zCModelNodeInst | null {
    const result: string | null = SendCommand({
      id: 'zCModelNodeInst-parentNode-zCModelNodeInst*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCModelNodeInst(result) : null
  }

  set_parentNode(a1: zCModelNodeInst): null {
    SendCommand({
      id: 'SET_zCModelNodeInst-parentNode-zCModelNodeInst*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  protoNode(): zCModelNode | null {
    const result: string | null = SendCommand({
      id: 'zCModelNodeInst-protoNode-zCModelNode*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCModelNode(result) : null
  }

  set_protoNode(a1: zCModelNode): null {
    SendCommand({
      id: 'SET_zCModelNodeInst-protoNode-zCModelNode*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  bbox3D(): zTBBox3D | null {
    const result: string | null = SendCommand({
      id: 'zCModelNodeInst-bbox3D-zTBBox3D-false-false',
      parentId: this.Uuid,
    })

    return result ? new zTBBox3D(result) : null
  }

  set_bbox3D(a1: zTBBox3D): null {
    SendCommand({
      id: 'SET_zCModelNodeInst-bbox3D-zTBBox3D-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  texAniState(): zCModelTexAniState | null {
    const result: string | null = SendCommand({
      id: 'zCModelNodeInst-texAniState-zCModelTexAniState-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCModelTexAniState(result) : null
  }

  set_texAniState(a1: zCModelTexAniState): null {
    SendCommand({
      id: 'SET_zCModelNodeInst-texAniState-zCModelTexAniState-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  numNodeAnis(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelNodeInst-numNodeAnis-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_numNodeAnis(a1: number): null {
    SendCommand({
      id: 'SET_zCModelNodeInst-numNodeAnis-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  masterAni(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelNodeInst-masterAni-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_masterAni(a1: number): null {
    SendCommand({
      id: 'SET_zCModelNodeInst-masterAni-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  masterAniSpeed(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelNodeInst-masterAniSpeed-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_masterAniSpeed(a1: number): null {
    SendCommand({
      id: 'SET_zCModelNodeInst-masterAniSpeed-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCModelNodeInst_OnInit(): zCModelNodeInst | null {
    const result: string | null = SendCommand({
      id: 'zCModelNodeInst-zCModelNodeInst_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCModelNodeInst(result) : null
  }

  static zCModelNodeInst_OnInit2(a1: zCModelNode): zCModelNodeInst | null {
    const result: string | null = SendCommand({
      id: 'zCModelNodeInst-zCModelNodeInst_OnInit-void-false-zCModelNode*',
      parentId: 'NULL',
      a1: a1.toString(),
    })

    return result ? new zCModelNodeInst(result) : null
  }

  Init(): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelNodeInst-Init-void-false-',
      parentId: this.Uuid,
    })
  }

  InitByModelProtoNode(a1: zCModelNode): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelNodeInst-InitByModelProtoNode-void-false-zCModelNode*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  AddNodeAni(a1: zCModelAniActive): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelNodeInst-AddNodeAni-void-false-zCModelAniActive*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RemoveAllNodeAnis(): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelNodeInst-RemoveAllNodeAnis-void-false-',
      parentId: this.Uuid,
    })
  }

  RemoveNodeAni(a1: zCModelAniActive): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelNodeInst-RemoveNodeAni-void-false-zCModelAniActive*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  FindMasterAni(): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelNodeInst-FindMasterAni-void-false-',
      parentId: this.Uuid,
    })
  }

  FadeOutNodeAni(a1: zCModelAniActive): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelNodeInst-FadeOutNodeAni-void-false-zCModelAniActive*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CalcWeights(a1: zCModel): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelNodeInst-CalcWeights-void-false-zCModel*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  AddToNodeAniWeight(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelNodeInst-AddToNodeAniWeight-void-false-int,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  CalcBlending(a1: zCModel): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelNodeInst-CalcBlending-void-false-zCModel*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetNodeAniListIndex(a1: zCModelAniActive): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelNodeInst-GetNodeAniListIndex-int-false-zCModelAniActive const*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }
}
