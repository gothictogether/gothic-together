import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { zCWorld } from './index.js'
import { zCZoneZFog } from './index.js'

export class zCZoneZFogDefault extends zCZoneZFog {
  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCZoneZFogDefault-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override ThisVobAddedToWorld(a1: zCWorld): void | null {
    const result: string | null = SendCommand({
      id: 'zCZoneZFogDefault-ThisVobAddedToWorld-void-false-zCWorld*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }
}
