import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zVEC3 } from './index.js'

export class zTMov_Keyframe extends BaseUnionObject {
  pos(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zTMov_Keyframe-pos-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_pos(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zTMov_Keyframe-pos-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
