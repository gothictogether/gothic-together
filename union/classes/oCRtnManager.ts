import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCWorld } from './index.js'
import { oCNpc } from './index.js'
import { oCRtnEntry } from './index.js'
import { zVEC3 } from './index.js'
import { zTBBox3D } from './index.js'

export class oCRtnManager extends BaseUnionObject {
  world(): zCWorld | null {
    const result: string | null = SendCommand({
      id: 'oCRtnManager-world-zCWorld*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCWorld(result) : null
  }

  set_world(a1: zCWorld): null {
    SendCommand({
      id: 'SET_oCRtnManager-world-zCWorld*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCRtnManager_OnInit(): oCRtnManager | null {
    const result: string | null = SendCommand({
      id: 'oCRtnManager-oCRtnManager_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCRtnManager(result) : null
  }

  SetWorld(a1: zCWorld): void | null {
    const result: string | null = SendCommand({
      id: 'oCRtnManager-SetWorld-void-false-zCWorld*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  ShowRoutine(a1: number, a2: number, a3: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCRtnManager-ShowRoutine-void-false-int,int,oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  GetRoutineString(a1: oCRtnEntry): string | null {
    const result: string | null = SendCommand({
      id: 'oCRtnManager-GetRoutineString-zSTRING-false-oCRtnEntry*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? String(result) : null
  }

  Insert(a1: oCNpc, a2: oCRtnEntry): void | null {
    const result: string | null = SendCommand({
      id: 'oCRtnManager-Insert-void-false-oCNpc*,oCRtnEntry*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  UpdateGlobalRoutineEntry(): void | null {
    const result: string | null = SendCommand({
      id: 'oCRtnManager-UpdateGlobalRoutineEntry-void-false-',
      parentId: this.Uuid,
    })
  }

  UpdateSingleRoutine(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCRtnManager-UpdateSingleRoutine-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RestartRoutines(): void | null {
    const result: string | null = SendCommand({
      id: 'oCRtnManager-RestartRoutines-void-false-',
      parentId: this.Uuid,
    })
  }

  CheckRoutines(): void | null {
    const result: string | null = SendCommand({
      id: 'oCRtnManager-CheckRoutines-void-false-',
      parentId: this.Uuid,
    })
  }

  GetRoutinePos(a1: oCNpc): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oCRtnManager-GetRoutinePos-zVEC3-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new zVEC3(result) : null
  }

  RemoveEntry(a1: oCRtnEntry): void | null {
    const result: string | null = SendCommand({
      id: 'oCRtnManager-RemoveEntry-void-false-oCRtnEntry*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RemoveRoutine(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCRtnManager-RemoveRoutine-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CheckConsistency(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCRtnManager-CheckConsistency-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RemoveOverlay(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCRtnManager-RemoveOverlay-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetDailyRoutinePos(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCRtnManager-SetDailyRoutinePos-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  InitWayBoxes(): void | null {
    const result: string | null = SendCommand({
      id: 'oCRtnManager-InitWayBoxes-void-false-',
      parentId: this.Uuid,
    })
  }

  InsertWaybox(a1: oCNpc, a2: zVEC3, a3: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'oCRtnManager-InsertWaybox-void-false-oCNpc*,zVEC3&,zVEC3&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  RemoveWayBoxes(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCRtnManager-RemoveWayBoxes-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CreateWayBoxes(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCRtnManager-CreateWayBoxes-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  DrawWayBoxes(): void | null {
    const result: string | null = SendCommand({
      id: 'oCRtnManager-DrawWayBoxes-void-false-',
      parentId: this.Uuid,
    })
  }

  CreateActiveList(a1: oCNpc, a2: zTBBox3D): void | null {
    const result: string | null = SendCommand({
      id: 'oCRtnManager-CreateActiveList-void-false-oCNpc*,zTBBox3D&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  GetNumOfWayBoxes(): number | null {
    const result: string | null = SendCommand({
      id: 'oCRtnManager-GetNumOfWayBoxes-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }
}
