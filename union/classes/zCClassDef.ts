import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCObject } from './index.js'

export class zCClassDef extends BaseUnionObject {
  className(): string | null {
    const result: string | null = SendCommand({
      id: 'zCClassDef-className-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_className(a1: string): null {
    SendCommand({
      id: 'SET_zCClassDef-className-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  baseClassName(): string | null {
    const result: string | null = SendCommand({
      id: 'zCClassDef-baseClassName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_baseClassName(a1: string): null {
    SendCommand({
      id: 'SET_zCClassDef-baseClassName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  scriptClassName(): string | null {
    const result: string | null = SendCommand({
      id: 'zCClassDef-scriptClassName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_scriptClassName(a1: string): null {
    SendCommand({
      id: 'SET_zCClassDef-scriptClassName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  classFlags(): number | null {
    const result: string | null = SendCommand({
      id: 'zCClassDef-classFlags-unsigned long-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_classFlags(a1: number): null {
    SendCommand({
      id: 'SET_zCClassDef-classFlags-unsigned long-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  classSize(): number | null {
    const result: string | null = SendCommand({
      id: 'zCClassDef-classSize-unsigned long-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_classSize(a1: number): null {
    SendCommand({
      id: 'SET_zCClassDef-classSize-unsigned long-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  numLivingObjects(): number | null {
    const result: string | null = SendCommand({
      id: 'zCClassDef-numLivingObjects-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_numLivingObjects(a1: number): null {
    SendCommand({
      id: 'SET_zCClassDef-numLivingObjects-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  numCtorCalled(): number | null {
    const result: string | null = SendCommand({
      id: 'zCClassDef-numCtorCalled-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_numCtorCalled(a1: number): null {
    SendCommand({
      id: 'SET_zCClassDef-numCtorCalled-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  archiveVersion(): number | null {
    const result: string | null = SendCommand({
      id: 'zCClassDef-archiveVersion-unsigned short-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_archiveVersion(a1: number): null {
    SendCommand({
      id: 'SET_zCClassDef-archiveVersion-unsigned short-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  archiveVersionSum(): number | null {
    const result: string | null = SendCommand({
      id: 'zCClassDef-archiveVersionSum-unsigned short-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_archiveVersionSum(a1: number): null {
    SendCommand({
      id: 'SET_zCClassDef-archiveVersionSum-unsigned short-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCClassDef_OnInit(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCClassDef-zCClassDef_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCClassDef(result) : null
  }

  Init(): void | null {
    const result: string | null = SendCommand({
      id: 'zCClassDef-Init-void-false-',
      parentId: this.Uuid,
    })
  }

  CreateNewInstance(): zCObject | null {
    const result: string | null = SendCommand({
      id: 'zCClassDef-CreateNewInstance-zCObject*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCObject(result) : null
  }

  OverwriteCreateNewInstance(a1: zCClassDef): void | null {
    const result: string | null = SendCommand({
      id: 'zCClassDef-OverwriteCreateNewInstance-void-false-zCClassDef*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  ResetCreateNewInstance(): void | null {
    const result: string | null = SendCommand({
      id: 'zCClassDef-ResetCreateNewInstance-void-false-',
      parentId: this.Uuid,
    })
  }

  GetClassID(): number | null {
    const result: string | null = SendCommand({
      id: 'zCClassDef-GetClassID-unsigned short-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetHashIndex(a1: string): number | null {
    const result: string | null = SendCommand({
      id: 'zCClassDef-GetHashIndex-unsigned long-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetHashIndex2(a1: zCObject): number | null {
    const result: string | null = SendCommand({
      id: 'zCClassDef-GetHashIndex-unsigned long-false-zCObject*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  InsertHashTable(a1: zCObject): void | null {
    const result: string | null = SendCommand({
      id: 'zCClassDef-InsertHashTable-void-false-zCObject*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RemoveHashTable(a1: zCObject): void | null {
    const result: string | null = SendCommand({
      id: 'zCClassDef-RemoveHashTable-void-false-zCObject*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SearchHashTable(a1: string): zCObject | null {
    const result: string | null = SendCommand({
      id: 'zCClassDef-SearchHashTable-zCObject*-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new zCObject(result) : null
  }

  CalcHashTableSpread(): number | null {
    const result: string | null = SendCommand({
      id: 'zCClassDef-CalcHashTableSpread-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  ReportLeaks(): void | null {
    const result: string | null = SendCommand({
      id: 'zCClassDef-ReportLeaks-void-false-',
      parentId: this.Uuid,
    })
  }
}
