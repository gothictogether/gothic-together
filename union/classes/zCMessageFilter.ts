import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zTMessageType } from './index.js'
import { zCVob } from './index.js'
import { zCClassDef } from './index.js'
import { zCTriggerBase } from './index.js'

export class zCMessageFilter extends zCTriggerBase {
  sendWhenTriggered(): number | null {
    const result: string | null = SendCommand({
      id: 'zCMessageFilter-sendWhenTriggered-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_sendWhenTriggered(a1: number): null {
    SendCommand({
      id: 'SET_zCMessageFilter-sendWhenTriggered-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  sendWhenUntriggered(): number | null {
    const result: string | null = SendCommand({
      id: 'zCMessageFilter-sendWhenUntriggered-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_sendWhenUntriggered(a1: number): null {
    SendCommand({
      id: 'SET_zCMessageFilter-sendWhenUntriggered-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCMessageFilter_OnInit(): zCMessageFilter | null {
    const result: string | null = SendCommand({
      id: 'zCMessageFilter-zCMessageFilter_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCMessageFilter(result) : null
  }

  ProcessMessage(a1: zTMessageType, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCMessageFilter-ProcessMessage-void-false-zTMessageType,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCMessageFilter-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override OnTrigger(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCMessageFilter-OnTrigger-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override OnUntrigger(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCMessageFilter-OnUntrigger-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override OnTouch(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCMessageFilter-OnTouch-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override OnUntouch(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCMessageFilter-OnUntouch-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }
}
