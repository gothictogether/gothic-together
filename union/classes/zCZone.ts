import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCWorld } from './index.js'
import { zCClassDef } from './index.js'
import { zCVob } from './index.js'

export class zCZone extends zCVob {
  world(): zCWorld | null {
    const result: string | null = SendCommand({
      id: 'zCZone-world-zCWorld*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCWorld(result) : null
  }

  set_world(a1: zCWorld): null {
    SendCommand({
      id: 'SET_zCZone-world-zCWorld*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCZone_OnInit(): zCZone | null {
    const result: string | null = SendCommand({
      id: 'zCZone-zCZone_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCZone(result) : null
  }

  GetCamPosWeight(): number | null {
    const result: string | null = SendCommand({
      id: 'zCZone-GetCamPosWeight-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetZoneVobFlags(): void | null {
    const result: string | null = SendCommand({
      id: 'zCZone-SetZoneVobFlags-void-false-',
      parentId: this.Uuid,
    })
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCZone-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override EndMovement(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCZone-EndMovement-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override SetVisual(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'zCZone-SetVisual-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override ThisVobAddedToWorld(a1: zCWorld): void | null {
    const result: string | null = SendCommand({
      id: 'zCZone-ThisVobAddedToWorld-void-false-zCWorld*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override ThisVobRemovedFromWorld(a1: zCWorld): void | null {
    const result: string | null = SendCommand({
      id: 'zCZone-ThisVobRemovedFromWorld-void-false-zCWorld*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetZoneMotherClass(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCZone-GetZoneMotherClass-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  GetDefaultZoneClass(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCZone-GetDefaultZoneClass-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  GetDebugDescString(): string | null {
    const result: string | null = SendCommand({
      id: 'zCZone-GetDebugDescString-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }
}
