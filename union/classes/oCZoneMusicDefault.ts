import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { oCZoneMusic } from './index.js'

export class oCZoneMusicDefault extends oCZoneMusic {
  static oCZoneMusicDefault_OnInit(): oCZoneMusicDefault | null {
    const result: string | null = SendCommand({
      id: 'oCZoneMusicDefault-oCZoneMusicDefault_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCZoneMusicDefault(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCZoneMusicDefault-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }
}
