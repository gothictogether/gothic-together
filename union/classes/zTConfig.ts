import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'

export class zTConfig extends BaseUnionObject {
  zMV_MIN_SPACE_MOVE_FORWARD(): number | null {
    const result: string | null = SendCommand({
      id: 'zTConfig-zMV_MIN_SPACE_MOVE_FORWARD-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_zMV_MIN_SPACE_MOVE_FORWARD(a1: number): null {
    SendCommand({
      id: 'SET_zTConfig-zMV_MIN_SPACE_MOVE_FORWARD-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  zMV_DETECT_CHASM_SCAN_AT(): number | null {
    const result: string | null = SendCommand({
      id: 'zTConfig-zMV_DETECT_CHASM_SCAN_AT-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_zMV_DETECT_CHASM_SCAN_AT(a1: number): null {
    SendCommand({
      id: 'SET_zTConfig-zMV_DETECT_CHASM_SCAN_AT-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  zMV_STEP_HEIGHT(): number | null {
    const result: string | null = SendCommand({
      id: 'zTConfig-zMV_STEP_HEIGHT-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_zMV_STEP_HEIGHT(a1: number): null {
    SendCommand({
      id: 'SET_zTConfig-zMV_STEP_HEIGHT-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  zMV_JUMP_UP_MIN_CEIL(): number | null {
    const result: string | null = SendCommand({
      id: 'zTConfig-zMV_JUMP_UP_MIN_CEIL-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_zMV_JUMP_UP_MIN_CEIL(a1: number): null {
    SendCommand({
      id: 'SET_zTConfig-zMV_JUMP_UP_MIN_CEIL-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  zMV_WATER_DEPTH_KNEE(): number | null {
    const result: string | null = SendCommand({
      id: 'zTConfig-zMV_WATER_DEPTH_KNEE-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_zMV_WATER_DEPTH_KNEE(a1: number): null {
    SendCommand({
      id: 'SET_zTConfig-zMV_WATER_DEPTH_KNEE-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  zMV_WATER_DEPTH_CHEST(): number | null {
    const result: string | null = SendCommand({
      id: 'zTConfig-zMV_WATER_DEPTH_CHEST-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_zMV_WATER_DEPTH_CHEST(a1: number): null {
    SendCommand({
      id: 'SET_zTConfig-zMV_WATER_DEPTH_CHEST-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  zMV_YMAX_SWIM_CLIMB_OUT(): number | null {
    const result: string | null = SendCommand({
      id: 'zTConfig-zMV_YMAX_SWIM_CLIMB_OUT-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_zMV_YMAX_SWIM_CLIMB_OUT(a1: number): null {
    SendCommand({
      id: 'SET_zTConfig-zMV_YMAX_SWIM_CLIMB_OUT-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  zMV_FORCE_JUMP_UP(): number | null {
    const result: string | null = SendCommand({
      id: 'zTConfig-zMV_FORCE_JUMP_UP-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_zMV_FORCE_JUMP_UP(a1: number): null {
    SendCommand({
      id: 'SET_zTConfig-zMV_FORCE_JUMP_UP-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  zMV_YMAX_JUMPLOW(): number | null {
    const result: string | null = SendCommand({
      id: 'zTConfig-zMV_YMAX_JUMPLOW-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_zMV_YMAX_JUMPLOW(a1: number): null {
    SendCommand({
      id: 'SET_zTConfig-zMV_YMAX_JUMPLOW-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  zMV_YMAX_JUMPMID(): number | null {
    const result: string | null = SendCommand({
      id: 'zTConfig-zMV_YMAX_JUMPMID-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_zMV_YMAX_JUMPMID(a1: number): null {
    SendCommand({
      id: 'SET_zTConfig-zMV_YMAX_JUMPMID-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  zMV_MAX_GROUND_ANGLE_WALK(): number | null {
    const result: string | null = SendCommand({
      id: 'zTConfig-zMV_MAX_GROUND_ANGLE_WALK-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_zMV_MAX_GROUND_ANGLE_WALK(a1: number): null {
    SendCommand({
      id: 'SET_zTConfig-zMV_MAX_GROUND_ANGLE_WALK-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  zMV_MAX_GROUND_ANGLE_SLIDE(): number | null {
    const result: string | null = SendCommand({
      id: 'zTConfig-zMV_MAX_GROUND_ANGLE_SLIDE-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_zMV_MAX_GROUND_ANGLE_SLIDE(a1: number): null {
    SendCommand({
      id: 'SET_zTConfig-zMV_MAX_GROUND_ANGLE_SLIDE-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  zMV_MAX_GROUND_ANGLE_SLIDE2(): number | null {
    const result: string | null = SendCommand({
      id: 'zTConfig-zMV_MAX_GROUND_ANGLE_SLIDE2-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_zMV_MAX_GROUND_ANGLE_SLIDE2(a1: number): null {
    SendCommand({
      id: 'SET_zTConfig-zMV_MAX_GROUND_ANGLE_SLIDE2-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  zMV_DCUL_WALL_HEADING_ANGLE(): number | null {
    const result: string | null = SendCommand({
      id: 'zTConfig-zMV_DCUL_WALL_HEADING_ANGLE-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_zMV_DCUL_WALL_HEADING_ANGLE(a1: number): null {
    SendCommand({
      id: 'SET_zTConfig-zMV_DCUL_WALL_HEADING_ANGLE-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  zMV_DCUL_WALL_HORIZ_ANGLE(): number | null {
    const result: string | null = SendCommand({
      id: 'zTConfig-zMV_DCUL_WALL_HORIZ_ANGLE-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_zMV_DCUL_WALL_HORIZ_ANGLE(a1: number): null {
    SendCommand({
      id: 'SET_zTConfig-zMV_DCUL_WALL_HORIZ_ANGLE-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  zMV_DCUL_GROUND_ANGLE(): number | null {
    const result: string | null = SendCommand({
      id: 'zTConfig-zMV_DCUL_GROUND_ANGLE-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_zMV_DCUL_GROUND_ANGLE(a1: number): null {
    SendCommand({
      id: 'SET_zTConfig-zMV_DCUL_GROUND_ANGLE-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
