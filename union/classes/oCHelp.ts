import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCVob } from './index.js'

export class oCHelp extends BaseUnionObject {
  dx(): number | null {
    const result: string | null = SendCommand({
      id: 'oCHelp-dx-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_dx(a1: number): null {
    SendCommand({
      id: 'SET_oCHelp-dx-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  dy(): number | null {
    const result: string | null = SendCommand({
      id: 'oCHelp-dy-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_dy(a1: number): null {
    SendCommand({
      id: 'SET_oCHelp-dy-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  sx(): number | null {
    const result: string | null = SendCommand({
      id: 'oCHelp-sx-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_sx(a1: number): null {
    SendCommand({
      id: 'SET_oCHelp-sx-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  sy(): number | null {
    const result: string | null = SendCommand({
      id: 'oCHelp-sy-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_sy(a1: number): null {
    SendCommand({
      id: 'SET_oCHelp-sy-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCHelp_OnInit(): oCHelp | null {
    const result: string | null = SendCommand({
      id: 'oCHelp-oCHelp_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCHelp(result) : null
  }

  CalcPos(): void | null {
    const result: string | null = SendCommand({
      id: 'oCHelp-CalcPos-void-false-',
      parentId: this.Uuid,
    })
  }

  Toggle(): void | null {
    const result: string | null = SendCommand({
      id: 'oCHelp-Toggle-void-false-',
      parentId: this.Uuid,
    })
  }

  Update(): void | null {
    const result: string | null = SendCommand({
      id: 'oCHelp-Update-void-false-',
      parentId: this.Uuid,
    })
  }

  Redraw(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCHelp-Redraw-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }
}
