import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { oCNpc } from './index.js'

export class oCRtnCutscene extends BaseUnionObject {
  npc(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCRtnCutscene-npc-oCNpc*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  set_npc(a1: oCNpc): null {
    SendCommand({
      id: 'SET_oCRtnCutscene-npc-oCNpc*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  csName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCRtnCutscene-csName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_csName(a1: string): null {
    SendCommand({
      id: 'SET_oCRtnCutscene-csName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  roleName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCRtnCutscene-roleName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_roleName(a1: string): null {
    SendCommand({
      id: 'SET_oCRtnCutscene-roleName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  ignorePlayState(): number | null {
    const result: string | null = SendCommand({
      id: 'oCRtnCutscene-ignorePlayState-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_ignorePlayState(a1: number): null {
    SendCommand({
      id: 'SET_oCRtnCutscene-ignorePlayState-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  active(): number | null {
    const result: string | null = SendCommand({
      id: 'oCRtnCutscene-active-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_active(a1: number): null {
    SendCommand({
      id: 'SET_oCRtnCutscene-active-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCRtnCutscene_OnInit(): oCRtnCutscene | null {
    const result: string | null = SendCommand({
      id: 'oCRtnCutscene-oCRtnCutscene_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCRtnCutscene(result) : null
  }

  ResetPlayState(): void | null {
    const result: string | null = SendCommand({
      id: 'oCRtnCutscene-ResetPlayState-void-false-',
      parentId: this.Uuid,
    })
  }
}
