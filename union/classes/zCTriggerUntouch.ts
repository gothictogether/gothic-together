import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { zCVob } from './index.js'
import { zCTriggerBase } from './index.js'

export class zCTriggerUntouch extends zCTriggerBase {
  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerUntouch-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override OnTrigger(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerUntouch-OnTrigger-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override OnUntrigger(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerUntouch-OnUntrigger-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override OnTouch(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerUntouch-OnTouch-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override OnUntouch(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerUntouch-OnUntouch-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }
}
