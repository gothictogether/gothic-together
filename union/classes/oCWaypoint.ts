import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCVob } from './index.js'
import { zCWaypoint } from './index.js'

export class oCWaypoint extends zCWaypoint {
  static oCWaypoint_OnInit(): oCWaypoint | null {
    const result: string | null = SendCommand({
      id: 'oCWaypoint-oCWaypoint_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCWaypoint(result) : null
  }

  override CanBeUsed(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'oCWaypoint-CanBeUsed-int-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }
}
