import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { oCItem } from './index.js'
import { oCNpc } from './index.js'
import { zCVob } from './index.js'
import { zCClassDef } from './index.js'
import { oCAIArrowBase } from './index.js'

export class oCAIArrow extends oCAIArrowBase {
  arrow(): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCAIArrow-arrow-oCItem*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCItem(result) : null
  }

  set_arrow(a1: oCItem): null {
    SendCommand({
      id: 'SET_oCAIArrow-arrow-oCItem*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  owner(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCAIArrow-owner-oCNpc*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  set_owner(a1: oCNpc): null {
    SendCommand({
      id: 'SET_oCAIArrow-owner-oCNpc*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  removeVob(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIArrow-removeVob-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_removeVob(a1: number): null {
    SendCommand({
      id: 'SET_oCAIArrow-removeVob-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  targetNPC(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCAIArrow-targetNPC-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_targetNPC(a1: zCVob): null {
    SendCommand({
      id: 'SET_oCAIArrow-targetNPC-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCAIArrow_OnInit(): oCAIArrow | null {
    const result: string | null = SendCommand({
      id: 'oCAIArrow-oCAIArrow_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCAIArrow(result) : null
  }

  SetTarget(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIArrow-SetTarget-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  ClearUsedVobs(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIArrow-ClearUsedVobs-void-false-',
      parentId: this.Uuid,
    })
  }

  SetupAIVob(a1: zCVob, a2: zCVob, a3: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIArrow-SetupAIVob-void-false-zCVob*,zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCAIArrow-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override DoAI(a1: zCVob, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIArrow-DoAI-void-false-zCVob*,int&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override CanThisCollideWith(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIArrow-CanThisCollideWith-int-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }
}
