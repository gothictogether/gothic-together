import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { zCVob } from './index.js'
import { zCTriggerBase } from './index.js'

export class zCTriggerWorldStart extends zCTriggerBase {
  fireOnlyFirstTime(): number | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerWorldStart-fireOnlyFirstTime-char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fireOnlyFirstTime(a1: number): null {
    SendCommand({
      id: 'SET_zCTriggerWorldStart-fireOnlyFirstTime-char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  hasFired(): number | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerWorldStart-hasFired-char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_hasFired(a1: number): null {
    SendCommand({
      id: 'SET_zCTriggerWorldStart-hasFired-char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCTriggerWorldStart_OnInit(): zCTriggerWorldStart | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerWorldStart-zCTriggerWorldStart_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCTriggerWorldStart(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerWorldStart-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override OnTrigger(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerWorldStart-OnTrigger-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override OnUntrigger(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerWorldStart-OnUntrigger-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override OnTouch(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerWorldStart-OnTouch-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override OnUntouch(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerWorldStart-OnUntouch-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override PostLoad(): void | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerWorldStart-PostLoad-void-false-',
      parentId: this.Uuid,
    })
  }
}
