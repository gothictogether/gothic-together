import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zTBBox3D } from './index.js'
import { zTBSphere3D } from './index.js'
import { zTVobType } from './index.js'
import { zCWorld } from './index.js'
import { zCPolygon } from './index.js'
import { zCAIBase } from './index.js'
import { zTAnimationMode } from './index.js'
import { zVEC3 } from './index.js'
import { zCEventManager } from './index.js'
import { zTMovementMode } from './index.js'
import { zTBBox2D } from './index.js'
import { zTCollisionContext } from './index.js'
import { zCClassDef } from './index.js'
import { zCEventMessage } from './index.js'
import { zTVobCharClass } from './index.js'
import { zTVobSleepingMode } from './index.js'
import { zTTraceRayReport } from './index.js'
import { zCObject } from './index.js'

export class zCVob extends zCObject {
  lastTimeDrawn(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVob-lastTimeDrawn-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_lastTimeDrawn(a1: number): null {
    SendCommand({
      id: 'SET_zCVob-lastTimeDrawn-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lastTimeCollected(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVob-lastTimeCollected-unsigned long-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_lastTimeCollected(a1: number): null {
    SendCommand({
      id: 'SET_zCVob-lastTimeCollected-unsigned long-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  bbox3D(): zTBBox3D | null {
    const result: string | null = SendCommand({
      id: 'zCVob-bbox3D-zTBBox3D-false-false',
      parentId: this.Uuid,
    })

    return result ? new zTBBox3D(result) : null
  }

  set_bbox3D(a1: zTBBox3D): null {
    SendCommand({
      id: 'SET_zCVob-bbox3D-zTBBox3D-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  bsphere3D(): zTBSphere3D | null {
    const result: string | null = SendCommand({
      id: 'zCVob-bsphere3D-zTBSphere3D-false-false',
      parentId: this.Uuid,
    })

    return result ? new zTBSphere3D(result) : null
  }

  set_bsphere3D(a1: zTBSphere3D): null {
    SendCommand({
      id: 'SET_zCVob-bsphere3D-zTBSphere3D-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  type(): zTVobType | null {
    const result: string | null = SendCommand({
      id: 'zCVob-type-zTVobType-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_type(a1: zTVobType): null {
    SendCommand({
      id: 'SET_zCVob-type-zTVobType-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  groundShadowSizePacked(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVob-groundShadowSizePacked-unsigned long-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_groundShadowSizePacked(a1: number): null {
    SendCommand({
      id: 'SET_zCVob-groundShadowSizePacked-unsigned long-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  homeWorld(): zCWorld | null {
    const result: string | null = SendCommand({
      id: 'zCVob-homeWorld-zCWorld*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCWorld(result) : null
  }

  set_homeWorld(a1: zCWorld): null {
    SendCommand({
      id: 'SET_zCVob-homeWorld-zCWorld*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  groundPoly(): zCPolygon | null {
    const result: string | null = SendCommand({
      id: 'zCVob-groundPoly-zCPolygon*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCPolygon(result) : null
  }

  set_groundPoly(a1: zCPolygon): null {
    SendCommand({
      id: 'SET_zCVob-groundPoly-zCPolygon*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  callback_ai(): zCAIBase | null {
    const result: string | null = SendCommand({
      id: 'zCVob-callback_ai-zCAIBase*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCAIBase(result) : null
  }

  set_callback_ai(a1: zCAIBase): null {
    SendCommand({
      id: 'SET_zCVob-callback_ai-zCAIBase*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  visualAlpha(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVob-visualAlpha-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_visualAlpha(a1: number): null {
    SendCommand({
      id: 'SET_zCVob-visualAlpha-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_fVobFarClipZScale(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVob-m_fVobFarClipZScale-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_fVobFarClipZScale(a1: number): null {
    SendCommand({
      id: 'SET_zCVob-m_fVobFarClipZScale-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_AniMode(): zTAnimationMode | null {
    const result: string | null = SendCommand({
      id: 'zCVob-m_AniMode-zTAnimationMode-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_AniMode(a1: zTAnimationMode): null {
    SendCommand({
      id: 'SET_zCVob-m_AniMode-zTAnimationMode-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_aniModeStrength(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVob-m_aniModeStrength-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_aniModeStrength(a1: number): null {
    SendCommand({
      id: 'SET_zCVob-m_aniModeStrength-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_zBias(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVob-m_zBias-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_zBias(a1: number): null {
    SendCommand({
      id: 'SET_zCVob-m_zBias-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lightDirectionStat(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCVob-lightDirectionStat-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_lightDirectionStat(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCVob-lightDirectionStat-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  eventManager(): zCEventManager | null {
    const result: string | null = SendCommand({
      id: 'zCVob-eventManager-zCEventManager*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCEventManager(result) : null
  }

  set_eventManager(a1: zCEventManager): null {
    SendCommand({
      id: 'SET_zCVob-eventManager-zCEventManager*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  nextOnTimer(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVob-nextOnTimer-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_nextOnTimer(a1: number): null {
    SendCommand({
      id: 'SET_zCVob-nextOnTimer-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  showVisual(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVob-showVisual-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_showVisual(a1: number): null {
    SendCommand({
      id: 'SET_zCVob-showVisual-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  drawBBox3D(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVob-drawBBox3D-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_drawBBox3D(a1: number): null {
    SendCommand({
      id: 'SET_zCVob-drawBBox3D-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  visualAlphaEnabled(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVob-visualAlphaEnabled-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_visualAlphaEnabled(a1: number): null {
    SendCommand({
      id: 'SET_zCVob-visualAlphaEnabled-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  physicsEnabled(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVob-physicsEnabled-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_physicsEnabled(a1: number): null {
    SendCommand({
      id: 'SET_zCVob-physicsEnabled-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  staticVob(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVob-staticVob-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_staticVob(a1: number): null {
    SendCommand({
      id: 'SET_zCVob-staticVob-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  ignoredByTraceRay(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVob-ignoredByTraceRay-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_ignoredByTraceRay(a1: number): null {
    SendCommand({
      id: 'SET_zCVob-ignoredByTraceRay-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  collDetectionStatic(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVob-collDetectionStatic-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_collDetectionStatic(a1: number): null {
    SendCommand({
      id: 'SET_zCVob-collDetectionStatic-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  collDetectionDynamic(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVob-collDetectionDynamic-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_collDetectionDynamic(a1: number): null {
    SendCommand({
      id: 'SET_zCVob-collDetectionDynamic-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  castDynShadow(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVob-castDynShadow-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_castDynShadow(a1: number): null {
    SendCommand({
      id: 'SET_zCVob-castDynShadow-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lightColorStatDirty(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVob-lightColorStatDirty-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_lightColorStatDirty(a1: number): null {
    SendCommand({
      id: 'SET_zCVob-lightColorStatDirty-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lightColorDynDirty(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVob-lightColorDynDirty-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_lightColorDynDirty(a1: number): null {
    SendCommand({
      id: 'SET_zCVob-lightColorDynDirty-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  isInMovementMode(): zTMovementMode | null {
    const result: string | null = SendCommand({
      id: 'zCVob-isInMovementMode-zTMovementMode-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_isInMovementMode(a1: zTMovementMode): null {
    SendCommand({
      id: 'SET_zCVob-isInMovementMode-zTMovementMode-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  sleepingMode(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVob-sleepingMode-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_sleepingMode(a1: number): null {
    SendCommand({
      id: 'SET_zCVob-sleepingMode-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  mbHintTrafoLocalConst(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVob-mbHintTrafoLocalConst-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_mbHintTrafoLocalConst(a1: number): null {
    SendCommand({
      id: 'SET_zCVob-mbHintTrafoLocalConst-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  mbInsideEndMovementMethod(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVob-mbInsideEndMovementMethod-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_mbInsideEndMovementMethod(a1: number): null {
    SendCommand({
      id: 'SET_zCVob-mbInsideEndMovementMethod-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  collButNoMove(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVob-collButNoMove-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_collButNoMove(a1: number): null {
    SendCommand({
      id: 'SET_zCVob-collButNoMove-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  dontWriteIntoArchive(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVob-dontWriteIntoArchive-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_dontWriteIntoArchive(a1: number): null {
    SendCommand({
      id: 'SET_zCVob-dontWriteIntoArchive-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  bIsInWater(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVob-bIsInWater-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_bIsInWater(a1: number): null {
    SendCommand({
      id: 'SET_zCVob-bIsInWater-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  bIsAmbientVob(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVob-bIsAmbientVob-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_bIsAmbientVob(a1: number): null {
    SendCommand({
      id: 'SET_zCVob-bIsAmbientVob-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCVob_OnInit(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'zCVob-zCVob_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCVob(result) : null
  }

  GetPositionWorld(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCVob-GetPositionWorld-zVEC3-false-',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  GetAtVectorWorld(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCVob-GetAtVectorWorld-zVEC3-false-',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  GetUpVectorWorld(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCVob-GetUpVectorWorld-zVEC3-false-',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  GetRightVectorWorld(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCVob-GetRightVectorWorld-zVEC3-false-',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  SetAI(a1: zCAIBase): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-SetAI-void-false-zCAIBase*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetVobPresetName(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-SetVobPresetName-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetVobPresetName(): string | null {
    const result: string | null = SendCommand({
      id: 'zCVob-GetVobPresetName-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  SetVobName(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-SetVobName-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CalcStaticLight(a1: zVEC3): number | null {
    const result: string | null = SendCommand({
      id: 'zCVob-CalcStaticLight-int-false-zVEC3&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetSectorNameVobIsIn(): string | null {
    const result: string | null = SendCommand({
      id: 'zCVob-GetSectorNameVobIsIn-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  RemoveVobFromWorld(): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-RemoveVobFromWorld-void-false-',
      parentId: this.Uuid,
    })
  }

  RemoveVobSubtreeFromWorld(): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-RemoveVobSubtreeFromWorld-void-false-',
      parentId: this.Uuid,
    })
  }

  RemoveWorldDependencies(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-RemoveWorldDependencies-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RemoveVobFromGlobalVobTree(): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-RemoveVobFromGlobalVobTree-void-false-',
      parentId: this.Uuid,
    })
  }

  RemoveVobFromBspTree(): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-RemoveVobFromBspTree-void-false-',
      parentId: this.Uuid,
    })
  }

  GetVobInfo(): string | null {
    const result: string | null = SendCommand({
      id: 'zCVob-GetVobInfo-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  SetSleeping(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-SetSleeping-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  UpdateVisualDependencies(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-UpdateVisualDependencies-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  AddVobToWorld_CorrectParentDependencies(): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-AddVobToWorld_CorrectParentDependencies-void-false-',
      parentId: this.Uuid,
    })
  }

  ResetOnTimer(): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-ResetOnTimer-void-false-',
      parentId: this.Uuid,
    })
  }

  SetOnTimer(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-SetOnTimer-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  IsOnTimer(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVob-IsOnTimer-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  ProcessOnTimer(): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-ProcessOnTimer-void-false-',
      parentId: this.Uuid,
    })
  }

  DoFrameActivity(): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-DoFrameActivity-void-false-',
      parentId: this.Uuid,
    })
  }

  GetScreenBBox2D(): zTBBox2D | null {
    const result: string | null = SendCommand({
      id: 'zCVob-GetScreenBBox2D-zTBBox2D-false-',
      parentId: this.Uuid,
    })

    return result ? new zTBBox2D(result) : null
  }

  GetIsProjectile(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVob-GetIsProjectile-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  CalcGroundPoly(): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-CalcGroundPoly-void-false-',
      parentId: this.Uuid,
    })
  }

  CalcWaterVob(): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-CalcWaterVob-void-false-',
      parentId: this.Uuid,
    })
  }

  SetCollTypeDefaultFromVisual(): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-SetCollTypeDefaultFromVisual-void-false-',
      parentId: this.Uuid,
    })
  }

  TouchMovement(): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-TouchMovement-void-false-',
      parentId: this.Uuid,
    })
  }

  SetBBox3DWorld(a1: zTBBox3D): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-SetBBox3DWorld-void-false-zTBBox3D const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetBBox3DLocal(a1: zTBBox3D): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-SetBBox3DLocal-void-false-zTBBox3D const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetBBox3DLocal(): zTBBox3D | null {
    const result: string | null = SendCommand({
      id: 'zCVob-GetBBox3DLocal-zTBBox3D-false-',
      parentId: this.Uuid,
    })

    return result ? new zTBBox3D(result) : null
  }

  Move(a1: number, a2: number, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-Move-void-false-float,float,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  MoveWorld(a1: number, a2: number, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-MoveWorld-void-false-float,float,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  MoveLocal(a1: number, a2: number, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-MoveLocal-void-false-float,float,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  RotateWorld(a1: zVEC3, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-RotateWorld-void-false-zVEC3 const&,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  RotateLocal(a1: zVEC3, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-RotateLocal-void-false-zVEC3 const&,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  RotateLocalX(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-RotateLocalX-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RotateLocalY(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-RotateLocalY-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RotateLocalZ(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-RotateLocalZ-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RotateWorldX(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-RotateWorldX-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RotateWorldY(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-RotateWorldY-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RotateWorldZ(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-RotateWorldZ-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetPositionWorld2(a1: number, a2: number, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-GetPositionWorld-void-false-float&,float&,float&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  GetPositionLocal(a1: number, a2: number, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-GetPositionLocal-void-false-float&,float&,float&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  GetDistanceToVob(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'zCVob-GetDistanceToVob-float-false-zCVob&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetDistanceToVobApprox(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'zCVob-GetDistanceToVobApprox-float-false-zCVob&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetDistanceToVob2(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'zCVob-GetDistanceToVob2-float-false-zCVob&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  SetPositionLocal(a1: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-SetPositionLocal-void-false-zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetPositionWorld(a1: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-SetPositionWorld-void-false-zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  ResetRotationsLocal(): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-ResetRotationsLocal-void-false-',
      parentId: this.Uuid,
    })
  }

  ResetXZRotationsLocal(): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-ResetXZRotationsLocal-void-false-',
      parentId: this.Uuid,
    })
  }

  ResetRotationsWorld(): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-ResetRotationsWorld-void-false-',
      parentId: this.Uuid,
    })
  }

  ResetXZRotationsWorld(): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-ResetXZRotationsWorld-void-false-',
      parentId: this.Uuid,
    })
  }

  SetHeadingYLocal(a1: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-SetHeadingYLocal-void-false-zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetHeadingYWorld(a1: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-SetHeadingYWorld-void-false-zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetHeadingYWorld2(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-SetHeadingYWorld-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetHeadingLocal(a1: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-SetHeadingLocal-void-false-zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetHeadingWorld(a1: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-SetHeadingWorld-void-false-zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetHeadingWorld2(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-SetHeadingWorld-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetHeadingAtLocal(a1: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-SetHeadingAtLocal-void-false-zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  HasParentVob(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVob-HasParentVob-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetHeadingAtWorld(a1: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-SetHeadingAtWorld-void-false-zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetCollDetStat(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-SetCollDetStat-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetCollDetDyn(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-SetCollDetDyn-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CorrectTrafo(): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-CorrectTrafo-void-false-',
      parentId: this.Uuid,
    })
  }

  DoneWithTrafoLocal(): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-DoneWithTrafoLocal-void-false-',
      parentId: this.Uuid,
    })
  }

  CreateTrafoLocal(): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-CreateTrafoLocal-void-false-',
      parentId: this.Uuid,
    })
  }

  SetPhysicsEnabled(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-SetPhysicsEnabled-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetVelocity(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCVob-GetVelocity-zVEC3-false-',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  UpdatePhysics(): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-UpdatePhysics-void-false-',
      parentId: this.Uuid,
    })
  }

  SetInMovement(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-SetInMovement-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetInMovementMode(a1: zTMovementMode): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-SetInMovementMode-void-false-zTMovementMode',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  BeginMovement(): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-BeginMovement-void-false-',
      parentId: this.Uuid,
    })
  }

  ResetToOldMovementState(): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-ResetToOldMovementState-void-false-',
      parentId: this.Uuid,
    })
  }

  CalcDestinationBBox3DWorld(a1: zTBBox3D): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-CalcDestinationBBox3DWorld-void-false-zTBBox3D&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  AdoptCollObjResults(): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-AdoptCollObjResults-void-false-',
      parentId: this.Uuid,
    })
  }

  CreateCollisionObject(): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-CreateCollisionObject-void-false-',
      parentId: this.Uuid,
    })
  }

  ResetCollisionObjectState(): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-ResetCollisionObjectState-void-false-',
      parentId: this.Uuid,
    })
  }

  DestroyCollisionObject(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-DestroyCollisionObject-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CheckEmergencyPutToSleep(): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-CheckEmergencyPutToSleep-void-false-',
      parentId: this.Uuid,
    })
  }

  CollectCollisionContext_Vobs(a1: zTBBox3D, a2: zTCollisionContext): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-CollectCollisionContext_Vobs-void-false-zTBBox3D const&,zTCollisionContext&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  CollectCollisionContext(a1: zTCollisionContext): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-CollectCollisionContext-void-false-zTCollisionContext&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CleanupCollisionContext(a1: zTCollisionContext): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-CleanupCollisionContext-void-false-zTCollisionContext const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CheckAndResolveCollisions(): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-CheckAndResolveCollisions-void-false-',
      parentId: this.Uuid,
    })
  }

  IsColliding(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVob-IsColliding-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetHomeWorld(): zCWorld | null {
    const result: string | null = SendCommand({
      id: 'zCVob-GetHomeWorld-zCWorld*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCWorld(result) : null
  }

  SetStaticVob(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-SetStaticVob-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetVobType(): zTVobType | null {
    const result: string | null = SendCommand({
      id: 'zCVob-GetVobType-zTVobType-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetDrawBBox3D(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-SetDrawBBox3D-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetCollDet(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-SetCollDet-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCVob-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  OnTrigger(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-OnTrigger-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  OnUntrigger(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-OnUntrigger-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  OnTouch(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-OnTouch-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  OnUntouch(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-OnUntouch-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  OnTouchLevel(): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-OnTouchLevel-void-false-',
      parentId: this.Uuid,
    })
  }

  OnDamage(a1: zCVob, a2: zCVob, a3: number, a4: number, a5: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-OnDamage-void-false-zCVob*,zCVob*,float,int,zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
      a5: a5.toString(),
    })
  }

  OnMessage(a1: zCEventMessage, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-OnMessage-void-false-zCEventMessage*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  OnTick(): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-OnTick-void-false-',
      parentId: this.Uuid,
    })
  }

  OnTimer(): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-OnTimer-void-false-',
      parentId: this.Uuid,
    })
  }

  PostLoad(): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-PostLoad-void-false-',
      parentId: this.Uuid,
    })
  }

  GetCharacterClass(): zTVobCharClass | null {
    const result: string | null = SendCommand({
      id: 'zCVob-GetCharacterClass-zTVobCharClass-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetSleepingMode(a1: zTVobSleepingMode): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-SetSleepingMode-void-false-zTVobSleepingMode',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  EndMovement(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-EndMovement-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CanThisCollideWith(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'zCVob-CanThisCollideWith-int-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  SetVisual(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-SetVisual-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetCSStateFlags(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVob-GetCSStateFlags-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  TraceRay(a1: zVEC3, a2: zVEC3, a3: number, a4: zTTraceRayReport): number | null {
    const result: string | null = SendCommand({
      id: 'zCVob-TraceRay-int-false-zVEC3 const&,zVEC3 const&,int,zTTraceRayReport&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
    })

    return result ? Number(result) : null
  }

  GetTriggerTarget(a1: number): string | null {
    const result: string | null = SendCommand({
      id: 'zCVob-GetTriggerTarget-zSTRING-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? String(result) : null
  }

  ThisVobAddedToWorld(a1: zCWorld): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-ThisVobAddedToWorld-void-false-zCWorld*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  ThisVobRemovedFromWorld(a1: zCWorld): void | null {
    const result: string | null = SendCommand({
      id: 'zCVob-ThisVobRemovedFromWorld-void-false-zCWorld*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }
}
