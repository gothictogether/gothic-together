import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCVob } from './index.js'
import { zCClassDef } from './index.js'
import { zVEC3 } from './index.js'
import { oCAISound } from './index.js'

export class oCAIArrowBase extends oCAISound {
  collisionOccured(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIArrowBase-collisionOccured-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_collisionOccured(a1: number): null {
    SendCommand({
      id: 'SET_oCAIArrowBase-collisionOccured-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  timeLeft(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIArrowBase-timeLeft-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_timeLeft(a1: number): null {
    SendCommand({
      id: 'SET_oCAIArrowBase-timeLeft-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  vob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCAIArrowBase-vob-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_vob(a1: zCVob): null {
    SendCommand({
      id: 'SET_oCAIArrowBase-vob-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  startDustFX(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIArrowBase-startDustFX-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_startDustFX(a1: number): null {
    SendCommand({
      id: 'SET_oCAIArrowBase-startDustFX-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  trailVob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCAIArrowBase-trailVob-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_trailVob(a1: zCVob): null {
    SendCommand({
      id: 'SET_oCAIArrowBase-trailVob-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  trailActive(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIArrowBase-trailActive-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_trailActive(a1: number): null {
    SendCommand({
      id: 'SET_oCAIArrowBase-trailActive-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  trailTime(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIArrowBase-trailTime-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_trailTime(a1: number): null {
    SendCommand({
      id: 'SET_oCAIArrowBase-trailTime-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  hasHit(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIArrowBase-hasHit-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_hasHit(a1: number): null {
    SendCommand({
      id: 'SET_oCAIArrowBase-hasHit-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCAIArrowBase_OnInit(): oCAIArrowBase | null {
    const result: string | null = SendCommand({
      id: 'oCAIArrowBase-oCAIArrowBase_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCAIArrowBase(result) : null
  }

  AddIgnoreCDVob(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIArrowBase-AddIgnoreCDVob-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  ClearIgnoreCDVob(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIArrowBase-ClearIgnoreCDVob-void-false-',
      parentId: this.Uuid,
    })
  }

  CreateTrail(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIArrowBase-CreateTrail-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCAIArrowBase-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override DoAI(a1: zCVob, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIArrowBase-DoAI-void-false-zCVob*,int&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override HasAIDetectedCollision(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIArrowBase-HasAIDetectedCollision-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override AICollisionResponseSelfDetected(a1: zVEC3, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIArrowBase-AICollisionResponseSelfDetected-void-false-zVEC3 const&,int&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override GetIsProjectile(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIArrowBase-GetIsProjectile-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }
}
