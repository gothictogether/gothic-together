import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCVob } from './index.js'
import { zCClassDef } from './index.js'
import { oCAISound } from './index.js'

export class oCAIVobMove extends oCAISound {
  vob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCAIVobMove-vob-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_vob(a1: zCVob): null {
    SendCommand({
      id: 'SET_oCAIVobMove-vob-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  owner(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCAIVobMove-owner-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_owner(a1: zCVob): null {
    SendCommand({
      id: 'SET_oCAIVobMove-owner-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCAIVobMove_OnInit(): oCAIVobMove | null {
    const result: string | null = SendCommand({
      id: 'oCAIVobMove-oCAIVobMove_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCAIVobMove(result) : null
  }

  AddIgnoreCDVob(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIVobMove-AddIgnoreCDVob-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  ClearIgnoreCDVob(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIVobMove-ClearIgnoreCDVob-void-false-',
      parentId: this.Uuid,
    })
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCAIVobMove-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override DoAI(a1: zCVob, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIVobMove-DoAI-void-false-zCVob*,int&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override CanThisCollideWith(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'oCAIVobMove-CanThisCollideWith-int-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }
}
