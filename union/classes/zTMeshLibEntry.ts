import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCModelTexAniState } from './index.js'
import { zCModelMeshLib } from './index.js'

export class zTMeshLibEntry extends BaseUnionObject {
  texAniState(): zCModelTexAniState | null {
    const result: string | null = SendCommand({
      id: 'zTMeshLibEntry-texAniState-zCModelTexAniState-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCModelTexAniState(result) : null
  }

  set_texAniState(a1: zCModelTexAniState): null {
    SendCommand({
      id: 'SET_zTMeshLibEntry-texAniState-zCModelTexAniState-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  meshLib(): zCModelMeshLib | null {
    const result: string | null = SendCommand({
      id: 'zTMeshLibEntry-meshLib-zCModelMeshLib*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCModelMeshLib(result) : null
  }

  set_meshLib(a1: zCModelMeshLib): null {
    SendCommand({
      id: 'SET_zTMeshLibEntry-meshLib-zCModelMeshLib*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
