import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { zCVob } from './index.js'
import { zCTrigger } from './index.js'

export class oCCSTrigger extends zCTrigger {
  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCCSTrigger-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override TriggerTarget(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCCSTrigger-TriggerTarget-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override UntriggerTarget(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCCSTrigger-UntriggerTarget-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }
}
