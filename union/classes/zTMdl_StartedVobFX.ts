import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCVob } from './index.js'

export class zTMdl_StartedVobFX extends BaseUnionObject {
  vob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'zTMdl_StartedVobFX-vob-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_vob(a1: zCVob): null {
    SendCommand({
      id: 'SET_zTMdl_StartedVobFX-vob-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  vobFXHandle(): number | null {
    const result: string | null = SendCommand({
      id: 'zTMdl_StartedVobFX-vobFXHandle-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_vobFXHandle(a1: number): null {
    SendCommand({
      id: 'SET_zTMdl_StartedVobFX-vobFXHandle-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
