import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { zCTouchDamage } from './index.js'

export class oCTouchDamage extends zCTouchDamage {
  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCTouchDamage-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override GetDamageTypeArcEnum(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCTouchDamage-GetDamageTypeArcEnum-char-false-unsigned long',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }
}
