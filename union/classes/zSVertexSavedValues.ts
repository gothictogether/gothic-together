import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCVertex } from './index.js'

export class zSVertexSavedValues extends BaseUnionObject {
  vert(): zCVertex | null {
    const result: string | null = SendCommand({
      id: 'zSVertexSavedValues-vert-zCVertex*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVertex(result) : null
  }

  set_vert(a1: zCVertex): null {
    SendCommand({
      id: 'SET_zSVertexSavedValues-vert-zCVertex*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  offset(): number | null {
    const result: string | null = SendCommand({
      id: 'zSVertexSavedValues-offset-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_offset(a1: number): null {
    SendCommand({
      id: 'SET_zSVertexSavedValues-offset-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
