import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zTBBox3D } from './index.js'
import { zVEC3 } from './index.js'
import { zTFileSourceType } from './index.js'
import { zCModelNode } from './index.js'
import { zCModelAni } from './index.js'

export class zCModelPrototype extends BaseUnionObject {
  next(): zCModelPrototype | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-next-zCModelPrototype*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCModelPrototype(result) : null
  }

  set_next(a1: zCModelPrototype): null {
    SendCommand({
      id: 'SET_zCModelPrototype-next-zCModelPrototype*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  prev(): zCModelPrototype | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-prev-zCModelPrototype*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCModelPrototype(result) : null
  }

  set_prev(a1: zCModelPrototype): null {
    SendCommand({
      id: 'SET_zCModelPrototype-prev-zCModelPrototype*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  refCtr(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-refCtr-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_refCtr(a1: number): null {
    SendCommand({
      id: 'SET_zCModelPrototype-refCtr-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  modelProtoName(): string | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-modelProtoName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_modelProtoName(a1: string): null {
    SendCommand({
      id: 'SET_zCModelPrototype-modelProtoName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  modelProtoFileName(): string | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-modelProtoFileName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_modelProtoFileName(a1: string): null {
    SendCommand({
      id: 'SET_zCModelPrototype-modelProtoFileName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  nodeListChecksum(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-nodeListChecksum-unsigned long-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_nodeListChecksum(a1: number): null {
    SendCommand({
      id: 'SET_zCModelPrototype-nodeListChecksum-unsigned long-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  hierarchySourceASC(): string | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-hierarchySourceASC-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_hierarchySourceASC(a1: string): null {
    SendCommand({
      id: 'SET_zCModelPrototype-hierarchySourceASC-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  bbox3D(): zTBBox3D | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-bbox3D-zTBBox3D-false-false',
      parentId: this.Uuid,
    })

    return result ? new zTBBox3D(result) : null
  }

  set_bbox3D(a1: zTBBox3D): null {
    SendCommand({
      id: 'SET_zCModelPrototype-bbox3D-zTBBox3D-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  bbox3DCollDet(): zTBBox3D | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-bbox3DCollDet-zTBBox3D-false-false',
      parentId: this.Uuid,
    })

    return result ? new zTBBox3D(result) : null
  }

  set_bbox3DCollDet(a1: zTBBox3D): null {
    SendCommand({
      id: 'SET_zCModelPrototype-bbox3DCollDet-zTBBox3D-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  baseModelProto(): zCModelPrototype | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-baseModelProto-zCModelPrototype*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCModelPrototype(result) : null
  }

  set_baseModelProto(a1: zCModelPrototype): null {
    SendCommand({
      id: 'SET_zCModelPrototype-baseModelProto-zCModelPrototype*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  rootNodeTrans(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-rootNodeTrans-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_rootNodeTrans(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCModelPrototype-rootNodeTrans-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fileSourceType(): zTFileSourceType | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-fileSourceType-zTFileSourceType-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fileSourceType(a1: zTFileSourceType): null {
    SendCommand({
      id: 'SET_zCModelPrototype-fileSourceType-zTFileSourceType-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCModelPrototype_OnInit(): zCModelPrototype | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-zCModelPrototype_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCModelPrototype(result) : null
  }

  Init(): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-Init-void-false-',
      parentId: this.Uuid,
    })
  }

  Clear(): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-Clear-void-false-',
      parentId: this.Uuid,
    })
  }

  ReleaseMeshSoftSkinList(): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-ReleaseMeshSoftSkinList-void-false-',
      parentId: this.Uuid,
    })
  }

  ReleaseMeshes(): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-ReleaseMeshes-void-false-',
      parentId: this.Uuid,
    })
  }

  Release(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-Release-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetModelProtoFileName(): string | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-GetModelProtoFileName-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  SetModelProtoName(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-SetModelProtoName-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetFileSourceType(a1: zTFileSourceType): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-SetFileSourceType-void-false-zTFileSourceType',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  PrepareAsModelProtoOverlay(a1: zCModelPrototype): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-PrepareAsModelProtoOverlay-int-false-zCModelPrototype*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  CalcNodeListChecksum(): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-CalcNodeListChecksum-void-false-',
      parentId: this.Uuid,
    })
  }

  SearchNode(a1: string): zCModelNode | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-SearchNode-zCModelNode*-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new zCModelNode(result) : null
  }

  FindNodeListIndex(a1: string): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-FindNodeListIndex-int-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  AddAni(a1: zCModelAni): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-AddAni-void-false-zCModelAni*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SearchAniIndex(a1: string): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-SearchAniIndex-int-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  SearchAni(a1: string): zCModelAni | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-SearchAni-zCModelAni*-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new zCModelAni(result) : null
  }

  LoadMDH(a1: string): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-LoadMDH-int-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  SaveMDH(): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-SaveMDH-void-false-',
      parentId: this.Uuid,
    })
  }

  ConvertVec3(a1: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-ConvertVec3-void-false-zVEC3&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  ConvertAngle(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-ConvertAngle-void-false-float&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SkipBlock(): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-SkipBlock-void-false-',
      parentId: this.Uuid,
    })
  }

  SkipBlockCmt(): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-SkipBlockCmt-void-false-',
      parentId: this.Uuid,
    })
  }

  ReadComment(): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-ReadComment-void-false-',
      parentId: this.Uuid,
    })
  }

  ReadScene(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-ReadScene-void-false-float&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  ReadTriple(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-ReadTriple-zVEC3-false-',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  ReadSoftSkinVertList(): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-ReadSoftSkinVertList-void-false-',
      parentId: this.Uuid,
    })
  }

  ReadMeshAnimation(a1: zCModelNode, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-ReadMeshAnimation-void-false-zCModelNode*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  ReadMaterialList(): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-ReadMaterialList-void-false-',
      parentId: this.Uuid,
    })
  }

  CalcAniBBox(a1: zCModelAni): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-CalcAniBBox-void-false-zCModelAni*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  ResolveReferences(): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-ResolveReferences-void-false-',
      parentId: this.Uuid,
    })
  }

  SkipBlockMDS(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-SkipBlockMDS-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  LoadModelScriptMSB(a1: string): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-LoadModelScriptMSB-int-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  LoadModelScript(a1: string): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-LoadModelScript-int-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  LoadModelASC(a1: string): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelPrototype-LoadModelASC-int-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }
}
