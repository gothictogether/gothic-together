import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { zCVob } from './index.js'
import { zCAIBase } from './index.js'

export class oCAICamera extends zCAIBase {
  static oCAICamera_OnInit(): oCAICamera | null {
    const result: string | null = SendCommand({
      id: 'oCAICamera-oCAICamera_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCAICamera(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCAICamera-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override DoAI(a1: zCVob, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCAICamera-DoAI-void-false-zCVob*,int&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override HasAIDetectedCollision(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAICamera-HasAIDetectedCollision-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }
}
