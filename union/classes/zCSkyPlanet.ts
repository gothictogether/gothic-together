import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zVEC3 } from './index.js'

export class zCSkyPlanet extends BaseUnionObject {
  size(): number | null {
    const result: string | null = SendCommand({
      id: 'zCSkyPlanet-size-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_size(a1: number): null {
    SendCommand({
      id: 'SET_zCSkyPlanet-size-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pos(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCSkyPlanet-pos-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_pos(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCSkyPlanet-pos-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  rotAxis(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCSkyPlanet-rotAxis-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_rotAxis(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCSkyPlanet-rotAxis-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCSkyPlanet_OnInit(): zCSkyPlanet | null {
    const result: string | null = SendCommand({
      id: 'zCSkyPlanet-zCSkyPlanet_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCSkyPlanet(result) : null
  }
}
