import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { zCCSProps } from './index.js'

export class oCCSProps extends zCCSProps {
  resultReaction(): number | null {
    const result: string | null = SendCommand({
      id: 'oCCSProps-resultReaction-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_resultReaction(a1: number): null {
    SendCommand({
      id: 'SET_oCCSProps-resultReaction-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCCSProps_OnInit(): oCCSProps | null {
    const result: string | null = SendCommand({
      id: 'oCCSProps-oCCSProps_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCCSProps(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCCSProps-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  CheckRoleResult(): number | null {
    const result: string | null = SendCommand({
      id: 'oCCSProps-CheckRoleResult-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }
}
