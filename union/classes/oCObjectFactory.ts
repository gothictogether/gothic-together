import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { zCEventManager } from './index.js'
import { zCVob } from './index.js'
import { zCCSManager } from './index.js'
import { zCWorld } from './index.js'
import { zCWaypoint } from './index.js'
import { zCWay } from './index.js'
import { oCNpc } from './index.js'
import { oCItem } from './index.js'
import { zCObjectFactory } from './index.js'

export class oCObjectFactory extends zCObjectFactory {
  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCObjectFactory-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override CreateEventManager(a1: zCVob): zCEventManager | null {
    const result: string | null = SendCommand({
      id: 'oCObjectFactory-CreateEventManager-zCEventManager*-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new zCEventManager(result) : null
  }

  override CreateCSManager(): zCCSManager | null {
    const result: string | null = SendCommand({
      id: 'oCObjectFactory-CreateCSManager-zCCSManager*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCCSManager(result) : null
  }

  override CreateWorld(): zCWorld | null {
    const result: string | null = SendCommand({
      id: 'oCObjectFactory-CreateWorld-zCWorld*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCWorld(result) : null
  }

  override CreateWaypoint(): zCWaypoint | null {
    const result: string | null = SendCommand({
      id: 'oCObjectFactory-CreateWaypoint-zCWaypoint*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCWaypoint(result) : null
  }

  override CreateWay(): zCWay | null {
    const result: string | null = SendCommand({
      id: 'oCObjectFactory-CreateWay-zCWay*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCWay(result) : null
  }

  CreateNpc(a1: number): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCObjectFactory-CreateNpc-oCNpc*-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new oCNpc(result) : null
  }

  CreateItem(a1: number): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCObjectFactory-CreateItem-oCItem*-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new oCItem(result) : null
  }
}
