import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'

export class zTScreenFXSet extends BaseUnionObject {
  weight(): number | null {
    const result: string | null = SendCommand({
      id: 'zTScreenFXSet-weight-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_weight(a1: number): null {
    SendCommand({
      id: 'SET_zTScreenFXSet-weight-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  velo(): number | null {
    const result: string | null = SendCommand({
      id: 'zTScreenFXSet-velo-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_velo(a1: number): null {
    SendCommand({
      id: 'SET_zTScreenFXSet-velo-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fovDeg(): number | null {
    const result: string | null = SendCommand({
      id: 'zTScreenFXSet-fovDeg-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fovDeg(a1: number): null {
    SendCommand({
      id: 'SET_zTScreenFXSet-fovDeg-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  visible(): number | null {
    const result: string | null = SendCommand({
      id: 'zTScreenFXSet-visible-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_visible(a1: number): null {
    SendCommand({
      id: 'SET_zTScreenFXSet-visible-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  texName(): string | null {
    const result: string | null = SendCommand({
      id: 'zTScreenFXSet-texName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_texName(a1: string): null {
    SendCommand({
      id: 'SET_zTScreenFXSet-texName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  texAniFPS(): number | null {
    const result: string | null = SendCommand({
      id: 'zTScreenFXSet-texAniFPS-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_texAniFPS(a1: number): null {
    SendCommand({
      id: 'SET_zTScreenFXSet-texAniFPS-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
