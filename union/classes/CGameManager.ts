import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { oCGame } from './index.js'

export class CGameManager extends BaseUnionObject {
  gameSession(): oCGame | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-gameSession-oCGame*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCGame(result) : null
  }

  set_gameSession(a1: oCGame): null {
    SendCommand({
      id: 'SET_CGameManager-gameSession-oCGame*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  backLoop(): oCGame | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-backLoop-oCGame*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCGame(result) : null
  }

  set_backLoop(a1: oCGame): null {
    SendCommand({
      id: 'SET_CGameManager-backLoop-oCGame*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  exitGame(): number | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-exitGame-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_exitGame(a1: number): null {
    SendCommand({
      id: 'SET_CGameManager-exitGame-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  exitSession(): number | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-exitSession-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_exitSession(a1: number): null {
    SendCommand({
      id: 'SET_CGameManager-exitSession-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  gameIdle(): number | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-gameIdle-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_gameIdle(a1: number): null {
    SendCommand({
      id: 'SET_CGameManager-gameIdle-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lastWorldWasGame(): number | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-lastWorldWasGame-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_lastWorldWasGame(a1: number): null {
    SendCommand({
      id: 'SET_CGameManager-lastWorldWasGame-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  backWorldRunning(): string | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-backWorldRunning-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_backWorldRunning(a1: string): null {
    SendCommand({
      id: 'SET_CGameManager-backWorldRunning-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  backDatFileRunning(): string | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-backDatFileRunning-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_backDatFileRunning(a1: string): null {
    SendCommand({
      id: 'SET_CGameManager-backDatFileRunning-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  introActive(): number | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-introActive-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_introActive(a1: number): null {
    SendCommand({
      id: 'SET_CGameManager-introActive-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  dontStartGame(): number | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-dontStartGame-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_dontStartGame(a1: number): null {
    SendCommand({
      id: 'SET_CGameManager-dontStartGame-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  videoPlayInGame(): number | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-videoPlayInGame-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_videoPlayInGame(a1: number): null {
    SendCommand({
      id: 'SET_CGameManager-videoPlayInGame-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  playTime(): number | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-playTime-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_playTime(a1: number): null {
    SendCommand({
      id: 'SET_CGameManager-playTime-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static CGameManager_OnInit(): CGameManager | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-CGameManager_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new CGameManager(result) : null
  }

  Tool_ConvertData(): void | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-Tool_ConvertData-void-false-',
      parentId: this.Uuid,
    })
  }

  Done(): void | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-Done-void-false-',
      parentId: this.Uuid,
    })
  }

  ExitGame(): number | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-ExitGame-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  ExitSession(): number | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-ExitSession-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  Run(): void | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-Run-void-false-',
      parentId: this.Uuid,
    })
  }

  InitScreen_Open(): void | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-InitScreen_Open-void-false-',
      parentId: this.Uuid,
    })
  }

  InitScreen_Menu(): void | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-InitScreen_Menu-void-false-',
      parentId: this.Uuid,
    })
  }

  InitScreen_Close(): void | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-InitScreen_Close-void-false-',
      parentId: this.Uuid,
    })
  }

  InitSettings(): void | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-InitSettings-void-false-',
      parentId: this.Uuid,
    })
  }

  ShowSplashScreen(): void | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-ShowSplashScreen-void-false-',
      parentId: this.Uuid,
    })
  }

  RemoveSplashScreen(): void | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-RemoveSplashScreen-void-false-',
      parentId: this.Uuid,
    })
  }

  GameInit(): void | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-GameInit-void-false-',
      parentId: this.Uuid,
    })
  }

  GameDone(): void | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-GameDone-void-false-',
      parentId: this.Uuid,
    })
  }

  GameSessionReset(): void | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-GameSessionReset-void-false-',
      parentId: this.Uuid,
    })
  }

  GameSessionInit(): void | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-GameSessionInit-void-false-',
      parentId: this.Uuid,
    })
  }

  GameSessionDone(): void | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-GameSessionDone-void-false-',
      parentId: this.Uuid,
    })
  }

  ShowIntro(): void | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-ShowIntro-void-false-',
      parentId: this.Uuid,
    })
  }

  ShowExtro(): void | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-ShowExtro-void-false-',
      parentId: this.Uuid,
    })
  }

  PreMenu(): void | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-PreMenu-void-false-',
      parentId: this.Uuid,
    })
  }

  PostMenu(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-PostMenu-void-false-zSTRING',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  ApplySomeSettings(): void | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-ApplySomeSettings-void-false-',
      parentId: this.Uuid,
    })
  }

  GetPlaytimeSeconds(): number | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-GetPlaytimeSeconds-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  Menu(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-Menu-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  Read_Savegame(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-Read_Savegame-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  Write_Savegame(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-Write_Savegame-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  HandleCancelKey(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-HandleCancelKey-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  MenuEnabled(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-MenuEnabled-int-false-int&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetGame(): oCGame | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-GetGame-oCGame*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCGame(result) : null
  }

  IsIntroActive(): number | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-IsIntroActive-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  InsertMenuWorld(a1: string, a2: string): void | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-InsertMenuWorld-void-false-zSTRING&,zSTRING&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  RemoveMenuWorld(): void | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-RemoveMenuWorld-void-false-',
      parentId: this.Uuid,
    })
  }

  IsGameRunning(): number | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-IsGameRunning-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IntroduceChapter(): number | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-IntroduceChapter-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IntroduceChapter2(a1: string, a2: string, a3: string, a4: string, a5: number): number | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-IntroduceChapter-int-false-zSTRING,zSTRING,zSTRING,zSTRING,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
      a5: a5.toString(),
    })

    return result ? Number(result) : null
  }

  PlayVideo(a1: string): number | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-PlayVideo-int-false-zSTRING',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  PlayVideoEx(a1: string, a2: number, a3: number): number | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-PlayVideoEx-int-false-zSTRING,short,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? Number(result) : null
  }

  ShowRealPlayTime(): void | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-ShowRealPlayTime-void-false-',
      parentId: this.Uuid,
    })
  }

  HandleEvent(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'CGameManager-HandleEvent-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }
}
