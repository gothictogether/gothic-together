import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { zCVob } from './index.js'
import { zTTimeBehavior } from './index.js'
import { zCObject } from './index.js'

export class zCEventMessage extends zCObject {
  inCutscene(): number | null {
    const result: string | null = SendCommand({
      id: 'zCEventMessage-inCutscene-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_inCutscene(a1: number): null {
    SendCommand({
      id: 'SET_zCEventMessage-inCutscene-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCEventMessage_OnInit(): zCEventMessage | null {
    const result: string | null = SendCommand({
      id: 'zCEventMessage-zCEventMessage_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCEventMessage(result) : null
  }

  static zCEventMessage_OnInit2(a1: number): zCEventMessage | null {
    const result: string | null = SendCommand({
      id: 'zCEventMessage-zCEventMessage_OnInit-void-false-unsigned short',
      parentId: 'NULL',
      a1: a1.toString(),
    })

    return result ? new zCEventMessage(result) : null
  }

  GetMessageID(): number | null {
    const result: string | null = SendCommand({
      id: 'zCEventMessage-GetMessageID-unsigned long-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetSubType(): number | null {
    const result: string | null = SendCommand({
      id: 'zCEventMessage-GetSubType-unsigned short-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCEventMessage-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  IsOverlay(): number | null {
    const result: string | null = SendCommand({
      id: 'zCEventMessage-IsOverlay-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsNetRelevant(): number | null {
    const result: string | null = SendCommand({
      id: 'zCEventMessage-IsNetRelevant-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsHighPriority(): number | null {
    const result: string | null = SendCommand({
      id: 'zCEventMessage-IsHighPriority-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsJob(): number | null {
    const result: string | null = SendCommand({
      id: 'zCEventMessage-IsJob-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetIgnoreCutsceneMode(): number | null {
    const result: string | null = SendCommand({
      id: 'zCEventMessage-GetIgnoreCutsceneMode-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  Delete(): void | null {
    const result: string | null = SendCommand({
      id: 'zCEventMessage-Delete-void-false-',
      parentId: this.Uuid,
    })
  }

  IsDeleteable(): number | null {
    const result: string | null = SendCommand({
      id: 'zCEventMessage-IsDeleteable-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsDeleted(): number | null {
    const result: string | null = SendCommand({
      id: 'zCEventMessage-IsDeleted-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetCutsceneMode(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCEventMessage-SetCutsceneMode-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetCutsceneMode(): number | null {
    const result: string | null = SendCommand({
      id: 'zCEventMessage-GetCutsceneMode-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  MD_GetNumOfSubTypes(): number | null {
    const result: string | null = SendCommand({
      id: 'zCEventMessage-MD_GetNumOfSubTypes-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  MD_GetSubTypeString(a1: number): string | null {
    const result: string | null = SendCommand({
      id: 'zCEventMessage-MD_GetSubTypeString-zSTRING-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? String(result) : null
  }

  MD_GetVobRefName(): string | null {
    const result: string | null = SendCommand({
      id: 'zCEventMessage-MD_GetVobRefName-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  MD_SetVobRefName(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'zCEventMessage-MD_SetVobRefName-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  MD_SetVobParam(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCEventMessage-MD_SetVobParam-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  MD_GetTimeBehavior(): zTTimeBehavior | null {
    const result: string | null = SendCommand({
      id: 'zCEventMessage-MD_GetTimeBehavior-zTTimeBehavior-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  MD_GetMinTime(): number | null {
    const result: string | null = SendCommand({
      id: 'zCEventMessage-MD_GetMinTime-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }
}
