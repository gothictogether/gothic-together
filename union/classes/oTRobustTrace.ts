import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zVEC3 } from './index.js'
import { zCVob } from './index.js'

export class oTRobustTrace extends BaseUnionObject {
  stand(): number | null {
    const result: string | null = SendCommand({
      id: 'oTRobustTrace-stand-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_stand(a1: number): null {
    SendCommand({
      id: 'SET_oTRobustTrace-stand-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  dirChoosed(): number | null {
    const result: string | null = SendCommand({
      id: 'oTRobustTrace-dirChoosed-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_dirChoosed(a1: number): null {
    SendCommand({
      id: 'SET_oTRobustTrace-dirChoosed-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  exactPosition(): number | null {
    const result: string | null = SendCommand({
      id: 'oTRobustTrace-exactPosition-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_exactPosition(a1: number): null {
    SendCommand({
      id: 'SET_oTRobustTrace-exactPosition-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  targetReached(): number | null {
    const result: string | null = SendCommand({
      id: 'oTRobustTrace-targetReached-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_targetReached(a1: number): null {
    SendCommand({
      id: 'SET_oTRobustTrace-targetReached-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  standIfTargetReached(): number | null {
    const result: string | null = SendCommand({
      id: 'oTRobustTrace-standIfTargetReached-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_standIfTargetReached(a1: number): null {
    SendCommand({
      id: 'SET_oTRobustTrace-standIfTargetReached-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  waiting(): number | null {
    const result: string | null = SendCommand({
      id: 'oTRobustTrace-waiting-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_waiting(a1: number): null {
    SendCommand({
      id: 'SET_oTRobustTrace-waiting-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  isObstVobSmall(): number | null {
    const result: string | null = SendCommand({
      id: 'oTRobustTrace-isObstVobSmall-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_isObstVobSmall(a1: number): null {
    SendCommand({
      id: 'SET_oTRobustTrace-isObstVobSmall-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  targetVisible(): number | null {
    const result: string | null = SendCommand({
      id: 'oTRobustTrace-targetVisible-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_targetVisible(a1: number): null {
    SendCommand({
      id: 'SET_oTRobustTrace-targetVisible-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  useChasmChecks(): number | null {
    const result: string | null = SendCommand({
      id: 'oTRobustTrace-useChasmChecks-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_useChasmChecks(a1: number): null {
    SendCommand({
      id: 'SET_oTRobustTrace-useChasmChecks-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  targetPos(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oTRobustTrace-targetPos-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_targetPos(a1: zVEC3): null {
    SendCommand({
      id: 'SET_oTRobustTrace-targetPos-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  targetVob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oTRobustTrace-targetVob-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_targetVob(a1: zCVob): null {
    SendCommand({
      id: 'SET_oTRobustTrace-targetVob-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  obstVob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oTRobustTrace-obstVob-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_obstVob(a1: zCVob): null {
    SendCommand({
      id: 'SET_oTRobustTrace-obstVob-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  targetDist(): number | null {
    const result: string | null = SendCommand({
      id: 'oTRobustTrace-targetDist-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_targetDist(a1: number): null {
    SendCommand({
      id: 'SET_oTRobustTrace-targetDist-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lastTargetDist(): number | null {
    const result: string | null = SendCommand({
      id: 'oTRobustTrace-lastTargetDist-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_lastTargetDist(a1: number): null {
    SendCommand({
      id: 'SET_oTRobustTrace-lastTargetDist-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  maxTargetDist(): number | null {
    const result: string | null = SendCommand({
      id: 'oTRobustTrace-maxTargetDist-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_maxTargetDist(a1: number): null {
    SendCommand({
      id: 'SET_oTRobustTrace-maxTargetDist-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  dirTurn(): number | null {
    const result: string | null = SendCommand({
      id: 'oTRobustTrace-dirTurn-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_dirTurn(a1: number): null {
    SendCommand({
      id: 'SET_oTRobustTrace-dirTurn-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  timer(): number | null {
    const result: string | null = SendCommand({
      id: 'oTRobustTrace-timer-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_timer(a1: number): null {
    SendCommand({
      id: 'SET_oTRobustTrace-timer-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  dirFirst(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oTRobustTrace-dirFirst-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_dirFirst(a1: zVEC3): null {
    SendCommand({
      id: 'SET_oTRobustTrace-dirFirst-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  dirLastAngle(): number | null {
    const result: string | null = SendCommand({
      id: 'oTRobustTrace-dirLastAngle-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_dirLastAngle(a1: number): null {
    SendCommand({
      id: 'SET_oTRobustTrace-dirLastAngle-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  frameCtr(): number | null {
    const result: string | null = SendCommand({
      id: 'oTRobustTrace-frameCtr-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_frameCtr(a1: number): null {
    SendCommand({
      id: 'SET_oTRobustTrace-frameCtr-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  targetPosCounter(): number | null {
    const result: string | null = SendCommand({
      id: 'oTRobustTrace-targetPosCounter-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_targetPosCounter(a1: number): null {
    SendCommand({
      id: 'SET_oTRobustTrace-targetPosCounter-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  targetPosIndex(): number | null {
    const result: string | null = SendCommand({
      id: 'oTRobustTrace-targetPosIndex-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_targetPosIndex(a1: number): null {
    SendCommand({
      id: 'SET_oTRobustTrace-targetPosIndex-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  checkVisibilityTime(): number | null {
    const result: string | null = SendCommand({
      id: 'oTRobustTrace-checkVisibilityTime-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_checkVisibilityTime(a1: number): null {
    SendCommand({
      id: 'SET_oTRobustTrace-checkVisibilityTime-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  positionUpdateTime(): number | null {
    const result: string | null = SendCommand({
      id: 'oTRobustTrace-positionUpdateTime-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_positionUpdateTime(a1: number): null {
    SendCommand({
      id: 'SET_oTRobustTrace-positionUpdateTime-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  failurePossibility(): number | null {
    const result: string | null = SendCommand({
      id: 'oTRobustTrace-failurePossibility-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_failurePossibility(a1: number): null {
    SendCommand({
      id: 'SET_oTRobustTrace-failurePossibility-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
