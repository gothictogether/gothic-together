import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { Tpd } from './index.js'

export class oCInfo extends BaseUnionObject {
  next(): oCInfo | null {
    const result: string | null = SendCommand({
      id: 'oCInfo-next-oCInfo*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCInfo(result) : null
  }

  set_next(a1: oCInfo): null {
    SendCommand({
      id: 'SET_oCInfo-next-oCInfo*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  name(): string | null {
    const result: string | null = SendCommand({
      id: 'oCInfo-name-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_name(a1: string): null {
    SendCommand({
      id: 'SET_oCInfo-name-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pd(): Tpd | null {
    const result: string | null = SendCommand({
      id: 'oCInfo-pd-Tpd-false-false',
      parentId: this.Uuid,
    })

    return result ? new Tpd(result) : null
  }

  set_pd(a1: Tpd): null {
    SendCommand({
      id: 'SET_oCInfo-pd-Tpd-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  told(): number | null {
    const result: string | null = SendCommand({
      id: 'oCInfo-told-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_told(a1: number): null {
    SendCommand({
      id: 'SET_oCInfo-told-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  instance(): number | null {
    const result: string | null = SendCommand({
      id: 'oCInfo-instance-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_instance(a1: number): null {
    SendCommand({
      id: 'SET_oCInfo-instance-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCInfo_OnInit(): oCInfo | null {
    const result: string | null = SendCommand({
      id: 'oCInfo-oCInfo_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCInfo(result) : null
  }

  SetInstance(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCInfo-SetInstance-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetInstance(): number | null {
    const result: string | null = SendCommand({
      id: 'oCInfo-GetInstance-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  DoCheck(): number | null {
    const result: string | null = SendCommand({
      id: 'oCInfo-DoCheck-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetNpcID(): number | null {
    const result: string | null = SendCommand({
      id: 'oCInfo-GetNpcID-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetConditionFunc(): number | null {
    const result: string | null = SendCommand({
      id: 'oCInfo-GetConditionFunc-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  WasTold(): number | null {
    const result: string | null = SendCommand({
      id: 'oCInfo-WasTold-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetTold(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCInfo-SetTold-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetDataSize(): number | null {
    const result: string | null = SendCommand({
      id: 'oCInfo-GetDataSize-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetText(): string | null {
    const result: string | null = SendCommand({
      id: 'oCInfo-GetText-zSTRING&-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  InfoConditions(): number | null {
    const result: string | null = SendCommand({
      id: 'oCInfo-InfoConditions-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  Info(): void | null {
    const result: string | null = SendCommand({
      id: 'oCInfo-Info-void-false-',
      parentId: this.Uuid,
    })
  }

  AddChoice(a1: string, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCInfo-AddChoice-void-false-zSTRING,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  RemoveChoice(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCInfo-RemoveChoice-void-false-zSTRING',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RemoveAllChoices(): void | null {
    const result: string | null = SendCommand({
      id: 'oCInfo-RemoveAllChoices-void-false-',
      parentId: this.Uuid,
    })
  }

  RestoreParserInstance(): void | null {
    const result: string | null = SendCommand({
      id: 'oCInfo-RestoreParserInstance-void-false-',
      parentId: this.Uuid,
    })
  }
}
