import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zVEC3 } from './index.js'

export class zCVertex extends BaseUnionObject {
  position(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCVertex-position-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_position(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCVertex-position-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  transformedIndex(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVertex-transformedIndex-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_transformedIndex(a1: number): null {
    SendCommand({
      id: 'SET_zCVertex-transformedIndex-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  myIndex(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVertex-myIndex-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_myIndex(a1: number): null {
    SendCommand({
      id: 'SET_zCVertex-myIndex-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCVertex_OnInit(): zCVertex | null {
    const result: string | null = SendCommand({
      id: 'zCVertex-zCVertex_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCVertex(result) : null
  }

  ResetVertexTransform(): void | null {
    const result: string | null = SendCommand({
      id: 'zCVertex-ResetVertexTransform-void-false-',
      parentId: this.Uuid,
    })
  }
}
