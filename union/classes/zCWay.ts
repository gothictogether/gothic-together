import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCWaypoint } from './index.js'
import { zVEC3 } from './index.js'
import { zCCamera } from './index.js'
import { zCWorld } from './index.js'
import { zCVob } from './index.js'

export class zCWay extends BaseUnionObject {
  cost(): number | null {
    const result: string | null = SendCommand({
      id: 'zCWay-cost-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_cost(a1: number): null {
    SendCommand({
      id: 'SET_zCWay-cost-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  usedCtr(): number | null {
    const result: string | null = SendCommand({
      id: 'zCWay-usedCtr-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_usedCtr(a1: number): null {
    SendCommand({
      id: 'SET_zCWay-usedCtr-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  chasmDepth(): number | null {
    const result: string | null = SendCommand({
      id: 'zCWay-chasmDepth-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_chasmDepth(a1: number): null {
    SendCommand({
      id: 'SET_zCWay-chasmDepth-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  chasm(): number | null {
    const result: string | null = SendCommand({
      id: 'zCWay-chasm-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_chasm(a1: number): null {
    SendCommand({
      id: 'SET_zCWay-chasm-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  jump(): number | null {
    const result: string | null = SendCommand({
      id: 'zCWay-jump-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_jump(a1: number): null {
    SendCommand({
      id: 'SET_zCWay-jump-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  left(): zCWaypoint | null {
    const result: string | null = SendCommand({
      id: 'zCWay-left-zCWaypoint*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCWaypoint(result) : null
  }

  set_left(a1: zCWaypoint): null {
    SendCommand({
      id: 'SET_zCWay-left-zCWaypoint*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  right(): zCWaypoint | null {
    const result: string | null = SendCommand({
      id: 'zCWay-right-zCWaypoint*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCWaypoint(result) : null
  }

  set_right(a1: zCWaypoint): null {
    SendCommand({
      id: 'SET_zCWay-right-zCWaypoint*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCWay_OnInit(): zCWay | null {
    const result: string | null = SendCommand({
      id: 'zCWay-zCWay_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCWay(result) : null
  }

  static zCWay_OnInit2(a1: zCWaypoint, a2: zCWaypoint): zCWay | null {
    const result: string | null = SendCommand({
      id: 'zCWay-zCWay_OnInit-void-false-zCWaypoint*,zCWaypoint*',
      parentId: 'NULL',
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new zCWay(result) : null
  }

  Free(): void | null {
    const result: string | null = SendCommand({
      id: 'zCWay-Free-void-false-',
      parentId: this.Uuid,
    })
  }

  GetGoalWaypoint(a1: zCWaypoint): zCWaypoint | null {
    const result: string | null = SendCommand({
      id: 'zCWay-GetGoalWaypoint-zCWaypoint*-false-zCWaypoint*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new zCWaypoint(result) : null
  }

  EstimateCost(): void | null {
    const result: string | null = SendCommand({
      id: 'zCWay-EstimateCost-void-false-',
      parentId: this.Uuid,
    })
  }

  GetLength(): number | null {
    const result: string | null = SendCommand({
      id: 'zCWay-GetLength-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetJumpDistance(): number | null {
    const result: string | null = SendCommand({
      id: 'zCWay-GetJumpDistance-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetDistancePos(a1: number, a2: zCWaypoint): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCWay-GetDistancePos-zVEC3-false-float,zCWaypoint*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new zVEC3(result) : null
  }

  Draw(a1: zCCamera): void | null {
    const result: string | null = SendCommand({
      id: 'zCWay-Draw-void-false-zCCamera*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetFloor(a1: zCWorld, a2: zVEC3): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCWay-GetFloor-zVEC3-false-zCWorld*,zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new zVEC3(result) : null
  }

  Init(a1: zCWaypoint, a2: zCWaypoint): void | null {
    const result: string | null = SendCommand({
      id: 'zCWay-Init-void-false-zCWaypoint*,zCWaypoint*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  CalcProperties(a1: zCWorld): void | null {
    const result: string | null = SendCommand({
      id: 'zCWay-CalcProperties-void-false-zCWorld*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetChasm(): number | null {
    const result: string | null = SendCommand({
      id: 'zCWay-GetChasm-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  CanJump(): number | null {
    const result: string | null = SendCommand({
      id: 'zCWay-CanJump-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  CanBeUsed(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'zCWay-CanBeUsed-int-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  IsObjectOnWay(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'zCWay-IsObjectOnWay-int-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }
}
