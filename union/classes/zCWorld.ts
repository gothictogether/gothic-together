import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zTTraceRayReport } from './index.js'
import { zCSession } from './index.js'
import { zCCSPlayer } from './index.js'
import { zTWld_RenderMode } from './index.js'
import { zCWayNet } from './index.js'
import { zCZone } from './index.js'
import { zCCamera } from './index.js'
import { zVEC3 } from './index.js'
import { zCVob } from './index.js'
import { zCClassDef } from './index.js'
import { zCPolygon } from './index.js'
import { zCPatchMap } from './index.js'
import { zTBBox3D } from './index.js'
import { zTWorldLoadMode } from './index.js'
import { zTWorldSaveMode } from './index.js'
import { zTWorldLoadMergeMode } from './index.js'
import { zCObject } from './index.js'

export class zCWorld extends zCObject {
  traceRayReport(): zTTraceRayReport | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-traceRayReport-zTTraceRayReport-false-false',
      parentId: this.Uuid,
    })

    return result ? new zTTraceRayReport(result) : null
  }

  set_traceRayReport(a1: zTTraceRayReport): null {
    SendCommand({
      id: 'SET_zCWorld-traceRayReport-zTTraceRayReport-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  ownerSession(): zCSession | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-ownerSession-zCSession*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCSession(result) : null
  }

  set_ownerSession(a1: zCSession): null {
    SendCommand({
      id: 'SET_zCWorld-ownerSession-zCSession*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  csPlayer(): zCCSPlayer | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-csPlayer-zCCSPlayer*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCCSPlayer(result) : null
  }

  set_csPlayer(a1: zCCSPlayer): null {
    SendCommand({
      id: 'SET_zCWorld-csPlayer-zCCSPlayer*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_strlevelName(): string | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-m_strlevelName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_m_strlevelName(a1: string): null {
    SendCommand({
      id: 'SET_zCWorld-m_strlevelName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  compiled(): number | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-compiled-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_compiled(a1: number): null {
    SendCommand({
      id: 'SET_zCWorld-compiled-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  compiledEditorMode(): number | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-compiledEditorMode-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_compiledEditorMode(a1: number): null {
    SendCommand({
      id: 'SET_zCWorld-compiledEditorMode-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  traceRayIgnoreVobFlag(): number | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-traceRayIgnoreVobFlag-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_traceRayIgnoreVobFlag(a1: number): null {
    SendCommand({
      id: 'SET_zCWorld-traceRayIgnoreVobFlag-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_bIsInventoryWorld(): number | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-m_bIsInventoryWorld-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_bIsInventoryWorld(a1: number): null {
    SendCommand({
      id: 'SET_zCWorld-m_bIsInventoryWorld-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  worldRenderMode(): zTWld_RenderMode | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-worldRenderMode-zTWld_RenderMode-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_worldRenderMode(a1: zTWld_RenderMode): null {
    SendCommand({
      id: 'SET_zCWorld-worldRenderMode-zTWld_RenderMode-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  wayNet(): zCWayNet | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-wayNet-zCWayNet*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCWayNet(result) : null
  }

  set_wayNet(a1: zCWayNet): null {
    SendCommand({
      id: 'SET_zCWorld-wayNet-zCWayNet*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  masterFrameCtr(): number | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-masterFrameCtr-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_masterFrameCtr(a1: number): null {
    SendCommand({
      id: 'SET_zCWorld-masterFrameCtr-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  vobFarClipZ(): number | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-vobFarClipZ-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_vobFarClipZ(a1: number): null {
    SendCommand({
      id: 'SET_zCWorld-vobFarClipZ-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  vobFarClipZScalability(): number | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-vobFarClipZScalability-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_vobFarClipZScalability(a1: number): null {
    SendCommand({
      id: 'SET_zCWorld-vobFarClipZScalability-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  renderingFirstTime(): number | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-renderingFirstTime-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_renderingFirstTime(a1: number): null {
    SendCommand({
      id: 'SET_zCWorld-renderingFirstTime-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  showWaynet(): number | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-showWaynet-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_showWaynet(a1: number): null {
    SendCommand({
      id: 'SET_zCWorld-showWaynet-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  showTraceRayLines(): number | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-showTraceRayLines-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_showTraceRayLines(a1: number): null {
    SendCommand({
      id: 'SET_zCWorld-showTraceRayLines-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  unarchiveFileLen(): number | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-unarchiveFileLen-unsigned long-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_unarchiveFileLen(a1: number): null {
    SendCommand({
      id: 'SET_zCWorld-unarchiveFileLen-unsigned long-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  unarchiveStartPosVobtree(): number | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-unarchiveStartPosVobtree-unsigned long-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_unarchiveStartPosVobtree(a1: number): null {
    SendCommand({
      id: 'SET_zCWorld-unarchiveStartPosVobtree-unsigned long-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  numVobsInWorld(): number | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-numVobsInWorld-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_numVobsInWorld(a1: number): null {
    SendCommand({
      id: 'SET_zCWorld-numVobsInWorld-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  addZonesToWorld(): number | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-addZonesToWorld-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_addZonesToWorld(a1: number): null {
    SendCommand({
      id: 'SET_zCWorld-addZonesToWorld-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  showZonesDebugInfo(): number | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-showZonesDebugInfo-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_showZonesDebugInfo(a1: number): null {
    SendCommand({
      id: 'SET_zCWorld-showZonesDebugInfo-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCWorld_OnInit(): zCWorld | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-zCWorld_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCWorld(result) : null
  }

  AddZone(a1: zCZone): void | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-AddZone-void-false-zCZone*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RemoveZone(a1: zCZone): void | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-RemoveZone-void-false-zCZone*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RemoveAllZones(): void | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-RemoveAllZones-void-false-',
      parentId: this.Uuid,
    })
  }

  UpdateZone(a1: zCZone): void | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-UpdateZone-void-false-zCZone*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  ProcessZones(): void | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-ProcessZones-void-false-',
      parentId: this.Uuid,
    })
  }

  ShowZonesDebugInfo(): void | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-ShowZonesDebugInfo-void-false-',
      parentId: this.Uuid,
    })
  }

  ShowTextureStats(): void | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-ShowTextureStats-void-false-',
      parentId: this.Uuid,
    })
  }

  Render(a1: zCCamera): void | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-Render-void-false-zCCamera&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  TraceRayFirstHit(a1: zVEC3, a2: zVEC3, a3: zCVob, a4: number): number | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-TraceRayFirstHit-int-false-zVEC3 const&,zVEC3 const&,zCVob*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
    })

    return result ? Number(result) : null
  }

  TraceRayNearestHit(a1: zVEC3, a2: zVEC3, a3: zCVob, a4: number): number | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-TraceRayNearestHit-int-false-zVEC3 const&,zVEC3 const&,zCVob*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
    })

    return result ? Number(result) : null
  }

  PickScene(a1: zCCamera, a2: number, a3: number, a4: number): number | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-PickScene-int-false-zCCamera&,int,int,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
    })

    return result ? Number(result) : null
  }

  SaveBspTreeMesh3DS(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-SaveBspTreeMesh3DS-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SearchZoneDefaultByClass(a1: zCClassDef): zCZone | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-SearchZoneDefaultByClass-zCZone*-false-zCClassDef*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new zCZone(result) : null
  }

  ShouldAddThisVobToBsp(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-ShouldAddThisVobToBsp-int-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  MoveVobSubtreeTo_novt(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-MoveVobSubtreeTo_novt-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  MoveVobSubtreeToWorldSpace(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-MoveVobSubtreeToWorldSpace-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RemoveVobFromLists(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-RemoveVobFromLists-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  PrintStatus(): void | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-PrintStatus-void-false-',
      parentId: this.Uuid,
    })
  }

  PrintActiveVobs(): void | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-PrintActiveVobs-void-false-',
      parentId: this.Uuid,
    })
  }

  MoveVobs(): void | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-MoveVobs-void-false-',
      parentId: this.Uuid,
    })
  }

  AdvanceClock(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-AdvanceClock-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  DebugMarkOccluderPolys(): void | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-DebugMarkOccluderPolys-void-false-',
      parentId: this.Uuid,
    })
  }

  GetVobHashIndex(a1: string): number | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-GetVobHashIndex-unsigned long-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetVobHashIndex2(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-GetVobHashIndex-unsigned long-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  InsertVobHashTable(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-InsertVobHashTable-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RemoveVobHashTable(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-RemoveVobHashTable-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SearchVobHashTable(a1: string): zCVob | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-SearchVobHashTable-zCVob*-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new zCVob(result) : null
  }

  SetOwnerSession(a1: zCSession): void | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-SetOwnerSession-void-false-zCSession*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetOwnerSession(): zCSession | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-GetOwnerSession-zCSession*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCSession(result) : null
  }

  ResetCutscenePlayer(): void | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-ResetCutscenePlayer-void-false-',
      parentId: this.Uuid,
    })
  }

  LightWorldStaticCompiled(): void | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-LightWorldStaticCompiled-void-false-',
      parentId: this.Uuid,
    })
  }

  GenerateStaticVertexLighting(): void | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-GenerateStaticVertexLighting-void-false-',
      parentId: this.Uuid,
    })
  }

  GetPhongNormal(a1: zCPolygon, a2: zVEC3): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-GetPhongNormal-zVEC3-false-zCPolygon*,zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new zVEC3(result) : null
  }

  GenerateLightmapFromPatchMap(a1: zCPatchMap): void | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-GenerateLightmapFromPatchMap-void-false-zCPatchMap*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  LightPatchMap(a1: zCPatchMap): void | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-LightPatchMap-void-false-zCPatchMap*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GenerateSurfaces(a1: number, a2: zTBBox3D): void | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-GenerateSurfaces-void-false-int,zTBBox3D*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  MakeTransfers(): void | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-MakeTransfers-void-false-',
      parentId: this.Uuid,
    })
  }

  GenerateLightmapsRadiosity(a1: zTBBox3D): void | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-GenerateLightmapsRadiosity-void-false-zTBBox3D*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  MakeVobLightingDirty(): void | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-MakeVobLightingDirty-void-false-',
      parentId: this.Uuid,
    })
  }

  SetVobFarClipZScalability(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-SetVobFarClipZScalability-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  LoadWorld(a1: string, a2: zTWorldLoadMode): number | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-LoadWorld-int-false-zSTRING const&,zTWorldLoadMode',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  SaveWorld(a1: string, a2: zTWorldSaveMode, a3: number, a4: number): number | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-SaveWorld-int-false-zSTRING const&,zTWorldSaveMode,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
    })

    return result ? Number(result) : null
  }

  MergeVobSubtree(a1: string, a2: zCVob, a3: zTWorldLoadMergeMode): zCVob | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-MergeVobSubtree-zCVob*-false-zSTRING const&,zCVob*,zTWorldLoadMergeMode',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? new zCVob(result) : null
  }

  SaveVobSubtree(a1: string, a2: zCVob, a3: number, a4: number): number | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-SaveVobSubtree-int-false-zSTRING const&,zCVob*,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
    })

    return result ? Number(result) : null
  }

  DisposeWorld(): void | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-DisposeWorld-void-false-',
      parentId: this.Uuid,
    })
  }

  DisposeStaticWorld(): void | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-DisposeStaticWorld-void-false-',
      parentId: this.Uuid,
    })
  }

  RemoveVob(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-RemoveVob-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RemoveVobSubtree(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-RemoveVobSubtree-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SearchVobByName(a1: string): zCVob | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-SearchVobByName-zCVob*-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new zCVob(result) : null
  }

  VobAddedToWorld(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-VobAddedToWorld-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  VobRemovedFromWorld(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-VobRemovedFromWorld-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RenderWaynet(a1: zCCamera): void | null {
    const result: string | null = SendCommand({
      id: 'zCWorld-RenderWaynet-void-false-zCCamera*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }
}
