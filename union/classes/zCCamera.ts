import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zTViewportData } from './index.js'
import { zVEC3 } from './index.js'
import { zCPolygon } from './index.js'
import { zCVob } from './index.js'
import { zTBBox3D } from './index.js'
import { zTBBox2D } from './index.js'

export class zCCamera extends BaseUnionObject {
  vpData(): zTViewportData | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-vpData-zTViewportData-false-false',
      parentId: this.Uuid,
    })

    return result ? new zTViewportData(result) : null
  }

  set_vpData(a1: zTViewportData): null {
    SendCommand({
      id: 'SET_zCCamera-vpData-zTViewportData-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  tremorToggle(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-tremorToggle-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_tremorToggle(a1: number): null {
    SendCommand({
      id: 'SET_zCCamera-tremorToggle-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  tremorScale(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-tremorScale-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_tremorScale(a1: number): null {
    SendCommand({
      id: 'SET_zCCamera-tremorScale-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  tremorAmplitude(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-tremorAmplitude-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_tremorAmplitude(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCCamera-tremorAmplitude-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  tremorOrigin(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-tremorOrigin-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_tremorOrigin(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCCamera-tremorOrigin-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  tremorVelo(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-tremorVelo-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_tremorVelo(a1: number): null {
    SendCommand({
      id: 'SET_zCCamera-tremorVelo-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  poly(): zCPolygon | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-poly-zCPolygon*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCPolygon(result) : null
  }

  set_poly(a1: zCPolygon): null {
    SendCommand({
      id: 'SET_zCCamera-poly-zCPolygon*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  screenFadeEnabled(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-screenFadeEnabled-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_screenFadeEnabled(a1: number): null {
    SendCommand({
      id: 'SET_zCCamera-screenFadeEnabled-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  screenFadeTexture(): string | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-screenFadeTexture-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_screenFadeTexture(a1: string): null {
    SendCommand({
      id: 'SET_zCCamera-screenFadeTexture-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  screenFadeTextureAniFPS(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-screenFadeTextureAniFPS-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_screenFadeTextureAniFPS(a1: number): null {
    SendCommand({
      id: 'SET_zCCamera-screenFadeTextureAniFPS-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  cinemaScopeEnabled(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-cinemaScopeEnabled-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_cinemaScopeEnabled(a1: number): null {
    SendCommand({
      id: 'SET_zCCamera-cinemaScopeEnabled-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  drawWire(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-drawWire-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_drawWire(a1: number): null {
    SendCommand({
      id: 'SET_zCCamera-drawWire-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  farClipZ(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-farClipZ-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_farClipZ(a1: number): null {
    SendCommand({
      id: 'SET_zCCamera-farClipZ-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  nearClipZ(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-nearClipZ-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_nearClipZ(a1: number): null {
    SendCommand({
      id: 'SET_zCCamera-nearClipZ-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  viewDistanceX(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-viewDistanceX-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_viewDistanceX(a1: number): null {
    SendCommand({
      id: 'SET_zCCamera-viewDistanceX-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  viewDistanceY(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-viewDistanceY-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_viewDistanceY(a1: number): null {
    SendCommand({
      id: 'SET_zCCamera-viewDistanceY-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  viewDistanceXInv(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-viewDistanceXInv-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_viewDistanceXInv(a1: number): null {
    SendCommand({
      id: 'SET_zCCamera-viewDistanceXInv-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  viewDistanceYInv(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-viewDistanceYInv-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_viewDistanceYInv(a1: number): null {
    SendCommand({
      id: 'SET_zCCamera-viewDistanceYInv-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  vobFarClipZ(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-vobFarClipZ-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_vobFarClipZ(a1: number): null {
    SendCommand({
      id: 'SET_zCCamera-vobFarClipZ-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fovH(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-fovH-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fovH(a1: number): null {
    SendCommand({
      id: 'SET_zCCamera-fovH-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fovV(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-fovV-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fovV(a1: number): null {
    SendCommand({
      id: 'SET_zCCamera-fovV-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  connectedVob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-connectedVob-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_connectedVob(a1: zCVob): null {
    SendCommand({
      id: 'SET_zCCamera-connectedVob-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  topBottomSin(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-topBottomSin-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_topBottomSin(a1: number): null {
    SendCommand({
      id: 'SET_zCCamera-topBottomSin-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  topBottomCos(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-topBottomCos-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_topBottomCos(a1: number): null {
    SendCommand({
      id: 'SET_zCCamera-topBottomCos-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  leftRightSin(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-leftRightSin-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_leftRightSin(a1: number): null {
    SendCommand({
      id: 'SET_zCCamera-leftRightSin-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  leftRightCos(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-leftRightCos-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_leftRightCos(a1: number): null {
    SendCommand({
      id: 'SET_zCCamera-leftRightCos-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCCamera_OnInit(): zCCamera | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-zCCamera_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCCamera(result) : null
  }

  Activate(): void | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-Activate-void-false-',
      parentId: this.Uuid,
    })
  }

  UpdateProjectionMatrix(): void | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-UpdateProjectionMatrix-void-false-',
      parentId: this.Uuid,
    })
  }

  GetFOV(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-GetFOV-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetFOV2(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-GetFOV-void-false-float&,float&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  SetFOV(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-SetFOV-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetFOV2(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-SetFOV-void-false-float,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  UpdateViewport(): void | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-UpdateViewport-void-false-',
      parentId: this.Uuid,
    })
  }

  SetRenderScreenFadeTex(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-SetRenderScreenFadeTex-void-false-zSTRING',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetUpFrustum(): void | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-SetUpFrustum-void-false-',
      parentId: this.Uuid,
    })
  }

  SetFarClipZ(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-SetFarClipZ-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  StopTremor(): void | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-StopTremor-void-false-',
      parentId: this.Uuid,
    })
  }

  AddTremor(a1: zVEC3, a2: number, a3: number, a4: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-AddTremor-void-false-zVEC3 const&,float,float,zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
    })
  }

  PreRenderProcessing(): void | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-PreRenderProcessing-void-false-',
      parentId: this.Uuid,
    })
  }

  PostRenderProcessing(): void | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-PostRenderProcessing-void-false-',
      parentId: this.Uuid,
    })
  }

  GetCamPos(a1: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-GetCamPos-void-false-zVEC3&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  InitDrawPolySimple(): void | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-InitDrawPolySimple-void-false-',
      parentId: this.Uuid,
    })
  }

  CleanupDrawPolySimple(): void | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-CleanupDrawPolySimple-void-false-',
      parentId: this.Uuid,
    })
  }

  SetRenderScreenFadeTexAniFPS(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-SetRenderScreenFadeTexAniFPS-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RenderScreenFade(): void | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-RenderScreenFade-void-false-',
      parentId: this.Uuid,
    })
  }

  RenderCinemaScope(): void | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-RenderCinemaScope-void-false-',
      parentId: this.Uuid,
    })
  }

  ScreenProjectionTouchesPortalRough(a1: zTBBox3D, a2: zTBBox2D): number | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-ScreenProjectionTouchesPortalRough-int-false-zTBBox3D const&,zTBBox2D const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  ScreenProjectionTouchesPortal(a1: zTBBox3D, a2: zTBBox2D): number | null {
    const result: string | null = SendCommand({
      id: 'zCCamera-ScreenProjectionTouchesPortal-int-false-zTBBox3D const&,zTBBox2D const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }
}
