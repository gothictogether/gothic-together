import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { oCNpc } from './index.js'
import { oCInfo } from './index.js'
import { oCInfoChoice } from './index.js'

export class oCInformationManager extends BaseUnionObject {
  LastMethod(): string | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-LastMethod-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_LastMethod(a1: string): null {
    SendCommand({
      id: 'SET_oCInformationManager-LastMethod-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  Npc(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-Npc-oCNpc*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  set_Npc(a1: oCNpc): null {
    SendCommand({
      id: 'SET_oCInformationManager-Npc-oCNpc*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  Player(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-Player-oCNpc*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  set_Player(a1: oCNpc): null {
    SendCommand({
      id: 'SET_oCInformationManager-Player-oCNpc*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  Info(): oCInfo | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-Info-oCInfo*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCInfo(result) : null
  }

  set_Info(a1: oCInfo): null {
    SendCommand({
      id: 'SET_oCInformationManager-Info-oCInfo*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  IsDone(): number | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-IsDone-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_IsDone(a1: number): null {
    SendCommand({
      id: 'SET_oCInformationManager-IsDone-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  IsWaitingForEnd(): number | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-IsWaitingForEnd-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_IsWaitingForEnd(a1: number): null {
    SendCommand({
      id: 'SET_oCInformationManager-IsWaitingForEnd-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  IsWaitingForScript(): number | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-IsWaitingForScript-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_IsWaitingForScript(a1: number): null {
    SendCommand({
      id: 'SET_oCInformationManager-IsWaitingForScript-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  IsWaitingForOpen(): number | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-IsWaitingForOpen-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_IsWaitingForOpen(a1: number): null {
    SendCommand({
      id: 'SET_oCInformationManager-IsWaitingForOpen-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  IsWaitingForClose(): number | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-IsWaitingForClose-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_IsWaitingForClose(a1: number): null {
    SendCommand({
      id: 'SET_oCInformationManager-IsWaitingForClose-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  IsWaitingForSelection(): number | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-IsWaitingForSelection-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_IsWaitingForSelection(a1: number): null {
    SendCommand({
      id: 'SET_oCInformationManager-IsWaitingForSelection-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  MustOpen(): number | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-MustOpen-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_MustOpen(a1: number): null {
    SendCommand({
      id: 'SET_oCInformationManager-MustOpen-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  IndexBye(): number | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-IndexBye-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_IndexBye(a1: number): null {
    SendCommand({
      id: 'SET_oCInformationManager-IndexBye-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  ImportantCurrent(): number | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-ImportantCurrent-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_ImportantCurrent(a1: number): null {
    SendCommand({
      id: 'SET_oCInformationManager-ImportantCurrent-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  ImportantMax(): number | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-ImportantMax-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_ImportantMax(a1: number): null {
    SendCommand({
      id: 'SET_oCInformationManager-ImportantMax-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  UpdateViewSettings(): void | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-UpdateViewSettings-void-false-',
      parentId: this.Uuid,
    })
  }

  ToggleStatus(): void | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-ToggleStatus-void-false-',
      parentId: this.Uuid,
    })
  }

  PrintStatus(): void | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-PrintStatus-void-false-',
      parentId: this.Uuid,
    })
  }

  HasFinished(): number | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-HasFinished-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  WaitingForEnd(): number | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-WaitingForEnd-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetNpc(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-SetNpc-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  Update(): void | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-Update-void-false-',
      parentId: this.Uuid,
    })
  }

  Exit(): void | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-Exit-void-false-',
      parentId: this.Uuid,
    })
  }

  CameraStart(): void | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-CameraStart-void-false-',
      parentId: this.Uuid,
    })
  }

  CameraStop(): void | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-CameraStop-void-false-',
      parentId: this.Uuid,
    })
  }

  CameraRefresh(): void | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-CameraRefresh-void-false-',
      parentId: this.Uuid,
    })
  }

  ProcessImportant(): void | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-ProcessImportant-void-false-',
      parentId: this.Uuid,
    })
  }

  ProcessNextImportant(): void | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-ProcessNextImportant-void-false-',
      parentId: this.Uuid,
    })
  }

  InfoWaitForEnd(): void | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-InfoWaitForEnd-void-false-',
      parentId: this.Uuid,
    })
  }

  CollectInfos(): void | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-CollectInfos-void-false-',
      parentId: this.Uuid,
    })
  }

  CollectChoices(a1: oCInfo): number | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-CollectChoices-int-false-oCInfo*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  OnImportantBegin(): void | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-OnImportantBegin-void-false-',
      parentId: this.Uuid,
    })
  }

  OnImportantEnd(): void | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-OnImportantEnd-void-false-',
      parentId: this.Uuid,
    })
  }

  OnInfoBegin(): void | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-OnInfoBegin-void-false-',
      parentId: this.Uuid,
    })
  }

  OnInfo(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-OnInfo-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  OnInfo2(a1: oCInfo): void | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-OnInfo-void-false-oCInfo*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  OnChoiceBegin(): void | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-OnChoiceBegin-void-false-',
      parentId: this.Uuid,
    })
  }

  OnChoice(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-OnChoice-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  OnChoice2(a1: oCInfoChoice): void | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-OnChoice-void-false-oCInfoChoice*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  OnChoiceEnd(): void | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-OnChoiceEnd-void-false-',
      parentId: this.Uuid,
    })
  }

  OnInfoEnd(): void | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-OnInfoEnd-void-false-',
      parentId: this.Uuid,
    })
  }

  OnTradeBegin(): void | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-OnTradeBegin-void-false-',
      parentId: this.Uuid,
    })
  }

  OnTradeEnd(): void | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-OnTradeEnd-void-false-',
      parentId: this.Uuid,
    })
  }

  OnExit(): void | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-OnExit-void-false-',
      parentId: this.Uuid,
    })
  }

  OnTermination(): void | null {
    const result: string | null = SendCommand({
      id: 'oCInformationManager-OnTermination-void-false-',
      parentId: this.Uuid,
    })
  }
}
