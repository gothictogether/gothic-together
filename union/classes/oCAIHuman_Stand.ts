import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCVob } from './index.js'
import { zCClassDef } from './index.js'
import { zCAIBase } from './index.js'

export class oCAIHuman_Stand extends zCAIBase {
  static oCAIHuman_Stand_OnInit(a1: zCVob): oCAIHuman_Stand | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman_Stand-oCAIHuman_Stand_OnInit-void-false-zCVob*',
      parentId: 'NULL',
      a1: a1.toString(),
    })

    return result ? new oCAIHuman_Stand(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman_Stand-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override DoAI(a1: zCVob, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCAIHuman_Stand-DoAI-void-false-zCVob*,int&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }
}
