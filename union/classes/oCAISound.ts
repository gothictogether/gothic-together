import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCVob } from './index.js'
import { zCClassDef } from './index.js'
import { zCAIBase } from './index.js'

export class oCAISound extends zCAIBase {
  slideSoundHandle(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAISound-slideSoundHandle-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_slideSoundHandle(a1: number): null {
    SendCommand({
      id: 'SET_oCAISound-slideSoundHandle-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  slideSoundOn(): number | null {
    const result: string | null = SendCommand({
      id: 'oCAISound-slideSoundOn-char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_slideSoundOn(a1: number): null {
    SendCommand({
      id: 'SET_oCAISound-slideSoundOn-char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  RemoveSlideSound(): void | null {
    const result: string | null = SendCommand({
      id: 'oCAISound-RemoveSlideSound-void-false-',
      parentId: this.Uuid,
    })
  }

  CheckSlideSound(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCAISound-CheckSlideSound-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCAISound-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override DoAI(a1: zCVob, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCAISound-DoAI-void-false-zCVob*,int&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }
}
