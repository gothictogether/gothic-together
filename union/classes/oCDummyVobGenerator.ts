import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { zCVob } from './index.js'

export class oCDummyVobGenerator extends zCVob {
  static oCDummyVobGenerator_OnInit(): oCDummyVobGenerator | null {
    const result: string | null = SendCommand({
      id: 'oCDummyVobGenerator-oCDummyVobGenerator_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCDummyVobGenerator(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCDummyVobGenerator-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override OnTrigger(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCDummyVobGenerator-OnTrigger-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }
}
