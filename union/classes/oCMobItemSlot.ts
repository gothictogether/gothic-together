import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { oCItem } from './index.js'
import { zCClassDef } from './index.js'
import { oCNpc } from './index.js'
import { oCMobInter } from './index.js'

export class oCMobItemSlot extends oCMobInter {
  insertedItem(): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCMobItemSlot-insertedItem-oCItem*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCItem(result) : null
  }

  set_insertedItem(a1: oCItem): null {
    SendCommand({
      id: 'SET_oCMobItemSlot-insertedItem-oCItem*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  removeable(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobItemSlot-removeable-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_removeable(a1: number): null {
    SendCommand({
      id: 'SET_oCMobItemSlot-removeable-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCMobItemSlot_OnInit(): oCMobItemSlot | null {
    const result: string | null = SendCommand({
      id: 'oCMobItemSlot-oCMobItemSlot_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCMobItemSlot(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCMobItemSlot-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override GetUseWithItem(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobItemSlot-GetUseWithItem-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override CanInteractWith(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobItemSlot-CanInteractWith-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  override IsIn(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobItemSlot-IsIn-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetInsertedItem(): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCMobItemSlot-GetInsertedItem-oCItem*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCItem(result) : null
  }

  PlaceItem(a1: oCItem): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobItemSlot-PlaceItem-int-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  RemoveItem(): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCMobItemSlot-RemoveItem-oCItem*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCItem(result) : null
  }
}
