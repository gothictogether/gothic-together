import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { oCNpc } from './index.js'

export class oCNews extends BaseUnionObject {
  told(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNews-told-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_told(a1: number): null {
    SendCommand({
      id: 'SET_oCNews-told-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  spread_time(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNews-spread_time-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_spread_time(a1: number): null {
    SendCommand({
      id: 'SET_oCNews-spread_time-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  news_id(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNews-news_id-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_news_id(a1: number): null {
    SendCommand({
      id: 'SET_oCNews-news_id-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  gossip(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNews-gossip-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_gossip(a1: number): null {
    SendCommand({
      id: 'SET_oCNews-gossip-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  mNpcWitness(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCNews-mNpcWitness-oCNpc*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  set_mNpcWitness(a1: oCNpc): null {
    SendCommand({
      id: 'SET_oCNews-mNpcWitness-oCNpc*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  mNpcOffender(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCNews-mNpcOffender-oCNpc*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  set_mNpcOffender(a1: oCNpc): null {
    SendCommand({
      id: 'SET_oCNews-mNpcOffender-oCNpc*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  mNpcVictim(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCNews-mNpcVictim-oCNpc*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  set_mNpcVictim(a1: oCNpc): null {
    SendCommand({
      id: 'SET_oCNews-mNpcVictim-oCNpc*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  witnessName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCNews-witnessName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_witnessName(a1: string): null {
    SendCommand({
      id: 'SET_oCNews-witnessName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  offenderName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCNews-offenderName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_offenderName(a1: string): null {
    SendCommand({
      id: 'SET_oCNews-offenderName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  victimName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCNews-victimName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_victimName(a1: string): null {
    SendCommand({
      id: 'SET_oCNews-victimName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  guildvictim(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNews-guildvictim-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_guildvictim(a1: number): null {
    SendCommand({
      id: 'SET_oCNews-guildvictim-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCNews_OnInit(): oCNews | null {
    const result: string | null = SendCommand({
      id: 'oCNews-oCNews_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCNews(result) : null
  }

  static oCNews_OnInit2(
    a1: number,
    a2: number,
    a3: oCNpc,
    a4: oCNpc,
    a5: oCNpc,
    a6: number,
  ): oCNews | null {
    const result: string | null = SendCommand({
      id: 'oCNews-oCNews_OnInit-void-false-int,int,oCNpc*,oCNpc*,oCNpc*,int',
      parentId: 'NULL',
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
      a5: a5.toString(),
      a6: a6.toString(),
    })

    return result ? new oCNews(result) : null
  }

  GetID(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNews-GetID-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsIdentical(a1: number, a2: oCNpc, a3: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCNews-IsIdentical-int-false-int,oCNpc*,oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? Number(result) : null
  }

  SetSpreadTime(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNews-SetSpreadTime-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetSpreadTime(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNews-GetSpreadTime-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetGossip(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNews-SetGossip-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  IsGossip(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNews-IsGossip-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsGuildVictim(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNews-IsGuildVictim-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }
}
