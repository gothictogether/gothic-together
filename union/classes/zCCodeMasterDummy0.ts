import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'

export class zCCodeMasterDummy0 extends BaseUnionObject {
  orderRelevant(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCodeMasterDummy0-orderRelevant-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_orderRelevant(a1: number): null {
    SendCommand({
      id: 'SET_zCCodeMasterDummy0-orderRelevant-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  untriggerCancels(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCodeMasterDummy0-untriggerCancels-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_untriggerCancels(a1: number): null {
    SendCommand({
      id: 'SET_zCCodeMasterDummy0-untriggerCancels-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  firstFalseIsFailure(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCodeMasterDummy0-firstFalseIsFailure-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_firstFalseIsFailure(a1: number): null {
    SendCommand({
      id: 'SET_zCCodeMasterDummy0-firstFalseIsFailure-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
