import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { TPlayerInstance } from './index.js'

export class oCPlayerInfo extends BaseUnionObject {
  instance(): TPlayerInstance | null {
    const result: string | null = SendCommand({
      id: 'oCPlayerInfo-instance-TPlayerInstance-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_instance(a1: TPlayerInstance): null {
    SendCommand({
      id: 'SET_oCPlayerInfo-instance-TPlayerInstance-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCPlayerInfo_OnInit(): oCPlayerInfo | null {
    const result: string | null = SendCommand({
      id: 'oCPlayerInfo-oCPlayerInfo_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCPlayerInfo(result) : null
  }

  GetInstanceName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCPlayerInfo-GetInstanceName-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  GetInstance(): TPlayerInstance | null {
    const result: string | null = SendCommand({
      id: 'oCPlayerInfo-GetInstance-TPlayerInstance-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  Init(a1: string, a2: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCPlayerInfo-Init-void-false-zSTRING const&,zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  Reset(): void | null {
    const result: string | null = SendCommand({
      id: 'oCPlayerInfo-Reset-void-false-',
      parentId: this.Uuid,
    })
  }

  SetInstance(a1: TPlayerInstance): void | null {
    const result: string | null = SendCommand({
      id: 'oCPlayerInfo-SetInstance-void-false-TPlayerInstance',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetInstanceByName(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCPlayerInfo-SetInstanceByName-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }
}
