import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCWay } from './index.js'
import { zVEC3 } from './index.js'
import { zCVobWaypoint } from './index.js'
import { zCWorld } from './index.js'
import { zCClassDef } from './index.js'
import { zCVob } from './index.js'
import { zCObject } from './index.js'

export class zCWaypoint extends zCObject {
  routeCtr(): number | null {
    const result: string | null = SendCommand({
      id: 'zCWaypoint-routeCtr-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_routeCtr(a1: number): null {
    SendCommand({
      id: 'SET_zCWaypoint-routeCtr-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  curCost(): number | null {
    const result: string | null = SendCommand({
      id: 'zCWaypoint-curCost-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_curCost(a1: number): null {
    SendCommand({
      id: 'SET_zCWaypoint-curCost-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  estCost(): number | null {
    const result: string | null = SendCommand({
      id: 'zCWaypoint-estCost-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_estCost(a1: number): null {
    SendCommand({
      id: 'SET_zCWaypoint-estCost-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  score(): number | null {
    const result: string | null = SendCommand({
      id: 'zCWaypoint-score-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_score(a1: number): null {
    SendCommand({
      id: 'SET_zCWaypoint-score-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  curList(): number | null {
    const result: string | null = SendCommand({
      id: 'zCWaypoint-curList-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_curList(a1: number): null {
    SendCommand({
      id: 'SET_zCWaypoint-curList-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  parent(): zCWay | null {
    const result: string | null = SendCommand({
      id: 'zCWaypoint-parent-zCWay*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCWay(result) : null
  }

  set_parent(a1: zCWay): null {
    SendCommand({
      id: 'SET_zCWaypoint-parent-zCWay*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  waterDepth(): number | null {
    const result: string | null = SendCommand({
      id: 'zCWaypoint-waterDepth-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_waterDepth(a1: number): null {
    SendCommand({
      id: 'SET_zCWaypoint-waterDepth-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  underWater(): number | null {
    const result: string | null = SendCommand({
      id: 'zCWaypoint-underWater-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_underWater(a1: number): null {
    SendCommand({
      id: 'SET_zCWaypoint-underWater-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pos(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCWaypoint-pos-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_pos(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCWaypoint-pos-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  dir(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCWaypoint-dir-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_dir(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCWaypoint-dir-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  name(): string | null {
    const result: string | null = SendCommand({
      id: 'zCWaypoint-name-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_name(a1: string): null {
    SendCommand({
      id: 'SET_zCWaypoint-name-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  wpvob(): zCVobWaypoint | null {
    const result: string | null = SendCommand({
      id: 'zCWaypoint-wpvob-zCVobWaypoint*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVobWaypoint(result) : null
  }

  set_wpvob(a1: zCVobWaypoint): null {
    SendCommand({
      id: 'SET_zCWaypoint-wpvob-zCVobWaypoint*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCWaypoint_OnInit(): zCWaypoint | null {
    const result: string | null = SendCommand({
      id: 'zCWaypoint-zCWaypoint_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCWaypoint(result) : null
  }

  Init(a1: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'zCWaypoint-Init-void-false-zVEC3&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  Init2(a1: number, a2: number, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCWaypoint-Init-void-false-float,float,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  Init3(a1: zCVobWaypoint): void | null {
    const result: string | null = SendCommand({
      id: 'zCWaypoint-Init-void-false-zCVobWaypoint*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  Free(): void | null {
    const result: string | null = SendCommand({
      id: 'zCWaypoint-Free-void-false-',
      parentId: this.Uuid,
    })
  }

  GetPositionWorld(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCWaypoint-GetPositionWorld-zVEC3&-false-',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  UpdatePositionWorld(): void | null {
    const result: string | null = SendCommand({
      id: 'zCWaypoint-UpdatePositionWorld-void-false-',
      parentId: this.Uuid,
    })
  }

  SetName(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'zCWaypoint-SetName-void-false-zSTRING&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetName(): string | null {
    const result: string | null = SendCommand({
      id: 'zCWaypoint-GetName-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  GetVob(): zCVobWaypoint | null {
    const result: string | null = SendCommand({
      id: 'zCWaypoint-GetVob-zCVobWaypoint*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCVobWaypoint(result) : null
  }

  CalcProperties(a1: zCWorld): void | null {
    const result: string | null = SendCommand({
      id: 'zCWaypoint-CalcProperties-void-false-zCWorld*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CorrectHeight(a1: zCWorld): void | null {
    const result: string | null = SendCommand({
      id: 'zCWaypoint-CorrectHeight-void-false-zCWorld*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  InsertWay(a1: zCWay): void | null {
    const result: string | null = SendCommand({
      id: 'zCWaypoint-InsertWay-void-false-zCWay*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RemoveWay(a1: zCWay): void | null {
    const result: string | null = SendCommand({
      id: 'zCWaypoint-RemoveWay-void-false-zCWay*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  HasWay(a1: zCWaypoint): zCWay | null {
    const result: string | null = SendCommand({
      id: 'zCWaypoint-HasWay-zCWay*-false-zCWaypoint*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new zCWay(result) : null
  }

  GetNumberOfWays(): number | null {
    const result: string | null = SendCommand({
      id: 'zCWaypoint-GetNumberOfWays-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetWaypointVob(a1: zCVobWaypoint): void | null {
    const result: string | null = SendCommand({
      id: 'zCWaypoint-SetWaypointVob-void-false-zCVobWaypoint*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  Draw(): void | null {
    const result: string | null = SendCommand({
      id: 'zCWaypoint-Draw-void-false-',
      parentId: this.Uuid,
    })
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCWaypoint-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  CanBeUsed(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'zCWaypoint-CanBeUsed-int-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }
}
