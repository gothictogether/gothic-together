import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { oTSndMaterial } from './index.js'
import { oCNpc } from './index.js'
import { zCVob } from './index.js'
import { zCClassDef } from './index.js'
import { zVEC3 } from './index.js'
import { zCEventMessage } from './index.js'
import { zCModel } from './index.js'
import { oCVob } from './index.js'

export class oCMOB extends oCVob {
  name(): string | null {
    const result: string | null = SendCommand({
      id: 'oCMOB-name-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_name(a1: string): null {
    SendCommand({
      id: 'SET_oCMOB-name-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  hitp(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMOB-hitp-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_hitp(a1: number): null {
    SendCommand({
      id: 'SET_oCMOB-hitp-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  damage(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMOB-damage-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_damage(a1: number): null {
    SendCommand({
      id: 'SET_oCMOB-damage-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  isDestroyed(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMOB-isDestroyed-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_isDestroyed(a1: number): null {
    SendCommand({
      id: 'SET_oCMOB-isDestroyed-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  moveable(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMOB-moveable-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_moveable(a1: number): null {
    SendCommand({
      id: 'SET_oCMOB-moveable-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  takeable(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMOB-takeable-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_takeable(a1: number): null {
    SendCommand({
      id: 'SET_oCMOB-takeable-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  focusOverride(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMOB-focusOverride-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_focusOverride(a1: number): null {
    SendCommand({
      id: 'SET_oCMOB-focusOverride-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  sndMat(): oTSndMaterial | null {
    const result: string | null = SendCommand({
      id: 'oCMOB-sndMat-oTSndMaterial-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_sndMat(a1: oTSndMaterial): null {
    SendCommand({
      id: 'SET_oCMOB-sndMat-oTSndMaterial-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  visualDestroyed(): string | null {
    const result: string | null = SendCommand({
      id: 'oCMOB-visualDestroyed-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_visualDestroyed(a1: string): null {
    SendCommand({
      id: 'SET_oCMOB-visualDestroyed-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  ownerStr(): string | null {
    const result: string | null = SendCommand({
      id: 'oCMOB-ownerStr-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_ownerStr(a1: string): null {
    SendCommand({
      id: 'SET_oCMOB-ownerStr-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  ownerGuildStr(): string | null {
    const result: string | null = SendCommand({
      id: 'oCMOB-ownerGuildStr-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_ownerGuildStr(a1: string): null {
    SendCommand({
      id: 'SET_oCMOB-ownerGuildStr-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  owner(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMOB-owner-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_owner(a1: number): null {
    SendCommand({
      id: 'SET_oCMOB-owner-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  ownerGuild(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMOB-ownerGuild-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_ownerGuild(a1: number): null {
    SendCommand({
      id: 'SET_oCMOB-ownerGuild-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  focusNameIndex(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMOB-focusNameIndex-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_focusNameIndex(a1: number): null {
    SendCommand({
      id: 'SET_oCMOB-focusNameIndex-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCMOB_OnInit(): oCMOB | null {
    const result: string | null = SendCommand({
      id: 'oCMOB-oCMOB_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCMOB(result) : null
  }

  SetMoveable(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCMOB-SetMoveable-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  IsMoveable(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMOB-IsMoveable-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetOwner(a1: string, a2: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCMOB-SetOwner-void-false-zSTRING const&,zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  SetOwner2(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCMOB-SetOwner-void-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  IsOwner(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCMOB-IsOwner-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  Hit(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCMOB-Hit-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  InsertInIgnoreList(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCMOB-InsertInIgnoreList-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RemoveFromIgnoreList(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCMOB-RemoveFromIgnoreList-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCMOB-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override OnDamage(a1: zCVob, a2: zCVob, a3: number, a4: number, a5: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'oCMOB-OnDamage-void-false-zCVob*,zCVob*,float,int,zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
      a5: a5.toString(),
    })
  }

  override OnMessage(a1: zCEventMessage, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCMOB-OnMessage-void-false-zCEventMessage*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override CanThisCollideWith(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'oCMOB-CanThisCollideWith-int-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  override IsOwnedByGuild(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCMOB-IsOwnedByGuild-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  override IsOwnedByNpc(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCMOB-IsOwnedByNpc-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  override SetSoundMaterial(a1: oTSndMaterial): void | null {
    const result: string | null = SendCommand({
      id: 'oCMOB-SetSoundMaterial-void-false-oTSndMaterial',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override GetSoundMaterial(): oTSndMaterial | null {
    const result: string | null = SendCommand({
      id: 'oCMOB-GetSoundMaterial-oTSndMaterial-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetName(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCMOB-SetName-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCMOB-GetName-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  GetModel(): zCModel | null {
    const result: string | null = SendCommand({
      id: 'oCMOB-GetModel-zCModel*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCModel(result) : null
  }

  GetScemeName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCMOB-GetScemeName-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  Destroy(): void | null {
    const result: string | null = SendCommand({
      id: 'oCMOB-Destroy-void-false-',
      parentId: this.Uuid,
    })
  }

  AllowDiscardingOfSubtree(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMOB-AllowDiscardingOfSubtree-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }
}
