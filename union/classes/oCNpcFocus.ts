import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'

export class oCNpcFocus extends BaseUnionObject {
  n_range3(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcFocus-n_range3-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_n_range3(a1: number): null {
    SendCommand({
      id: 'SET_oCNpcFocus-n_range3-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  n_range1(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcFocus-n_range1-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_n_range1(a1: number): null {
    SendCommand({
      id: 'SET_oCNpcFocus-n_range1-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  n_range2(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcFocus-n_range2-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_n_range2(a1: number): null {
    SendCommand({
      id: 'SET_oCNpcFocus-n_range2-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  n_azi(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcFocus-n_azi-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_n_azi(a1: number): null {
    SendCommand({
      id: 'SET_oCNpcFocus-n_azi-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  n_elev1(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcFocus-n_elev1-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_n_elev1(a1: number): null {
    SendCommand({
      id: 'SET_oCNpcFocus-n_elev1-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  n_elev2(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcFocus-n_elev2-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_n_elev2(a1: number): null {
    SendCommand({
      id: 'SET_oCNpcFocus-n_elev2-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  n_prio(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcFocus-n_prio-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_n_prio(a1: number): null {
    SendCommand({
      id: 'SET_oCNpcFocus-n_prio-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  i_range1(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcFocus-i_range1-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_i_range1(a1: number): null {
    SendCommand({
      id: 'SET_oCNpcFocus-i_range1-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  i_range2(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcFocus-i_range2-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_i_range2(a1: number): null {
    SendCommand({
      id: 'SET_oCNpcFocus-i_range2-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  i_azi(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcFocus-i_azi-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_i_azi(a1: number): null {
    SendCommand({
      id: 'SET_oCNpcFocus-i_azi-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  i_elev1(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcFocus-i_elev1-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_i_elev1(a1: number): null {
    SendCommand({
      id: 'SET_oCNpcFocus-i_elev1-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  i_elev2(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcFocus-i_elev2-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_i_elev2(a1: number): null {
    SendCommand({
      id: 'SET_oCNpcFocus-i_elev2-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  i_prio(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcFocus-i_prio-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_i_prio(a1: number): null {
    SendCommand({
      id: 'SET_oCNpcFocus-i_prio-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_range1(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcFocus-m_range1-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_range1(a1: number): null {
    SendCommand({
      id: 'SET_oCNpcFocus-m_range1-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_range2(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcFocus-m_range2-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_range2(a1: number): null {
    SendCommand({
      id: 'SET_oCNpcFocus-m_range2-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_azi(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcFocus-m_azi-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_azi(a1: number): null {
    SendCommand({
      id: 'SET_oCNpcFocus-m_azi-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_elev1(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcFocus-m_elev1-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_elev1(a1: number): null {
    SendCommand({
      id: 'SET_oCNpcFocus-m_elev1-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_elev2(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcFocus-m_elev2-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_elev2(a1: number): null {
    SendCommand({
      id: 'SET_oCNpcFocus-m_elev2-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_prio(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcFocus-m_prio-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_prio(a1: number): null {
    SendCommand({
      id: 'SET_oCNpcFocus-m_prio-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  max_range(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcFocus-max_range-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_max_range(a1: number): null {
    SendCommand({
      id: 'SET_oCNpcFocus-max_range-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCNpcFocus_OnInit(): oCNpcFocus | null {
    const result: string | null = SendCommand({
      id: 'oCNpcFocus-oCNpcFocus_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCNpcFocus(result) : null
  }

  Init(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpcFocus-Init-void-false-zSTRING&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  Init2(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCNpcFocus-Init-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  IsValid(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcFocus-IsValid-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetMaxRange(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcFocus-GetMaxRange-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetRange2(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcFocus-GetRange2-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetItemThrowRange(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcFocus-GetItemThrowRange-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetMobThrowRange(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcFocus-GetMobThrowRange-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  HasLowerPriority(a1: number, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcFocus-HasLowerPriority-int-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  GetPriority(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcFocus-GetPriority-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  IsInRange(a1: number, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcFocus-IsInRange-int-false-int,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  IsInAngle(a1: number, a2: number, a3: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcFocus-IsInAngle-int-false-int,float,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? Number(result) : null
  }

  IsNpcInRange(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcFocus-IsNpcInRange-int-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  IsNpcInAngle(a1: number, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcFocus-IsNpcInAngle-int-false-float,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  IsItemInRange(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcFocus-IsItemInRange-int-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  IsItemInAngle(a1: number, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcFocus-IsItemInAngle-int-false-float,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  IsMobInRange(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcFocus-IsMobInRange-int-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  IsMobInAngle(a1: number, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcFocus-IsMobInAngle-int-false-float,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  GetAzi(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcFocus-GetAzi-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }
}
