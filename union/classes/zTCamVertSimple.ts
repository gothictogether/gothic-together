import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'

export class zTCamVertSimple extends BaseUnionObject {
  x(): number | null {
    const result: string | null = SendCommand({
      id: 'zTCamVertSimple-x-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_x(a1: number): null {
    SendCommand({
      id: 'SET_zTCamVertSimple-x-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  y(): number | null {
    const result: string | null = SendCommand({
      id: 'zTCamVertSimple-y-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_y(a1: number): null {
    SendCommand({
      id: 'SET_zTCamVertSimple-y-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  z(): number | null {
    const result: string | null = SendCommand({
      id: 'zTCamVertSimple-z-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_z(a1: number): null {
    SendCommand({
      id: 'SET_zTCamVertSimple-z-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
