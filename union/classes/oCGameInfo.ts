import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'

export class oCGameInfo extends BaseUnionObject {
  static oCGameInfo_OnInit(): oCGameInfo | null {
    const result: string | null = SendCommand({
      id: 'oCGameInfo-oCGameInfo_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCGameInfo(result) : null
  }

  Init(): void | null {
    const result: string | null = SendCommand({
      id: 'oCGameInfo-Init-void-false-',
      parentId: this.Uuid,
    })
  }
}
