import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zVEC3 } from './index.js'
import { zTBBox3D } from './index.js'

export class zTBCylinder3D extends BaseUnionObject {
  center(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zTBCylinder3D-center-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_center(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zTBCylinder3D-center-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  radius(): number | null {
    const result: string | null = SendCommand({
      id: 'zTBCylinder3D-radius-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_radius(a1: number): null {
    SendCommand({
      id: 'SET_zTBCylinder3D-radius-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  heightY(): number | null {
    const result: string | null = SendCommand({
      id: 'zTBCylinder3D-heightY-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_heightY(a1: number): null {
    SendCommand({
      id: 'SET_zTBCylinder3D-heightY-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  IsIntersecting(a1: zTBCylinder3D): number | null {
    const result: string | null = SendCommand({
      id: 'zTBCylinder3D-IsIntersecting-int-false-zTBCylinder3D const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  SetByBBox3D(a1: zTBBox3D): void | null {
    const result: string | null = SendCommand({
      id: 'zTBCylinder3D-SetByBBox3D-void-false-zTBBox3D',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }
}
