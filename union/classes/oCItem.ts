import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { oTSndMaterial } from './index.js'
import { oCVisualFX } from './index.js'
import { oEIndexDamage } from './index.js'
import { oETypeDamage } from './index.js'
import { zCCamera } from './index.js'
import { zCClassDef } from './index.js'
import { zCWorld } from './index.js'
import { oCAIVobMove } from './index.js'
import { oCVob } from './index.js'

export class oCItem extends oCVob {
  idx(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-idx-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_idx(a1: number): null {
    SendCommand({
      id: 'SET_oCItem-idx-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  name(): string | null {
    const result: string | null = SendCommand({
      id: 'oCItem-name-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_name(a1: string): null {
    SendCommand({
      id: 'SET_oCItem-name-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  nameID(): string | null {
    const result: string | null = SendCommand({
      id: 'oCItem-nameID-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_nameID(a1: string): null {
    SendCommand({
      id: 'SET_oCItem-nameID-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  hitp(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-hitp-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_hitp(a1: number): null {
    SendCommand({
      id: 'SET_oCItem-hitp-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  max_hitp(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-max_hitp-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_max_hitp(a1: number): null {
    SendCommand({
      id: 'SET_oCItem-max_hitp-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  mainflag(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-mainflag-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_mainflag(a1: number): null {
    SendCommand({
      id: 'SET_oCItem-mainflag-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  flags(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-flags-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_flags(a1: number): null {
    SendCommand({
      id: 'SET_oCItem-flags-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  weight(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-weight-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_weight(a1: number): null {
    SendCommand({
      id: 'SET_oCItem-weight-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  value(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-value-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_value(a1: number): null {
    SendCommand({
      id: 'SET_oCItem-value-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  damageTypes(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-damageTypes-unsigned long-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_damageTypes(a1: number): null {
    SendCommand({
      id: 'SET_oCItem-damageTypes-unsigned long-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  damageTotal(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-damageTotal-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_damageTotal(a1: number): null {
    SendCommand({
      id: 'SET_oCItem-damageTotal-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  wear(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-wear-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_wear(a1: number): null {
    SendCommand({
      id: 'SET_oCItem-wear-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  nutrition(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-nutrition-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_nutrition(a1: number): null {
    SendCommand({
      id: 'SET_oCItem-nutrition-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  magic(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-magic-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_magic(a1: number): null {
    SendCommand({
      id: 'SET_oCItem-magic-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  on_equip(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-on_equip-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_on_equip(a1: number): null {
    SendCommand({
      id: 'SET_oCItem-on_equip-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  on_unequip(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-on_unequip-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_on_unequip(a1: number): null {
    SendCommand({
      id: 'SET_oCItem-on_unequip-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  owner(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-owner-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_owner(a1: number): null {
    SendCommand({
      id: 'SET_oCItem-owner-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  ownerGuild(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-ownerGuild-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_ownerGuild(a1: number): null {
    SendCommand({
      id: 'SET_oCItem-ownerGuild-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  disguiseGuild(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-disguiseGuild-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_disguiseGuild(a1: number): null {
    SendCommand({
      id: 'SET_oCItem-disguiseGuild-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  file(): string | null {
    const result: string | null = SendCommand({
      id: 'oCItem-file-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_file(a1: string): null {
    SendCommand({
      id: 'SET_oCItem-file-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  visual_change(): string | null {
    const result: string | null = SendCommand({
      id: 'oCItem-visual_change-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_visual_change(a1: string): null {
    SendCommand({
      id: 'SET_oCItem-visual_change-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  effectName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCItem-effectName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_effectName(a1: string): null {
    SendCommand({
      id: 'SET_oCItem-effectName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  visual_skin(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-visual_skin-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_visual_skin(a1: number): null {
    SendCommand({
      id: 'SET_oCItem-visual_skin-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  scemeName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCItem-scemeName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_scemeName(a1: string): null {
    SendCommand({
      id: 'SET_oCItem-scemeName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  sndMat(): oTSndMaterial | null {
    const result: string | null = SendCommand({
      id: 'oCItem-sndMat-oTSndMaterial-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_sndMat(a1: oTSndMaterial): null {
    SendCommand({
      id: 'SET_oCItem-sndMat-oTSndMaterial-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  munition(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-munition-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_munition(a1: number): null {
    SendCommand({
      id: 'SET_oCItem-munition-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  spell(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-spell-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_spell(a1: number): null {
    SendCommand({
      id: 'SET_oCItem-spell-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  range(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-range-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_range(a1: number): null {
    SendCommand({
      id: 'SET_oCItem-range-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  mag_circle(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-mag_circle-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_mag_circle(a1: number): null {
    SendCommand({
      id: 'SET_oCItem-mag_circle-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  description(): string | null {
    const result: string | null = SendCommand({
      id: 'oCItem-description-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_description(a1: string): null {
    SendCommand({
      id: 'SET_oCItem-description-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  inv_zbias(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-inv_zbias-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_inv_zbias(a1: number): null {
    SendCommand({
      id: 'SET_oCItem-inv_zbias-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  inv_rotx(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-inv_rotx-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_inv_rotx(a1: number): null {
    SendCommand({
      id: 'SET_oCItem-inv_rotx-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  inv_roty(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-inv_roty-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_inv_roty(a1: number): null {
    SendCommand({
      id: 'SET_oCItem-inv_roty-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  inv_rotz(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-inv_rotz-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_inv_rotz(a1: number): null {
    SendCommand({
      id: 'SET_oCItem-inv_rotz-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  inv_animate(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-inv_animate-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_inv_animate(a1: number): null {
    SendCommand({
      id: 'SET_oCItem-inv_animate-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  amount(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-amount-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_amount(a1: number): null {
    SendCommand({
      id: 'SET_oCItem-amount-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  instanz(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-instanz-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_instanz(a1: number): null {
    SendCommand({
      id: 'SET_oCItem-instanz-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  c_manipulation(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-c_manipulation-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_c_manipulation(a1: number): null {
    SendCommand({
      id: 'SET_oCItem-c_manipulation-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  last_manipulation(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-last_manipulation-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_last_manipulation(a1: number): null {
    SendCommand({
      id: 'SET_oCItem-last_manipulation-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  magic_value(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-magic_value-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_magic_value(a1: number): null {
    SendCommand({
      id: 'SET_oCItem-magic_value-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  effectVob(): oCVisualFX | null {
    const result: string | null = SendCommand({
      id: 'oCItem-effectVob-oCVisualFX*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCVisualFX(result) : null
  }

  set_effectVob(a1: oCVisualFX): null {
    SendCommand({
      id: 'SET_oCItem-effectVob-oCVisualFX*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  next(): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCItem-next-oCItem*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCItem(result) : null
  }

  set_next(a1: oCItem): null {
    SendCommand({
      id: 'SET_oCItem-next-oCItem*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCItem_OnInit(): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCItem-oCItem_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCItem(result) : null
  }

  static oCItem_OnInit2(a1: string, a2: number): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCItem-oCItem_OnInit-void-false-zSTRING&,int',
      parentId: 'NULL',
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCItem(result) : null
  }

  static oCItem_OnInit3(a1: number, a2: number): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCItem-oCItem_OnInit-void-false-int,int',
      parentId: 'NULL',
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCItem(result) : null
  }

  GetSchemeName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCItem-GetSchemeName-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  CreateVisual(): void | null {
    const result: string | null = SendCommand({
      id: 'oCItem-CreateVisual-void-false-',
      parentId: this.Uuid,
    })
  }

  GetDescription(): string | null {
    const result: string | null = SendCommand({
      id: 'oCItem-GetDescription-zSTRING&-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  GetText(a1: number): string | null {
    const result: string | null = SendCommand({
      id: 'oCItem-GetText-zSTRING&-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? String(result) : null
  }

  GetCount(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-GetCount-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetHealMode(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-GetHealMode-int-false-int&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetProtectionByIndex(a1: oEIndexDamage): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-GetProtectionByIndex-int-false-oEIndexDamage',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetProtectionByType(a1: oETypeDamage): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-GetProtectionByType-int-false-oETypeDamage',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetProtectionByMode(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-GetProtectionByMode-int-false-unsigned long',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetFullProtection(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-GetFullProtection-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  CopyDamage(a1: oCItem): void | null {
    const result: string | null = SendCommand({
      id: 'oCItem-CopyDamage-void-false-oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetDamageByIndex(a1: oEIndexDamage): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-GetDamageByIndex-int-false-oEIndexDamage',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetDamageByType(a1: oETypeDamage): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-GetDamageByType-int-false-oETypeDamage',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetDamageByMode(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-GetDamageByMode-int-false-unsigned long',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetFullDamage(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-GetFullDamage-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetDamageTypes(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-GetDamageTypes-unsigned long-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  HasDamageType(a1: oETypeDamage): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-HasDamageType-int-false-oETypeDamage',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  IsDeadly(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-IsDeadly-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsOneHandWeapon(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-IsOneHandWeapon-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  MultiSlot(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-MultiSlot-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SplitSlot(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-SplitSlot-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetValue(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-GetValue-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetWeight(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-GetWeight-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  HasFlag(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-HasFlag-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  ClearFlag(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCItem-ClearFlag-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetFlag(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCItem-SetFlag-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  TwoHanded(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-TwoHanded-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsOwned(a1: number, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-IsOwned-int-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  GetDisguiseGuild(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-GetDisguiseGuild-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  AddManipulation(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-AddManipulation-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  Identify(): void | null {
    const result: string | null = SendCommand({
      id: 'oCItem-Identify-void-false-',
      parentId: this.Uuid,
    })
  }

  GetName(a1: number): string | null {
    const result: string | null = SendCommand({
      id: 'oCItem-GetName-zSTRING-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? String(result) : null
  }

  GetStatus(): string | null {
    const result: string | null = SendCommand({
      id: 'oCItem-GetStatus-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  UseItem(): void | null {
    const result: string | null = SendCommand({
      id: 'oCItem-UseItem-void-false-',
      parentId: this.Uuid,
    })
  }

  GetVisualChange(): string | null {
    const result: string | null = SendCommand({
      id: 'oCItem-GetVisualChange-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  GetEffectName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCItem-GetEffectName-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  GetStateEffectFunc(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-GetStateEffectFunc-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  SplitItem(a1: number): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oCItem-SplitItem-oCItem*-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new oCItem(result) : null
  }

  RemoveEffect(): void | null {
    const result: string | null = SendCommand({
      id: 'oCItem-RemoveEffect-void-false-',
      parentId: this.Uuid,
    })
  }

  InsertEffect(): void | null {
    const result: string | null = SendCommand({
      id: 'oCItem-InsertEffect-void-false-',
      parentId: this.Uuid,
    })
  }

  RotateForInventory(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCItem-RotateForInventory-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  RotateInInventory(): void | null {
    const result: string | null = SendCommand({
      id: 'oCItem-RotateInInventory-void-false-',
      parentId: this.Uuid,
    })
  }

  RenderItem(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCItem-RenderItem-void-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  RenderItemPlaceCamera(a1: zCCamera, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCItem-RenderItemPlaceCamera-void-false-zCCamera*,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCItem-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override ThisVobAddedToWorld(a1: zCWorld): void | null {
    const result: string | null = SendCommand({
      id: 'oCItem-ThisVobAddedToWorld-void-false-zCWorld*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override ThisVobRemovedFromWorld(a1: zCWorld): void | null {
    const result: string | null = SendCommand({
      id: 'oCItem-ThisVobRemovedFromWorld-void-false-zCWorld*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override Init(): void | null {
    const result: string | null = SendCommand({
      id: 'oCItem-Init-void-false-',
      parentId: this.Uuid,
    })
  }

  override GetInstance(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-GetInstance-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override GetInstanceName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCItem-GetInstanceName-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  override IsOwnedByGuild(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-IsOwnedByGuild-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  override IsOwnedByNpc(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCItem-IsOwnedByNpc-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  override GetAIVobMove(): oCAIVobMove | null {
    const result: string | null = SendCommand({
      id: 'oCItem-GetAIVobMove-oCAIVobMove*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCAIVobMove(result) : null
  }

  override GetSoundMaterial(): oTSndMaterial | null {
    const result: string | null = SendCommand({
      id: 'oCItem-GetSoundMaterial-oTSndMaterial-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }
}
