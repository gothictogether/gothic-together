import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { zCVob } from './index.js'
import { zCTrigger } from './index.js'

export class oCTriggerChangeLevel extends zCTrigger {
  levelName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCTriggerChangeLevel-levelName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_levelName(a1: string): null {
    SendCommand({
      id: 'SET_oCTriggerChangeLevel-levelName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  startVob(): string | null {
    const result: string | null = SendCommand({
      id: 'oCTriggerChangeLevel-startVob-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_startVob(a1: string): null {
    SendCommand({
      id: 'SET_oCTriggerChangeLevel-startVob-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCTriggerChangeLevel_OnInit(): oCTriggerChangeLevel | null {
    const result: string | null = SendCommand({
      id: 'oCTriggerChangeLevel-oCTriggerChangeLevel_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCTriggerChangeLevel(result) : null
  }

  SetLevelName(a1: string, a2: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCTriggerChangeLevel-SetLevelName-void-false-zSTRING const&,zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCTriggerChangeLevel-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override TriggerTarget(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCTriggerChangeLevel-TriggerTarget-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override UntriggerTarget(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCTriggerChangeLevel-UntriggerTarget-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }
}
