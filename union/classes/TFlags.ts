import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'

export class TFlags extends BaseUnionObject {
  portalPoly(): number | null {
    const result: string | null = SendCommand({
      id: 'TFlags-portalPoly-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_portalPoly(a1: number): null {
    SendCommand({
      id: 'SET_TFlags-portalPoly-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  occluder(): number | null {
    const result: string | null = SendCommand({
      id: 'TFlags-occluder-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_occluder(a1: number): null {
    SendCommand({
      id: 'SET_TFlags-occluder-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  sectorPoly(): number | null {
    const result: string | null = SendCommand({
      id: 'TFlags-sectorPoly-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_sectorPoly(a1: number): null {
    SendCommand({
      id: 'SET_TFlags-sectorPoly-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  mustRelight(): number | null {
    const result: string | null = SendCommand({
      id: 'TFlags-mustRelight-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_mustRelight(a1: number): null {
    SendCommand({
      id: 'SET_TFlags-mustRelight-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  portalIndoorOutdoor(): number | null {
    const result: string | null = SendCommand({
      id: 'TFlags-portalIndoorOutdoor-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_portalIndoorOutdoor(a1: number): null {
    SendCommand({
      id: 'SET_TFlags-portalIndoorOutdoor-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  ghostOccluder(): number | null {
    const result: string | null = SendCommand({
      id: 'TFlags-ghostOccluder-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_ghostOccluder(a1: number): null {
    SendCommand({
      id: 'SET_TFlags-ghostOccluder-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  noDynLightNear(): number | null {
    const result: string | null = SendCommand({
      id: 'TFlags-noDynLightNear-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_noDynLightNear(a1: number): null {
    SendCommand({
      id: 'SET_TFlags-noDynLightNear-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  sectorIndex(): number | null {
    const result: string | null = SendCommand({
      id: 'TFlags-sectorIndex-unsigned short-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_sectorIndex(a1: number): null {
    SendCommand({
      id: 'SET_TFlags-sectorIndex-unsigned short-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
