import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zTEventMoverSubType } from './index.js'
import { zCClassDef } from './index.js'
import { zCVob } from './index.js'
import { zCTriggerBase } from './index.js'

export class zCMoverControler extends zCTriggerBase {
  moverMessage(): zTEventMoverSubType | null {
    const result: string | null = SendCommand({
      id: 'zCMoverControler-moverMessage-zTEventMoverSubType-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_moverMessage(a1: zTEventMoverSubType): null {
    SendCommand({
      id: 'SET_zCMoverControler-moverMessage-zTEventMoverSubType-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  gotoFixedKeyframe(): number | null {
    const result: string | null = SendCommand({
      id: 'zCMoverControler-gotoFixedKeyframe-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_gotoFixedKeyframe(a1: number): null {
    SendCommand({
      id: 'SET_zCMoverControler-gotoFixedKeyframe-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCMoverControler_OnInit(): zCMoverControler | null {
    const result: string | null = SendCommand({
      id: 'zCMoverControler-zCMoverControler_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCMoverControler(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCMoverControler-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override OnTrigger(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCMoverControler-OnTrigger-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override OnUntrigger(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCMoverControler-OnUntrigger-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override OnTouch(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCMoverControler-OnTouch-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override OnUntouch(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCMoverControler-OnUntouch-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }
}
