import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCVob } from './index.js'
import { zVEC3 } from './index.js'
import { zCClassDef } from './index.js'
import { zCEventMessage } from './index.js'

export class zCEventCore extends zCEventMessage {
  otherVob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'zCEventCore-otherVob-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_otherVob(a1: zCVob): null {
    SendCommand({
      id: 'SET_zCEventCore-otherVob-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  vobInstigator(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'zCEventCore-vobInstigator-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_vobInstigator(a1: zCVob): null {
    SendCommand({
      id: 'SET_zCEventCore-vobInstigator-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  damage(): number | null {
    const result: string | null = SendCommand({
      id: 'zCEventCore-damage-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_damage(a1: number): null {
    SendCommand({
      id: 'SET_zCEventCore-damage-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  damageType(): number | null {
    const result: string | null = SendCommand({
      id: 'zCEventCore-damageType-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_damageType(a1: number): null {
    SendCommand({
      id: 'SET_zCEventCore-damageType-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  inflictorVob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'zCEventCore-inflictorVob-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_inflictorVob(a1: zCVob): null {
    SendCommand({
      id: 'SET_zCEventCore-inflictorVob-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  hitLocation(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCEventCore-hitLocation-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_hitLocation(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCEventCore-hitLocation-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  Clear(): void | null {
    const result: string | null = SendCommand({
      id: 'zCEventCore-Clear-void-false-',
      parentId: this.Uuid,
    })
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCEventCore-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override IsNetRelevant(): number | null {
    const result: string | null = SendCommand({
      id: 'zCEventCore-IsNetRelevant-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override MD_GetNumOfSubTypes(): number | null {
    const result: string | null = SendCommand({
      id: 'zCEventCore-MD_GetNumOfSubTypes-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override MD_GetSubTypeString(a1: number): string | null {
    const result: string | null = SendCommand({
      id: 'zCEventCore-MD_GetSubTypeString-zSTRING-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? String(result) : null
  }
}
