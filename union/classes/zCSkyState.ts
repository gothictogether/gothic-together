import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zVEC3 } from './index.js'

export class zCSkyState extends BaseUnionObject {
  time(): number | null {
    const result: string | null = SendCommand({
      id: 'zCSkyState-time-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_time(a1: number): null {
    SendCommand({
      id: 'SET_zCSkyState-time-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  polyColor(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCSkyState-polyColor-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_polyColor(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCSkyState-polyColor-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fogColor(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCSkyState-fogColor-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_fogColor(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCSkyState-fogColor-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  domeColor1(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCSkyState-domeColor1-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_domeColor1(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCSkyState-domeColor1-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  domeColor0(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCSkyState-domeColor0-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_domeColor0(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCSkyState-domeColor0-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fogDist(): number | null {
    const result: string | null = SendCommand({
      id: 'zCSkyState-fogDist-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fogDist(a1: number): null {
    SendCommand({
      id: 'SET_zCSkyState-fogDist-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  sunOn(): number | null {
    const result: string | null = SendCommand({
      id: 'zCSkyState-sunOn-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_sunOn(a1: number): null {
    SendCommand({
      id: 'SET_zCSkyState-sunOn-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  cloudShadowOn(): number | null {
    const result: string | null = SendCommand({
      id: 'zCSkyState-cloudShadowOn-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_cloudShadowOn(a1: number): null {
    SendCommand({
      id: 'SET_zCSkyState-cloudShadowOn-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCSkyState_OnInit(): zCSkyState | null {
    const result: string | null = SendCommand({
      id: 'zCSkyState-zCSkyState_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCSkyState(result) : null
  }

  PresetDay0(): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyState-PresetDay0-void-false-',
      parentId: this.Uuid,
    })
  }

  PresetDay1(): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyState-PresetDay1-void-false-',
      parentId: this.Uuid,
    })
  }

  PresetDay2(): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyState-PresetDay2-void-false-',
      parentId: this.Uuid,
    })
  }

  PresetEvening(): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyState-PresetEvening-void-false-',
      parentId: this.Uuid,
    })
  }

  PresetNight0(): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyState-PresetNight0-void-false-',
      parentId: this.Uuid,
    })
  }

  PresetNight1(): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyState-PresetNight1-void-false-',
      parentId: this.Uuid,
    })
  }

  PresetNight2(): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyState-PresetNight2-void-false-',
      parentId: this.Uuid,
    })
  }

  PresetDawn(): void | null {
    const result: string | null = SendCommand({
      id: 'zCSkyState-PresetDawn-void-false-',
      parentId: this.Uuid,
    })
  }
}
