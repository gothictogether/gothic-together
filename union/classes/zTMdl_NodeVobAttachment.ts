import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCVob } from './index.js'
import { zCModelNodeInst } from './index.js'

export class zTMdl_NodeVobAttachment extends BaseUnionObject {
  vob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'zTMdl_NodeVobAttachment-vob-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_vob(a1: zCVob): null {
    SendCommand({
      id: 'SET_zTMdl_NodeVobAttachment-vob-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  mnode(): zCModelNodeInst | null {
    const result: string | null = SendCommand({
      id: 'zTMdl_NodeVobAttachment-mnode-zCModelNodeInst*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCModelNodeInst(result) : null
  }

  set_mnode(a1: zCModelNodeInst): null {
    SendCommand({
      id: 'SET_zTMdl_NodeVobAttachment-mnode-zCModelNodeInst*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
