import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zVEC3 } from './index.js'

export class oSDirectionInfo extends BaseUnionObject {
  checkDirection(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oSDirectionInfo-checkDirection-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_checkDirection(a1: zVEC3): null {
    SendCommand({
      id: 'SET_oSDirectionInfo-checkDirection-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  checkForChasm(): number | null {
    const result: string | null = SendCommand({
      id: 'oSDirectionInfo-checkForChasm-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_checkForChasm(a1: number): null {
    SendCommand({
      id: 'SET_oSDirectionInfo-checkForChasm-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  checkForSmallObject(): number | null {
    const result: string | null = SendCommand({
      id: 'oSDirectionInfo-checkForSmallObject-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_checkForSmallObject(a1: number): null {
    SendCommand({
      id: 'SET_oSDirectionInfo-checkForSmallObject-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  checkIfClimbable(): number | null {
    const result: string | null = SendCommand({
      id: 'oSDirectionInfo-checkIfClimbable-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_checkIfClimbable(a1: number): null {
    SendCommand({
      id: 'SET_oSDirectionInfo-checkIfClimbable-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
