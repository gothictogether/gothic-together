import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCCSManager } from './index.js'
import { zCWorld } from './index.js'
import { zVEC3 } from './index.js'
import { zCClassDef } from './index.js'
import { zCVob } from './index.js'

export class zCCSPlayer extends BaseUnionObject {
  lastProcessDay(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCSPlayer-lastProcessDay-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_lastProcessDay(a1: number): null {
    SendCommand({
      id: 'SET_zCCSPlayer-lastProcessDay-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lastProcessHour(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCSPlayer-lastProcessHour-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_lastProcessHour(a1: number): null {
    SendCommand({
      id: 'SET_zCCSPlayer-lastProcessHour-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  processingCtr(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCSPlayer-processingCtr-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_processingCtr(a1: number): null {
    SendCommand({
      id: 'SET_zCCSPlayer-processingCtr-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  debugInfoOnScreen(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCSPlayer-debugInfoOnScreen-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_debugInfoOnScreen(a1: number): null {
    SendCommand({
      id: 'SET_zCCSPlayer-debugInfoOnScreen-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  timerCtr(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCSPlayer-timerCtr-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_timerCtr(a1: number): null {
    SendCommand({
      id: 'SET_zCCSPlayer-timerCtr-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  ownerManager(): zCCSManager | null {
    const result: string | null = SendCommand({
      id: 'zCCSPlayer-ownerManager-zCCSManager*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCCSManager(result) : null
  }

  set_ownerManager(a1: zCCSManager): null {
    SendCommand({
      id: 'SET_zCCSPlayer-ownerManager-zCCSManager*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  ownerWorld(): zCWorld | null {
    const result: string | null = SendCommand({
      id: 'zCCSPlayer-ownerWorld-zCWorld*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCWorld(result) : null
  }

  set_ownerWorld(a1: zCWorld): null {
    SendCommand({
      id: 'SET_zCCSPlayer-ownerWorld-zCWorld*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCCSPlayer_OnInit(): zCCSPlayer | null {
    const result: string | null = SendCommand({
      id: 'zCCSPlayer-zCCSPlayer_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCCSPlayer(result) : null
  }

  Interrupt(): void | null {
    const result: string | null = SendCommand({
      id: 'zCCSPlayer-Interrupt-void-false-',
      parentId: this.Uuid,
    })
  }

  Resume(): void | null {
    const result: string | null = SendCommand({
      id: 'zCCSPlayer-Resume-void-false-',
      parentId: this.Uuid,
    })
  }

  IsAssigned(a1: string, a2: string): number | null {
    const result: string | null = SendCommand({
      id: 'zCCSPlayer-IsAssigned-int-false-zSTRING&,zSTRING&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  PrintListOfCutscenes(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'zCCSPlayer-PrintListOfCutscenes-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  UpdateCutscenesNow(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCSPlayer-UpdateCutscenesNow-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  DebugResetInfoscreen(): void | null {
    const result: string | null = SendCommand({
      id: 'zCCSPlayer-DebugResetInfoscreen-void-false-',
      parentId: this.Uuid,
    })
  }

  DebugAddCutscene(a1: number, a2: zVEC3, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCCSPlayer-DebugAddCutscene-void-false-int,zVEC3,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  ToggleDebugInfo(): void | null {
    const result: string | null = SendCommand({
      id: 'zCCSPlayer-ToggleDebugInfo-void-false-',
      parentId: this.Uuid,
    })
  }

  _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCCSPlayer-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  ResetCutscenePlayer(): void | null {
    const result: string | null = SendCommand({
      id: 'zCCSPlayer-ResetCutscenePlayer-void-false-',
      parentId: this.Uuid,
    })
  }

  StopAllCutscenes(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'zCCSPlayer-StopAllCutscenes-int-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  StopAllOutputUnits(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'zCCSPlayer-StopAllOutputUnits-int-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  OnCSTrigger(a1: string, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCCSPlayer-OnCSTrigger-void-false-zSTRING&,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  OnCSUntrigger(a1: string, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCCSPlayer-OnCSUntrigger-void-false-zSTRING&,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  SetOwnerWorld(a1: zCWorld): void | null {
    const result: string | null = SendCommand({
      id: 'zCCSPlayer-SetOwnerWorld-void-false-zCWorld*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetOwnerManager(a1: zCCSManager): void | null {
    const result: string | null = SendCommand({
      id: 'zCCSPlayer-SetOwnerManager-void-false-zCCSManager*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  Process(): void | null {
    const result: string | null = SendCommand({
      id: 'zCCSPlayer-Process-void-false-',
      parentId: this.Uuid,
    })
  }

  ProcessList(a1: zVEC3, a2: number, a3: number, a4: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCCSPlayer-ProcessList-void-false-zVEC3 const&,int,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
    })
  }
}
