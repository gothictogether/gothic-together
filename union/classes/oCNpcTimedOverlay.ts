import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'

export class oCNpcTimedOverlay extends BaseUnionObject {
  mdsOverlayName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCNpcTimedOverlay-mdsOverlayName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_mdsOverlayName(a1: string): null {
    SendCommand({
      id: 'SET_oCNpcTimedOverlay-mdsOverlayName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  timer(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcTimedOverlay-timer-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_timer(a1: number): null {
    SendCommand({
      id: 'SET_oCNpcTimedOverlay-timer-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  GetMdsName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCNpcTimedOverlay-GetMdsName-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  Process(): number | null {
    const result: string | null = SendCommand({
      id: 'oCNpcTimedOverlay-Process-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }
}
