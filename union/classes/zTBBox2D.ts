import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'

export class zTBBox2D extends BaseUnionObject {
  static zTBBox2D_OnInit(): zTBBox2D | null {
    const result: string | null = SendCommand({
      id: 'zTBBox2D-zTBBox2D_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zTBBox2D(result) : null
  }

  Classify(a1: zTBBox2D): number | null {
    const result: string | null = SendCommand({
      id: 'zTBBox2D-Classify-int-false-zTBBox2D const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  ClipToBBox2D(a1: zTBBox2D): void | null {
    const result: string | null = SendCommand({
      id: 'zTBBox2D-ClipToBBox2D-void-false-zTBBox2D const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  Draw(): void | null {
    const result: string | null = SendCommand({
      id: 'zTBBox2D-Draw-void-false-',
      parentId: this.Uuid,
    })
  }
}
