import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zTBBox3D } from './index.js'
import { oCNpc } from './index.js'

export class TRtn_WayBox extends BaseUnionObject {
  bbox(): zTBBox3D | null {
    const result: string | null = SendCommand({
      id: 'TRtn_WayBox-bbox-zTBBox3D-false-false',
      parentId: this.Uuid,
    })

    return result ? new zTBBox3D(result) : null
  }

  set_bbox(a1: zTBBox3D): null {
    SendCommand({
      id: 'SET_TRtn_WayBox-bbox-zTBBox3D-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  npc(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'TRtn_WayBox-npc-oCNpc*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  set_npc(a1: oCNpc): null {
    SendCommand({
      id: 'SET_TRtn_WayBox-npc-oCNpc*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  found(): number | null {
    const result: string | null = SendCommand({
      id: 'TRtn_WayBox-found-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_found(a1: number): null {
    SendCommand({
      id: 'SET_TRtn_WayBox-found-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
