import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCVob } from './index.js'
import { oCNpc } from './index.js'
import { oCVisualFX } from './index.js'
import { oCItem } from './index.js'
import { zVEC3 } from './index.js'

export class oSDamageDescriptor extends BaseUnionObject {
  dwFieldsValid(): number | null {
    const result: string | null = SendCommand({
      id: 'oSDamageDescriptor-dwFieldsValid-unsigned long-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_dwFieldsValid(a1: number): null {
    SendCommand({
      id: 'SET_oSDamageDescriptor-dwFieldsValid-unsigned long-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pVobAttacker(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oSDamageDescriptor-pVobAttacker-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_pVobAttacker(a1: zCVob): null {
    SendCommand({
      id: 'SET_oSDamageDescriptor-pVobAttacker-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pNpcAttacker(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oSDamageDescriptor-pNpcAttacker-oCNpc*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  set_pNpcAttacker(a1: oCNpc): null {
    SendCommand({
      id: 'SET_oSDamageDescriptor-pNpcAttacker-oCNpc*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pVobHit(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oSDamageDescriptor-pVobHit-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_pVobHit(a1: zCVob): null {
    SendCommand({
      id: 'SET_oSDamageDescriptor-pVobHit-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pFXHit(): oCVisualFX | null {
    const result: string | null = SendCommand({
      id: 'oSDamageDescriptor-pFXHit-oCVisualFX*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCVisualFX(result) : null
  }

  set_pFXHit(a1: oCVisualFX): null {
    SendCommand({
      id: 'SET_oSDamageDescriptor-pFXHit-oCVisualFX*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pItemWeapon(): oCItem | null {
    const result: string | null = SendCommand({
      id: 'oSDamageDescriptor-pItemWeapon-oCItem*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCItem(result) : null
  }

  set_pItemWeapon(a1: oCItem): null {
    SendCommand({
      id: 'SET_oSDamageDescriptor-pItemWeapon-oCItem*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  nSpellID(): number | null {
    const result: string | null = SendCommand({
      id: 'oSDamageDescriptor-nSpellID-unsigned long-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_nSpellID(a1: number): null {
    SendCommand({
      id: 'SET_oSDamageDescriptor-nSpellID-unsigned long-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  nSpellCat(): number | null {
    const result: string | null = SendCommand({
      id: 'oSDamageDescriptor-nSpellCat-unsigned long-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_nSpellCat(a1: number): null {
    SendCommand({
      id: 'SET_oSDamageDescriptor-nSpellCat-unsigned long-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  nSpellLevel(): number | null {
    const result: string | null = SendCommand({
      id: 'oSDamageDescriptor-nSpellLevel-unsigned long-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_nSpellLevel(a1: number): null {
    SendCommand({
      id: 'SET_oSDamageDescriptor-nSpellLevel-unsigned long-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  enuModeDamage(): number | null {
    const result: string | null = SendCommand({
      id: 'oSDamageDescriptor-enuModeDamage-unsigned long-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_enuModeDamage(a1: number): null {
    SendCommand({
      id: 'SET_oSDamageDescriptor-enuModeDamage-unsigned long-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  enuModeWeapon(): number | null {
    const result: string | null = SendCommand({
      id: 'oSDamageDescriptor-enuModeWeapon-unsigned long-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_enuModeWeapon(a1: number): null {
    SendCommand({
      id: 'SET_oSDamageDescriptor-enuModeWeapon-unsigned long-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fDamageTotal(): number | null {
    const result: string | null = SendCommand({
      id: 'oSDamageDescriptor-fDamageTotal-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fDamageTotal(a1: number): null {
    SendCommand({
      id: 'SET_oSDamageDescriptor-fDamageTotal-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fDamageMultiplier(): number | null {
    const result: string | null = SendCommand({
      id: 'oSDamageDescriptor-fDamageMultiplier-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fDamageMultiplier(a1: number): null {
    SendCommand({
      id: 'SET_oSDamageDescriptor-fDamageMultiplier-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  vecLocationHit(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oSDamageDescriptor-vecLocationHit-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_vecLocationHit(a1: zVEC3): null {
    SendCommand({
      id: 'SET_oSDamageDescriptor-vecLocationHit-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  vecDirectionFly(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oSDamageDescriptor-vecDirectionFly-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_vecDirectionFly(a1: zVEC3): null {
    SendCommand({
      id: 'SET_oSDamageDescriptor-vecDirectionFly-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  strVisualFX(): string | null {
    const result: string | null = SendCommand({
      id: 'oSDamageDescriptor-strVisualFX-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_strVisualFX(a1: string): null {
    SendCommand({
      id: 'SET_oSDamageDescriptor-strVisualFX-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fTimeDuration(): number | null {
    const result: string | null = SendCommand({
      id: 'oSDamageDescriptor-fTimeDuration-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fTimeDuration(a1: number): null {
    SendCommand({
      id: 'SET_oSDamageDescriptor-fTimeDuration-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fTimeInterval(): number | null {
    const result: string | null = SendCommand({
      id: 'oSDamageDescriptor-fTimeInterval-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fTimeInterval(a1: number): null {
    SendCommand({
      id: 'SET_oSDamageDescriptor-fTimeInterval-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fDamagePerInterval(): number | null {
    const result: string | null = SendCommand({
      id: 'oSDamageDescriptor-fDamagePerInterval-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fDamagePerInterval(a1: number): null {
    SendCommand({
      id: 'SET_oSDamageDescriptor-fDamagePerInterval-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  bDamageDontKill(): number | null {
    const result: string | null = SendCommand({
      id: 'oSDamageDescriptor-bDamageDontKill-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_bDamageDontKill(a1: number): null {
    SendCommand({
      id: 'SET_oSDamageDescriptor-bDamageDontKill-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  bOnce(): number | null {
    const result: string | null = SendCommand({
      id: 'oSDamageDescriptor-bOnce-unsigned long-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_bOnce(a1: number): null {
    SendCommand({
      id: 'SET_oSDamageDescriptor-bOnce-unsigned long-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  bFinished(): number | null {
    const result: string | null = SendCommand({
      id: 'oSDamageDescriptor-bFinished-unsigned long-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_bFinished(a1: number): null {
    SendCommand({
      id: 'SET_oSDamageDescriptor-bFinished-unsigned long-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  bIsDead(): number | null {
    const result: string | null = SendCommand({
      id: 'oSDamageDescriptor-bIsDead-unsigned long-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_bIsDead(a1: number): null {
    SendCommand({
      id: 'SET_oSDamageDescriptor-bIsDead-unsigned long-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  bIsUnconscious(): number | null {
    const result: string | null = SendCommand({
      id: 'oSDamageDescriptor-bIsUnconscious-unsigned long-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_bIsUnconscious(a1: number): null {
    SendCommand({
      id: 'SET_oSDamageDescriptor-bIsUnconscious-unsigned long-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lReserved(): number | null {
    const result: string | null = SendCommand({
      id: 'oSDamageDescriptor-lReserved-unsigned long-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_lReserved(a1: number): null {
    SendCommand({
      id: 'SET_oSDamageDescriptor-lReserved-unsigned long-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fAzimuth(): number | null {
    const result: string | null = SendCommand({
      id: 'oSDamageDescriptor-fAzimuth-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fAzimuth(a1: number): null {
    SendCommand({
      id: 'SET_oSDamageDescriptor-fAzimuth-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fElevation(): number | null {
    const result: string | null = SendCommand({
      id: 'oSDamageDescriptor-fElevation-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fElevation(a1: number): null {
    SendCommand({
      id: 'SET_oSDamageDescriptor-fElevation-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fTimeCurrent(): number | null {
    const result: string | null = SendCommand({
      id: 'oSDamageDescriptor-fTimeCurrent-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fTimeCurrent(a1: number): null {
    SendCommand({
      id: 'SET_oSDamageDescriptor-fTimeCurrent-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fDamageReal(): number | null {
    const result: string | null = SendCommand({
      id: 'oSDamageDescriptor-fDamageReal-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fDamageReal(a1: number): null {
    SendCommand({
      id: 'SET_oSDamageDescriptor-fDamageReal-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fDamageEffective(): number | null {
    const result: string | null = SendCommand({
      id: 'oSDamageDescriptor-fDamageEffective-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fDamageEffective(a1: number): null {
    SendCommand({
      id: 'SET_oSDamageDescriptor-fDamageEffective-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pVobParticleFX(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oSDamageDescriptor-pVobParticleFX-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_pVobParticleFX(a1: zCVob): null {
    SendCommand({
      id: 'SET_oSDamageDescriptor-pVobParticleFX-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pVisualFX(): oCVisualFX | null {
    const result: string | null = SendCommand({
      id: 'oSDamageDescriptor-pVisualFX-oCVisualFX*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCVisualFX(result) : null
  }

  set_pVisualFX(a1: oCVisualFX): null {
    SendCommand({
      id: 'SET_oSDamageDescriptor-pVisualFX-oCVisualFX*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oSDamageDescriptor_OnInit(a1: oSDamageDescriptor): oSDamageDescriptor | null {
    const result: string | null = SendCommand({
      id: 'oSDamageDescriptor-oSDamageDescriptor_OnInit-void-false-oSDamageDescriptor const&',
      parentId: 'NULL',
      a1: a1.toString(),
    })

    return result ? new oSDamageDescriptor(result) : null
  }

  Release(): void | null {
    const result: string | null = SendCommand({
      id: 'oSDamageDescriptor-Release-void-false-',
      parentId: this.Uuid,
    })
  }

  SetVisualFX(a1: oCVisualFX): void | null {
    const result: string | null = SendCommand({
      id: 'oSDamageDescriptor-SetVisualFX-void-false-oCVisualFX*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetFXHit(a1: oCVisualFX): void | null {
    const result: string | null = SendCommand({
      id: 'oSDamageDescriptor-SetFXHit-void-false-oCVisualFX*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }
}
