import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zVEC3 } from './index.js'

export class zSParticle extends BaseUnionObject {
  m_killTotalTime(): number | null {
    const result: string | null = SendCommand({
      id: 'zSParticle-m_killTotalTime-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_killTotalTime(a1: number): null {
    SendCommand({
      id: 'SET_zSParticle-m_killTotalTime-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_destPosition(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zSParticle-m_destPosition-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_m_destPosition(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zSParticle-m_destPosition-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_destNormal(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zSParticle-m_destNormal-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_m_destNormal(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zSParticle-m_destNormal-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
