import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { zCVobSound } from './index.js'

export class zCVobSoundDaytime extends zCVobSound {
  soundStartTime(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobSoundDaytime-soundStartTime-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_soundStartTime(a1: number): null {
    SendCommand({
      id: 'SET_zCVobSoundDaytime-soundStartTime-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  soundEndTime(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobSoundDaytime-soundEndTime-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_soundEndTime(a1: number): null {
    SendCommand({
      id: 'SET_zCVobSoundDaytime-soundEndTime-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  soundName2(): string | null {
    const result: string | null = SendCommand({
      id: 'zCVobSoundDaytime-soundName2-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_soundName2(a1: string): null {
    SendCommand({
      id: 'SET_zCVobSoundDaytime-soundName2-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  activeSection(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobSoundDaytime-activeSection-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_activeSection(a1: number): null {
    SendCommand({
      id: 'SET_zCVobSoundDaytime-activeSection-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCVobSoundDaytime_OnInit(): zCVobSoundDaytime | null {
    const result: string | null = SendCommand({
      id: 'zCVobSoundDaytime-zCVobSoundDaytime_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCVobSoundDaytime(result) : null
  }

  ActivateSection(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCVobSoundDaytime-ActivateSection-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CalcTimeFrac(a1: number, a2: number, a3: number, a4: number): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobSoundDaytime-CalcTimeFrac-int-false-float,float,float,float&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
    })

    return result ? Number(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCVobSoundDaytime-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override GetZoneMotherClass(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCVobSoundDaytime-GetZoneMotherClass-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override GetDebugDescString(): string | null {
    const result: string | null = SendCommand({
      id: 'zCVobSoundDaytime-GetDebugDescString-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  override DoSoundUpdate(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCVobSoundDaytime-DoSoundUpdate-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }
}
