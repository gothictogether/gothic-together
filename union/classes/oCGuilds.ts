import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'

export class oCGuilds extends BaseUnionObject {
  size(): number | null {
    const result: string | null = SendCommand({
      id: 'oCGuilds-size-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_size(a1: number): null {
    SendCommand({
      id: 'SET_oCGuilds-size-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCGuilds_OnInit(): oCGuilds | null {
    const result: string | null = SendCommand({
      id: 'oCGuilds-oCGuilds_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCGuilds(result) : null
  }

  GetAttitude(a1: number, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCGuilds-GetAttitude-int-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  SetAttitude(a1: number, a2: number, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCGuilds-SetAttitude-void-false-int,int,unsigned char',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  InitGuildTable(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCGuilds-InitGuildTable-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetGuildName(a1: number): string | null {
    const result: string | null = SendCommand({
      id: 'oCGuilds-GetGuildName-zSTRING-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? String(result) : null
  }
}
