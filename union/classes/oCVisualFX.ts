import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zVEC3 } from './index.js'
import { TEaseFunc } from './index.js'
import { TTrjLoopMode } from './index.js'
import { zTVFXState } from './index.js'
import { oCVisualFXAI } from './index.js'
import { oCTrajectory } from './index.js'
import { zCEarthquake } from './index.js'
import { zCVobScreenFX } from './index.js'
import { zCModelNodeInst } from './index.js'
import { zCVob } from './index.js'
import { zCVobLight } from './index.js'
import { oCEmitterKey } from './index.js'
import { zSVisualFXColl } from './index.js'
import { zCClassDef } from './index.js'
import { zCEffect } from './index.js'

export class oCVisualFX extends zCEffect {
  visName_S(): string | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-visName_S-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_visName_S(a1: string): null {
    SendCommand({
      id: 'SET_oCVisualFX-visName_S-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  visSize_S(): string | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-visSize_S-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_visSize_S(a1: string): null {
    SendCommand({
      id: 'SET_oCVisualFX-visSize_S-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  visAlpha(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-visAlpha-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_visAlpha(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-visAlpha-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  visAlphaBlendFunc_S(): string | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-visAlphaBlendFunc_S-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_visAlphaBlendFunc_S(a1: string): null {
    SendCommand({
      id: 'SET_oCVisualFX-visAlphaBlendFunc_S-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  visTexAniFPS(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-visTexAniFPS-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_visTexAniFPS(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-visTexAniFPS-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  visTexAniIsLooping(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-visTexAniIsLooping-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_visTexAniIsLooping(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-visTexAniIsLooping-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emTrjMode_S(): string | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emTrjMode_S-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_emTrjMode_S(a1: string): null {
    SendCommand({
      id: 'SET_oCVisualFX-emTrjMode_S-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emTrjOriginNode_S(): string | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emTrjOriginNode_S-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_emTrjOriginNode_S(a1: string): null {
    SendCommand({
      id: 'SET_oCVisualFX-emTrjOriginNode_S-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emTrjTargetNode_S(): string | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emTrjTargetNode_S-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_emTrjTargetNode_S(a1: string): null {
    SendCommand({
      id: 'SET_oCVisualFX-emTrjTargetNode_S-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emTrjTargetRange(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emTrjTargetRange-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_emTrjTargetRange(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-emTrjTargetRange-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emTrjTargetAzi(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emTrjTargetAzi-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_emTrjTargetAzi(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-emTrjTargetAzi-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emTrjTargetElev(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emTrjTargetElev-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_emTrjTargetElev(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-emTrjTargetElev-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emTrjNumKeys(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emTrjNumKeys-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_emTrjNumKeys(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-emTrjNumKeys-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emTrjNumKeysVar(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emTrjNumKeysVar-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_emTrjNumKeysVar(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-emTrjNumKeysVar-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emTrjAngleElevVar(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emTrjAngleElevVar-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_emTrjAngleElevVar(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-emTrjAngleElevVar-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emTrjAngleHeadVar(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emTrjAngleHeadVar-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_emTrjAngleHeadVar(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-emTrjAngleHeadVar-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emTrjKeyDistVar(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emTrjKeyDistVar-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_emTrjKeyDistVar(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-emTrjKeyDistVar-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emTrjLoopMode_S(): string | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emTrjLoopMode_S-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_emTrjLoopMode_S(a1: string): null {
    SendCommand({
      id: 'SET_oCVisualFX-emTrjLoopMode_S-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emTrjEaseFunc_S(): string | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emTrjEaseFunc_S-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_emTrjEaseFunc_S(a1: string): null {
    SendCommand({
      id: 'SET_oCVisualFX-emTrjEaseFunc_S-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emTrjEaseVel(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emTrjEaseVel-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_emTrjEaseVel(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-emTrjEaseVel-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emTrjDynUpdateDelay(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emTrjDynUpdateDelay-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_emTrjDynUpdateDelay(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-emTrjDynUpdateDelay-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emTrjDynUpdateTargetOnly(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emTrjDynUpdateTargetOnly-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_emTrjDynUpdateTargetOnly(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-emTrjDynUpdateTargetOnly-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emFXCreate_S(): string | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emFXCreate_S-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_emFXCreate_S(a1: string): null {
    SendCommand({
      id: 'SET_oCVisualFX-emFXCreate_S-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emFXInvestOrigin_S(): string | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emFXInvestOrigin_S-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_emFXInvestOrigin_S(a1: string): null {
    SendCommand({
      id: 'SET_oCVisualFX-emFXInvestOrigin_S-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emFXInvestTarget_S(): string | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emFXInvestTarget_S-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_emFXInvestTarget_S(a1: string): null {
    SendCommand({
      id: 'SET_oCVisualFX-emFXInvestTarget_S-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emFXTriggerDelay(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emFXTriggerDelay-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_emFXTriggerDelay(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-emFXTriggerDelay-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emFXCreatedOwnTrj(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emFXCreatedOwnTrj-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_emFXCreatedOwnTrj(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-emFXCreatedOwnTrj-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emActionCollDyn_S(): string | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emActionCollDyn_S-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_emActionCollDyn_S(a1: string): null {
    SendCommand({
      id: 'SET_oCVisualFX-emActionCollDyn_S-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emActionCollStat_S(): string | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emActionCollStat_S-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_emActionCollStat_S(a1: string): null {
    SendCommand({
      id: 'SET_oCVisualFX-emActionCollStat_S-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emFXCollStat_S(): string | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emFXCollStat_S-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_emFXCollStat_S(a1: string): null {
    SendCommand({
      id: 'SET_oCVisualFX-emFXCollStat_S-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emFXCollDyn_S(): string | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emFXCollDyn_S-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_emFXCollDyn_S(a1: string): null {
    SendCommand({
      id: 'SET_oCVisualFX-emFXCollDyn_S-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emFXCollDynPerc_S(): string | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emFXCollDynPerc_S-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_emFXCollDynPerc_S(a1: string): null {
    SendCommand({
      id: 'SET_oCVisualFX-emFXCollDynPerc_S-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emFXCollStatAlign_S(): string | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emFXCollStatAlign_S-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_emFXCollStatAlign_S(a1: string): null {
    SendCommand({
      id: 'SET_oCVisualFX-emFXCollStatAlign_S-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emFXCollDynAlign_S(): string | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emFXCollDynAlign_S-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_emFXCollDynAlign_S(a1: string): null {
    SendCommand({
      id: 'SET_oCVisualFX-emFXCollDynAlign_S-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emFXLifeSpan(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emFXLifeSpan-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_emFXLifeSpan(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-emFXLifeSpan-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emCheckCollision(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emCheckCollision-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_emCheckCollision(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-emCheckCollision-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emAdjustShpToOrigin(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emAdjustShpToOrigin-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_emAdjustShpToOrigin(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-emAdjustShpToOrigin-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emInvestNextKeyDuration(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emInvestNextKeyDuration-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_emInvestNextKeyDuration(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-emInvestNextKeyDuration-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emFlyGravity(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emFlyGravity-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_emFlyGravity(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-emFlyGravity-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emSelfRotVel_S(): string | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emSelfRotVel_S-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_emSelfRotVel_S(a1: string): null {
    SendCommand({
      id: 'SET_oCVisualFX-emSelfRotVel_S-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lightPresetName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-lightPresetName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_lightPresetName(a1: string): null {
    SendCommand({
      id: 'SET_oCVisualFX-lightPresetName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  sfxID(): string | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-sfxID-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_sfxID(a1: string): null {
    SendCommand({
      id: 'SET_oCVisualFX-sfxID-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  sfxIsAmbient(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-sfxIsAmbient-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_sfxIsAmbient(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-sfxIsAmbient-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  sendAssessMagic(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-sendAssessMagic-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_sendAssessMagic(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-sendAssessMagic-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  secsPerDamage(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-secsPerDamage-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_secsPerDamage(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-secsPerDamage-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  dScriptEnd(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-dScriptEnd-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_dScriptEnd(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-dScriptEnd-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  visSize(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-visSize-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_visSize(a1: zVEC3): null {
    SendCommand({
      id: 'SET_oCVisualFX-visSize-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emTrjMode(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emTrjMode-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_emTrjMode(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-emTrjMode-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emActionCollDyn(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emActionCollDyn-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_emActionCollDyn(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-emActionCollDyn-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emActionCollStat(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emActionCollStat-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_emActionCollStat(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-emActionCollStat-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emSelfRotVel(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emSelfRotVel-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_emSelfRotVel(a1: zVEC3): null {
    SendCommand({
      id: 'SET_oCVisualFX-emSelfRotVel-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emTrjEaseFunc(): TEaseFunc | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emTrjEaseFunc-TEaseFunc-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_emTrjEaseFunc(a1: TEaseFunc): null {
    SendCommand({
      id: 'SET_oCVisualFX-emTrjEaseFunc-TEaseFunc-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emTrjLoopMode(): TTrjLoopMode | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emTrjLoopMode-TTrjLoopMode-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_emTrjLoopMode(a1: TTrjLoopMode): null {
    SendCommand({
      id: 'SET_oCVisualFX-emTrjLoopMode-TTrjLoopMode-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fxState(): zTVFXState | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-fxState-zTVFXState-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fxState(a1: zTVFXState): null {
    SendCommand({
      id: 'SET_oCVisualFX-fxState-zTVFXState-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  root(): oCVisualFX | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-root-oCVisualFX*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCVisualFX(result) : null
  }

  set_root(a1: oCVisualFX): null {
    SendCommand({
      id: 'SET_oCVisualFX-root-oCVisualFX*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  parent(): oCVisualFX | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-parent-oCVisualFX*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCVisualFX(result) : null
  }

  set_parent(a1: oCVisualFX): null {
    SendCommand({
      id: 'SET_oCVisualFX-parent-oCVisualFX*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fxInvestOrigin(): oCVisualFX | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-fxInvestOrigin-oCVisualFX*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCVisualFX(result) : null
  }

  set_fxInvestOrigin(a1: oCVisualFX): null {
    SendCommand({
      id: 'SET_oCVisualFX-fxInvestOrigin-oCVisualFX*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fxInvestTarget(): oCVisualFX | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-fxInvestTarget-oCVisualFX*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCVisualFX(result) : null
  }

  set_fxInvestTarget(a1: oCVisualFX): null {
    SendCommand({
      id: 'SET_oCVisualFX-fxInvestTarget-oCVisualFX*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  ai(): oCVisualFXAI | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-ai-oCVisualFXAI*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCVisualFXAI(result) : null
  }

  set_ai(a1: oCVisualFXAI): null {
    SendCommand({
      id: 'SET_oCVisualFX-ai-oCVisualFXAI*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  trajectory(): oCTrajectory | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-trajectory-oCTrajectory-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCTrajectory(result) : null
  }

  set_trajectory(a1: oCTrajectory): null {
    SendCommand({
      id: 'SET_oCVisualFX-trajectory-oCTrajectory-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  earthQuake(): zCEarthquake | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-earthQuake-zCEarthquake*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCEarthquake(result) : null
  }

  set_earthQuake(a1: zCEarthquake): null {
    SendCommand({
      id: 'SET_oCVisualFX-earthQuake-zCEarthquake*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  screenFX(): zCVobScreenFX | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-screenFX-zCVobScreenFX*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVobScreenFX(result) : null
  }

  set_screenFX(a1: zCVobScreenFX): null {
    SendCommand({
      id: 'SET_oCVisualFX-screenFX-zCVobScreenFX*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  screenFXTime(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-screenFXTime-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_screenFXTime(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-screenFXTime-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  screenFXDir(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-screenFXDir-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_screenFXDir(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-screenFXDir-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  orgNode(): zCModelNodeInst | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-orgNode-zCModelNodeInst*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCModelNodeInst(result) : null
  }

  set_orgNode(a1: zCModelNodeInst): null {
    SendCommand({
      id: 'SET_oCVisualFX-orgNode-zCModelNodeInst*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  targetNode(): zCModelNodeInst | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-targetNode-zCModelNodeInst*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCModelNodeInst(result) : null
  }

  set_targetNode(a1: zCModelNodeInst): null {
    SendCommand({
      id: 'SET_oCVisualFX-targetNode-zCModelNodeInst*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  origin(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-origin-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_origin(a1: zCVob): null {
    SendCommand({
      id: 'SET_oCVisualFX-origin-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  inflictor(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-inflictor-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_inflictor(a1: zCVob): null {
    SendCommand({
      id: 'SET_oCVisualFX-inflictor-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  target(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-target-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_target(a1: zCVob): null {
    SendCommand({
      id: 'SET_oCVisualFX-target-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  light(): zCVobLight | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-light-zCVobLight*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVobLight(result) : null
  }

  set_light(a1: zCVobLight): null {
    SendCommand({
      id: 'SET_oCVisualFX-light-zCVobLight*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lightRange(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-lightRange-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_lightRange(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-lightRange-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  sfxHnd(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-sfxHnd-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_sfxHnd(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-sfxHnd-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fxName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-fxName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_fxName(a1: string): null {
    SendCommand({
      id: 'SET_oCVisualFX-fxName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fxBackup(): oCEmitterKey | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-fxBackup-oCEmitterKey*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCEmitterKey(result) : null
  }

  set_fxBackup(a1: oCEmitterKey): null {
    SendCommand({
      id: 'SET_oCVisualFX-fxBackup-oCEmitterKey*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lastSetKey(): oCEmitterKey | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-lastSetKey-oCEmitterKey*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCEmitterKey(result) : null
  }

  set_lastSetKey(a1: oCEmitterKey): null {
    SendCommand({
      id: 'SET_oCVisualFX-lastSetKey-oCEmitterKey*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  actKey(): oCEmitterKey | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-actKey-oCEmitterKey*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCEmitterKey(result) : null
  }

  set_actKey(a1: oCEmitterKey): null {
    SendCommand({
      id: 'SET_oCVisualFX-actKey-oCEmitterKey*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  frameTime(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-frameTime-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_frameTime(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-frameTime-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  collisionTime(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-collisionTime-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_collisionTime(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-collisionTime-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  deleteTime(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-deleteTime-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_deleteTime(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-deleteTime-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  damageTime(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-damageTime-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_damageTime(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-damageTime-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  targetPos(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-targetPos-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_targetPos(a1: zVEC3): null {
    SendCommand({
      id: 'SET_oCVisualFX-targetPos-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lastTrjDir(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-lastTrjDir-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_lastTrjDir(a1: zVEC3): null {
    SendCommand({
      id: 'SET_oCVisualFX-lastTrjDir-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  keySize(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-keySize-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_keySize(a1: zVEC3): null {
    SendCommand({
      id: 'SET_oCVisualFX-keySize-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  actSize(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-actSize-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_actSize(a1: zVEC3): null {
    SendCommand({
      id: 'SET_oCVisualFX-actSize-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  castEndSize(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-castEndSize-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_castEndSize(a1: zVEC3): null {
    SendCommand({
      id: 'SET_oCVisualFX-castEndSize-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  nextLevelTime(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-nextLevelTime-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_nextLevelTime(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-nextLevelTime-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  easeTime(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-easeTime-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_easeTime(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-easeTime-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  age(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-age-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_age(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-age-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  trjUpdateTime(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-trjUpdateTime-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_trjUpdateTime(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-trjUpdateTime-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emTrjDist(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emTrjDist-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_emTrjDist(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-emTrjDist-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  trjSign(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-trjSign-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_trjSign(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-trjSign-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  levelTime(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-levelTime-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_levelTime(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-levelTime-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lifeSpanTimer(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-lifeSpanTimer-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_lifeSpanTimer(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-lifeSpanTimer-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fxStartTime(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-fxStartTime-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fxStartTime(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-fxStartTime-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  oldFovX(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-oldFovX-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_oldFovX(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-oldFovX-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  oldFovY(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-oldFovY-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_oldFovY(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-oldFovY-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  collisionOccured(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-collisionOccured-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_collisionOccured(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-collisionOccured-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  override showVisual(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-showVisual-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override set_showVisual(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-showVisual-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  isChild(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-isChild-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_isChild(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-isChild-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  isDeleted(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-isDeleted-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_isDeleted(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-isDeleted-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  initialized(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-initialized-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_initialized(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-initialized-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  shouldDelete(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-shouldDelete-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_shouldDelete(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-shouldDelete-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lightning(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-lightning-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_lightning(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-lightning-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fxInvestOriginInitialized(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-fxInvestOriginInitialized-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fxInvestOriginInitialized(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-fxInvestOriginInitialized-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fxInvestTargetInitialized(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-fxInvestTargetInitialized-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fxInvestTargetInitialized(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-fxInvestTargetInitialized-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fxInvestStopped(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-fxInvestStopped-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fxInvestStopped(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-fxInvestStopped-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  timeScaled(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-timeScaled-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_timeScaled(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-timeScaled-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fovMorph(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-fovMorph-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fovMorph(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-fovMorph-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  level(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-level-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_level(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-level-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  collisionCtr(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-collisionCtr-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_collisionCtr(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-collisionCtr-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  queueSetLevel(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-queueSetLevel-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_queueSetLevel(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-queueSetLevel-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  damage(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-damage-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_damage(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-damage-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  damageType(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-damageType-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_damageType(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-damageType-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  spellType(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-spellType-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_spellType(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-spellType-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  spellCat(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-spellCat-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_spellCat(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-spellCat-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  spellTargetTypes(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-spellTargetTypes-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_spellTargetTypes(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-spellTargetTypes-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  savePpsValue(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-savePpsValue-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_savePpsValue(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-savePpsValue-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  ringPos(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-ringPos-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_ringPos(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-ringPos-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emTrjFollowHitLastCheck(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-emTrjFollowHitLastCheck-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_emTrjFollowHitLastCheck(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-emTrjFollowHitLastCheck-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  bIsProjectile(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-bIsProjectile-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_bIsProjectile(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-bIsProjectile-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  bPfxMeshSetByVisualFX(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-bPfxMeshSetByVisualFX-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_bPfxMeshSetByVisualFX(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-bPfxMeshSetByVisualFX-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_bAllowMovement(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-m_bAllowMovement-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_bAllowMovement(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-m_bAllowMovement-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_fSleepTimer(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-m_fSleepTimer-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_fSleepTimer(a1: number): null {
    SendCommand({
      id: 'SET_oCVisualFX-m_fSleepTimer-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCVisualFX_OnInit(): oCVisualFX | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-oCVisualFX_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCVisualFX(result) : null
  }

  CleanUpCriticalFX(): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-CleanUpCriticalFX-void-false-',
      parentId: this.Uuid,
    })
  }

  CreateHierachy(): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-CreateHierachy-void-false-',
      parentId: this.Uuid,
    })
  }

  DisposeHierachy(): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-DisposeHierachy-void-false-',
      parentId: this.Uuid,
    })
  }

  InitValues(): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-InitValues-void-false-',
      parentId: this.Uuid,
    })
  }

  ParseStrings(): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-ParseStrings-void-false-',
      parentId: this.Uuid,
    })
  }

  UpdateFXByEmitterKey(a1: oCEmitterKey): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-UpdateFXByEmitterKey-void-false-oCEmitterKey*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CreateBackup(): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-CreateBackup-void-false-',
      parentId: this.Uuid,
    })
  }

  FindKey(a1: string): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-FindKey-int-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  CreateAndCastFX(a1: string, a2: zCVob, a3: zCVob): oCVisualFX | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-CreateAndCastFX-oCVisualFX*-false-zSTRING const&,zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? new oCVisualFX(result) : null
  }

  CreateInvestFX(): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-CreateInvestFX-void-false-',
      parentId: this.Uuid,
    })
  }

  InitInvestFX(): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-InitInvestFX-void-false-',
      parentId: this.Uuid,
    })
  }

  StopInvestFX(): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-StopInvestFX-void-false-',
      parentId: this.Uuid,
    })
  }

  InitEffect(): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-InitEffect-void-false-',
      parentId: this.Uuid,
    })
  }

  EndEffect(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-EndEffect-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetShowVisual(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-SetShowVisual-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  ProcessQueuedCollisions(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-ProcessQueuedCollisions-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  ProcessCollision(a1: zSVisualFXColl): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-ProcessCollision-int-false-zSVisualFXColl&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  CheckDeletion(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-CheckDeletion-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  UpdateActKey(): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-UpdateActKey-void-false-',
      parentId: this.Uuid,
    })
  }

  DoMovements(): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-DoMovements-void-false-',
      parentId: this.Uuid,
    })
  }

  CheckDelayedTrigger(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-CheckDelayedTrigger-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  AdjustShapeToOrigin(): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-AdjustShapeToOrigin-void-false-',
      parentId: this.Uuid,
    })
  }

  UpdateEffects(): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-UpdateEffects-void-false-',
      parentId: this.Uuid,
    })
  }

  CalcPFXMesh(): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-CalcPFXMesh-void-false-',
      parentId: this.Uuid,
    })
  }

  ReleasePFXMesh(): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-ReleasePFXMesh-void-false-',
      parentId: this.Uuid,
    })
  }

  Edit(): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-Edit-void-false-',
      parentId: this.Uuid,
    })
  }

  SetupEmitterKeysByVisual(): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-SetupEmitterKeysByVisual-void-false-',
      parentId: this.Uuid,
    })
  }

  GetName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-GetName-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override OnTick(): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-OnTick-void-false-',
      parentId: this.Uuid,
    })
  }

  override CanThisCollideWith(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-CanThisCollideWith-int-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  Open(): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-Open-void-false-',
      parentId: this.Uuid,
    })
  }

  SetOrigin(a1: zCVob, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-SetOrigin-void-false-zCVob*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  SetTarget(a1: zCVob, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-SetTarget-void-false-zCVob*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  SetTarget2(a1: zVEC3, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-SetTarget-void-false-zVEC3&,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  SetInflictor(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-SetInflictor-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetOrigin(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-GetOrigin-zCVob*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  GetTarget(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-GetTarget-zCVob*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  GetInflictor(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-GetInflictor-zCVob*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  Init(a1: zCVob, a2: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-Init-void-false-zCVob*,zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  Init2(a1: zCVob, a2: zCVob, a3: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-Init-void-false-zCVob*,zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  InvestNext(): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-InvestNext-void-false-',
      parentId: this.Uuid,
    })
  }

  SetLevel(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-SetLevel-void-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  GetLevel(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-GetLevel-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  Cast(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-Cast-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  Stop(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-Stop-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  Kill(): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-Kill-void-false-',
      parentId: this.Uuid,
    })
  }

  CanBeDeleted(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-CanBeDeleted-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsFinished(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-IsFinished-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsLooping(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-IsLooping-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetByScript(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-SetByScript-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetDuration(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-SetDuration-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  Reset(): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-Reset-void-false-',
      parentId: this.Uuid,
    })
  }

  ResetForEditing(): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-ResetForEditing-void-false-',
      parentId: this.Uuid,
    })
  }

  SetCollisionEnabled(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-SetCollisionEnabled-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetNumCollisionCandidates(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-GetNumCollisionCandidates-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetDamage(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-SetDamage-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetDamageType(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-SetDamageType-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetDamage(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-GetDamage-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetDamageType(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-GetDamageType-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsASpell(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-IsASpell-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetSpellType(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-SetSpellType-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetSpellType(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-GetSpellType-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetSpellCat(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-SetSpellCat-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetSpellCat(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-GetSpellCat-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetSpellTargetTypes(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-GetSpellTargetTypes-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetSpellTargetTypes(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-SetSpellTargetTypes-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetSendsAssessMagic(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-GetSendsAssessMagic-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetSendsAssessMagic(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-SetSendsAssessMagic-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override GetIsProjectile(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-GetIsProjectile-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  SetIsProjectile(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-SetIsProjectile-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetVisualByString(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-SetVisualByString-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  Collide(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-Collide-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CollisionResponse(a1: zVEC3, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisualFX-CollisionResponse-void-false-zVEC3 const&,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }
}
