import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { oCNpc } from './index.js'
import { zCClassDef } from './index.js'
import { zCEventMessage } from './index.js'
import { zCVob } from './index.js'
import { oCMobInter } from './index.js'

export class oCMobLockable extends oCMobInter {
  locked(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobLockable-locked-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_locked(a1: number): null {
    SendCommand({
      id: 'SET_oCMobLockable-locked-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  autoOpen(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobLockable-autoOpen-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_autoOpen(a1: number): null {
    SendCommand({
      id: 'SET_oCMobLockable-autoOpen-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pickLockNr(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobLockable-pickLockNr-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_pickLockNr(a1: number): null {
    SendCommand({
      id: 'SET_oCMobLockable-pickLockNr-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  keyInstance(): string | null {
    const result: string | null = SendCommand({
      id: 'oCMobLockable-keyInstance-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_keyInstance(a1: string): null {
    SendCommand({
      id: 'SET_oCMobLockable-keyInstance-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pickLockStr(): string | null {
    const result: string | null = SendCommand({
      id: 'oCMobLockable-pickLockStr-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_pickLockStr(a1: string): null {
    SendCommand({
      id: 'SET_oCMobLockable-pickLockStr-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  CanOpen(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobLockable-CanOpen-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCMobLockable-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override OnMessage(a1: zCEventMessage, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobLockable-OnMessage-void-false-zCEventMessage*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override Interact(
    a1: oCNpc,
    a2: number,
    a3: number,
    a4: number,
    a5: number,
    a6: number,
  ): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobLockable-Interact-void-false-oCNpc*,int,int,int,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
      a5: a5.toString(),
      a6: a6.toString(),
    })
  }

  override CanInteractWith(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobLockable-CanInteractWith-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  override CanChangeState(a1: oCNpc, a2: number, a3: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobLockable-CanChangeState-int-false-oCNpc*,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? Number(result) : null
  }

  override OnEndStateChange(a1: oCNpc, a2: number, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobLockable-OnEndStateChange-void-false-oCNpc*,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  SetLocked(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobLockable-SetLocked-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetKeyInstance(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobLockable-SetKeyInstance-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetPickLockStr(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobLockable-SetPickLockStr-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  Open(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobLockable-Open-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  Close(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobLockable-Close-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  Lock(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobLockable-Lock-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  Unlock(a1: oCNpc, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobLockable-Unlock-void-false-oCNpc*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  PickLock(a1: oCNpc, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobLockable-PickLock-int-false-oCNpc*,char',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }
}
