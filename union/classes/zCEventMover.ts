import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { zCEventMessage } from './index.js'

export class zCEventMover extends zCEventMessage {
  gotoFixedKeyframe(): number | null {
    const result: string | null = SendCommand({
      id: 'zCEventMover-gotoFixedKeyframe-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_gotoFixedKeyframe(a1: number): null {
    SendCommand({
      id: 'SET_zCEventMover-gotoFixedKeyframe-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCEventMover-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override IsNetRelevant(): number | null {
    const result: string | null = SendCommand({
      id: 'zCEventMover-IsNetRelevant-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override MD_GetNumOfSubTypes(): number | null {
    const result: string | null = SendCommand({
      id: 'zCEventMover-MD_GetNumOfSubTypes-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override MD_GetSubTypeString(a1: number): string | null {
    const result: string | null = SendCommand({
      id: 'zCEventMover-MD_GetSubTypeString-zSTRING-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? String(result) : null
  }
}
