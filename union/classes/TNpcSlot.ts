import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { oCVob } from './index.js'

export class TNpcSlot extends BaseUnionObject {
  name(): string | null {
    const result: string | null = SendCommand({
      id: 'TNpcSlot-name-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_name(a1: string): null {
    SendCommand({
      id: 'SET_TNpcSlot-name-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  inInventory(): number | null {
    const result: string | null = SendCommand({
      id: 'TNpcSlot-inInventory-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_inInventory(a1: number): null {
    SendCommand({
      id: 'SET_TNpcSlot-inInventory-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  tmpLevel(): number | null {
    const result: string | null = SendCommand({
      id: 'TNpcSlot-tmpLevel-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_tmpLevel(a1: number): null {
    SendCommand({
      id: 'SET_TNpcSlot-tmpLevel-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  itemName(): string | null {
    const result: string | null = SendCommand({
      id: 'TNpcSlot-itemName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_itemName(a1: string): null {
    SendCommand({
      id: 'SET_TNpcSlot-itemName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  vob(): oCVob | null {
    const result: string | null = SendCommand({
      id: 'TNpcSlot-vob-oCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCVob(result) : null
  }

  set_vob(a1: oCVob): null {
    SendCommand({
      id: 'SET_TNpcSlot-vob-oCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  wasVobTreeWhenInserted(): number | null {
    const result: string | null = SendCommand({
      id: 'TNpcSlot-wasVobTreeWhenInserted-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_wasVobTreeWhenInserted(a1: number): null {
    SendCommand({
      id: 'SET_TNpcSlot-wasVobTreeWhenInserted-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static TNpcSlot_OnInit(): TNpcSlot | null {
    const result: string | null = SendCommand({
      id: 'TNpcSlot-TNpcSlot_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new TNpcSlot(result) : null
  }

  SetVob(a1: oCVob): void | null {
    const result: string | null = SendCommand({
      id: 'TNpcSlot-SetVob-void-false-oCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  ClearVob(): void | null {
    const result: string | null = SendCommand({
      id: 'TNpcSlot-ClearVob-void-false-',
      parentId: this.Uuid,
    })
  }
}
