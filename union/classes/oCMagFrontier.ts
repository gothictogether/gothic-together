import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { oCVisualFX } from './index.js'
import { oCNpc } from './index.js'
import { zVEC3 } from './index.js'

export class oCMagFrontier extends BaseUnionObject {
  warningFX(): oCVisualFX | null {
    const result: string | null = SendCommand({
      id: 'oCMagFrontier-warningFX-oCVisualFX*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCVisualFX(result) : null
  }

  set_warningFX(a1: oCVisualFX): null {
    SendCommand({
      id: 'SET_oCMagFrontier-warningFX-oCVisualFX*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  shootFX(): oCVisualFX | null {
    const result: string | null = SendCommand({
      id: 'oCMagFrontier-shootFX-oCVisualFX*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCVisualFX(result) : null
  }

  set_shootFX(a1: oCVisualFX): null {
    SendCommand({
      id: 'SET_oCMagFrontier-shootFX-oCVisualFX*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  npc(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCMagFrontier-npc-oCNpc*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  set_npc(a1: oCNpc): null {
    SendCommand({
      id: 'SET_oCMagFrontier-npc-oCNpc*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  isWarning(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMagFrontier-isWarning-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_isWarning(a1: number): null {
    SendCommand({
      id: 'SET_oCMagFrontier-isWarning-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  isShooting(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMagFrontier-isShooting-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_isShooting(a1: number): null {
    SendCommand({
      id: 'SET_oCMagFrontier-isShooting-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCMagFrontier_OnInit(): oCMagFrontier | null {
    const result: string | null = SendCommand({
      id: 'oCMagFrontier-oCMagFrontier_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCMagFrontier(result) : null
  }

  SetNPC(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCMagFrontier-SetNPC-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  DoCheck(): void | null {
    const result: string | null = SendCommand({
      id: 'oCMagFrontier-DoCheck-void-false-',
      parentId: this.Uuid,
    })
  }

  GetDistanceNewWorld(a1: zVEC3, a2: number, a3: zVEC3): number | null {
    const result: string | null = SendCommand({
      id: 'oCMagFrontier-GetDistanceNewWorld-float-false-zVEC3 const&,float&,zVEC3&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? Number(result) : null
  }

  GetDistanceDragonIsland(a1: zVEC3, a2: number, a3: zVEC3): number | null {
    const result: string | null = SendCommand({
      id: 'oCMagFrontier-GetDistanceDragonIsland-float-false-zVEC3 const&,float&,zVEC3&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? Number(result) : null
  }

  GetDistanceAddonWorld(a1: zVEC3, a2: number, a3: zVEC3): number | null {
    const result: string | null = SendCommand({
      id: 'oCMagFrontier-GetDistanceAddonWorld-float-false-zVEC3 const&,float&,zVEC3&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? Number(result) : null
  }

  StartLightningAtPos(a1: zVEC3, a2: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'oCMagFrontier-StartLightningAtPos-void-false-zVEC3&,zVEC3&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  DoWarningFX(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCMagFrontier-DoWarningFX-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  DisposeWarningFX(): void | null {
    const result: string | null = SendCommand({
      id: 'oCMagFrontier-DisposeWarningFX-void-false-',
      parentId: this.Uuid,
    })
  }

  DoShootFX(a1: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'oCMagFrontier-DoShootFX-void-false-zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  DisposeShootFX(): void | null {
    const result: string | null = SendCommand({
      id: 'oCMagFrontier-DisposeShootFX-void-false-',
      parentId: this.Uuid,
    })
  }
}
