import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCVob } from './index.js'
import { zCPolygon } from './index.js'
import { zVEC3 } from './index.js'
import { zCVertex } from './index.js'

export class zTTraceRayReport extends BaseUnionObject {
  foundHit(): number | null {
    const result: string | null = SendCommand({
      id: 'zTTraceRayReport-foundHit-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_foundHit(a1: number): null {
    SendCommand({
      id: 'SET_zTTraceRayReport-foundHit-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  foundVob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'zTTraceRayReport-foundVob-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_foundVob(a1: zCVob): null {
    SendCommand({
      id: 'SET_zTTraceRayReport-foundVob-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  foundPoly(): zCPolygon | null {
    const result: string | null = SendCommand({
      id: 'zTTraceRayReport-foundPoly-zCPolygon*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCPolygon(result) : null
  }

  set_foundPoly(a1: zCPolygon): null {
    SendCommand({
      id: 'SET_zTTraceRayReport-foundPoly-zCPolygon*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  foundIntersection(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zTTraceRayReport-foundIntersection-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_foundIntersection(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zTTraceRayReport-foundIntersection-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  foundPolyNormal(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zTTraceRayReport-foundPolyNormal-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_foundPolyNormal(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zTTraceRayReport-foundPolyNormal-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  foundVertex(): zCVertex | null {
    const result: string | null = SendCommand({
      id: 'zTTraceRayReport-foundVertex-zCVertex*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVertex(result) : null
  }

  set_foundVertex(a1: zCVertex): null {
    SendCommand({
      id: 'SET_zTTraceRayReport-foundVertex-zCVertex*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
