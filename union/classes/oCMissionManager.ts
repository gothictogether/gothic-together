import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { oCMission } from './index.js'
import { oCNpc } from './index.js'

export class oCMissionManager extends BaseUnionObject {
  static oCMissionManager_OnInit(): oCMissionManager | null {
    const result: string | null = SendCommand({
      id: 'oCMissionManager-oCMissionManager_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCMissionManager(result) : null
  }

  ClearMissions(): void | null {
    const result: string | null = SendCommand({
      id: 'oCMissionManager-ClearMissions-void-false-',
      parentId: this.Uuid,
    })
  }

  GetMission(a1: number): oCMission | null {
    const result: string | null = SendCommand({
      id: 'oCMissionManager-GetMission-oCMission*-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new oCMission(result) : null
  }

  CreateAllMissions(): void | null {
    const result: string | null = SendCommand({
      id: 'oCMissionManager-CreateAllMissions-void-false-',
      parentId: this.Uuid,
    })
  }

  OfferThisMission(a1: oCNpc, a2: oCNpc, a3: number): oCMission | null {
    const result: string | null = SendCommand({
      id: 'oCMissionManager-OfferThisMission-oCMission*-false-oCNpc*,oCNpc*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? new oCMission(result) : null
  }

  GetAvailableMission(a1: oCNpc, a2: oCNpc, a3: number): oCMission | null {
    const result: string | null = SendCommand({
      id: 'oCMissionManager-GetAvailableMission-oCMission*-false-oCNpc*,oCNpc*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? new oCMission(result) : null
  }

  GetStatus(a1: oCNpc, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCMissionManager-GetStatus-int-false-oCNpc*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  SetStatus(a1: oCNpc, a2: number, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCMissionManager-SetStatus-void-false-oCNpc*,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  CancelMission(a1: oCNpc, a2: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCMissionManager-CancelMission-int-false-oCNpc*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? Number(result) : null
  }

  TestNextMission(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMissionManager-TestNextMission-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }
}
