import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCVob } from './index.js'
import { oCNpc } from './index.js'
import { oCRtnCutscene } from './index.js'

export class oCRtnEntry extends BaseUnionObject {
  hour1(): number | null {
    const result: string | null = SendCommand({
      id: 'oCRtnEntry-hour1-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_hour1(a1: number): null {
    SendCommand({
      id: 'SET_oCRtnEntry-hour1-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  min1(): number | null {
    const result: string | null = SendCommand({
      id: 'oCRtnEntry-min1-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_min1(a1: number): null {
    SendCommand({
      id: 'SET_oCRtnEntry-min1-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  hour2(): number | null {
    const result: string | null = SendCommand({
      id: 'oCRtnEntry-hour2-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_hour2(a1: number): null {
    SendCommand({
      id: 'SET_oCRtnEntry-hour2-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  min2(): number | null {
    const result: string | null = SendCommand({
      id: 'oCRtnEntry-min2-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_min2(a1: number): null {
    SendCommand({
      id: 'SET_oCRtnEntry-min2-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  f_script(): number | null {
    const result: string | null = SendCommand({
      id: 'oCRtnEntry-f_script-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_f_script(a1: number): null {
    SendCommand({
      id: 'SET_oCRtnEntry-f_script-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  wpname(): string | null {
    const result: string | null = SendCommand({
      id: 'oCRtnEntry-wpname-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_wpname(a1: string): null {
    SendCommand({
      id: 'SET_oCRtnEntry-wpname-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  instance(): number | null {
    const result: string | null = SendCommand({
      id: 'oCRtnEntry-instance-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_instance(a1: number): null {
    SendCommand({
      id: 'SET_oCRtnEntry-instance-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  poi(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCRtnEntry-poi-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_poi(a1: zCVob): null {
    SendCommand({
      id: 'SET_oCRtnEntry-poi-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  npc(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCRtnEntry-npc-oCNpc*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  set_npc(a1: oCNpc): null {
    SendCommand({
      id: 'SET_oCRtnEntry-npc-oCNpc*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  cutscene(): oCRtnCutscene | null {
    const result: string | null = SendCommand({
      id: 'oCRtnEntry-cutscene-oCRtnCutscene*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCRtnCutscene(result) : null
  }

  set_cutscene(a1: oCRtnCutscene): null {
    SendCommand({
      id: 'SET_oCRtnEntry-cutscene-oCRtnCutscene*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  overlay(): number | null {
    const result: string | null = SendCommand({
      id: 'oCRtnEntry-overlay-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_overlay(a1: number): null {
    SendCommand({
      id: 'SET_oCRtnEntry-overlay-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCRtnEntry_OnInit(): oCRtnEntry | null {
    const result: string | null = SendCommand({
      id: 'oCRtnEntry-oCRtnEntry_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCRtnEntry(result) : null
  }

  static oCRtnEntry_OnInit2(
    a1: number,
    a2: number,
    a3: number,
    a4: number,
    a5: number,
    a6: string,
    a7: number,
  ): oCRtnEntry | null {
    const result: string | null = SendCommand({
      id: 'oCRtnEntry-oCRtnEntry_OnInit-void-false-int,int,int,int,int,zSTRING const&,int',
      parentId: 'NULL',
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
      a5: a5.toString(),
      a6: a6.toString(),
      a7: a7.toString(),
    })

    return result ? new oCRtnEntry(result) : null
  }

  GetTime(a1: number, a2: number, a3: number, a4: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCRtnEntry-GetTime-void-false-int&,int&,int&,int&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
    })
  }

  SetTime(a1: number, a2: number, a3: number, a4: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCRtnEntry-SetTime-void-false-int,int,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
    })
  }

  GetState(): number | null {
    const result: string | null = SendCommand({
      id: 'oCRtnEntry-GetState-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetWaypoint(): string | null {
    const result: string | null = SendCommand({
      id: 'oCRtnEntry-GetWaypoint-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }
}
