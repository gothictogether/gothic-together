import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { oCNpc } from './index.js'
import { TMobOptPos } from './index.js'
import { oCMobInter } from './index.js'

export class oCMobLadder extends oCMobInter {
  Interacting(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobLadder-Interacting-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_Interacting(a1: number): null {
    SendCommand({
      id: 'SET_oCMobLadder-Interacting-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  PrevAction(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobLadder-PrevAction-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_PrevAction(a1: number): null {
    SendCommand({
      id: 'SET_oCMobLadder-PrevAction-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCMobLadder_OnInit(): oCMobLadder | null {
    const result: string | null = SendCommand({
      id: 'oCMobLadder-oCMobLadder_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCMobLadder(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCMobLadder-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override DoFocusCheckBBox(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobLadder-DoFocusCheckBBox-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override Interact(
    a1: oCNpc,
    a2: number,
    a3: number,
    a4: number,
    a5: number,
    a6: number,
  ): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobLadder-Interact-void-false-oCNpc*,int,int,int,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
      a5: a5.toString(),
      a6: a6.toString(),
    })
  }

  override EndInteraction(a1: oCNpc, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobLadder-EndInteraction-void-false-oCNpc*,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override CanInteractWith(a1: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobLadder-CanInteractWith-int-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  override StartInteraction(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCMobLadder-StartInteraction-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override CanChangeState(a1: oCNpc, a2: number, a3: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobLadder-CanChangeState-int-false-oCNpc*,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? Number(result) : null
  }

  SearchFreePositionLadder(a1: oCNpc, a2: number): TMobOptPos | null {
    const result: string | null = SendCommand({
      id: 'oCMobLadder-SearchFreePosition-TMobOptPos*-false-oCNpc*,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new TMobOptPos(result) : null
  }
}
