import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { oCNews } from './index.js'
import { oCNpc } from './index.js'

export class oCNewsMemory extends BaseUnionObject {
  static oCNewsMemory_OnInit(): oCNewsMemory | null {
    const result: string | null = SendCommand({
      id: 'oCNewsMemory-oCNewsMemory_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCNewsMemory(result) : null
  }

  Insert(a1: oCNews): void | null {
    const result: string | null = SendCommand({
      id: 'oCNewsMemory-Insert-void-false-oCNews*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  Remove(a1: oCNews): void | null {
    const result: string | null = SendCommand({
      id: 'oCNewsMemory-Remove-void-false-oCNews*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetNews(a1: number, a2: oCNpc, a3: oCNpc): oCNews | null {
    const result: string | null = SendCommand({
      id: 'oCNewsMemory-GetNews-oCNews*-false-int,oCNpc*,oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? new oCNews(result) : null
  }

  CheckSpreadNews(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNewsMemory-CheckSpreadNews-void-false-',
      parentId: this.Uuid,
    })
  }

  SearchNews(a1: number, a2: oCNpc, a3: oCNpc): number | null {
    const result: string | null = SendCommand({
      id: 'oCNewsMemory-SearchNews-int-false-int,oCNpc*,oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? Number(result) : null
  }

  ShowDebugInfos(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNewsMemory-ShowDebugInfos-void-false-',
      parentId: this.Uuid,
    })
  }

  GetNewsByNumber(a1: number): oCNews | null {
    const result: string | null = SendCommand({
      id: 'oCNewsMemory-GetNewsByNumber-oCNews*-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new oCNews(result) : null
  }

  ClearNews(): void | null {
    const result: string | null = SendCommand({
      id: 'oCNewsMemory-ClearNews-void-false-',
      parentId: this.Uuid,
    })
  }

  DeleteNewsByNumber(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCNewsMemory-DeleteNewsByNumber-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }
}
