import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { zCVob } from './index.js'
import { zCEffect } from './index.js'

export class zCPFXControler extends zCEffect {
  pfxName(): string | null {
    const result: string | null = SendCommand({
      id: 'zCPFXControler-pfxName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_pfxName(a1: string): null {
    SendCommand({
      id: 'SET_zCPFXControler-pfxName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  killVobWhenDone(): number | null {
    const result: string | null = SendCommand({
      id: 'zCPFXControler-killVobWhenDone-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_killVobWhenDone(a1: number): null {
    SendCommand({
      id: 'SET_zCPFXControler-killVobWhenDone-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pfxStartOn(): number | null {
    const result: string | null = SendCommand({
      id: 'zCPFXControler-pfxStartOn-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_pfxStartOn(a1: number): null {
    SendCommand({
      id: 'SET_zCPFXControler-pfxStartOn-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCPFXControler_OnInit(): zCPFXControler | null {
    const result: string | null = SendCommand({
      id: 'zCPFXControler-zCPFXControler_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCPFXControler(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCPFXControler-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override OnTrigger(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCPFXControler-OnTrigger-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override OnUntrigger(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCPFXControler-OnUntrigger-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override PostLoad(): void | null {
    const result: string | null = SendCommand({
      id: 'zCPFXControler-PostLoad-void-false-',
      parentId: this.Uuid,
    })
  }
}
