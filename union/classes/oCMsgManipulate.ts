import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCVob } from './index.js'
import { TManipulateSubType } from './index.js'
import { zCClassDef } from './index.js'
import { oCNpcMessage } from './index.js'

export class oCMsgManipulate extends oCNpcMessage {
  name(): string | null {
    const result: string | null = SendCommand({
      id: 'oCMsgManipulate-name-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_name(a1: string): null {
    SendCommand({
      id: 'SET_oCMsgManipulate-name-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  slot(): string | null {
    const result: string | null = SendCommand({
      id: 'oCMsgManipulate-slot-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_slot(a1: string): null {
    SendCommand({
      id: 'SET_oCMsgManipulate-slot-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  targetVob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCMsgManipulate-targetVob-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_targetVob(a1: zCVob): null {
    SendCommand({
      id: 'SET_oCMsgManipulate-targetVob-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  flag(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgManipulate-flag-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_flag(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgManipulate-flag-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  aniCombY(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgManipulate-aniCombY-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_aniCombY(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgManipulate-aniCombY-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  npcSlot(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgManipulate-npcSlot-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_npcSlot(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgManipulate-npcSlot-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  targetState(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgManipulate-targetState-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_targetState(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgManipulate-targetState-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  aniID(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgManipulate-aniID-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_aniID(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgManipulate-aniID-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCMsgManipulate_OnInit(): oCMsgManipulate | null {
    const result: string | null = SendCommand({
      id: 'oCMsgManipulate-oCMsgManipulate_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCMsgManipulate(result) : null
  }

  static oCMsgManipulate_OnInit2(a1: TManipulateSubType): oCMsgManipulate | null {
    const result: string | null = SendCommand({
      id: 'oCMsgManipulate-oCMsgManipulate_OnInit-void-false-TManipulateSubType',
      parentId: 'NULL',
      a1: a1.toString(),
    })

    return result ? new oCMsgManipulate(result) : null
  }

  static oCMsgManipulate_OnInit3(a1: TManipulateSubType, a2: zCVob): oCMsgManipulate | null {
    const result: string | null = SendCommand({
      id: 'oCMsgManipulate-oCMsgManipulate_OnInit-void-false-TManipulateSubType,zCVob*',
      parentId: 'NULL',
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCMsgManipulate(result) : null
  }

  static oCMsgManipulate_OnInit4(a1: TManipulateSubType, a2: number): oCMsgManipulate | null {
    const result: string | null = SendCommand({
      id: 'oCMsgManipulate-oCMsgManipulate_OnInit-void-false-TManipulateSubType,int',
      parentId: 'NULL',
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCMsgManipulate(result) : null
  }

  static oCMsgManipulate_OnInit5(
    a1: TManipulateSubType,
    a2: string,
    a3: number,
  ): oCMsgManipulate | null {
    const result: string | null = SendCommand({
      id: 'oCMsgManipulate-oCMsgManipulate_OnInit-void-false-TManipulateSubType,zSTRING const&,int',
      parentId: 'NULL',
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? new oCMsgManipulate(result) : null
  }

  static oCMsgManipulate_OnInit6(
    a1: TManipulateSubType,
    a2: zCVob,
    a3: number,
  ): oCMsgManipulate | null {
    const result: string | null = SendCommand({
      id: 'oCMsgManipulate-oCMsgManipulate_OnInit-void-false-TManipulateSubType,zCVob*,int',
      parentId: 'NULL',
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? new oCMsgManipulate(result) : null
  }

  static oCMsgManipulate_OnInit7(
    a1: TManipulateSubType,
    a2: string,
    a3: string,
  ): oCMsgManipulate | null {
    const result: string | null = SendCommand({
      id: 'oCMsgManipulate-oCMsgManipulate_OnInit-void-false-TManipulateSubType,zSTRING const&,zSTRING const&',
      parentId: 'NULL',
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? new oCMsgManipulate(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCMsgManipulate-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override IsNetRelevant(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgManipulate-IsNetRelevant-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override MD_GetNumOfSubTypes(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgManipulate-MD_GetNumOfSubTypes-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override MD_GetSubTypeString(a1: number): string | null {
    const result: string | null = SendCommand({
      id: 'oCMsgManipulate-MD_GetSubTypeString-zSTRING-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? String(result) : null
  }

  override MD_GetVobRefName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCMsgManipulate-MD_GetVobRefName-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  override MD_SetVobRefName(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCMsgManipulate-MD_SetVobRefName-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override MD_SetVobParam(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'oCMsgManipulate-MD_SetVobParam-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override MD_GetMinTime(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgManipulate-MD_GetMinTime-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }
}
