import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zTBBox3D } from './index.js'
import { zVEC3 } from './index.js'
import { zCPolygon } from './index.js'

export class zCPatchMap extends BaseUnionObject {
  hit(): number | null {
    const result: string | null = SendCommand({
      id: 'zCPatchMap-hit-char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_hit(a1: number): null {
    SendCommand({
      id: 'SET_zCPatchMap-hit-char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  xdim(): number | null {
    const result: string | null = SendCommand({
      id: 'zCPatchMap-xdim-short-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_xdim(a1: number): null {
    SendCommand({
      id: 'SET_zCPatchMap-xdim-short-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  ydim(): number | null {
    const result: string | null = SendCommand({
      id: 'zCPatchMap-ydim-short-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_ydim(a1: number): null {
    SendCommand({
      id: 'SET_zCPatchMap-ydim-short-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  bbox3D(): zTBBox3D | null {
    const result: string | null = SendCommand({
      id: 'zCPatchMap-bbox3D-zTBBox3D-false-false',
      parentId: this.Uuid,
    })

    return result ? new zTBBox3D(result) : null
  }

  set_bbox3D(a1: zTBBox3D): null {
    SendCommand({
      id: 'SET_zCPatchMap-bbox3D-zTBBox3D-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lightmapOrigin(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCPatchMap-lightmapOrigin-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_lightmapOrigin(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCPatchMap-lightmapOrigin-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lightmapUp(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCPatchMap-lightmapUp-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_lightmapUp(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCPatchMap-lightmapUp-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lightmapRight(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCPatchMap-lightmapRight-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_lightmapRight(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zCPatchMap-lightmapRight-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lastRayHitPoly(): zCPolygon | null {
    const result: string | null = SendCommand({
      id: 'zCPatchMap-lastRayHitPoly-zCPolygon*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCPolygon(result) : null
  }

  set_lastRayHitPoly(a1: zCPolygon): null {
    SendCommand({
      id: 'SET_zCPatchMap-lastRayHitPoly-zCPolygon*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
