import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zTDamageCollType } from './index.js'
import { zCVob } from './index.js'
import { zCClassDef } from './index.js'
import { zCEffect } from './index.js'

export class zCTouchDamage extends zCEffect {
  damage(): number | null {
    const result: string | null = SendCommand({
      id: 'zCTouchDamage-damage-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_damage(a1: number): null {
    SendCommand({
      id: 'SET_zCTouchDamage-damage-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  damageType(): number | null {
    const result: string | null = SendCommand({
      id: 'zCTouchDamage-damageType-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_damageType(a1: number): null {
    SendCommand({
      id: 'SET_zCTouchDamage-damageType-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  damageRepeatDelaySec(): number | null {
    const result: string | null = SendCommand({
      id: 'zCTouchDamage-damageRepeatDelaySec-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_damageRepeatDelaySec(a1: number): null {
    SendCommand({
      id: 'SET_zCTouchDamage-damageRepeatDelaySec-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  damageVolDownScale(): number | null {
    const result: string | null = SendCommand({
      id: 'zCTouchDamage-damageVolDownScale-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_damageVolDownScale(a1: number): null {
    SendCommand({
      id: 'SET_zCTouchDamage-damageVolDownScale-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  damageCollType(): zTDamageCollType | null {
    const result: string | null = SendCommand({
      id: 'zCTouchDamage-damageCollType-zTDamageCollType-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_damageCollType(a1: zTDamageCollType): null {
    SendCommand({
      id: 'SET_zCTouchDamage-damageCollType-zTDamageCollType-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCTouchDamage_OnInit(): zCTouchDamage | null {
    const result: string | null = SendCommand({
      id: 'zCTouchDamage-zCTouchDamage_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCTouchDamage(result) : null
  }

  ProcessToucher(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCTouchDamage-ProcessToucher-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  TestCollType(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'zCTouchDamage-TestCollType-int-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  FireDamageMessage(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCTouchDamage-FireDamageMessage-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetVobProperties(): void | null {
    const result: string | null = SendCommand({
      id: 'zCTouchDamage-SetVobProperties-void-false-',
      parentId: this.Uuid,
    })
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCTouchDamage-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override OnTouch(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCTouchDamage-OnTouch-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override OnUntouch(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCTouchDamage-OnUntouch-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override OnTimer(): void | null {
    const result: string | null = SendCommand({
      id: 'zCTouchDamage-OnTimer-void-false-',
      parentId: this.Uuid,
    })
  }

  GetDamageTypeArcEnum(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'zCTouchDamage-GetDamageTypeArcEnum-char-false-unsigned long',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }
}
