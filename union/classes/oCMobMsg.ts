import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { oCNpc } from './index.js'
import { TMobMsgSubType } from './index.js'
import { zCClassDef } from './index.js'
import { zCEventMessage } from './index.js'

export class oCMobMsg extends zCEventMessage {
  npc(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCMobMsg-npc-oCNpc*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  set_npc(a1: oCNpc): null {
    SendCommand({
      id: 'SET_oCMobMsg-npc-oCNpc*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  from(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobMsg-from-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_from(a1: number): null {
    SendCommand({
      id: 'SET_oCMobMsg-from-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  to(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobMsg-to-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_to(a1: number): null {
    SendCommand({
      id: 'SET_oCMobMsg-to-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  playAni(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobMsg-playAni-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_playAni(a1: number): null {
    SendCommand({
      id: 'SET_oCMobMsg-playAni-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCMobMsg_OnInit(a1: TMobMsgSubType, a2: oCNpc): oCMobMsg | null {
    const result: string | null = SendCommand({
      id: 'oCMobMsg-oCMobMsg_OnInit-void-false-TMobMsgSubType,oCNpc*',
      parentId: 'NULL',
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCMobMsg(result) : null
  }

  static oCMobMsg_OnInit2(): oCMobMsg | null {
    const result: string | null = SendCommand({
      id: 'oCMobMsg-oCMobMsg_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCMobMsg(result) : null
  }

  static oCMobMsg_OnInit3(a1: TMobMsgSubType, a2: oCNpc, a3: number): oCMobMsg | null {
    const result: string | null = SendCommand({
      id: 'oCMobMsg-oCMobMsg_OnInit-void-false-TMobMsgSubType,oCNpc*,int',
      parentId: 'NULL',
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? new oCMobMsg(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCMobMsg-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override IsNetRelevant(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobMsg-IsNetRelevant-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override MD_GetNumOfSubTypes(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMobMsg-MD_GetNumOfSubTypes-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override MD_GetSubTypeString(a1: number): string | null {
    const result: string | null = SendCommand({
      id: 'oCMobMsg-MD_GetSubTypeString-zSTRING-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? String(result) : null
  }
}
