import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { oCItemReactModule } from './index.js'
import { oCNpc } from './index.js'
import { oCItem } from './index.js'

export class oCTradeManager extends BaseUnionObject {
  GetModule(a1: oCNpc, a2: oCNpc, a3: oCItem): oCItemReactModule | null {
    const result: string | null = SendCommand({
      id: 'oCTradeManager-GetModule-oCItemReactModule*-false-oCNpc*,oCNpc*,oCItem*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? new oCItemReactModule(result) : null
  }
}
