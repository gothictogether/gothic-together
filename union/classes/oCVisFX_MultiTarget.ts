import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { oCVisualFX } from './index.js'

export class oCVisFX_MultiTarget extends oCVisualFX {
  static oCVisFX_MultiTarget_OnInit(): oCVisFX_MultiTarget | null {
    const result: string | null = SendCommand({
      id: 'oCVisFX_MultiTarget-oCVisFX_MultiTarget_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCVisFX_MultiTarget(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCVisFX_MultiTarget-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override Open(): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisFX_MultiTarget-Open-void-false-',
      parentId: this.Uuid,
    })
  }

  override InvestNext(): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisFX_MultiTarget-InvestNext-void-false-',
      parentId: this.Uuid,
    })
  }

  override SetLevel(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisFX_MultiTarget-SetLevel-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override Cast(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisFX_MultiTarget-Cast-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override Stop(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisFX_MultiTarget-Stop-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override IsFinished(): number | null {
    const result: string | null = SendCommand({
      id: 'oCVisFX_MultiTarget-IsFinished-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override SetByScript(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisFX_MultiTarget-SetByScript-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override Reset(): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisFX_MultiTarget-Reset-void-false-',
      parentId: this.Uuid,
    })
  }

  override SetDamage(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisFX_MultiTarget-SetDamage-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override SetDamageType(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'oCVisFX_MultiTarget-SetDamageType-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }
}
