import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zTSoundMode } from './index.js'
import { zCClassDef } from './index.js'
import { zCVob } from './index.js'
import { zCEventMessage } from './index.js'
import { zCWorld } from './index.js'
import { zCZone } from './index.js'

export class zCVobSound extends zCZone {
  soundName(): string | null {
    const result: string | null = SendCommand({
      id: 'zCVobSound-soundName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_soundName(a1: string): null {
    SendCommand({
      id: 'SET_zCVobSound-soundName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  soundRadius(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobSound-soundRadius-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_soundRadius(a1: number): null {
    SendCommand({
      id: 'SET_zCVobSound-soundRadius-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  soundMode(): zTSoundMode | null {
    const result: string | null = SendCommand({
      id: 'zCVobSound-soundMode-zTSoundMode-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_soundMode(a1: zTSoundMode): null {
    SendCommand({
      id: 'SET_zCVobSound-soundMode-zTSoundMode-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  soundStartOn(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobSound-soundStartOn-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_soundStartOn(a1: number): null {
    SendCommand({
      id: 'SET_zCVobSound-soundStartOn-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  soundIsRunning(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobSound-soundIsRunning-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_soundIsRunning(a1: number): null {
    SendCommand({
      id: 'SET_zCVobSound-soundIsRunning-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  soundIsAmbient3D(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobSound-soundIsAmbient3D-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_soundIsAmbient3D(a1: number): null {
    SendCommand({
      id: 'SET_zCVobSound-soundIsAmbient3D-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  soundHasObstruction(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobSound-soundHasObstruction-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_soundHasObstruction(a1: number): null {
    SendCommand({
      id: 'SET_zCVobSound-soundHasObstruction-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  soundVolType(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobSound-soundVolType-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_soundVolType(a1: number): null {
    SendCommand({
      id: 'SET_zCVobSound-soundVolType-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  soundAllowedToRun(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobSound-soundAllowedToRun-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_soundAllowedToRun(a1: number): null {
    SendCommand({
      id: 'SET_zCVobSound-soundAllowedToRun-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  soundAutoStart(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobSound-soundAutoStart-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_soundAutoStart(a1: number): null {
    SendCommand({
      id: 'SET_zCVobSound-soundAutoStart-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  soundRandDelay(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobSound-soundRandDelay-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_soundRandDelay(a1: number): null {
    SendCommand({
      id: 'SET_zCVobSound-soundRandDelay-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  soundRandDelayVar(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobSound-soundRandDelayVar-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_soundRandDelayVar(a1: number): null {
    SendCommand({
      id: 'SET_zCVobSound-soundRandDelayVar-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  soundVolume(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobSound-soundVolume-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_soundVolume(a1: number): null {
    SendCommand({
      id: 'SET_zCVobSound-soundVolume-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  soundConeAngle(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobSound-soundConeAngle-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_soundConeAngle(a1: number): null {
    SendCommand({
      id: 'SET_zCVobSound-soundConeAngle-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  sfxHandle(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobSound-sfxHandle-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_sfxHandle(a1: number): null {
    SendCommand({
      id: 'SET_zCVobSound-sfxHandle-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  soundRandTimer(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobSound-soundRandTimer-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_soundRandTimer(a1: number): null {
    SendCommand({
      id: 'SET_zCVobSound-soundRandTimer-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  obstruction0(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobSound-obstruction0-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_obstruction0(a1: number): null {
    SendCommand({
      id: 'SET_zCVobSound-obstruction0-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  obstruction1(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobSound-obstruction1-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_obstruction1(a1: number): null {
    SendCommand({
      id: 'SET_zCVobSound-obstruction1-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  obstructionFrameTime(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobSound-obstructionFrameTime-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_obstructionFrameTime(a1: number): null {
    SendCommand({
      id: 'SET_zCVobSound-obstructionFrameTime-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCVobSound_OnInit(): zCVobSound | null {
    const result: string | null = SendCommand({
      id: 'zCVobSound-zCVobSound_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCVobSound(result) : null
  }

  SetSound(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'zCVobSound-SetSound-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetSoundVolume(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCVobSound-SetSoundVolume-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetSoundRadius(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCVobSound-SetSoundRadius-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  CalcObstruction(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobSound-CalcObstruction-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  CalcVolumeScale(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobSound-CalcVolumeScale-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  StartSound(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCVobSound-StartSound-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  StopSound(): void | null {
    const result: string | null = SendCommand({
      id: 'zCVobSound-StopSound-void-false-',
      parentId: this.Uuid,
    })
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCVobSound-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override OnTrigger(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCVobSound-OnTrigger-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override OnUntrigger(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCVobSound-OnUntrigger-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override OnMessage(a1: zCEventMessage, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCVobSound-OnMessage-void-false-zCEventMessage*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override EndMovement(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCVobSound-EndMovement-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override ThisVobAddedToWorld(a1: zCWorld): void | null {
    const result: string | null = SendCommand({
      id: 'zCVobSound-ThisVobAddedToWorld-void-false-zCWorld*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override ThisVobRemovedFromWorld(a1: zCWorld): void | null {
    const result: string | null = SendCommand({
      id: 'zCVobSound-ThisVobRemovedFromWorld-void-false-zCWorld*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override GetDebugDescString(): string | null {
    const result: string | null = SendCommand({
      id: 'zCVobSound-GetDebugDescString-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  DoSoundUpdate(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCVobSound-DoSoundUpdate-void-false-float',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }
}
