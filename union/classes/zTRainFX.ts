import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCOutdoorRainFX } from './index.js'

export class zTRainFX extends BaseUnionObject {
  outdoorRainFX(): zCOutdoorRainFX | null {
    const result: string | null = SendCommand({
      id: 'zTRainFX-outdoorRainFX-zCOutdoorRainFX*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCOutdoorRainFX(result) : null
  }

  set_outdoorRainFX(a1: zCOutdoorRainFX): null {
    SendCommand({
      id: 'SET_zTRainFX-outdoorRainFX-zCOutdoorRainFX*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  outdoorRainFXWeight(): number | null {
    const result: string | null = SendCommand({
      id: 'zTRainFX-outdoorRainFXWeight-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_outdoorRainFXWeight(a1: number): null {
    SendCommand({
      id: 'SET_zTRainFX-outdoorRainFXWeight-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  soundVolume(): number | null {
    const result: string | null = SendCommand({
      id: 'zTRainFX-soundVolume-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_soundVolume(a1: number): null {
    SendCommand({
      id: 'SET_zTRainFX-soundVolume-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  timerInsideSectorCantSeeOutside(): number | null {
    const result: string | null = SendCommand({
      id: 'zTRainFX-timerInsideSectorCantSeeOutside-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_timerInsideSectorCantSeeOutside(a1: number): null {
    SendCommand({
      id: 'SET_zTRainFX-timerInsideSectorCantSeeOutside-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  timeStartRain(): number | null {
    const result: string | null = SendCommand({
      id: 'zTRainFX-timeStartRain-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_timeStartRain(a1: number): null {
    SendCommand({
      id: 'SET_zTRainFX-timeStartRain-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  timeStopRain(): number | null {
    const result: string | null = SendCommand({
      id: 'zTRainFX-timeStopRain-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_timeStopRain(a1: number): null {
    SendCommand({
      id: 'SET_zTRainFX-timeStopRain-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  renderLightning(): number | null {
    const result: string | null = SendCommand({
      id: 'zTRainFX-renderLightning-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_renderLightning(a1: number): null {
    SendCommand({
      id: 'SET_zTRainFX-renderLightning-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_bRaining(): number | null {
    const result: string | null = SendCommand({
      id: 'zTRainFX-m_bRaining-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_bRaining(a1: number): null {
    SendCommand({
      id: 'SET_zTRainFX-m_bRaining-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_iRainCtr(): number | null {
    const result: string | null = SendCommand({
      id: 'zTRainFX-m_iRainCtr-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_iRainCtr(a1: number): null {
    SendCommand({
      id: 'SET_zTRainFX-m_iRainCtr-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
