import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'

export class oCItemReactModuleDummy0 extends BaseUnionObject {
  trade_npc(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemReactModuleDummy0-trade_npc-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_trade_npc(a1: number): null {
    SendCommand({
      id: 'SET_oCItemReactModuleDummy0-trade_npc-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  trade_instance(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemReactModuleDummy0-trade_instance-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_trade_instance(a1: number): null {
    SendCommand({
      id: 'SET_oCItemReactModuleDummy0-trade_instance-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  trade_amount(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemReactModuleDummy0-trade_amount-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_trade_amount(a1: number): null {
    SendCommand({
      id: 'SET_oCItemReactModuleDummy0-trade_amount-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  other_category(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemReactModuleDummy0-other_category-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_other_category(a1: number): null {
    SendCommand({
      id: 'SET_oCItemReactModuleDummy0-other_category-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  other_instance(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemReactModuleDummy0-other_instance-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_other_instance(a1: number): null {
    SendCommand({
      id: 'SET_oCItemReactModuleDummy0-other_instance-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  other_amount(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemReactModuleDummy0-other_amount-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_other_amount(a1: number): null {
    SendCommand({
      id: 'SET_oCItemReactModuleDummy0-other_amount-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  reaction(): number | null {
    const result: string | null = SendCommand({
      id: 'oCItemReactModuleDummy0-reaction-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_reaction(a1: number): null {
    SendCommand({
      id: 'SET_oCItemReactModuleDummy0-reaction-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
