import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { oCNpc } from './index.js'
import { oCItemContainer } from './index.js'

export class oCStealContainer extends oCItemContainer {
  owner(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCStealContainer-owner-oCNpc*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  set_owner(a1: oCNpc): null {
    SendCommand({
      id: 'SET_oCStealContainer-owner-oCNpc*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCStealContainer_OnInit(): oCStealContainer | null {
    const result: string | null = SendCommand({
      id: 'oCStealContainer-oCStealContainer_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCStealContainer(result) : null
  }

  override HandleEvent(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'oCStealContainer-HandleEvent-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  SetOwner(a1: oCNpc): void | null {
    const result: string | null = SendCommand({
      id: 'oCStealContainer-SetOwner-void-false-oCNpc*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetOwner(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCStealContainer-GetOwner-oCNpc*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  CreateList(): void | null {
    const result: string | null = SendCommand({
      id: 'oCStealContainer-CreateList-void-false-',
      parentId: this.Uuid,
    })
  }
}
