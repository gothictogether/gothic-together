import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'

export class zCVobLightData extends BaseUnionObject {
  lensFlareFXNo(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobLightData-lensFlareFXNo-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_lensFlareFXNo(a1: number): null {
    SendCommand({
      id: 'SET_zCVobLightData-lensFlareFXNo-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  range(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobLightData-range-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_range(a1: number): null {
    SendCommand({
      id: 'SET_zCVobLightData-range-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  rangeInv(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobLightData-rangeInv-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_rangeInv(a1: number): null {
    SendCommand({
      id: 'SET_zCVobLightData-rangeInv-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  rangeBackup(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobLightData-rangeBackup-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_rangeBackup(a1: number): null {
    SendCommand({
      id: 'SET_zCVobLightData-rangeBackup-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  rangeAniActFrame(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobLightData-rangeAniActFrame-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_rangeAniActFrame(a1: number): null {
    SendCommand({
      id: 'SET_zCVobLightData-rangeAniActFrame-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  rangeAniFPS(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobLightData-rangeAniFPS-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_rangeAniFPS(a1: number): null {
    SendCommand({
      id: 'SET_zCVobLightData-rangeAniFPS-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  colorAniActFrame(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobLightData-colorAniActFrame-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_colorAniActFrame(a1: number): null {
    SendCommand({
      id: 'SET_zCVobLightData-colorAniActFrame-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  colorAniFPS(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobLightData-colorAniFPS-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_colorAniFPS(a1: number): null {
    SendCommand({
      id: 'SET_zCVobLightData-colorAniFPS-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  spotConeAngleDeg(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobLightData-spotConeAngleDeg-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_spotConeAngleDeg(a1: number): null {
    SendCommand({
      id: 'SET_zCVobLightData-spotConeAngleDeg-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  isStatic(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobLightData-isStatic-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_isStatic(a1: number): null {
    SendCommand({
      id: 'SET_zCVobLightData-isStatic-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  rangeAniSmooth(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobLightData-rangeAniSmooth-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_rangeAniSmooth(a1: number): null {
    SendCommand({
      id: 'SET_zCVobLightData-rangeAniSmooth-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  rangeAniLoop(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobLightData-rangeAniLoop-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_rangeAniLoop(a1: number): null {
    SendCommand({
      id: 'SET_zCVobLightData-rangeAniLoop-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  colorAniSmooth(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobLightData-colorAniSmooth-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_colorAniSmooth(a1: number): null {
    SendCommand({
      id: 'SET_zCVobLightData-colorAniSmooth-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  colorAniLoop(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobLightData-colorAniLoop-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_colorAniLoop(a1: number): null {
    SendCommand({
      id: 'SET_zCVobLightData-colorAniLoop-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  isTurnedOn(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobLightData-isTurnedOn-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_isTurnedOn(a1: number): null {
    SendCommand({
      id: 'SET_zCVobLightData-isTurnedOn-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lightQuality(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobLightData-lightQuality-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_lightQuality(a1: number): null {
    SendCommand({
      id: 'SET_zCVobLightData-lightQuality-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lightType(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobLightData-lightType-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_lightType(a1: number): null {
    SendCommand({
      id: 'SET_zCVobLightData-lightType-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  m_bCanMove(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobLightData-m_bCanMove-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_m_bCanMove(a1: number): null {
    SendCommand({
      id: 'SET_zCVobLightData-m_bCanMove-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCVobLightData_OnInit(): zCVobLightData | null {
    const result: string | null = SendCommand({
      id: 'zCVobLightData-zCVobLightData_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCVobLightData(result) : null
  }

  SetLensFlareFXByName(a1: string): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobLightData-SetLensFlareFXByName-int-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetLensFlareFXName(): string | null {
    const result: string | null = SendCommand({
      id: 'zCVobLightData-GetLensFlareFXName-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  SetRange(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCVobLightData-SetRange-void-false-float,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }
}
