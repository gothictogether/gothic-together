import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { oCNpc } from './index.js'
import { TStateSubType } from './index.js'
import { zCClassDef } from './index.js'
import { oCNpcMessage } from './index.js'

export class oCMsgState extends oCNpcMessage {
  function(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgState-function-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_function(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgState-function-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  minutes(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgState-minutes-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_minutes(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgState-minutes-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  instance(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgState-instance-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_instance(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgState-instance-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  wp(): string | null {
    const result: string | null = SendCommand({
      id: 'oCMsgState-wp-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_wp(a1: string): null {
    SendCommand({
      id: 'SET_oCMsgState-wp-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  timer(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgState-timer-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_timer(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgState-timer-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  other(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCMsgState-other-oCNpc*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  set_other(a1: oCNpc): null {
    SendCommand({
      id: 'SET_oCMsgState-other-oCNpc*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  victim(): oCNpc | null {
    const result: string | null = SendCommand({
      id: 'oCMsgState-victim-oCNpc*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCNpc(result) : null
  }

  set_victim(a1: oCNpc): null {
    SendCommand({
      id: 'SET_oCMsgState-victim-oCNpc*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  endOldState(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgState-endOldState-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_endOldState(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgState-endOldState-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  inRoutine(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgState-inRoutine-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_inRoutine(a1: number): null {
    SendCommand({
      id: 'SET_oCMsgState-inRoutine-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCMsgState_OnInit(): oCMsgState | null {
    const result: string | null = SendCommand({
      id: 'oCMsgState-oCMsgState_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCMsgState(result) : null
  }

  static oCMsgState_OnInit2(a1: TStateSubType, a2: number): oCMsgState | null {
    const result: string | null = SendCommand({
      id: 'oCMsgState-oCMsgState_OnInit-void-false-TStateSubType,float',
      parentId: 'NULL',
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCMsgState(result) : null
  }

  static oCMsgState_OnInit3(a1: TStateSubType, a2: number): oCMsgState | null {
    const result: string | null = SendCommand({
      id: 'oCMsgState-oCMsgState_OnInit-void-false-TStateSubType,int',
      parentId: 'NULL',
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new oCMsgState(result) : null
  }

  static oCMsgState_OnInit4(
    a1: TStateSubType,
    a2: number,
    a3: number,
    a4: string,
  ): oCMsgState | null {
    const result: string | null = SendCommand({
      id: 'oCMsgState-oCMsgState_OnInit-void-false-TStateSubType,int,int,zSTRING const&',
      parentId: 'NULL',
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
    })

    return result ? new oCMsgState(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCMsgState-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override IsOverlay(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgState-IsOverlay-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override MD_GetNumOfSubTypes(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgState-MD_GetNumOfSubTypes-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override MD_GetSubTypeString(a1: number): string | null {
    const result: string | null = SendCommand({
      id: 'oCMsgState-MD_GetSubTypeString-zSTRING-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? String(result) : null
  }

  override MD_GetMinTime(): number | null {
    const result: string | null = SendCommand({
      id: 'oCMsgState-MD_GetMinTime-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }
}
