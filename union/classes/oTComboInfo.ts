import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'

export class oTComboInfo extends BaseUnionObject {
  hitAni(): number | null {
    const result: string | null = SendCommand({
      id: 'oTComboInfo-hitAni-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_hitAni(a1: number): null {
    SendCommand({
      id: 'SET_oTComboInfo-hitAni-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  hitOptFrame(): number | null {
    const result: string | null = SendCommand({
      id: 'oTComboInfo-hitOptFrame-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_hitOptFrame(a1: number): null {
    SendCommand({
      id: 'SET_oTComboInfo-hitOptFrame-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  hitEndFrame(): number | null {
    const result: string | null = SendCommand({
      id: 'oTComboInfo-hitEndFrame-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_hitEndFrame(a1: number): null {
    SendCommand({
      id: 'SET_oTComboInfo-hitEndFrame-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  comboStartFrame(): number | null {
    const result: string | null = SendCommand({
      id: 'oTComboInfo-comboStartFrame-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_comboStartFrame(a1: number): null {
    SendCommand({
      id: 'SET_oTComboInfo-comboStartFrame-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  comboEndFrame(): number | null {
    const result: string | null = SendCommand({
      id: 'oTComboInfo-comboEndFrame-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_comboEndFrame(a1: number): null {
    SendCommand({
      id: 'SET_oTComboInfo-comboEndFrame-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  comboDir(): number | null {
    const result: string | null = SendCommand({
      id: 'oTComboInfo-comboDir-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_comboDir(a1: number): null {
    SendCommand({
      id: 'SET_oTComboInfo-comboDir-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
