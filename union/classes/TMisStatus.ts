import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'

export class TMisStatus extends BaseUnionObject {
  vobID(): number | null {
    const result: string | null = SendCommand({
      id: 'TMisStatus-vobID-unsigned long-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_vobID(a1: number): null {
    SendCommand({
      id: 'SET_TMisStatus-vobID-unsigned long-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  status(): number | null {
    const result: string | null = SendCommand({
      id: 'TMisStatus-status-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_status(a1: number): null {
    SendCommand({
      id: 'SET_TMisStatus-status-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  startTime(): number | null {
    const result: string | null = SendCommand({
      id: 'TMisStatus-startTime-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_startTime(a1: number): null {
    SendCommand({
      id: 'SET_TMisStatus-startTime-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
