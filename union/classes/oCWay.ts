import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { oCMobLadder } from './index.js'
import { oCMobDoor } from './index.js'
import { zCWaypoint } from './index.js'
import { zCVob } from './index.js'
import { zCWay } from './index.js'

export class oCWay extends zCWay {
  ladder(): oCMobLadder | null {
    const result: string | null = SendCommand({
      id: 'oCWay-ladder-oCMobLadder*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCMobLadder(result) : null
  }

  set_ladder(a1: oCMobLadder): null {
    SendCommand({
      id: 'SET_oCWay-ladder-oCMobLadder*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  door(): oCMobDoor | null {
    const result: string | null = SendCommand({
      id: 'oCWay-door-oCMobDoor*-false-false',
      parentId: this.Uuid,
    })

    return result ? new oCMobDoor(result) : null
  }

  set_door(a1: oCMobDoor): null {
    SendCommand({
      id: 'SET_oCWay-door-oCMobDoor*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCWay_OnInit(): oCWay | null {
    const result: string | null = SendCommand({
      id: 'oCWay-oCWay_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCWay(result) : null
  }

  GetLadder(): oCMobLadder | null {
    const result: string | null = SendCommand({
      id: 'oCWay-GetLadder-oCMobLadder*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCMobLadder(result) : null
  }

  GetDoor(): oCMobDoor | null {
    const result: string | null = SendCommand({
      id: 'oCWay-GetDoor-oCMobDoor*-false-',
      parentId: this.Uuid,
    })

    return result ? new oCMobDoor(result) : null
  }

  override Init(a1: zCWaypoint, a2: zCWaypoint): void | null {
    const result: string | null = SendCommand({
      id: 'oCWay-Init-void-false-zCWaypoint*,zCWaypoint*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override CanBeUsed(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'oCWay-CanBeUsed-int-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  override IsObjectOnWay(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'oCWay-IsObjectOnWay-int-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }
}
