import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCCodeMasterDummy0 } from './index.js'
import { zCVob } from './index.js'
import { zCClassDef } from './index.js'
import { zCEventMessage } from './index.js'
import { zCTriggerBase } from './index.js'

export class zCCodeMaster extends zCTriggerBase {
  triggerTargetFailure(): string | null {
    const result: string | null = SendCommand({
      id: 'zCCodeMaster-triggerTargetFailure-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_triggerTargetFailure(a1: string): null {
    SendCommand({
      id: 'SET_zCCodeMaster-triggerTargetFailure-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  flags(): zCCodeMasterDummy0 | null {
    const result: string | null = SendCommand({
      id: 'zCCodeMaster-flags-zCCodeMasterDummy0-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCCodeMasterDummy0(result) : null
  }

  set_flags(a1: zCCodeMasterDummy0): null {
    SendCommand({
      id: 'SET_zCCodeMaster-flags-zCCodeMasterDummy0-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  numSlavesTriggered(): number | null {
    const result: string | null = SendCommand({
      id: 'zCCodeMaster-numSlavesTriggered-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_numSlavesTriggered(a1: number): null {
    SendCommand({
      id: 'SET_zCCodeMaster-numSlavesTriggered-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCCodeMaster_OnInit(): zCCodeMaster | null {
    const result: string | null = SendCommand({
      id: 'zCCodeMaster-zCCodeMaster_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCCodeMaster(result) : null
  }

  Init(): void | null {
    const result: string | null = SendCommand({
      id: 'zCCodeMaster-Init-void-false-',
      parentId: this.Uuid,
    })
  }

  Reset(): void | null {
    const result: string | null = SendCommand({
      id: 'zCCodeMaster-Reset-void-false-',
      parentId: this.Uuid,
    })
  }

  FireTriggerSuccess(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCCodeMaster-FireTriggerSuccess-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  FireTriggerFailure(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCCodeMaster-FireTriggerFailure-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetSlaveNr(a1: zCVob): number | null {
    const result: string | null = SendCommand({
      id: 'zCCodeMaster-GetSlaveNr-int-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCCodeMaster-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override OnTrigger(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCCodeMaster-OnTrigger-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override OnUntrigger(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCCodeMaster-OnUntrigger-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override OnTouch(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCCodeMaster-OnTouch-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override OnUntouch(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCCodeMaster-OnUntouch-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override OnMessage(a1: zCEventMessage, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCCodeMaster-OnMessage-void-false-zCEventMessage*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override GetTriggerTarget(a1: number): string | null {
    const result: string | null = SendCommand({
      id: 'zCCodeMaster-GetTriggerTarget-zSTRING-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? String(result) : null
  }
}
