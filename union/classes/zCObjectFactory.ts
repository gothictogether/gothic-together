import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { zCEventManager } from './index.js'
import { zCVob } from './index.js'
import { zCSession } from './index.js'
import { zCCSManager } from './index.js'
import { zCWorld } from './index.js'
import { zCWaypoint } from './index.js'
import { zCWay } from './index.js'
import { zCObject } from './index.js'

export class zCObjectFactory extends zCObject {
  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCObjectFactory-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  CreateEventManager(a1: zCVob): zCEventManager | null {
    const result: string | null = SendCommand({
      id: 'zCObjectFactory-CreateEventManager-zCEventManager*-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new zCEventManager(result) : null
  }

  CreateSession(): zCSession | null {
    const result: string | null = SendCommand({
      id: 'zCObjectFactory-CreateSession-zCSession*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCSession(result) : null
  }

  CreateCSManager(): zCCSManager | null {
    const result: string | null = SendCommand({
      id: 'zCObjectFactory-CreateCSManager-zCCSManager*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCCSManager(result) : null
  }

  CreateWorld(): zCWorld | null {
    const result: string | null = SendCommand({
      id: 'zCObjectFactory-CreateWorld-zCWorld*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCWorld(result) : null
  }

  CreateWaypoint(): zCWaypoint | null {
    const result: string | null = SendCommand({
      id: 'zCObjectFactory-CreateWaypoint-zCWaypoint*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCWaypoint(result) : null
  }

  CreateWay(): zCWay | null {
    const result: string | null = SendCommand({
      id: 'zCObjectFactory-CreateWay-zCWay*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCWay(result) : null
  }
}
