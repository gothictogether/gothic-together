import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zTScreenFXSet } from './index.js'
import { zCClassDef } from './index.js'
import { zCEventMessage } from './index.js'
import { zCVob } from './index.js'
import { zCEffect } from './index.js'

export class zCVobScreenFX extends zCEffect {
  blend(): zTScreenFXSet | null {
    const result: string | null = SendCommand({
      id: 'zCVobScreenFX-blend-zTScreenFXSet-false-false',
      parentId: this.Uuid,
    })

    return result ? new zTScreenFXSet(result) : null
  }

  set_blend(a1: zTScreenFXSet): null {
    SendCommand({
      id: 'SET_zCVobScreenFX-blend-zTScreenFXSet-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  cinema(): zTScreenFXSet | null {
    const result: string | null = SendCommand({
      id: 'zCVobScreenFX-cinema-zTScreenFXSet-false-false',
      parentId: this.Uuid,
    })

    return result ? new zTScreenFXSet(result) : null
  }

  set_cinema(a1: zTScreenFXSet): null {
    SendCommand({
      id: 'SET_zCVobScreenFX-cinema-zTScreenFXSet-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fovMorph(): zTScreenFXSet | null {
    const result: string | null = SendCommand({
      id: 'zCVobScreenFX-fovMorph-zTScreenFXSet-false-false',
      parentId: this.Uuid,
    })

    return result ? new zTScreenFXSet(result) : null
  }

  set_fovMorph(a1: zTScreenFXSet): null {
    SendCommand({
      id: 'SET_zCVobScreenFX-fovMorph-zTScreenFXSet-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCVobScreenFX_OnInit(): zCVobScreenFX | null {
    const result: string | null = SendCommand({
      id: 'zCVobScreenFX-zCVobScreenFX_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCVobScreenFX(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCVobScreenFX-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override OnMessage(a1: zCEventMessage, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCVobScreenFX-OnMessage-void-false-zCEventMessage*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override OnTick(): void | null {
    const result: string | null = SendCommand({
      id: 'zCVobScreenFX-OnTick-void-false-',
      parentId: this.Uuid,
    })
  }
}
