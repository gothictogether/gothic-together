import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zVEC3 } from './index.js'
import { zCVob } from './index.js'
import { oCVisualFX } from './index.js'

export class oCEmitterKey extends BaseUnionObject {
  visName_S(): string | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-visName_S-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_visName_S(a1: string): null {
    SendCommand({
      id: 'SET_oCEmitterKey-visName_S-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  visSizeScale(): number | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-visSizeScale-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_visSizeScale(a1: number): null {
    SendCommand({
      id: 'SET_oCEmitterKey-visSizeScale-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  scaleDuration(): number | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-scaleDuration-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_scaleDuration(a1: number): null {
    SendCommand({
      id: 'SET_oCEmitterKey-scaleDuration-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pfx_ppsValue(): number | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-pfx_ppsValue-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_pfx_ppsValue(a1: number): null {
    SendCommand({
      id: 'SET_oCEmitterKey-pfx_ppsValue-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pfx_ppsIsSmoothChg(): number | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-pfx_ppsIsSmoothChg-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_pfx_ppsIsSmoothChg(a1: number): null {
    SendCommand({
      id: 'SET_oCEmitterKey-pfx_ppsIsSmoothChg-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pfx_ppsIsLoopingChg(): number | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-pfx_ppsIsLoopingChg-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_pfx_ppsIsLoopingChg(a1: number): null {
    SendCommand({
      id: 'SET_oCEmitterKey-pfx_ppsIsLoopingChg-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pfx_scTime(): number | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-pfx_scTime-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_pfx_scTime(a1: number): null {
    SendCommand({
      id: 'SET_oCEmitterKey-pfx_scTime-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pfx_flyGravity_S(): string | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-pfx_flyGravity_S-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_pfx_flyGravity_S(a1: string): null {
    SendCommand({
      id: 'SET_oCEmitterKey-pfx_flyGravity_S-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pfx_shpDim_S(): string | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-pfx_shpDim_S-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_pfx_shpDim_S(a1: string): null {
    SendCommand({
      id: 'SET_oCEmitterKey-pfx_shpDim_S-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pfx_shpIsVolumeChg(): number | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-pfx_shpIsVolumeChg-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_pfx_shpIsVolumeChg(a1: number): null {
    SendCommand({
      id: 'SET_oCEmitterKey-pfx_shpIsVolumeChg-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pfx_shpScaleFPS(): number | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-pfx_shpScaleFPS-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_pfx_shpScaleFPS(a1: number): null {
    SendCommand({
      id: 'SET_oCEmitterKey-pfx_shpScaleFPS-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pfx_shpDistribWalkSpeed(): number | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-pfx_shpDistribWalkSpeed-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_pfx_shpDistribWalkSpeed(a1: number): null {
    SendCommand({
      id: 'SET_oCEmitterKey-pfx_shpDistribWalkSpeed-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pfx_shpOffsetVec_S(): string | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-pfx_shpOffsetVec_S-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_pfx_shpOffsetVec_S(a1: string): null {
    SendCommand({
      id: 'SET_oCEmitterKey-pfx_shpOffsetVec_S-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pfx_shpDistribType_S(): string | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-pfx_shpDistribType_S-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_pfx_shpDistribType_S(a1: string): null {
    SendCommand({
      id: 'SET_oCEmitterKey-pfx_shpDistribType_S-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pfx_dirMode_S(): string | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-pfx_dirMode_S-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_pfx_dirMode_S(a1: string): null {
    SendCommand({
      id: 'SET_oCEmitterKey-pfx_dirMode_S-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pfx_dirFOR_S(): string | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-pfx_dirFOR_S-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_pfx_dirFOR_S(a1: string): null {
    SendCommand({
      id: 'SET_oCEmitterKey-pfx_dirFOR_S-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pfx_dirModeTargetFOR_S(): string | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-pfx_dirModeTargetFOR_S-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_pfx_dirModeTargetFOR_S(a1: string): null {
    SendCommand({
      id: 'SET_oCEmitterKey-pfx_dirModeTargetFOR_S-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pfx_dirModeTargetPos_S(): string | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-pfx_dirModeTargetPos_S-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_pfx_dirModeTargetPos_S(a1: string): null {
    SendCommand({
      id: 'SET_oCEmitterKey-pfx_dirModeTargetPos_S-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pfx_velAvg(): number | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-pfx_velAvg-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_pfx_velAvg(a1: number): null {
    SendCommand({
      id: 'SET_oCEmitterKey-pfx_velAvg-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pfx_lspPartAvg(): number | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-pfx_lspPartAvg-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_pfx_lspPartAvg(a1: number): null {
    SendCommand({
      id: 'SET_oCEmitterKey-pfx_lspPartAvg-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pfx_visAlphaStart(): number | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-pfx_visAlphaStart-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_pfx_visAlphaStart(a1: number): null {
    SendCommand({
      id: 'SET_oCEmitterKey-pfx_visAlphaStart-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lightPresetName(): string | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-lightPresetName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_lightPresetName(a1: string): null {
    SendCommand({
      id: 'SET_oCEmitterKey-lightPresetName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  lightRange(): number | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-lightRange-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_lightRange(a1: number): null {
    SendCommand({
      id: 'SET_oCEmitterKey-lightRange-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  sfxID(): string | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-sfxID-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_sfxID(a1: string): null {
    SendCommand({
      id: 'SET_oCEmitterKey-sfxID-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  sfxIsAmbient(): number | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-sfxIsAmbient-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_sfxIsAmbient(a1: number): null {
    SendCommand({
      id: 'SET_oCEmitterKey-sfxIsAmbient-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emCreateFXID(): string | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-emCreateFXID-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_emCreateFXID(a1: string): null {
    SendCommand({
      id: 'SET_oCEmitterKey-emCreateFXID-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emFlyGravity(): number | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-emFlyGravity-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_emFlyGravity(a1: number): null {
    SendCommand({
      id: 'SET_oCEmitterKey-emFlyGravity-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emSelfRotVel_S(): string | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-emSelfRotVel_S-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_emSelfRotVel_S(a1: string): null {
    SendCommand({
      id: 'SET_oCEmitterKey-emSelfRotVel_S-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emTrjMode_S(): string | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-emTrjMode_S-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_emTrjMode_S(a1: string): null {
    SendCommand({
      id: 'SET_oCEmitterKey-emTrjMode_S-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emTrjEaseVel(): number | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-emTrjEaseVel-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_emTrjEaseVel(a1: number): null {
    SendCommand({
      id: 'SET_oCEmitterKey-emTrjEaseVel-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emCheckCollision(): number | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-emCheckCollision-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_emCheckCollision(a1: number): null {
    SendCommand({
      id: 'SET_oCEmitterKey-emCheckCollision-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emFXLifeSpan(): number | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-emFXLifeSpan-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_emFXLifeSpan(a1: number): null {
    SendCommand({
      id: 'SET_oCEmitterKey-emFXLifeSpan-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  dScriptEnd(): number | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-dScriptEnd-unsigned char-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_dScriptEnd(a1: number): null {
    SendCommand({
      id: 'SET_oCEmitterKey-dScriptEnd-unsigned char-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pfx_flyGravity(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-pfx_flyGravity-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_pfx_flyGravity(a1: zVEC3): null {
    SendCommand({
      id: 'SET_oCEmitterKey-pfx_flyGravity-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pfx_shpOffsetVec(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-pfx_shpOffsetVec-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_pfx_shpOffsetVec(a1: zVEC3): null {
    SendCommand({
      id: 'SET_oCEmitterKey-pfx_shpOffsetVec-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  pfx_dirModeTargetPos(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-pfx_dirModeTargetPos-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_pfx_dirModeTargetPos(a1: zVEC3): null {
    SendCommand({
      id: 'SET_oCEmitterKey-pfx_dirModeTargetPos-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emSelfRotVel(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-emSelfRotVel-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_emSelfRotVel(a1: zVEC3): null {
    SendCommand({
      id: 'SET_oCEmitterKey-emSelfRotVel-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  emTrjMode(): number | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-emTrjMode-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_emTrjMode(a1: number): null {
    SendCommand({
      id: 'SET_oCEmitterKey-emTrjMode-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  name(): string | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-name-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_name(a1: string): null {
    SendCommand({
      id: 'SET_oCEmitterKey-name-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  vob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-vob-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_vob(a1: zCVob): null {
    SendCommand({
      id: 'SET_oCEmitterKey-vob-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  targetPos(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-targetPos-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_targetPos(a1: zVEC3): null {
    SendCommand({
      id: 'SET_oCEmitterKey-targetPos-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  sfxHnd(): number | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-sfxHnd-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_sfxHnd(a1: number): null {
    SendCommand({
      id: 'SET_oCEmitterKey-sfxHnd-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static oCEmitterKey_OnInit(): oCEmitterKey | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-oCEmitterKey_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCEmitterKey(result) : null
  }

  SetByScript(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-SetByScript-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  ParseStrings(): void | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-ParseStrings-void-false-',
      parentId: this.Uuid,
    })
  }

  Edit(): void | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-Edit-void-false-',
      parentId: this.Uuid,
    })
  }

  SetDefaultByFX(a1: oCVisualFX): void | null {
    const result: string | null = SendCommand({
      id: 'oCEmitterKey-SetDefaultByFX-void-false-oCVisualFX*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }
}
