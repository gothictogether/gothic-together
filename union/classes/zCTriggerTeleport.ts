import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCVob } from './index.js'
import { zCClassDef } from './index.js'
import { zCTrigger } from './index.js'

export class zCTriggerTeleport extends zCTrigger {
  teleportSoundName(): string | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerTeleport-teleportSoundName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_teleportSoundName(a1: string): null {
    SendCommand({
      id: 'SET_zCTriggerTeleport-teleportSoundName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCTriggerTeleport_OnInit(): zCTriggerTeleport | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerTeleport-zCTriggerTeleport_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCTriggerTeleport(result) : null
  }

  DoTeleport(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerTeleport-DoTeleport-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerTeleport-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override OnTrigger(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerTeleport-OnTrigger-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override OnUntrigger(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerTeleport-OnUntrigger-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override OnTouch(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCTriggerTeleport-OnTouch-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }
}
