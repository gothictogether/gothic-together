import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { zTTimeBehavior } from './index.js'
import { zCEventMessage } from './index.js'

export class zCEventScreenFX extends zCEventMessage {
  duration(): number | null {
    const result: string | null = SendCommand({
      id: 'zCEventScreenFX-duration-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_duration(a1: number): null {
    SendCommand({
      id: 'SET_zCEventScreenFX-duration-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fovDeg(): number | null {
    const result: string | null = SendCommand({
      id: 'zCEventScreenFX-fovDeg-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fovDeg(a1: number): null {
    SendCommand({
      id: 'SET_zCEventScreenFX-fovDeg-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  Clear(): void | null {
    const result: string | null = SendCommand({
      id: 'zCEventScreenFX-Clear-void-false-',
      parentId: this.Uuid,
    })
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCEventScreenFX-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override MD_GetNumOfSubTypes(): number | null {
    const result: string | null = SendCommand({
      id: 'zCEventScreenFX-MD_GetNumOfSubTypes-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override MD_GetSubTypeString(a1: number): string | null {
    const result: string | null = SendCommand({
      id: 'zCEventScreenFX-MD_GetSubTypeString-zSTRING-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? String(result) : null
  }

  override MD_GetTimeBehavior(): zTTimeBehavior | null {
    const result: string | null = SendCommand({
      id: 'zCEventScreenFX-MD_GetTimeBehavior-zTTimeBehavior-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override MD_GetMinTime(): number | null {
    const result: string | null = SendCommand({
      id: 'zCEventScreenFX-MD_GetMinTime-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }
}
