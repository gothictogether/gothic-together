import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCVob } from './index.js'
import { zVEC3 } from './index.js'

export class zSVisualFXColl extends BaseUnionObject {
  foundVob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'zSVisualFXColl-foundVob-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_foundVob(a1: zCVob): null {
    SendCommand({
      id: 'SET_zSVisualFXColl-foundVob-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  foundContactPoint(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zSVisualFXColl-foundContactPoint-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_foundContactPoint(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zSVisualFXColl-foundContactPoint-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  foundNormal(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zSVisualFXColl-foundNormal-zVEC3-false-false',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  set_foundNormal(a1: zVEC3): null {
    SendCommand({
      id: 'SET_zSVisualFXColl-foundNormal-zVEC3-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
