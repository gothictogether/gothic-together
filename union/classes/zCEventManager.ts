import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCVob } from './index.js'
import { zCClassDef } from './index.js'
import { zVEC3 } from './index.js'
import { zCEventMessage } from './index.js'
import { zCObject } from './index.js'

export class zCEventManager extends zCObject {
  cleared(): number | null {
    const result: string | null = SendCommand({
      id: 'zCEventManager-cleared-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_cleared(a1: number): null {
    SendCommand({
      id: 'SET_zCEventManager-cleared-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  active(): number | null {
    const result: string | null = SendCommand({
      id: 'zCEventManager-active-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_active(a1: number): null {
    SendCommand({
      id: 'SET_zCEventManager-active-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  hostVob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'zCEventManager-hostVob-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_hostVob(a1: zCVob): null {
    SendCommand({
      id: 'SET_zCEventManager-hostVob-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCEventManager_OnInit(): zCEventManager | null {
    const result: string | null = SendCommand({
      id: 'zCEventManager-zCEventManager_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCEventManager(result) : null
  }

  static zCEventManager_OnInit2(a1: zCVob): zCEventManager | null {
    const result: string | null = SendCommand({
      id: 'zCEventManager-zCEventManager_OnInit-void-false-zCVob*',
      parentId: 'NULL',
      a1: a1.toString(),
    })

    return result ? new zCEventManager(result) : null
  }

  KillMessages(): void | null {
    const result: string | null = SendCommand({
      id: 'zCEventManager-KillMessages-void-false-',
      parentId: this.Uuid,
    })
  }

  ShowMessageCommunication(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCEventManager-ShowMessageCommunication-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  Print_db(a1: string, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCEventManager-Print_db-void-false-zSTRING const&,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCEventManager-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  OnTrigger(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCEventManager-OnTrigger-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  OnUntrigger(a1: zCVob, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCEventManager-OnUntrigger-void-false-zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  OnTouch(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCEventManager-OnTouch-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  OnUntouch(a1: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCEventManager-OnUntouch-void-false-zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  OnTouchLevel(): void | null {
    const result: string | null = SendCommand({
      id: 'zCEventManager-OnTouchLevel-void-false-',
      parentId: this.Uuid,
    })
  }

  OnDamage(a1: zCVob, a2: zCVob, a3: number, a4: number, a5: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'zCEventManager-OnDamage-void-false-zCVob*,zCVob*,float,int,zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
      a4: a4.toString(),
      a5: a5.toString(),
    })
  }

  OnMessage(a1: zCEventMessage, a2: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCEventManager-OnMessage-void-false-zCEventMessage*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  Clear(): void | null {
    const result: string | null = SendCommand({
      id: 'zCEventManager-Clear-void-false-',
      parentId: this.Uuid,
    })
  }

  IsEmpty(a1: number): number | null {
    const result: string | null = SendCommand({
      id: 'zCEventManager-IsEmpty-int-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  GetCutsceneMode(): number | null {
    const result: string | null = SendCommand({
      id: 'zCEventManager-GetCutsceneMode-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  IsRunning(a1: zCEventMessage): number | null {
    const result: string | null = SendCommand({
      id: 'zCEventManager-IsRunning-int-false-zCEventMessage*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  SetActive(a1: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCEventManager-SetActive-void-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetNumMessages(): number | null {
    const result: string | null = SendCommand({
      id: 'zCEventManager-GetNumMessages-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetEventMessage(a1: number): zCEventMessage | null {
    const result: string | null = SendCommand({
      id: 'zCEventManager-GetEventMessage-zCEventMessage*-false-int',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? new zCEventMessage(result) : null
  }

  GetActiveMessage(): zCEventMessage | null {
    const result: string | null = SendCommand({
      id: 'zCEventManager-GetActiveMessage-zCEventMessage*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCEventMessage(result) : null
  }

  ShowList(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCEventManager-ShowList-void-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  RemoveFromList(a1: zCEventMessage): void | null {
    const result: string | null = SendCommand({
      id: 'zCEventManager-RemoveFromList-void-false-zCEventMessage*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  InsertInList(a1: zCEventMessage): void | null {
    const result: string | null = SendCommand({
      id: 'zCEventManager-InsertInList-void-false-zCEventMessage*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  ProcessMessageList(): void | null {
    const result: string | null = SendCommand({
      id: 'zCEventManager-ProcessMessageList-void-false-',
      parentId: this.Uuid,
    })
  }

  SendMessageToHost(a1: zCEventMessage, a2: zCVob, a3: zCVob): void | null {
    const result: string | null = SendCommand({
      id: 'zCEventManager-SendMessageToHost-void-false-zCEventMessage*,zCVob*,zCVob*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  Delete(a1: zCEventMessage): void | null {
    const result: string | null = SendCommand({
      id: 'zCEventManager-Delete-void-false-zCEventMessage*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }
}
