import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { zCVob } from './index.js'

export class zCVobLevelCompo extends zCVob {
  static zCVobLevelCompo_OnInit(): zCVobLevelCompo | null {
    const result: string | null = SendCommand({
      id: 'zCVobLevelCompo-zCVobLevelCompo_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCVobLevelCompo(result) : null
  }

  HasIdentityTrafo(): number | null {
    const result: string | null = SendCommand({
      id: 'zCVobLevelCompo-HasIdentityTrafo-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCVobLevelCompo-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }

  override SetVisual(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'zCVobLevelCompo-SetVisual-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }
}
