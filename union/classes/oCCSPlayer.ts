import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCClassDef } from './index.js'
import { zCCSPlayer } from './index.js'

export class oCCSPlayer extends zCCSPlayer {
  static oCCSPlayer_OnInit(): oCCSPlayer | null {
    const result: string | null = SendCommand({
      id: 'oCCSPlayer-oCCSPlayer_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new oCCSPlayer(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'oCCSPlayer-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }
}
