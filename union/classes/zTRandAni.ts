import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'

export class zTRandAni extends BaseUnionObject {
  randAniProtoID(): number | null {
    const result: string | null = SendCommand({
      id: 'zTRandAni-randAniProtoID-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_randAniProtoID(a1: number): null {
    SendCommand({
      id: 'SET_zTRandAni-randAniProtoID-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  prob(): number | null {
    const result: string | null = SendCommand({
      id: 'zTRandAni-prob-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_prob(a1: number): null {
    SendCommand({
      id: 'SET_zTRandAni-prob-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }
}
