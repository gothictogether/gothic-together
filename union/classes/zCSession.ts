import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zCCSManager } from './index.js'
import { zCWorld } from './index.js'
import { zCCamera } from './index.js'
import { zCAICamera } from './index.js'
import { zCVob } from './index.js'

export class zCSession extends BaseUnionObject {
  csMan(): zCCSManager | null {
    const result: string | null = SendCommand({
      id: 'zCSession-csMan-zCCSManager*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCCSManager(result) : null
  }

  set_csMan(a1: zCCSManager): null {
    SendCommand({
      id: 'SET_zCSession-csMan-zCCSManager*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  world(): zCWorld | null {
    const result: string | null = SendCommand({
      id: 'zCSession-world-zCWorld*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCWorld(result) : null
  }

  set_world(a1: zCWorld): null {
    SendCommand({
      id: 'SET_zCSession-world-zCWorld*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  camera(): zCCamera | null {
    const result: string | null = SendCommand({
      id: 'zCSession-camera-zCCamera*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCCamera(result) : null
  }

  set_camera(a1: zCCamera): null {
    SendCommand({
      id: 'SET_zCSession-camera-zCCamera*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  aiCam(): zCAICamera | null {
    const result: string | null = SendCommand({
      id: 'zCSession-aiCam-zCAICamera*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCAICamera(result) : null
  }

  set_aiCam(a1: zCAICamera): null {
    SendCommand({
      id: 'SET_zCSession-aiCam-zCAICamera*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  camVob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'zCSession-camVob-zCVob*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  set_camVob(a1: zCVob): null {
    SendCommand({
      id: 'SET_zCSession-camVob-zCVob*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCSession_OnInit(): zCSession | null {
    const result: string | null = SendCommand({
      id: 'zCSession-zCSession_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCSession(result) : null
  }

  Init(): void | null {
    const result: string | null = SendCommand({
      id: 'zCSession-Init-void-false-',
      parentId: this.Uuid,
    })
  }

  Done(): void | null {
    const result: string | null = SendCommand({
      id: 'zCSession-Done-void-false-',
      parentId: this.Uuid,
    })
  }

  Render(): void | null {
    const result: string | null = SendCommand({
      id: 'zCSession-Render-void-false-',
      parentId: this.Uuid,
    })
  }

  RenderBlit(): void | null {
    const result: string | null = SendCommand({
      id: 'zCSession-RenderBlit-void-false-',
      parentId: this.Uuid,
    })
  }

  CamInit(): void | null {
    const result: string | null = SendCommand({
      id: 'zCSession-CamInit-void-false-',
      parentId: this.Uuid,
    })
  }

  CamInit2(a1: zCVob, a2: zCCamera): void | null {
    const result: string | null = SendCommand({
      id: 'zCSession-CamInit-void-false-zCVob*,zCCamera*',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  SetCamera(a1: zCCamera): void | null {
    const result: string | null = SendCommand({
      id: 'zCSession-SetCamera-void-false-zCCamera*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  GetCamera(): zCCamera | null {
    const result: string | null = SendCommand({
      id: 'zCSession-GetCamera-zCCamera*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCCamera(result) : null
  }

  GetCameraAI(): zCAICamera | null {
    const result: string | null = SendCommand({
      id: 'zCSession-GetCameraAI-zCAICamera*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCAICamera(result) : null
  }

  GetCameraVob(): zCVob | null {
    const result: string | null = SendCommand({
      id: 'zCSession-GetCameraVob-zCVob*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCVob(result) : null
  }

  GetWorld(): zCWorld | null {
    const result: string | null = SendCommand({
      id: 'zCSession-GetWorld-zCWorld*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCWorld(result) : null
  }

  GetCutsceneManager(): zCCSManager | null {
    const result: string | null = SendCommand({
      id: 'zCSession-GetCutsceneManager-zCCSManager*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCCSManager(result) : null
  }

  SetTime(a1: number, a2: number, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCSession-SetTime-void-false-int,int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  GetTime(a1: number, a2: number, a3: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCSession-GetTime-void-false-int&,int&,int&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  SetWorld(a1: zCWorld): void | null {
    const result: string | null = SendCommand({
      id: 'zCSession-SetWorld-void-false-zCWorld*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  DesktopInit(): void | null {
    const result: string | null = SendCommand({
      id: 'zCSession-DesktopInit-void-false-',
      parentId: this.Uuid,
    })
  }

  CutsceneSystemInit(): void | null {
    const result: string | null = SendCommand({
      id: 'zCSession-CutsceneSystemInit-void-false-',
      parentId: this.Uuid,
    })
  }
}
