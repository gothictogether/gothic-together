import { SendCommand } from '../../client.js'
import { BaseUnionObject } from '../base-union-object.js'
import { zTBBox3D } from './index.js'
import { zCModelAniEvent } from './index.js'
import { zTMdl_AniSample } from './index.js'
import { zTMdl_AniType } from './index.js'
import { zTMdl_AniDir } from './index.js'
import { zVEC3 } from './index.js'
import { zCModelPrototype } from './index.js'
import { zCClassDef } from './index.js'
import { zCObject } from './index.js'

export class zCModelAni extends zCObject {
  aniName(): string | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-aniName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_aniName(a1: string): null {
    SendCommand({
      id: 'SET_zCModelAni-aniName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  ascName(): string | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-ascName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_ascName(a1: string): null {
    SendCommand({
      id: 'SET_zCModelAni-ascName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  aniID(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-aniID-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_aniID(a1: number): null {
    SendCommand({
      id: 'SET_zCModelAni-aniID-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  aliasName(): string | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-aliasName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_aliasName(a1: string): null {
    SendCommand({
      id: 'SET_zCModelAni-aliasName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  layer(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-layer-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_layer(a1: number): null {
    SendCommand({
      id: 'SET_zCModelAni-layer-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  blendInSpeed(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-blendInSpeed-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_blendInSpeed(a1: number): null {
    SendCommand({
      id: 'SET_zCModelAni-blendInSpeed-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  blendOutSpeed(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-blendOutSpeed-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_blendOutSpeed(a1: number): null {
    SendCommand({
      id: 'SET_zCModelAni-blendOutSpeed-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  aniBBox3DObjSpace(): zTBBox3D | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-aniBBox3DObjSpace-zTBBox3D-false-false',
      parentId: this.Uuid,
    })

    return result ? new zTBBox3D(result) : null
  }

  set_aniBBox3DObjSpace(a1: zTBBox3D): null {
    SendCommand({
      id: 'SET_zCModelAni-aniBBox3DObjSpace-zTBBox3D-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  collisionVolumeScale(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-collisionVolumeScale-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_collisionVolumeScale(a1: number): null {
    SendCommand({
      id: 'SET_zCModelAni-collisionVolumeScale-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  nextAni(): zCModelAni | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-nextAni-zCModelAni*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCModelAni(result) : null
  }

  set_nextAni(a1: zCModelAni): null {
    SendCommand({
      id: 'SET_zCModelAni-nextAni-zCModelAni*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  nextAniName(): string | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-nextAniName-zSTRING-false-false',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  set_nextAniName(a1: string): null {
    SendCommand({
      id: 'SET_zCModelAni-nextAniName-zSTRING-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  aniEvents(): zCModelAniEvent | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-aniEvents-zCModelAniEvent*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zCModelAniEvent(result) : null
  }

  set_aniEvents(a1: zCModelAniEvent): null {
    SendCommand({
      id: 'SET_zCModelAni-aniEvents-zCModelAniEvent*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fpsRate(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-fpsRate-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fpsRate(a1: number): null {
    SendCommand({
      id: 'SET_zCModelAni-fpsRate-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  fpsRateSource(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-fpsRateSource-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_fpsRateSource(a1: number): null {
    SendCommand({
      id: 'SET_zCModelAni-fpsRateSource-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  rootNodeIndex(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-rootNodeIndex-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_rootNodeIndex(a1: number): null {
    SendCommand({
      id: 'SET_zCModelAni-rootNodeIndex-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  aniSampleMatrix(): zTMdl_AniSample | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-aniSampleMatrix-zTMdl_AniSample*-false-false',
      parentId: this.Uuid,
    })

    return result ? new zTMdl_AniSample(result) : null
  }

  set_aniSampleMatrix(a1: zTMdl_AniSample): null {
    SendCommand({
      id: 'SET_zCModelAni-aniSampleMatrix-zTMdl_AniSample*-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  samplePosRangeMin(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-samplePosRangeMin-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_samplePosRangeMin(a1: number): null {
    SendCommand({
      id: 'SET_zCModelAni-samplePosRangeMin-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  samplePosScaler(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-samplePosScaler-float-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_samplePosScaler(a1: number): null {
    SendCommand({
      id: 'SET_zCModelAni-samplePosScaler-float-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  numFrames(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-numFrames-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_numFrames(a1: number): null {
    SendCommand({
      id: 'SET_zCModelAni-numFrames-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  numNodes(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-numNodes-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_numNodes(a1: number): null {
    SendCommand({
      id: 'SET_zCModelAni-numNodes-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  aniType(): zTMdl_AniType | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-aniType-zTMdl_AniType-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_aniType(a1: zTMdl_AniType): null {
    SendCommand({
      id: 'SET_zCModelAni-aniType-zTMdl_AniType-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  aniDir(): zTMdl_AniDir | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-aniDir-zTMdl_AniDir-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_aniDir(a1: zTMdl_AniDir): null {
    SendCommand({
      id: 'SET_zCModelAni-aniDir-zTMdl_AniDir-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  numAniEvents(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-numAniEvents-int-false-false',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  set_numAniEvents(a1: number): null {
    SendCommand({
      id: 'SET_zCModelAni-numAniEvents-int-false-false',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return null
  }

  static zCModelAni_OnInit(): zCModelAni | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-zCModelAni_OnInit-void-false-',
      parentId: 'NULL',
    })

    return result ? new zCModelAni(result) : null
  }

  PrecacheAniEventData(): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-PrecacheAniEventData-void-false-',
      parentId: this.Uuid,
    })
  }

  GetAniVelocity(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-GetAniVelocity-float-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetAniTranslation(): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-GetAniTranslation-zVEC3-false-',
      parentId: this.Uuid,
    })

    return result ? new zVEC3(result) : null
  }

  GetTrans(a1: number, a2: number): zVEC3 | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-GetTrans-zVEC3-false-int,int',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })

    return result ? new zVEC3(result) : null
  }

  SetTrans(a1: number, a2: number, a3: zVEC3): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-SetTrans-void-false-int,int,zVEC3 const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })
  }

  SetFlags(a1: string): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-SetFlags-void-false-zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
    })
  }

  SetBlendingSec(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-SetBlendingSec-void-false-float,float',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  GetBlendingSec(a1: number, a2: number): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-GetBlendingSec-void-false-float&,float&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  CorrectRootNodeIdleMovement(): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-CorrectRootNodeIdleMovement-void-false-',
      parentId: this.Uuid,
    })
  }

  CalcInPlaceFlag(): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-CalcInPlaceFlag-void-false-',
      parentId: this.Uuid,
    })
  }

  ResolveAlias(a1: zCModelPrototype): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-ResolveAlias-int-false-zCModelPrototype*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  ResolveComb(a1: zCModelPrototype): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-ResolveComb-int-false-zCModelPrototype*',
      parentId: this.Uuid,
      a1: a1.toString(),
    })

    return result ? Number(result) : null
  }

  SaveMAN(a1: zCModelPrototype, a2: string): void | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-SaveMAN-void-false-zCModelPrototype*,zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
    })
  }

  LoadMAN(a1: string, a2: zCModelPrototype, a3: string): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-LoadMAN-int-false-zSTRING const&,zCModelPrototype*,zSTRING const&',
      parentId: this.Uuid,
      a1: a1.toString(),
      a2: a2.toString(),
      a3: a3.toString(),
    })

    return result ? Number(result) : null
  }

  GetAniName(): string | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-GetAniName-zSTRING-false-',
      parentId: this.Uuid,
    })

    return result ? String(result) : null
  }

  GetAniType(): zTMdl_AniType | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-GetAniType-zTMdl_AniType-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  GetAniID(): number | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-GetAniID-int-false-',
      parentId: this.Uuid,
    })

    return result ? Number(result) : null
  }

  override _GetClassDef(): zCClassDef | null {
    const result: string | null = SendCommand({
      id: 'zCModelAni-_GetClassDef-zCClassDef*-false-',
      parentId: this.Uuid,
    })

    return result ? new zCClassDef(result) : null
  }
}
