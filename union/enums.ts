export const zVOB_CDMODE_EXACT = 0
export const zVOB_CDMODE_SPEEDBOX = 1
export const zVOB_CDMODE_ALL = 2
export const zWAY_JUMP = 1
export const zWAY_CLIMB = 2
export const zWAY_SWIM = 4
export const zWAY_DIVE = 8
export const zWAY_FREE = 16
export const WAY_LIST_NONE = 0
export const WAY_LIST_OPEN = 1
export const WAY_LIST_CLOSED = 2
export const zMDL_ANI_FLAG_VOB_ROT = 1
export const zMDL_ANI_FLAG_VOB_POS = 2
export const zMDL_ANI_FLAG_END_SYNC = 4
export const zMDL_ANI_FLAG_FLY = 8
export const zMDL_ANI_FLAG_IDLE = 16
export const zMDL_MAX_ANI_CHANNELS = 2
export const zMDL_MAX_TEX = 4
export const zMDL_BLEND_STATE_FADEIN = 0
export const zMDL_BLEND_STATE_CONST = 1
export const zMDL_BLEND_STATE_FADEOUT = 2
export const zMDL_DYNLIGHT_SCALEPRELIT = 0
export const zMDL_DYNLIGHT_EXACT = 1
export const zMDL_STARTANI_DEFAULT = 0
export const zMDL_STARTANI_ISNEXTANI = 1
export const zMDL_STARTANI_FORCE = 2
export const ANI_WALKMODE_RUN = 0
export const ANI_WALKMODE_WALK = 1
export const ANI_WALKMODE_SNEAK = 2
export const ANI_WALKMODE_WATER = 3
export const ANI_WALKMODE_SWIM = 4
export const ANI_WALKMODE_DIVE = 5
export const ANI_ACTION_STAND = 0
export const ANI_ACTION_WALK = 1
export const ANI_ACTION_SNEAK = 2
export const ANI_ACTION_RUN = 3
export const ANI_ACTION_WATERWALK = 4
export const ANI_ACTION_SWIM = 5
export const ANI_ACTION_DIVE = 6
export const ANI_ACTION_CHOOSEWEAPON = 7
export const ANI_HITTYPE_NONE = 0
export const ANI_HITTYPE_FORWARD = 1
export const FOCUS_NORMAL = 0
export const FOCUS_MELEE = 1
export const FOCUS_RANGED = 2
export const FOCUS_THROW_ITEM = 3
export const FOCUS_THROW_MOB = 4
export const FOCUS_MAGIC = 5
export const INF_ANSWER_DEFAULT = 0
export const INF_ANSWER_HERO = 1
export const INF_ANSWER_FIGHTER = 2
export const INF_ANSWER_MAGE = 3
export const INF_ANSWER_THIEF = 4
export const INF_ANSWER_PSIONIC = 5
export const INV_NONE = 0
export const INV_COMBAT = 1
export const INV_ARMOR = 2
export const INV_RUNE = 3
export const INV_MAGIC = 4
export const INV_FOOD = 5
export const INV_POTION = 6
export const INV_DOCS = 7
export const INV_OTHER = 8
export const INV_MAX = 9
export const INV_MODE_DEFAULT = 0
export const INV_MODE_CONTAINER = 1
export const INV_MODE_PLUNDER = 2
export const INV_MODE_STEAL = 3
export const INV_MODE_BUY = 4
export const INV_MODE_SELL = 5
export const INV_MODE_MAX = 6
export const ITM_WEAR_NO = 0
export const ITM_WEAR_TORSO = 1
export const ITM_WEAR_HEAD = 2
export const ITM_WEAR_LIGHT = 8
export const NPC_GAME_NORMAL = 0
export const NPC_GAME_PLUNDER = 1
export const NPC_GAME_STEAL = 2
export const BS_INT_OVERRIDE_NONE = 0
export const BS_INT_OVERRIDE_ALL = 1
export const BS_INT_OVERRIDE_STUMBLE_ONLY = 2
export const NPC_ATT_HOSTILE = 0
export const NPC_ATT_ANGRY = 1
export const NPC_ATT_NEUTRAL = 2
export const NPC_ATT_FRIENDLY = 3
export const NPC_WEAPON_NONE = 0
export const NPC_WEAPON_FIST = 1
export const NPC_WEAPON_DAG = 2
export const NPC_WEAPON_1HS = 3
export const NPC_WEAPON_2HS = 4
export const NPC_WEAPON_BOW = 5
export const NPC_WEAPON_CBOW = 6
export const NPC_WEAPON_MAG = 7
export const NPC_WEAPON_MAX = 8
export const NPC_PERC_ASSESSPLAYER = 1
export const NPC_PERC_ASSESSENEMY = 2
export const NPC_PERC_ASSESSFIGHTER = 3
export const NPC_PERC_ASSESSBODY = 4
export const NPC_PERC_ASSESSITEM = 5
export const NPC_PERC_ASSESSMURDER = 6
export const NPC_PERC_ASSESSDEFEAT = 7
export const NPC_PERC_ASSESSDAMAG = 8
export const NPC_PERC_ASSESSOTHERSDAMAGE = 9
export const NPC_PERC_ASSESSTHREAT = 10
export const NPC_PERC_ASSESSREMOVEWEAPON = 11
export const NPC_PERC_OBSERVEINTRUDER = 12
export const NPC_PERC_ASSESSFIGHTSOUND = 13
export const NPC_PERC_ASSESSQUIETSOUND = 14
export const NPC_PERC_ASSESSWARN = 15
export const NPC_PERC_CATCHTHIEF = 16
export const NPC_PERC_ASSESSTHEFT = 17
export const NPC_PERC_ASSESSCALL = 18
export const NPC_PERC_ASSESSTALK = 19
export const NPC_PERC_ASSESSGIVENITEM = 20
export const NPC_PERC_ASSESSFAKEGUILD = 21
export const NPC_PERC_MOVEMOB = 22
export const NPC_PERC_MOVENPC = 23
export const NPC_PERC_DRAWWEAPON = 24
export const NPC_PERC_OBSERVESUSPECT = 25
export const NPC_PERC_NPCCOMMAND = 26
export const NPC_PERC_ASSESSMAGIC = 27
export const NPC_PERC_ASSESSSTOPMAGIC = 28
export const NPC_PERC_ASSESSCASTER = 29
export const NPC_PERC_ASSESSSURPRISE = 30
export const NPC_PERC_ASSESSENTERROOM = 31
export const NPC_PERC_ASSESSUSEMOB = 32
export const NPC_PERC_MAX = 33
export const NPCTYPE_AMBIENT = 0
export const NPCTYPE_MAIN = 1
export const NPCTYPE_GUARD = 2
export const NPCTYPE_FRIEND = 3
export const NPCTYPE_MINE_AMBIENT = 4
export const NPCTYPE_MINE_GUARD = 5
export const NPCTYPE_OW_AMBIENT = 6
export const NPCTYPE_OW_GUARD = 7
export const AITIME_NO = 0
export const AITIME_ONCE = 1
export const AITIME_TIMED = 2
export const NPC_AISTATE_ANSWER = -2
export const NPC_AISTATE_DEAD = -3
export const NPC_AISTATE_UNCONSCIOUS = -4
export const NPC_AISTATE_FADEAWAY = -5
export const NPC_AISTATE_FOLLOW = -6
export const SPL_STATUS_DONTINVEST = 0
export const SPL_STATUS_CANINVEST = 1
export const SPL_STATUS_CAST = 2
export const SPL_STATUS_STOP = 3
export const SPL_STATUS_NEXTLEVEL = 4
export const SPL_STATUS_CANINVEST_NO_MANADEC = 8
export const SPL_STATUS_FORCEINVEST = 65536
export const SPL_CAT_GOOD = 0
export const SPL_CAT_NEUTRAL = 1
export const SPL_CAT_BAD = 2
export const TARGET_COLLECT_NONE = 0
export const TARGET_COLLECT_CASTER = 1
export const TARGET_COLLECT_FOCUS = 2
export const TARGET_COLLECT_ALL = 3
export const TARGET_COLLECT_FOCUS_FALLBACK_NONE = 4
export const TARGET_COLLECT_FOCUS_FALLBACK_CASTER = 5
export const TARGET_COLLECT_ALL_FALLBACK_NONE = 6
export const TARGET_COLLECT_ALL_FALLBACK_CASTER = 7
export const TARGET_FLAG_NONE = 0
export const TARGET_FLAG_ALL = 1
export const TARGET_FLAG_ITEMS = 2
export const TARGET_FLAG_NPCS = 4
export const TARGET_FLAG_ORCS = 8
export const TARGET_FLAG_HUMANS = 16
export const TARGET_FLAG_UNDEAD = 32
export const TARGET_FLAG_LIVING = 64

export const NPC_ATR_HITPOINTS = 0
export const NPC_ATR_HITPOINTSMAX = 1
export const NPC_ATR_MANA = 2
export const NPC_ATR_MANAMAX = 3
export const NPC_ATR_STRENGTH = 4
export const NPC_ATR_DEXTERITY = 5
export const NPC_ATR_REGENERATEHP = 6
export const NPC_ATR_REGENERATEMANA = 7
export const NPC_ATR_MAX = 8
export const NPC_TAL_FIGHT_USED = 0x00000fff
export const NPC_TAL_FIGHT_MAX = 12
export const NPC_TAL_USED = 0x0000024d
export const NPC_TAL_MAX = 17
export const NPC_HITCHANCE_1H = 1
export const NPC_HITCHANCE_2H = 2
export const NPC_HITCHANCE_BOW = 3
export const NPC_HITCHANCE_CROSSBOW = 4
export const NPC_HITCHANCE_MAX = 5
export const PLAYER_STEAL_NPC_IS_AWARE = 'PLAYER_STEAL_NPC_IS_AWARE'
export const PLAYER_STEAL_NO_TALENT = 'PLAYER_STEAL_NO_TALENT'
export const PLAYER_STEAL_NOT_IN_RANGE = 'PLAYER_STEAL_NOT_IN_RANGE'
export const PLAYER_STEAL_NPC_IS_EMPTY = 'PLAYER_STEAL_NPC_IS_EMPTY'
export const PLAYER_MOB_MISSING_KEY = 'PLAYER_MOB_MISSING_KEY'
export const PLAYER_MOB_MISSING_LOCKPICK = 'PLAYER_MOB_MISSING_LOCKPICK'
export const PLAYER_MOB_MISSING_KEY_OR_LOCKPICK = 'PLAYER_MOB_MISSING_KEY_OR_LOCKPICK'
export const PLAYER_MOB_NEVER_OPEN = 'PLAYER_MOB_NEVER_OPEN'
export const PLAYER_MOB_TOO_FAR_AWAY = 'PLAYER_MOB_TOO_FAR_AWAY'
export const PLAYER_MOB_WRONG_SIDE = 'PLAYER_MOB_WRONG_SIDE'
export const PLAYER_MOB_MISSING_ITEM = 'PLAYER_MOB_MISSING_ITEM'
export const PLAYER_MOB_ANOTHER_IS_USING = 'PLAYER_MOB_ANOTHER_IS_USING'
export const PLAYER_PLUNDER_IS_EMPTY = 'PLAYER_PLUNDER_IS_EMPTY'
export const PLAYER_RANGED_NO_AMMO = 'PLAYER_RANGED_NO_AMMO'
export const NPC_NODE_RIGHTHAND = 'ZS_RIGHTHAND'
export const NPC_NODE_LEFTHAND = 'ZS_LEFTHAND'
export const NPC_NODE_SWORD = 'ZS_SWORD'
export const NPC_NODE_LONGSWORD = 'ZS_LONGSWORD'
export const NPC_NODE_BOW = 'ZS_BOW'
export const NPC_NODE_CROSSBOW = 'ZS_CROSSBOW'
export const NPC_NODE_SHIELD = 'ZS_SHIELD'
export const NPC_NODE_HELMET = 'ZS_HELMET'
export const NPC_NODE_JAWS = 'ZS_JAWS'
export const NPC_NODE_TORSO = 'ZS_TORSO'
export const NPC_NODE_LEFTARM = 'ZS_LEFTARM'
export const NPC_FLAG_NFOCUS = 262144
export const BS_STAND = 0
export const BS_WALK = 1
export const BS_SNEAK = 2
export const BS_RUN = 3
export const BS_SPRINT = 4
export const BS_SWIM = 5
export const BS_CRAWL = 6
export const BS_DIVE = 7
export const BS_JUMP = 8
export const BS_CLIMB = 9
export const BS_FALL = 10
export const BS_SIT = 11
export const BS_LIE = 12
export const BS_INVENTORY = 13
export const BS_ITEMINTERACT = 14
export const BS_MOBINTERACT = 15
export const BS_MOBINTERACT_INTERRUPT = 16
export const BS_TAKEITEM = 17
export const BS_DROPITEM = 18
export const BS_THROWITEM = 19
export const BS_PICKPOCKET = 20
export const BS_STUMBLE = 21
export const BS_UNCONSCIOUS = 22
export const BS_DEAD = 23
export const BS_AIMNEAR = 24
export const BS_AIMFAR = 25
export const BS_HIT = 26
export const BS_PARADE = 27
export const BS_CASTING = 28
export const BS_PETRIFIED = 29
export const BS_CONTROLLING = 30
export const BS_MAX = 31
export const BS_MOD_HIDDEN = 128
export const BS_MOD_DRUNK = 256
export const BS_MOD_NUTS = 512
export const BS_MOD_BURNING = 1024
export const BS_MOD_CONTROLLED = 2048
export const BS_MOD_TRANSFORMED = 4096
export const BS_MOD_CONTROLLING = 8192
export const BS_FLAG_INTERRUPTABLE = 32768
export const BS_FLAG_FREEHANDS = 65536
export const BS_ONLY_STATE = 127
export const BS_MOD_ACTIVE = 16256
export const NPC_ATT_MAX = 4
export const NPC_NAME_MAX = 5
export const NPC_MIS_MAX = 5
export const NPC_FLAG_FRIENDS = 1
export const NPC_FLAG_IMMORTAL = 2
export const NPC_FLAG_GHOST = 4
export const NPC_FLAG_PROTECTED = 1024
export const NPC_SENSE_SEE = 1
export const NPC_SENSE_HEAR = 2
export const NPC_SENSE_SMELL = 4
export const NPC_TURNVELOCITY = 150.0
export const NPC_GIL_NONE = 0
export const NPC_GIL_PALADIN = 1
export const NPC_GIL_MILIZ = 2
export const NPC_GIL_VOLK = 3
export const NPC_GIL_FEUERKREIS = 4
export const NPC_GIL_NOVIZE = 5
export const NPC_GIL_DRACHENJAEGER = 6
export const NPC_GIL_SOELDNER = 7
export const NPC_GIL_BAUERN = 8
export const NPC_GIL_BANDIT = 9
export const NPC_GIL_PRISONER = 10
export const NPC_GIL_DEMENTOR = 11
export const NPC_GIL_OUTLANDER = 12
export const NPC_GIL_PIRAT = 13
export const NPC_GIL_WASSERKREIS = 14
export const NPC_GIL_PUBLIC = 15
export const NPC_GIL_HUMANS = 16
export const NPC_GIL_MEATBUG = 17
export const NPC_GIL_SHEEP = 18
export const NPC_GIL_GOBBO = 19
export const NPC_GIL_GOBBO_SKELETON = 20
export const NPC_GIL_SUMMONED_GOBBO_SKELETON = 21
export const NPC_GIL_SCAVANGER = 22
export const NPC_GIL_GIANT_RAT = 23
export const NPC_GIL_GIANT_BUG = 24
export const NPC_GIL_BLOODFLY = 25
export const NPC_GIL_WARAN = 26
export const NPC_GIL_WOLF = 27
export const NPC_GIL_SUMMONED_WOLF = 28
export const NPC_GIL_MINECRAWLER = 29
export const NPC_GIL_LURKER = 30
export const NPC_GIL_SKELETON = 31
export const NPC_GIL_SUMMONED_SKELETON = 32
export const NPC_GIL_SKELETON_MAGE = 33
export const NPC_GIL_ZOMBIE = 34
export const NPC_GIL_SNAPPER = 35
export const NPC_GIL_SHADOWBEAST = 36
export const NPC_GIL_SHADOWBEAST_SKELETON = 37
export const NPC_GIL_HARPY = 38
export const NPC_GIL_STONEGOLEM = 39
export const NPC_GIL_FIREGOLEM = 40
export const NPC_GIL_ICEGOLEM = 41
export const NPC_GIL_SUMMONED_GOLEM = 42
export const NPC_GIL_DEMON = 43
export const NPC_GIL_SUMMONED_DEMON = 44
export const NPC_GIL_TROLL = 45
export const NPC_GIL_SWAMPSHARK = 46
export const NPC_GIL_DRAGON = 47
export const NPC_GIL_MOLERAT = 48
export const NPC_GIL_ALLIGATOR = 49
export const NPC_GIL_SWAMPGOLEM = 50
export const NPC_GIL_STONEGUARDIAN = 51
export const NPC_GIL_GARGOYLE = 52
export const NPC_GIL_EMPTY_A = 53
export const NPC_GIL_SUMMONED_GUARDIAN = 54
export const NPC_GIL_SUMMONED_ZOMBIE = 55
export const NPC_GIL_EMPTY_B = 56
export const NPC_GIL_EMPTY_C = 57
export const NPC_GIL_ORCS = 58
export const NPC_GIL_ORC = 59
export const NPC_GIL_FRIENDLYORC = 60
export const NPC_GIL_UNDEADORC = 61
export const NPC_GIL_DRACONIAN = 62
export const NPC_GIL_EMPTYORC1 = 63
export const NPC_GIL_EMPTYORC2 = 64
export const NPC_GIL_EMPTYORC3 = 65
export const NPC_GIL_MAX = 66
export const FA_MAX_SITUATIONS = 19
export const FA_MAX_ENTRY = 6

export enum zTClassFlags {
  zCLASS_FLAG_SHARED_OBJECTS = 1,
  zCLASS_FLAG_TRANSIENT = 2,
  zCLASS_FLAG_RESOURCE = 4,
}

export enum zTVobType {
  zVOB_TYPE_NORMAL,
  zVOB_TYPE_LIGHT,
  zVOB_TYPE_SOUND,
  zVOB_TYPE_LEVEL_COMPONENT,
  zVOB_TYPE_SPOT,
  zVOB_TYPE_CAMERA,
  zVOB_TYPE_STARTPOINT,
  zVOB_TYPE_WAYPOINT,
  zVOB_TYPE_MARKER,
  zVOB_TYPE_SEPARATOR = 127,
  zVOB_TYPE_MOB,
  zVOB_TYPE_ITEM,
  zVOB_TYPE_NSC,
}

export enum zTVobSleepingMode {
  zVOB_SLEEPING,
  zVOB_AWAKE,
  zVOB_AWAKE_DOAI_ONLY,
}

export enum zTAnimationMode {
  zVISUAL_ANIMODE_NONE,
  zVISUAL_ANIMODE_WIND,
  zVISUAL_ANIMODE_WIND2,
}

export enum zTVobLightType {
  zVOBLIGHT_TYPE_POINT,
  zVOBLIGHT_TYPE_SPOT,
  zVOBLIGHT_TYPE_DIR,
  zVOBLIGHT_TYPE_AMBIENT,
}

export enum zTVobLightQuality {
  zVOBLIGHT_QUAL_HI,
  zVOBLIGHT_QUAL_MID,
  zVOBLIGHT_QUAL_FASTEST,
}

export enum zTTimeBehavior {
  TBZero,
  TBFix,
  TBUnknown,
  TBAssembled,
}

export enum zTEventCoreSubType {
  zEVENT_TRIGGER,
  zEVENT_UNTRIGGER,
  zEVENT_TOUCH,
  zEVENT_UNTOUCH,
  zEVENT_TOUCHLEVEL,
  zEVENT_DAMAGE,
  zEVENT_CORE_NUM_SUBTYPES,
}

export enum zTVobCharClass {
  zVOB_CHAR_CLASS_NONE,
  zVOB_CHAR_CLASS_PC,
  zVOB_CHAR_CLASS_NPC,
}

export enum zTMovementMode {
  zVOB_MOVE_MODE_NOTINBLOCK,
  zVOB_MOVE_MODE_INBLOCK,
  zVOB_MOVE_MODE_INBLOCK_NOCD,
}

export enum zTDynShadowType {
  zDYN_SHADOW_TYPE_NONE,
  zDYN_SHADOW_TYPE_BLOB,
  zDYN_SHADOW_TYPE_COUNT,
}

export enum zTSurfaceAlignMode {
  zMV_SURFACE_ALIGN_NONE,
  zMV_SURFACE_ALIGN_NORMAL,
  zMV_SURFACE_ALIGN_HIGH,
}

export enum zTMovementState {
  zMV_STATE_STAND,
  zMV_STATE_FLY,
  zMV_STATE_SWIM,
  zMV_STATE_DIVE,
}

export enum zESkyLayerMode {
  zSKY_MODE_POLY,
  zSKY_MODE_BOX,
}

export enum zTSkyStateEffect {
  zSKY_STATE_EFFECT_SUN,
  zSKY_STATE_EFFECT_CLOUDSHADOW,
}

export enum zTCS_SCReaction {
  SCR_RESULT_NOTHING,
  SCR_RESULT_END,
  SCR_RESULT_INTERRUPT,
}

export enum zTCSRunBehaviour {
  RUN_ALWAYS,
  RUN_TIMES,
  RUN_PERHOUR,
  RUN_PERDAY,
}

export enum zTEventCommonSubType {
  zEVENT_ENABLE,
  zEVENT_DISABLE,
  zEVENT_TOGGLE_ENABLED,
  zEVENT_RESET,
  zEVENT_MISC_NUM_SUBTYPES,
}

export enum zTEventMoverSubType {
  zEVENT_GOTO_KEY_FIXED_DIRECTLY,
  zEVENT_GOTO_KEY_FIXED_ORDER,
  zEVENT_GOTO_KEY_NEXT,
  zEVENT_GOTO_KEY_PREV,
  zEVENT_MISC_NUM_SUBTYPES,
}

export enum zTMoverState {
  MOVER_STATE_OPEN,
  MOVER_STATE_OPENING,
  MOVER_STATE_CLOSED,
  MOVER_STATE_CLOSING,
}

export enum zTMoverAniType {
  MA_KEYFRAME,
  MA_MODEL_ANI,
  MA_WAYPOINT,
}

export enum zTMoverBehavior {
  MB_2STATE_TOGGLE,
  MB_2STATE_TRIGGER_CONTROL,
  MB_2STATE_OPEN_TIMED,
  MB_NSTATE_LOOP,
  MB_NSTATE_SINGLE_KEYS,
}

export enum zTTouchBehavior {
  TB_TOGGLE,
  TB_WAIT,
}

export enum zTPosLerpType {
  PL_LINEAR,
  PL_CURVE,
}

export enum zTSpeedType {
  ST_CONST,
  ST_SLOW_START_END,
  ST_SLOW_START,
  ST_SLOW_END,
  ST_SEG_SLOW_START_END,
  ST_SEG_SLOW_START,
  ST_SEG_SLOW_END,
}

export enum zTListProcess {
  LP_ALL,
  LP_NEXT_ONE,
  LP_RAND_ONE,
}

export enum zTDamageCollType {
  CT_NONE,
  CT_BOX,
  CT_POINT,
}

export enum zTEventScreenFXSubType {
  zEVENT_BLEND_FADEIN,
  zEVENT_BLEND_FADEOUT,
  zEVENT_CINEMA_FADEIN,
  zEVENT_CINEMA_FADEOUT,
  zEVENT_FOV_MORPH,
  zEVENT_SCREENFX_COUNT,
}

export enum zTMessageType {
  MT_NONE,
  MT_TRIGGER,
  MT_UNTRIGGER,
  MT_ENABLE,
  MT_DISABLE,
  MT_TOGGLE_ENABLED,
  MT_RESET,
}

export enum zTSoundVolType {
  SV_SPHERE = 0,
  SV_ELLIPSOID,
}

export enum zTSoundMode {
  SM_LOOPING,
  SM_ONCE,
  SM_RANDOM,
}

export enum zTTraceRayFlags {
  zTRACERAY_VOB_IGNORE_NO_CD_DYN = 1,
  zTRACERAY_VOB_IGNORE = 2,
  zTRACERAY_VOB_BBOX = 4,
  zTRACERAY_VOB_OBB = 8,
  zTRACERAY_STAT_IGNORE = 16,
  zTRACERAY_STAT_POLY = 32,
  zTRACERAY_STAT_PORTALS = 64,
  zTRACERAY_POLY_NORMAL = 128,
  zTRACERAY_POLY_IGNORE_TRANSP = 256,
  zTRACERAY_POLY_TEST_WATER = 512,
  zTRACERAY_POLY_2SIDED = 1024,
  zTRACERAY_VOB_IGNORE_CHARACTER = 2048,
  zTRACERAY_FIRSTHIT = 4096,
  zTRACERAY_VOB_TEST_HELPER_VISUALS = 8192,
  zTRACERAY_VOB_IGNORE_PROJECTILES = 16384,
}

export enum zTWld_RenderMode {
  zWLD_RENDER_MODE_VERT_LIGHT,
  zWLD_RENDER_MODE_LIGHTMAPS,
}

export enum zTWorldLoadMode {
  zWLD_LOAD_GAME_STARTUP,
  zWLD_LOAD_GAME_SAVED_DYN,
  zWLD_LOAD_GAME_SAVED_STAT,
  zWLD_LOAD_EDITOR_COMPILED,
  zWLD_LOAD_EDITOR_UNCOMPILED,
  zWLD_LOAD_MERGE,
}

export enum zTWorldSaveMode {
  zWLD_SAVE_GAME_SAVED_DYN,
  zWLD_SAVE_EDITOR_COMPILED,
  zWLD_SAVE_EDITOR_UNCOMPILED,
  zWLD_SAVE_COMPILED_ONLY,
}

export enum zTWorldLoadMergeMode {
  zWLD_LOAD_MERGE_ADD,
  zWLD_LOAD_MERGE_REPLACE_ROOT_VISUAL,
}

export enum zTStaticWorldLightMode {
  zWLD_LIGHT_VERTLIGHT_ONLY,
  zWLD_LIGHT_VERTLIGHT_LIGHTMAPS_LOW_QUAL,
  zWLD_LIGHT_VERTLIGHT_LIGHTMAPS_MID_QUAL,
  zWLD_LIGHT_VERTLIGHT_LIGHTMAPS_HI_QUAL,
}

export enum zTMdl_AniDir {
  zMDL_ANIDIR_FORWARD,
  zMDL_ANIDIR_REVERSE,
  zMDL_ANIDIR_ENDFASTEST,
}

export enum zTMdl_AniEventType {
  zMDL_EVENT_TAG,
  zMDL_EVENT_SOUND,
  zMDL_EVENT_SOUND_GRND,
  zMDL_EVENT_ANIBATCH,
  zMDL_EVENT_SWAPMESH,
  zMDL_EVENT_HEADING,
  zMDL_EVENT_PFX,
  zMDL_EVENT_PFX_GRND,
  zMDL_EVENT_PFX_STOP,
  zMDL_EVENT_SETMESH,
  zMDL_EVENT_MM_STARTANI,
  zMDL_EVENT_CAM_TREMOR,
}

export enum zTMdl_AniType {
  zMDL_ANI_TYPE_NORMAL,
  zMDL_ANI_TYPE_BLEND,
  zMDL_ANI_TYPE_SYNC,
  zMDL_ANI_TYPE_ALIAS,
  zMDL_ANI_TYPE_BATCH,
  zMDL_ANI_TYPE_COMB,
  zMDL_ANI_TYPE_DISABLED,
}

export enum zTModelProtoImportMAXFlags {
  zMDL_MAX_IMPORT_ANI = 1,
  zMDL_MAX_IMPORT_MESH = 2,
  zMDL_MAX_IMPORT_TREE = 4,
  zMDL_MAX_IMPORT_PASS_ZCMESH = 8,
  zMDL_MAX_IMPORT_NO_LOD = 16,
}

export enum zTFileSourceType {
  zFROM_MDS,
  zFROM_ASC,
}

export enum oEIndexDamage {
  oEDamageIndex_Barrier,
  oEDamageIndex_Blunt,
  oEDamageIndex_Edge,
  oEDamageIndex_Fire,
  oEDamageIndex_Fly,
  oEDamageIndex_Magic,
  oEDamageIndex_Point,
  oEDamageIndex_Fall,
  oEDamageIndex_MAX,
}

export enum oETypeDamage {
  oEDamageType_Unknown = 0,
  oEDamageType_Barrier = 1,
  oEDamageType_Blunt = 2,
  oEDamageType_Edge = 4,
  oEDamageType_Fire = 8,
  oEDamageType_Fly = 16,
  oEDamageType_Magic = 32,
  oEDamageType_Point = 64,
  oEDamageType_Fall = 128,
  oEDamageType_ForceDWORD = 0xffffffff,
}

export enum oETypeWeapon {
  oETypeWeapon_Unknown = 0,
  oETypeWeapon_Fist = 1,
  oETypeWeapon_Melee = 2,
  oETypeWeapon_Range = 4,
  oETypeWeapon_Magic = 8,
  oETypeWeapon_Special = 16,
  oETypeWeapon_ForceDWORD = 0xffffffff,
}

export enum oEGameDialogView {
  GAME_VIEW_SCREEN,
  GAME_VIEW_CONVERSATION,
  GAME_VIEW_AMBIENT,
  GAME_VIEW_CINEMA,
  GAME_VIEW_CHOICE,
  GAME_VIEW_NOISE,
  GAME_VIEW_MAX,
}

export enum zEInformationManagerMode {
  INFO_MGR_MODE_IMPORTANT,
  INFO_MGR_MODE_INFO,
  INFO_MGR_MODE_CHOICE,
  INFO_MGR_MODE_TRADE,
}

export enum TMobInterDirection {
  MOBINTER_DIRECTION_NONE,
  MOBINTER_DIRECTION_UP,
  MOBINTER_DIRECTION_DOWN,
}

export enum TMobMsgSubType {
  EV_STARTINTERACTION,
  EV_STARTSTATECHANGE,
  EV_ENDINTERACTION,
  EV_UNLOCK,
  EV_LOCK,
  EV_CALLSCRIPT,
}

export enum oHEROSTATUS {
  oHERO_STATUS_STD,
  oHERO_STATUS_THR,
  oHERO_STATUS_FGT,
}

export enum oENewsSpreadType {
  NEWS_DONT_SPREAD,
  NEWS_SPREAD_NPC_FRIENDLY_TOWARDS_VICTIM,
  NEWS_SPREAD_NPC_FRIENDLY_TOWARDS_WITNESS,
  NEWS_SPREAD_NPC_FRIENDLY_TOWARDS_OFFENDER,
  NEWS_SPREAD_NPC_SAME_GUILD_VICTIM,
}

export enum oTEnumNpcTalent {
  NPC_TAL_UNKNOWN,
  NPC_TAL_1H,
  NPC_TAL_2H,
  NPC_TAL_BOW,
  NPC_TAL_CROSSBOW,
  NPC_TAL_PICKLOCK,
  NPC_TAL_PICKPOCKET,
  NPC_TAL_MAGE,
  NPC_TAL_SNEAK,
  NPC_TAL_REGENERATE,
  NPC_TAL_FIREMASTER,
  NPC_TAL_ACROBAT,
  NPC_TALENT_PICKPOCKET,
  NPC_TAL_SMITH,
  NPC_TAL_RUNES,
  NPC_TAL_ALCHEMY,
  NPC_TAL_TAKEANIMALTROPHY,
  NPC_TAL_A,
  NPC_TAL_B,
  NPC_TAL_C,
  NPC_TAL_D,
  NPC_TAL_E,
  NPC_TAL_MAX,
}

export enum oEBloodMode {
  oEBloodMode_None,
  oEBloodMode_Particles,
  oEBloodMode_Decals,
  oEBloodMode_Trails,
  oEBloodMode_Amplification,
}

export enum oEFightAction {
  NPC_FIGHTMOVE_NOTINITIALISED,
  NPC_FIGHTMOVE_RUN,
  NPC_FIGHTMOVE_RUNBACK,
  NPC_FIGHTMOVE_JUMPBACK,
  NPC_FIGHTMOVE_TURN,
  NPC_FIGHTMOVE_STRAFE,
  NPC_FIGHTMOVE_ATTACK,
  NPC_FIGHTMOVE_SIDEATTACK,
  NPC_FIGHTMOVE_FRONTATTACK,
  NPC_FIGHTMOVE_TRIPLEATTACK,
  NPC_FIGHTMOVE_WHIRLATTACK,
  NPC_FIGHTMOVE_MASTERATTACK,
  NPC_FIGHTMOVE_PREHIT,
  NPC_FIGHTMOVE_COMBOZONE,
  NPC_FIGHTMOVE_POSTHIT,
  NPC_FIGHTMOVE_TURNTOHIT,
  NPC_FIGHTMOVE_STORMPREHIT,
  NPC_FIGHTMOVE_PARADE,
  NPC_FIGHTMOVE_STANDUP,
  NPC_FIGHTMOVE_WAIT,
  NPC_FIGHTMOVE_ONGROUND,
  NPC_FIGHTMOVE_STUMBLE,
  NPC_FIGHTMOVE_SKIP,
  NPC_FIGHTMOVE_WAIT_LONG,
  NPC_FIGHTMOVE_WAIT_EXT,
  NPC_FIGHTMOVE_MAX,
}

export enum oEFlagsDamageDescriptor {
  oEDamageDescFlag_Attacker = 1,
  oEDamageDescFlag_Npc = 2,
  oEDamageDescFlag_Inflictor = 4,
  oEDamageDescFlag_Weapon = 8,
  oEDamageDescFlag_VisualFX = 16,
  oEDamageDescFlag_SpellID = 32,
  oEDamageDescFlag_SpellLevel = 64,
  oEDamageDescFlag_DamageType = 128,
  oEDamageDescFlag_WeaponType = 256,
  oEDamageDescFlag_Damage = 512,
  oEDamageDescFlag_HitLocation = 1024,
  oEDamageDescFlag_FlyDirection = 2048,
  oEDamageDescFlag_OverlayActivate = 4096,
  oEDamageDescFlag_OverlayInterval = 8192,
  oEDamageDescFlag_OverlayDuration = 16384,
  oEDamageDescFlag_OverlayDamage = 32768,
  oEDamageDescFlag_ForceDWORD = 0xffffffff,
}

export enum TDamageSubType {
  EV_DAMAGE_ONCE,
  EV_DAMAGE_PER_FRAME,
  EV_DAMAGE_MAX,
}

export enum TWeaponSubType {
  EV_DRAWWEAPON,
  EV_DRAWWEAPON1,
  EV_DRAWWEAPON2,
  EV_REMOVEWEAPON,
  EV_REMOVEWEAPON1,
  EV_REMOVEWEAPON2,
  EV_CHOOSEWEAPON,
  EV_FORCEREMOVEWEAPON,
  EV_ATTACK,
  EV_EQUIPBESTWEAPON,
  EV_EQUIPBESTARMOR,
  EV_UNEQUIPWEAPONS,
  EV_UNEQUIPARMOR,
  EV_EQUIPARMOR,
  EV_WEAPON_MAX,
}

export enum TMovementSubType {
  EV_ROBUSTTRACE,
  EV_GOTOPOS,
  EV_GOTOVOB,
  EV_GOROUTE,
  EV_TURN,
  EV_TURNTOPOS,
  EV_TURNTOVOB,
  EV_TURNAWAY,
  EV_JUMP,
  EV_SETWALKMODE,
  EV_WHIRLAROUND,
  EV_STANDUP,
  EV_CANSEENPC,
  EV_STRAFE,
  EV_GOTOFP,
  EV_DODGE,
  EV_BEAMTO,
  EV_ALIGNTOFP,
  EV_MOVE_MAX,
}

export enum TAttackSubType {
  EV_ATTACKFORWARD,
  EV_ATTACKLEFT,
  EV_ATTACKRIGHT,
  EV_ATTACKRUN,
  EV_ATTACKFINISH,
  EV_PARADE,
  EV_AIMAT,
  EV_SHOOTAT,
  EV_STOPAIM,
  EV_DEFEND,
  EV_ATTACKBOW,
  EV_ATTACKMAGIC,
  EV_ATTACK_MAX,
}

export enum TUseItemSubType {
  EV_DRINK,
  EV_EQUIPITEM,
  EV_UNEQUIPITEM,
  EV_USEITEM_MAX,
}

export enum TStateSubType {
  EV_STARTSTATE,
  EV_WAIT,
  EV_SETNPCSTOSTATE,
  EV_SETTIME,
  EV_APPLYTIMEDOVERLAY,
  EV_STATE_MAX,
}

export enum TManipulateSubType {
  EV_TAKEVOB,
  EV_DROPVOB,
  EV_THROWVOB,
  EV_EXCHANGE,
  EV_USEMOB,
  EV_USEITEM,
  EV_INSERTINTERACTITEM,
  EV_REMOVEINTERACTITEM,
  EV_CREATEINTERACTITEM,
  EV_DESTROYINTERACTITEM,
  EV_PLACEINTERACTITEM,
  EV_EXCHANGEINTERACTITEM,
  EV_USEMOBWITHITEM,
  EV_CALLSCRIPT,
  EV_EQUIPITEM,
  EV_USEITEMTOSTATE,
  EV_TAKEMOB,
  EV_DROPMOB,
  EV_MANIP_MAX,
}

export enum TConversationSubType {
  EV_PLAYANISOUND,
  EV_PLAYANI,
  EV_PLAYSOUND,
  EV_LOOKAT,
  EV_OUTPUT,
  EV_OUTPUTSVM,
  EV_CUTSCENE,
  EV_WAITTILLEND,
  EV_ASK,
  EV_WAITFORQUESTION,
  EV_STOPLOOKAT,
  EV_STOPPOINTAT,
  EV_POINTAT,
  EV_QUICKLOOK,
  EV_PLAYANI_NOOVERLAY,
  EV_PLAYANI_FACE,
  EV_PROCESSINFOS,
  EV_STOPPROCESSINFOS,
  EV_OUTPUTSVM_OVERLAY,
  EV_SNDPLAY,
  EV_SNDPLAY3D,
  EV_PRINTSCREEN,
  EV_STARTFX,
  EV_STOPFX,
  EV_CONV_MAX,
  EV_OPEN,
  EV_CLOSE,
  EV_MOVE,
  EV_INVEST,
  EV_CAST,
  EV_SETLEVEL,
  EV_SHOWSYMBOL,
  EV_SETFRONTSPELL,
  EV_CASTSPELL,
  EV_READY,
  EV_UNREADY,
  EV_MAGIC_MAX,
}

export enum TConversationSubTypeMagic {
  EV_OPEN,
  EV_CLOSE,
  EV_MOVE,
  EV_INVEST,
  EV_CAST,
  EV_SETLEVEL,
  EV_SHOWSYMBOL,
  EV_SETFRONTSPELL,
  EV_CASTSPELL,
  EV_READY,
  EV_UNREADY,
  EV_MAGIC_MAX,
}

export enum TPlayerInstance {
  PC_UNKNOWN,
  PC_HERO,
  PC_FIGHTER,
  PC_THIEF,
  PC_MAGE,
  PC_PSIONIC,
}

export enum zTVFXState {
  zVFXSTATE_UNDEF,
  zVFXSTATE_OPEN,
  zVFXSTATE_INIT,
  zVFXSTATE_INVESTNEXT,
  zVFXSTATE_CAST,
  zVFXSTATE_STOP,
  zVFXSTATE_COLLIDE,
}

export enum TEmTrajectoryMode {
  EM_TRJ_UNDEF,
  EM_TRJ_FIXED,
  EM_TRJ_TARGET,
  EM_TRJ_LINE = 4,
  EM_TRJ_SPLINE = 8,
  EM_TRJ_RANDOM = 16,
  EM_TRJ_CIRCLE = 32,
  EM_TRJ_FOLLOW = 64,
  EM_TRJ_MISSILE = 128,
}

export enum TTrjLoopMode {
  TRJ_LOOP_NONE,
  TRJ_LOOP_RESTART,
  TRJ_LOOP_PINGPONG,
  TRJ_LOOP_HALT,
  TRJ_LOOP_PINGPONG_ONCE,
}

export enum TEaseFunc {
  TEASEFUNC_LINEAR,
  TEASEFUNC_SINE,
  TEASEFUNC_EXP,
}

export enum TActionColl {
  TACTION_COLL_NONE,
  TACTION_COLL_COLLIDE,
  TACTION_COLL_BOUNCE,
  TACTION_COLL_CREATE = 4,
  TACTION_COLL_CREATE_ONCE = 8,
  TACTION_COLL_NORESP = 16,
  TACTION_COLL_CREATE_QUAD = 32,
}

export enum oTSndMaterial {
  SND_MAT_WOOD,
  SND_MAT_STONE,
  SND_MAT_METAL,
  SND_MAT_LEATHER,
  SND_MAT_CLAY,
  SND_MAT_GLAS,
}
