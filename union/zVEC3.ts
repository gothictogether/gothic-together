export class zVEC3 {
  X: number = 0
  Y: number = 0
  Z: number = 0

  constructor(initialValue: string | [number, number, number]) {
    if (typeof initialValue == 'string') {
      const splittedVEC = initialValue.split(';')

      this.X = parseFloat(splittedVEC[0] as string)
      this.Y = parseFloat(splittedVEC[1] as string)
      this.Z = parseFloat(splittedVEC[2] as string)
    } else {
      this.X = initialValue[0]
      this.Y = initialValue[1]
      this.Z = initialValue[2]
    }
  }

  toString = (): string => {
    return `${this.X};${this.Y};${this.Z}`
  }

  rotateX = (rotation: number): zVEC3 => {
    if (rotation < 0 || rotation >= 360) {
      throw new Error('Rotation must be between 0 and 359 degrees.')
    }

    const radians = (rotation * Math.PI) / 180
    const newY = this.Y * Math.cos(radians) - this.Z * Math.sin(radians)
    const newZ = this.Y * Math.sin(radians) + this.Z * Math.cos(radians)

    return new zVEC3([this.X, newY, newZ])
  }

  rotateY = (rotation: number): zVEC3 => {
    if (rotation < 0 || rotation >= 360) {
      throw new Error('Rotation must be between 0 and 359 degrees.')
    }

    const radians = (rotation * Math.PI) / 180
    const newX = this.X * Math.cos(radians) + this.Z * Math.sin(radians)
    const newZ = this.Z * Math.cos(radians) - this.X * Math.sin(radians)

    return new zVEC3([newX, this.Y, newZ])
  }

  rotateZ = (rotation: number): zVEC3 => {
    if (rotation < 0 || rotation >= 360) {
      throw new Error('Rotation must be between 0 and 359 degrees.')
    }

    const radians = (rotation * Math.PI) / 180
    const newX = this.X * Math.cos(radians) - this.Y * Math.sin(radians)
    const newY = this.X * Math.sin(radians) + this.Y * Math.cos(radians)

    return new zVEC3([newX, newY, this.Z])
  }
}
