# Gothic Together NPM Package
This repository contains all core files for Gothic Together project. It should be installed into your project.

# Npm
https://www.npmjs.com/package/gothic-together

# Project Examples
https://gitlab.com/gothictogether/survival-gamemode