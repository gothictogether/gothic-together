import { GameModeBase } from './game-mode-base.js'
import { SetGameMode, StartGameMode } from './client.js'
import { SetMiddlewares } from './callbacks.js'
import { zVEC3 } from './union/zVEC3.js'
import * as ServerCommands from './server-commands.js'
import * as ClientCommands from './client-commands.js'
import { Player } from './player.js'

export let PlayerClass = Player
export const SetPlayerClass = <T extends typeof Player>(playerClass: T) => {
  PlayerClass = playerClass
}

export {
  zVEC3,
  GameModeBase,
  SetGameMode,
  StartGameMode,
  SetMiddlewares,
  ServerCommands as Server,
  ClientCommands as Client,
}
