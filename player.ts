import { oCNpc } from './union/classes/index.js'

export class Player {
  Id: number
  Name: string
  Npc: oCNpc

  constructor(id: number, name: string) {
    this.Id = id
    this.Name = name
    this.Npc = new oCNpc(`PLAYER_NPC_${id}`)
  }

  toString(): string {
    return `PLAYER_${this.Id}`
  }

  get Attrs() {
    return {} as any
  }

  SetAttrs(attr: any) {}
}
