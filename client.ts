import fs from 'fs'
import findProcess from 'find-process'
import { GameModeBase } from './game-mode-base.js'
import { FireCallback } from './callbacks.js'
import { Player } from './player.js'

// @ts-ignore
import Bindings from 'bindings'
const GameStream = Bindings('addon.node')

export enum CommandType {
  UNION = 1,
  SERVER = 2,
  CLIENT = 3,
}

export let CurrentGameMode: GameModeBase<Player> | null = null
let ProcessingCommand: boolean = false

export class GameCoreError extends Error {
  json = {}

  constructor(msg: string, jsonMessage: any) {
    super(msg)
    Object.setPrototypeOf(this, GameCoreError.prototype)
    this.json = jsonMessage
  }
}

const CheckAndWritePidFile = async () => {
  if (fs.existsSync('./tmp/gamemode.pid')) {
    const currentPid = fs.readFileSync('./tmp/gamemode.pid').toString()
    const currentProcesses = await findProcess('pid', currentPid, false)
    if (currentProcesses.length > 0 && currentProcesses[0]!.name == 'node.exe') {
      console.error('There is already a second instance of the gamemode running!')
      console.error(currentProcesses[0])
      process.exit(1)
    }
  }

  fs.writeFileSync('./tmp/gamemode.pid', process.pid.toString())
}

const ProcessCallback = async (message: any) => {
  if (message['type'] == 'CALLBACK') {
    await FireCallback(message)
    CompleteCallback()
  } else {
    console.log('GameMode: INVALID MESSAGE, EXPECTED CALLBACK:')
    console.log(message)
    process.exit(1)
  }
}

const CompleteCallback = () => {
  GameStream.WriteMessage(
    JSON.stringify({
      id: 'CALLBACK_COMPLETED',
    }),
  )
}

export const StartGameMode = async () => {
  await CheckAndWritePidFile()

  process.title = 'GothicTogetherGameMode'
  console.log('GM starting...')

  GameStream.InitFileMapping()

  GameStream.WriteMessage(
    JSON.stringify({
      id: 'INIT',
    }),
  )

  while (true) {
    const message = GameStream.ReadMessage()
    const json = JSON.parse(message)
    await ProcessCallback(json)
    await new Promise((resolve) => setTimeout(resolve, 0)) // allow process event loop
  }
}

export const SendCommand = (json: any, type = CommandType.UNION) => {
  if (ProcessingCommand) {
    console.log('ERROR: Another command was sent before the previous one finished.')
    process.exit(1)
  }
  ProcessingCommand = true

  json['type'] = type
  const jsonText = JSON.stringify(json)
  GameStream.WriteMessage(jsonText)

  const commandResult = GameStream.ReadMessage()
  ProcessingCommand = false

  if (commandResult == '_GT_COMMAND_CRASH_') {
    console.log(json)
    throw new GameCoreError(`Error durring executing ${json['id']} command.`, json)
  }

  return commandResult == 'NULL' ? null : commandResult
}

export const SetGameMode = (gameMode: GameModeBase<Player>) => {
  CurrentGameMode = gameMode
}
