import { ModuleBase } from './module-base.js'
import { Player } from './player.js'
import { CGameManager, oCRtnManager } from './union/classes/index.js'
import { oCGame } from './union/classes/oCGame.js'
import { oCNpc } from './union/classes/oCNpc.js'
import { oCObjectFactory } from './union/classes/oCObjectFactory.js'

export class GameModeBase<PlayerType extends Player> extends ModuleBase<PlayerType> {
  HostNpc: oCNpc
  Game: oCGame
  Factory: oCObjectFactory
  RoutineManager: oCRtnManager
  GameManager: CGameManager
  Players: PlayerType[]
  IdToPlayerMap: Record<number, PlayerType>

  constructor() {
    super()

    this.HostNpc = new oCNpc('PLAYER_NPC_0')
    this.Game = new oCGame('GLOBALS_OGAME')
    this.Factory = new oCObjectFactory('GLOBALS_ZFACTORY')
    this.RoutineManager = new oCRtnManager('GLOBALS_RTNMAN')
    this.GameManager = new CGameManager('GLOBALS_GAMEMAN')
    this.Players = []
    this.IdToPlayerMap = {}
  }

  Init() {
    return true
  }

  GetPlayerFromNpc(npc: oCNpc): PlayerType | null {
    if (!npc || !npc.Uuid.startsWith('PLAYER_NPC_')) {
      return null
    }

    const playerId = parseInt(npc.Uuid.replace('PLAYER_NPC_', ''))
    return this.IdToPlayerMap[playerId] || null
  }
}
